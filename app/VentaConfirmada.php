<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;

class VentaConfirmada extends Model
{
    protected $table = 'ventas_confirmadas';
    protected $fillable = ['ventas_id',
                           'tipo_pago',
                           'estado'];
    public static function getAll(){
    	return DB::table('ventas_confirmadas')
    			   ->select('id',
    			   			'ventas_id',
                           	'tipo_pago',
                            'estado',
    			   			'created_at',
    			   			'updated_at')
	               ->orderBy('ventas_confirmadas.updated_at','desc')
	    	       ->paginate(15);
    }
}
