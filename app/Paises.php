<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;

class Paises extends Model
{
    protected $table = 'paises';
    protected $fillable = ['nombre_largo',
    					   'nombre_corto'];
}
