<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;
use DB;

class PrecioVentaMercaderia extends Model
{
    protected $table = 'precio_venta_mer';
    protected $fillable = ['transporte',
						   'adicional',
						   'precio_total',
						   'cotizacion_mercaderias_id',
						   'proveedores_cotizacion_mer_id',
						   'proveedores_id'];
	
	public static function getPrecioVentaMer($id_cotizacionMer){
		return DB::table('precio_venta_mer')
					->select('transporte','adicional','precio_total','cotizacion_mercaderias_id',
							 'proveedores_cotizacion_mer_id','proveedores_id')
					->where('cotizacion_mercaderias_id','=',$id_cotizacionMer)
					->get();
	}

	public static function getPrecioVentaDetalle($id){
		return DB::table('precio_venta_mer')
					->join('proveedores','precio_venta_mer.proveedores_id','=','proveedores.id')
					->join('proveedores_cotizacion_mer','precio_venta_mer.proveedores_cotizacion_mer_id','=','proveedores_cotizacion_mer.id')
					->select('precio_venta_mer.transporte','precio_venta_mer.adicional','precio_venta_mer.precio_total',
							 'proveedores.nombre_compania_p','proveedores_cotizacion_mer.precio_cot_mer')
					->where('precio_venta_mer.cotizacion_mercaderias_id','=',$id)
					->get();
	}
	public static function getPreciosCotizacionMercaderias(){
		return DB::table('precio_venta_mer')
					->join('cotizacion_mercaderias','precio_venta_mer.cotizacion_mercaderias_id','=','cotizacion_mercaderias.id')
					->join('mercaderias','cotizacion_mercaderias.mercaderias_id','=','mercaderias.id')
					->select('precio_venta_mer.transporte','precio_venta_mer.adicional','precio_venta_mer.precio_total','mercaderias.nombre_m')
					->distinct()->get();
	}
}
