<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;
use DB;

class PrecioVentaRepuesto extends Model
{
    protected $table = 'precio_venta_rep';
    protected $fillable = ['transporte',
						   'adicional',
						   'precio_total',
						   'cotizacion_repuestos_id',
						   'proveedores_cotizacion_rep_id',
						   'proveedores_id'];
	
	public static function getPrecioVentaRep($id_cotizacionRep){
		return DB::table('precio_venta_rep')
					->select('transporte','adicional','precio_total','cotizacion_repuestos_id',
							 'proveedores_cotizacion_rep_id','proveedores_id')
					->where('cotizacion_repuestos_id','=',$id_cotizacionRep)
					->get();
	}

	public static function getPrecioVentaDetalle($id){
		return DB::table('precio_venta_rep')
					->join('proveedores','precio_venta_rep.proveedores_id','=','proveedores.id')
					->join('proveedores_cotizacion_rep','precio_venta_rep.proveedores_cotizacion_rep_id','=','proveedores_cotizacion_rep.id')
					->select('precio_venta_rep.transporte','precio_venta_rep.adicional','precio_venta_rep.precio_total',
							 'proveedores.nombre_compania_p','proveedores_cotizacion_rep.precio_cot_rep')
					->where('precio_venta_rep.cotizacion_repuestos_id','=',$id)
					->get();
	}

	public static function getPreciosCotizacionRepuestos(){
		return DB::table('precio_venta_rep')
					->join('cotizacion_repuestos','precio_venta_rep.cotizacion_repuestos_id','=','cotizacion_repuestos.id')
					->join('repuestos','cotizacion_repuestos.repuestos_id','=','repuestos.id')
					->select('precio_venta_rep.transporte','precio_venta_rep.adicional','precio_venta_rep.precio_total','repuestos.marca_r','repuestos.modelo_r','repuestos.detalle_r','repuestos.anio_r')
					->distinct()->get();
	}
}
