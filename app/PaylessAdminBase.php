<?php 
namespace PaylessAdmin;

use Illuminate\Foundation\Application;

class PaylessAdminBase extends Application  
{
    public function publicPath()  
    {
        return $this->basePath.DIRECTORY_SEPARATOR.'public_html';
    }
}