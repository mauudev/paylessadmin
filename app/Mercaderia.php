<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Mercaderia extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'mercaderias';
    protected $fillable = ['nombre_m',
                           'precio_venta_m',
                           'nro_item',
                           'deleted_at'];

    public static function getAll(){
    	return DB::table('mercaderias')
    			   ->select('id',
    			   			'nombre_m',
                  'precio_venta_m',
                  'nro_item',
    			   			'created_at',
    			   			'updated_at')
             ->where('precio_venta_m','<>',null)
             ->orderBy('mercaderias.updated_at','desc')
    			   ->paginate(15);
    }
    public function scopeSearch($query, $search){
        if("" != trim($search)){
        return DB::table('mercaderias')
               ->select('mercaderias.id','mercaderias.nombre_m',
                        'mercaderias.precio_venta_m','mercaderias.nro_item')
               ->orWhere(function ($query) use ($search) {
                    $query->orWhere("mercaderias.nombre_m","rlike",$search)
                          ->orWhere("mercaderias.precio_venta_m","rlike",$search)
                          ->orWhere("mercaderias.nro_item","rlike",$search);
                    })
               ->where("mercaderias.precio_venta_m","<>",null)
               ->distinct();
        }
    }
}
