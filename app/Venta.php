<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Venta extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'ventas';
    protected $fillable = ['estado','finalizado','pago_parcial','segundo_pago',
                           'pago_total','ventas.pago_total_bs','clientes_id','usuarios_id','deleted_at'];


    public static function getAll($estado, $usuario_id){
    	if($usuario_id != -123){
        return DB::table('ventas')
             ->join('clientes','ventas.clientes_id','=','clientes.id')
             ->join('usuarios','ventas.usuarios_id','=','usuarios.id')
             ->leftjoin('cotizacion_repuestos','ventas.id','=','cotizacion_repuestos.ventas_id')
             ->leftjoin('repuestos','cotizacion_repuestos.repuestos_id','=','repuestos.id')
             ->leftjoin('cotizacion_mercaderias','ventas.id','=','cotizacion_mercaderias.ventas_id')
             ->leftjoin('mercaderias','cotizacion_mercaderias.mercaderias_id','=','mercaderias.id')
             ->select('ventas.id','ventas.estado','ventas.finalizado',
                      'ventas.clientes_id','ventas.created_at',
                      'clientes.nombre_c','clientes.apellidos_c',
                      'usuarios.nombre_u','usuarios.apellidos_u',
                      'ventas.pago_parcial','ventas.segundo_pago','ventas.pago_total_bs',
                      DB::raw('group_concat(DISTINCT repuestos.marca_r) as marca_r'), 
                      DB::raw('group_concat(DISTINCT repuestos.modelo_r) as modelo_r'),
                      DB::raw('group_concat(DISTINCT repuestos.anio_r) as anio_r'),
                      DB::raw('group_concat(DISTINCT repuestos.detalle_r) as detalle_r'),
                      DB::raw('group_concat(DISTINCT mercaderias.nombre_m) as nombre_m'),
                      'ventas.deleted_at')
             ->where('ventas.estado','=',$estado)
             ->where('ventas.finalizado','=',0)
             ->where('ventas.usuarios_id','=',$usuario_id)
             ->where('ventas.deleted_at','=',null)
             ->orderBy('ventas.id','desc')
             ->groupBy('ventas.id')
             ->paginate(15);
           }else{
            return DB::table('ventas')
             ->join('clientes','ventas.clientes_id','=','clientes.id')
             ->join('usuarios','ventas.usuarios_id','=','usuarios.id')
             ->leftjoin('cotizacion_repuestos','ventas.id','=','cotizacion_repuestos.ventas_id')
             ->leftjoin('repuestos','cotizacion_repuestos.repuestos_id','=','repuestos.id')
             ->leftjoin('cotizacion_mercaderias','ventas.id','=','cotizacion_mercaderias.ventas_id')
             ->leftjoin('mercaderias','cotizacion_mercaderias.mercaderias_id','=','mercaderias.id')
             ->select('ventas.id','ventas.estado','ventas.finalizado',
                      'ventas.clientes_id','ventas.created_at',
                      'clientes.nombre_c','clientes.apellidos_c',
                      'usuarios.nombre_u','usuarios.apellidos_u',
                      'ventas.pago_parcial','ventas.segundo_pago','ventas.pago_total_bs',
                      DB::raw('group_concat(DISTINCT repuestos.marca_r) as marca_r'), 
                      DB::raw('group_concat(DISTINCT repuestos.modelo_r) as modelo_r'),
                      DB::raw('group_concat(DISTINCT repuestos.anio_r) as anio_r'),
                      DB::raw('group_concat(DISTINCT repuestos.detalle_r) as detalle_r'),
                      DB::raw('group_concat(DISTINCT mercaderias.nombre_m) as nombre_m'),
                      'ventas.deleted_at')
             ->where('ventas.estado','=',$estado)
             ->where('ventas.finalizado','=',0)
             ->where('ventas.deleted_at','=',null)
             ->orderBy('ventas.id','desc')
             ->groupBy('ventas.id')
             ->paginate(15);
           }
    }
    public static function getAll_accessible($estado){
      return DB::table('ventas')
             ->join('clientes','ventas.clientes_id','=','clientes.id')
             ->join('usuarios','ventas.usuarios_id','=','usuarios.id')
             ->leftjoin('cotizacion_repuestos','ventas.id','=','cotizacion_repuestos.ventas_id')
             ->leftjoin('repuestos','cotizacion_repuestos.repuestos_id','=','repuestos.id')
             ->leftjoin('cotizacion_mercaderias','ventas.id','=','cotizacion_mercaderias.ventas_id')
             ->leftjoin('mercaderias','cotizacion_mercaderias.mercaderias_id','=','mercaderias.id')
             ->select('ventas.id','ventas.estado','ventas.finalizado',
                      'ventas.clientes_id','ventas.created_at','ventas.segundo_pago',
                      'clientes.nombre_c','clientes.apellidos_c','ventas.pago_parcial',
                      'usuarios.nombre_u','usuarios.apellidos_u','ventas.pago_total_bs',
                      DB::raw('group_concat(DISTINCT repuestos.marca_r) as marca_r'), 
                      DB::raw('group_concat(DISTINCT repuestos.modelo_r) as modelo_r'),
                      DB::raw('group_concat(DISTINCT repuestos.anio_r) as anio_r'),
                      DB::raw('group_concat(DISTINCT repuestos.detalle_r) as detalle_r'),
                      DB::raw('group_concat(DISTINCT mercaderias.nombre_m) as nombre_m'),
                      'ventas.deleted_at')
             ->where('ventas.estado','=',$estado)
             ->where('ventas.finalizado','=',0)
             ->where('ventas.deleted_at','=',null)
             ->orderBy('ventas.id','desc')
             ->get();
    }
    public static function getVentas($estado){
        return DB::table('ventas')
             ->join('clientes','ventas.clientes_id','=','clientes.id')
             ->join('usuarios','ventas.usuarios_id','=','usuarios.id')
             ->leftjoin('cotizacion_repuestos','ventas.id','=','cotizacion_repuestos.ventas_id')
             ->leftjoin('repuestos','cotizacion_repuestos.repuestos_id','=','repuestos.id')
             ->leftjoin('cotizacion_mercaderias','ventas.id','=','cotizacion_mercaderias.ventas_id')
             ->leftjoin('mercaderias','cotizacion_mercaderias.mercaderias_id','=','mercaderias.id')
             ->select('ventas.id','ventas.estado','ventas.finalizado',
                      'ventas.clientes_id','ventas.created_at','ventas.segundo_pago',
                      'clientes.nombre_c','clientes.apellidos_c','ventas.pago_parcial',
                      'usuarios.nombre_u','usuarios.apellidos_u','ventas.pago_total_bs',
                      DB::raw('group_concat(DISTINCT repuestos.marca_r) as marca_r'), 
                      DB::raw('group_concat(DISTINCT repuestos.modelo_r) as modelo_r'),
                      DB::raw('group_concat(DISTINCT repuestos.anio_r) as anio_r'),
                      DB::raw('group_concat(DISTINCT repuestos.detalle_r) as detalle_r'),
                      DB::raw('group_concat(DISTINCT mercaderias.nombre_m) as nombre_m'),
                      'ventas.deleted_at')
             ->where('ventas.deleted_at','=',null)
             ->where('ventas.estado','=',$estado)
             ->where('ventas.pago_parcial','<>',null)
             ->orderBy('ventas.id','desc')
             ->groupBy('ventas.id')
             ->paginate(15);
    }
    public static function filtro($estado,$filtro){
        if(\Auth::user()->type == 'admin'){
          if($estado == 1){
            return DB::table('ventas')
                        ->join('clientes','ventas.clientes_id','=','clientes.id')
                        ->join('usuarios','ventas.usuarios_id','=','usuarios.id')
                        ->leftjoin('cotizacion_repuestos','ventas.id','=','cotizacion_repuestos.ventas_id')
                        ->leftjoin('repuestos','cotizacion_repuestos.repuestos_id','=','repuestos.id')
                        ->leftjoin('cotizacion_mercaderias','ventas.id','=','cotizacion_mercaderias.ventas_id')
                        ->leftjoin('mercaderias','cotizacion_mercaderias.mercaderias_id','=','mercaderias.id')
                        ->select('ventas.id','ventas.estado','ventas.finalizado',
                                  'ventas.clientes_id','ventas.created_at','ventas.segundo_pago',
                                  'clientes.nombre_c','clientes.apellidos_c','ventas.pago_parcial',
                                  'usuarios.nombre_u','usuarios.apellidos_u','ventas.pago_total_bs',
                                  DB::raw('group_concat(DISTINCT repuestos.marca_r) as marca_r'), 
                                  DB::raw('group_concat(DISTINCT repuestos.modelo_r) as modelo_r'),
                                  DB::raw('group_concat(DISTINCT repuestos.anio_r) as anio_r'),
                                  DB::raw('group_concat(DISTINCT repuestos.detalle_r) as detalle_r'),
                                  DB::raw('group_concat(DISTINCT mercaderias.nombre_m) as nombre_m'),
                                  'ventas.deleted_at')
                        ->where('ventas.deleted_at', '=',null)
                        ->where('ventas.estado','=',$filtro)
                        ->where('ventas.finalizado','=',0)
                        ->orderBy('ventas.id','desc')
                        ->groupBy('ventas.id')
                        ->get();
        }else{
            return DB::table('ventas')
                        ->join('clientes','ventas.clientes_id','=','clientes.id')
                        ->join('usuarios','ventas.usuarios_id','=','usuarios.id')
                        ->leftjoin('cotizacion_repuestos','ventas.id','=','cotizacion_repuestos.ventas_id')
                        ->leftjoin('repuestos','cotizacion_repuestos.repuestos_id','=','repuestos.id')
                        ->leftjoin('cotizacion_mercaderias','ventas.id','=','cotizacion_mercaderias.ventas_id')
                        ->leftjoin('mercaderias','cotizacion_mercaderias.mercaderias_id','=','mercaderias.id')
                        ->select('ventas.id','ventas.estado','ventas.finalizado',
                                  'ventas.clientes_id','ventas.created_at','ventas.segundo_pago',
                                  'clientes.nombre_c','clientes.apellidos_c','ventas.pago_parcial',
                                  'usuarios.nombre_u','usuarios.apellidos_u','ventas.pago_total_bs',
                                  DB::raw('group_concat(DISTINCT repuestos.marca_r) as marca_r'), 
                                  DB::raw('group_concat(DISTINCT repuestos.modelo_r) as modelo_r'),
                                  DB::raw('group_concat(DISTINCT repuestos.anio_r) as anio_r'),
                                  DB::raw('group_concat(DISTINCT repuestos.detalle_r) as detalle_r'),
                                  DB::raw('group_concat(DISTINCT mercaderias.nombre_m) as nombre_m'),
                                  'ventas.deleted_at')
                        ->where('ventas.deleted_at', '<>',null)
                        ->where('ventas.estado','=',$filtro)
                        ->where('ventas.finalizado','=',0)
                        ->orderBy('ventas.id','desc')
                        ->groupBy('ventas.id')
                        ->get();
        }
      }else{
        if($estado == 1){
            return DB::table('ventas')
                        ->join('clientes','ventas.clientes_id','=','clientes.id')
                        ->join('usuarios','ventas.usuarios_id','=','usuarios.id')
                        ->leftjoin('cotizacion_repuestos','ventas.id','=','cotizacion_repuestos.ventas_id')
                        ->leftjoin('repuestos','cotizacion_repuestos.repuestos_id','=','repuestos.id')
                        ->leftjoin('cotizacion_mercaderias','ventas.id','=','cotizacion_mercaderias.ventas_id')
                        ->leftjoin('mercaderias','cotizacion_mercaderias.mercaderias_id','=','mercaderias.id')
                        ->select('ventas.id','ventas.estado','ventas.finalizado',
                                  'ventas.clientes_id','ventas.created_at','ventas.segundo_pago',
                                  'clientes.nombre_c','clientes.apellidos_c','ventas.pago_parcial',
                                  'usuarios.nombre_u','usuarios.apellidos_u','ventas.pago_total_bs',
                                  DB::raw('group_concat(DISTINCT repuestos.marca_r) as marca_r'), 
                                  DB::raw('group_concat(DISTINCT repuestos.modelo_r) as modelo_r'),
                                  DB::raw('group_concat(DISTINCT repuestos.anio_r) as anio_r'),
                                  DB::raw('group_concat(DISTINCT repuestos.detalle_r) as detalle_r'),
                                  DB::raw('group_concat(DISTINCT mercaderias.nombre_m) as nombre_m'),
                                  'ventas.deleted_at')
                        ->where('ventas.deleted_at', '=',null)
                        ->where('ventas.estado','=',$filtro)
                        ->where('ventas.finalizado','=',0)
                        ->where('ventas.usuarios_id','=',\Auth::user()->id)
                        ->orderBy('ventas.id','desc')
                        ->groupBy('ventas.id')
                        ->get();
        }else{
            return DB::table('ventas')
                        ->join('clientes','ventas.clientes_id','=','clientes.id')
                        ->join('usuarios','ventas.usuarios_id','=','usuarios.id')
                        ->leftjoin('cotizacion_repuestos','ventas.id','=','cotizacion_repuestos.ventas_id')
                        ->leftjoin('repuestos','cotizacion_repuestos.repuestos_id','=','repuestos.id')
                        ->leftjoin('cotizacion_mercaderias','ventas.id','=','cotizacion_mercaderias.ventas_id')
                        ->leftjoin('mercaderias','cotizacion_mercaderias.mercaderias_id','=','mercaderias.id')
                        ->select('ventas.id','ventas.estado','ventas.finalizado',
                                  'ventas.clientes_id','ventas.created_at','ventas.segundo_pago',
                                  'clientes.nombre_c','clientes.apellidos_c','ventas.pago_parcial',
                                  'usuarios.nombre_u','usuarios.apellidos_u','ventas.pago_total_bs',
                                  DB::raw('group_concat(DISTINCT repuestos.marca_r) as marca_r'), 
                                  DB::raw('group_concat(DISTINCT repuestos.modelo_r) as modelo_r'),
                                  DB::raw('group_concat(DISTINCT repuestos.anio_r) as anio_r'),
                                  DB::raw('group_concat(DISTINCT repuestos.detalle_r) as detalle_r'),
                                  DB::raw('group_concat(DISTINCT mercaderias.nombre_m) as nombre_m'),
                                  'ventas.deleted_at')
                        ->where('ventas.deleted_at', '<>',null)
                        ->where('ventas.estado','=',$filtro)
                        ->where('ventas.finalizado','=',0)
                        ->where('ventas.usuarios_id','=',\Auth::user()->id)
                        ->orderBy('ventas.id','desc')
                        ->groupBy('ventas.id')
                        ->get();
        }
      }
    }
    public function scopeSearch($query, $search){
        //dd("scope: ".$search);
        if("" != trim($search)){
         $target = trim($search);
         $query->join('clientes','ventas.clientes_id','=','clientes.id')
               ->join('usuarios','ventas.usuarios_id','=','usuarios.id')
               ->leftjoin('cotizacion_repuestos','ventas.id','=','cotizacion_repuestos.ventas_id')
               ->leftjoin('repuestos','cotizacion_repuestos.repuestos_id','=','repuestos.id')
               ->leftjoin('cotizacion_mercaderias','ventas.id','=','cotizacion_mercaderias.ventas_id')
               ->leftjoin('mercaderias','cotizacion_mercaderias.mercaderias_id','=','mercaderias.id')
               ->select('ventas.id','ventas.estado','ventas.created_at','clientes.nombre_c',
                      'clientes.id as clientes_id','clientes.apellidos_c',
                      'clientes.email_c','ventas.finalizado','ventas.pago_parcial',
                      'ventas.segundo_pago','usuarios.nombre_u','repuestos.marca_r',
                      'repuestos.modelo_r','repuestos.anio_r','repuestos.detalle_r',
                      'mercaderias.nombre_m','mercaderias.nro_item','ventas.pago_total_bs',
                      'usuarios.apellidos_u','ventas.deleted_at')
               ->orWhere(function ($query) use ($target) {
                  $query->where("clientes.nombre_c", "rlike", $target)
                        ->orWhere("clientes.apellidos_c","rlike",$target)
                        ->orWhere('clientes.email_c',"rlike",$target)
                        ->orWhere('repuestos.marca_r',"rlike",$target)
                        ->orWhere('repuestos.modelo_r',"rlike",$target)
                        ->orWhere('repuestos.anio_r',"rlike",$target)
                        ->orWhere('repuestos.detalle_r',"rlike",$target)
                        ->orWhere('mercaderias.nombre_m',"rlike",$target)
                        ->orWhere('mercaderias.nro_item',"rlike",$target)
                        ->orWhere("ventas.pago_parcial","rlike",$target)
                        ->orWhere("ventas.segundo_pago","rlike",$target)
                        ->orWhere("ventas.created_at","rlike",$target)
                        ->orWhere("usuarios.nombre_u","rlike",$target)
                        ->orWhere("usuarios.apellidos_u","rlike",$target)
                        ->orWhere("usuarios.user_name","rlike",$target)
                        ->orWhere("usuarios.tipo_usuarios_id","rlike",$target);
                  })
               //->where('ventas.deleted_at','=',null)
               ->where('ventas.estado','=',1)
               ->groupBy('ventas.id')
               ->orderBy('ventas.id','desc');
        }
    }

    public static function searchCotizacion($search,$pointer,$usuario_id){
        if($usuario_id != -123){
          if("" != trim($search)){
          $target = trim($search);
          return DB::table('ventas') 
                 ->join('clientes','ventas.clientes_id','=','clientes.id')
                 ->join('usuarios','ventas.usuarios_id','=','usuarios.id')
                 ->leftjoin('cotizacion_repuestos','ventas.id','=','cotizacion_repuestos.ventas_id')
                 ->leftjoin('repuestos','cotizacion_repuestos.repuestos_id','=','repuestos.id')
                 ->leftjoin('cotizacion_mercaderias','ventas.id','=','cotizacion_mercaderias.ventas_id')
                 ->leftjoin('mercaderias','cotizacion_mercaderias.mercaderias_id','=','mercaderias.id')
                 ->select('ventas.id','ventas.estado','ventas.created_at','clientes.nombre_c',
                        'clientes.id as clientes_id','clientes.apellidos_c',
                        'clientes.email_c','ventas.finalizado','ventas.pago_parcial',
                        'ventas.segundo_pago','usuarios.nombre_u','repuestos.marca_r',
                        'repuestos.modelo_r','repuestos.anio_r','repuestos.detalle_r',
                        'mercaderias.nombre_m','ventas.pago_total_bs',
                        'usuarios.apellidos_u','ventas.deleted_at')
                 ->orWhere(function ($query) use ($target) {
                    $query->where("clientes.nombre_c", "rlike", $target)
                          ->orWhere("clientes.apellidos_c","rlike",$target)
                          ->orWhere('clientes.email_c',"rlike",$target)
                          ->orWhere('repuestos.marca_r',"rlike",$target)
                          ->orWhere('repuestos.modelo_r',"rlike",$target)
                          ->orWhere('repuestos.anio_r',"rlike",$target)
                          ->orWhere('repuestos.detalle_r',"rlike",$target)
                          ->orWhere('mercaderias.nombre_m',"rlike",$target)
                          ->orWhere('mercaderias.nro_item',"rlike",$target)
                          ->orWhere("ventas.pago_parcial","rlike",$target)
                          ->orWhere("ventas.segundo_pago","rlike",$target)
                          ->orWhere("ventas.created_at","rlike",$target)
                          ->orWhere("usuarios.nombre_u","rlike",$target)
                          ->orWhere("usuarios.apellidos_u","rlike",$target)
                          ->orWhere("usuarios.user_name","rlike",$target)
                          ->orWhere("usuarios.tipo_usuarios_id","rlike",$target);
                    })
                 //->where('ventas.deleted_at','=',null)
                 ->where('ventas.usuarios_id','=',$usuario_id)
                 ->where('ventas.estado','=',$pointer)
                 ->where('ventas.finalizado','=',0)
                 ->groupBy('ventas.id')
                 ->orderBy('ventas.id','desc');
          }
        }else{
          if("" != trim($search)){
          $target = trim($search);
          return DB::table('ventas') 
                 ->join('clientes','ventas.clientes_id','=','clientes.id')
                 ->join('usuarios','ventas.usuarios_id','=','usuarios.id')
                 ->leftjoin('cotizacion_repuestos','ventas.id','=','cotizacion_repuestos.ventas_id')
                 ->leftjoin('repuestos','cotizacion_repuestos.repuestos_id','=','repuestos.id')
                 ->leftjoin('cotizacion_mercaderias','ventas.id','=','cotizacion_mercaderias.ventas_id')
                 ->leftjoin('mercaderias','cotizacion_mercaderias.mercaderias_id','=','mercaderias.id')
                 ->select('ventas.id','ventas.estado','ventas.created_at','clientes.nombre_c',
                        'clientes.id as clientes_id','clientes.apellidos_c',
                        'clientes.email_c','ventas.finalizado','ventas.pago_parcial',
                        'ventas.segundo_pago','usuarios.nombre_u','repuestos.marca_r',
                        'repuestos.modelo_r','repuestos.anio_r','repuestos.detalle_r',
                        'mercaderias.nombre_m','ventas.pago_total_bs',
                        'usuarios.apellidos_u','ventas.deleted_at')
                 ->orWhere(function ($query) use ($target) {
                    $query->where("clientes.nombre_c", "rlike", $target)
                          ->orWhere("clientes.apellidos_c","rlike",$target)
                          ->orWhere('clientes.email_c',"rlike",$target)
                          ->orWhere('repuestos.marca_r',"rlike",$target)
                          ->orWhere('repuestos.modelo_r',"rlike",$target)
                          ->orWhere('repuestos.anio_r',"rlike",$target)
                          ->orWhere('repuestos.detalle_r',"rlike",$target)
                          ->orWhere('mercaderias.nombre_m',"rlike",$target)
                          ->orWhere('mercaderias.nro_item',"rlike",$target)
                          ->orWhere("ventas.pago_parcial","rlike",$target)
                          ->orWhere("ventas.segundo_pago","rlike",$target)
                          ->orWhere("ventas.created_at","rlike",$target)
                          ->orWhere("usuarios.nombre_u","rlike",$target)
                          ->orWhere("usuarios.apellidos_u","rlike",$target)
                          ->orWhere("usuarios.user_name","rlike",$target)
                          ->orWhere("usuarios.tipo_usuarios_id","rlike",$target);
                    })
                 //->where('ventas.deleted_at','=',null)
                 ->where('ventas.estado','=',$pointer)
                 ->where('ventas.finalizado','=',0)
                 ->groupBy('ventas.id')
                 ->orderBy('ventas.id','desc');
        }
        } 
    }
}