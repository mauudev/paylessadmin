<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Usuario extends Authenticatable
{
	protected $table = 'usuarios';
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [
        'nombre_u',
        'apellidos_u',
        'telefono_u',
        'celular_u',
        'email_u',
        'type',
        'user_name', 
        'password',
        'deleted_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public static function getAll(){
    	return DB::table('usuarios')
                   ->select('usuarios.id','usuarios.nombre_u','usuarios.apellidos_u','usuarios.telefono_u','usuarios.celular_u','usuarios.email_u','usuarios.user_name','usuarios.tipo_usuarios_id','tipo_usuarios.tipo','usuarios.deleted_at')
                   ->join('tipo_usuarios','usuarios.tipo_usuarios_id','=','tipo_usuarios.id')
                   ->where('usuarios.deleted_at', '=',null)
                   ->orderBy('usuarios.updated_at','desc')
                   ->paginate(15);
    }
    public static function getRows_count(){
        return DB::table("usuarios")->count();         
    }
    public static function getUsuario($id){
        return  DB::table('usuarios')
                ->select('usuarios.id as usuarios_id','usuarios.nombre_u','usuarios.apellidos_u','usuarios.telefono_u',
                         'usuarios.celular_u','usuarios.email_u','usuarios.user_name',
                         'tipo_usuarios.tipo','usuarios.deleted_at','usuarios.type')
                ->join('tipo_usuarios','usuarios.tipo_usuarios_id','=','tipo_usuarios.id')
                ->where('usuarios.id','=',$id)
                ->first();     
    }
    public static function filtro($filtro){
        if($filtro == 1){
            return DB::table('usuarios')
                        ->join('tipo_usuarios','usuarios.tipo_usuarios_id','=','tipo_usuarios.id')
                        ->select('usuarios.id','usuarios.nombre_u','usuarios.apellidos_u',
                                 'usuarios.telefono_u',
                                 'usuarios.celular_u','usuarios.email_u','usuarios.user_name',
                                 'usuarios.tipo_usuarios_id','tipo_usuarios.tipo','usuarios.deleted_at')
                        ->where('usuarios.deleted_at', '=',null)->get();
        }else{
            return DB::table('usuarios')
                        ->join('tipo_usuarios','usuarios.tipo_usuarios_id','=','tipo_usuarios.id')
                        ->select('usuarios.id','usuarios.nombre_u','usuarios.apellidos_u',
                                 'usuarios.telefono_u','usuarios.celular_u','usuarios.email_u',
                                 'usuarios.user_name','usuarios.tipo_usuarios_id','tipo_usuarios.tipo'
                                 ,'usuarios.deleted_at')
                        ->where('usuarios.deleted_at','<>',null)->get();
        }
    }
    public static function scopeSearch($query,$search){
        if("" != trim($search)){
          $target = trim($search);
          return DB::table('usuarios') 
                 ->select('usuarios.id','usuarios.nombre_u','usuarios.apellidos_u',
                          'usuarios.telefono_u','usuarios.celular_u','usuarios.email_u',
                          'usuarios.user_name','usuarios.tipo_usuarios_id','usuarios.deleted_at',
                          'tipo_usuarios.tipo')
                 ->join('tipo_usuarios','usuarios.tipo_usuarios_id','=','tipo_usuarios.id')
                 ->orWhere(function ($query) use ($search) {
                    $query->where("usuarios.nombre_u","rlike",$search)
                          ->orWhere("usuarios.apellidos_u","rlike",$search)
                          ->orWhere("usuarios.telefono_u","rlike",$search)
                          ->orWhere("usuarios.celular_u","rlike",$search)
                          ->orWhere("usuarios.email_u","rlike",$search)
                          ->orWhere("usuarios.user_name","rlike",$search)
                          ->orWhere("usuarios.tipo_usuarios_id","rlike",$search);
                    })
                 ->orderBy('usuarios.updated_at','desc')->distinct();
        } 
    }
    public static function searchUsuarios($search){
        if("" != trim($search)) 
            return  DB::table('usuarios')
                    ->select('usuarios.id as usuarios_id','usuarios.nombre_u','usuarios.apellidos_u',
                             'usuarios.telefono_u','usuarios.celular_u','usuarios.email_u',
                             'usuarios.user_name','usuarios.tipo_usuarios_id')
                    ->where("usuarios.nombre_u","LIKE","%$search%")
                    ->orWhere("usuarios.apellidos_u","LIKE","%$search%")
                    ->orWhere("usuarios.telefono_u","LIKE","%$search%")
                    ->orWhere("usuarios.celular_u","LIKE","%$search%")
                    ->orWhere("usuarios.email_u","LIKE","%$search%")
                    ->orWhere("usuarios.user_name","LIKE","%$search%")
                    ->orWhere("usuarios.tipo_usuarios_id","LIKE","%$search%")->get();
    }
}
