<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Repuesto extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'repuestos';
    protected $fillable = ['marca_r',
              					   'modelo_r',
              					   'anio_r',
              					   'vin_r',
              					   'detalle_r',
                           'precio_venta_r',
                           'codigo_repuesto',
                           'deleted_at'];

    public static function getAll(){
    	return DB::table('repuestos')
    			   ->select('id',
    			   			'marca_r',
    			   			'modelo_r',
    			   			'anio_r',
    			   			'vin_r',
    			   			'detalle_r',
                  'precio_venta_r',
                  'codigo_repuesto',
    			   			'created_at',
    			   			'updated_at')
             ->where('precio_venta_r','<>',null)
             ->orderBy('repuestos.updated_at','desc')
    			   ->paginate(15);
    }
    public function scopeSearch($query, $search){
        if("" != trim($search)){
         return DB::table('repuestos')
               ->select('repuestos.id','repuestos.marca_r','repuestos.precio_venta_r',
                        'repuestos.modelo_r','repuestos.anio_r','repuestos.vin_r',
                        'repuestos.detalle_r','repuestos.codigo_repuesto')
               ->orWhere(function ($query) use ($search) {
                    $query->orWhere("repuestos.marca_r","rlike",$search)
                          ->orWhere("repuestos.modelo_r","rlike",$search)
                          ->orWhere("repuestos.anio_r","rlike",$search)
                          ->orWhere("repuestos.vin_r","rlike",$search)
                          ->orWhere("repuestos.detalle_r","rlike",$search)
                          ->orWhere("repuestos.precio_venta_r","rlike",$search)
                          ->orWhere("repuestos.codigo_repuesto","rlike",$search);
                    })
               ->where("repuestos.precio_venta_r","<>",null)
               ->distinct();
        }
    }
}
