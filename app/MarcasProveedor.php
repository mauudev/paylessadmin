<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;

class MarcasProveedor extends Model
{
    protected $table = 'marcas_proveedores';
    protected $fillable = ['marca_prov',
                           'proveedores_id'];


    public static function getMarcasProveedor($id){
      return MarcasProveedor::where('proveedores_id','=',$id)->get();
    }
}
