<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;
class Proveedor extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $table = 'proveedores';
    protected $fillable = ['nombre_compania_p',
    					   'direccion_p',
    					   'telefono_p',
    					   'celular_p',
    					   'email_p',
    					   'persona_contacto_p',
                 'tipo_proveedores_id',
                 'deleted_at'];

    public static function getAll(){
    	return DB::table('proveedores')
    			   ->select('id',
        			   			'nombre_compania_p',
        			   			'direccion_p',
        			   			'telefono_p',
        			   			'celular_p',
        			   			'email_p',
        			   			'persona_contacto_p',
                      'tipo_proveedores_id',
        			   			'created_at',
        			   			'updated_at',
                      'deleted_at')
    			   ->paginate(15);
    }
    public static function getProveedores(){
        return DB::table('proveedores')
                   ->join('tipo_proveedores','proveedores.tipo_proveedores_id','=',
                          'tipo_proveedores.id','left outer')
                   ->select('proveedores.id','proveedores.nombre_compania_p',
                          'proveedores.direccion_p','proveedores.telefono_p',
                          'proveedores.celular_p','proveedores.email_p',
                          'proveedores.persona_contacto_p',
                          'proveedores.tipo_proveedores_id','tipo_proveedores.tipo as tipo',
                          'proveedores.deleted_at')
                   ->where('proveedores.deleted_at', '=',null)
                   ->orderBy('proveedores.updated_at','desc')
                   ->paginate(15);
    }
    public static function getProveedores_with($id) {
      return Proveedor::where('tipo_proveedores_id', '=', $id)->get();
    }
    public static function getProveedor($id){
      return Proveedor::where('id','=',$id)->get();
    }
    public function tipos_proveedor(){
        return $this->belongsTo('TipoProveedor','tipo_proveedores_id');
    }
    public static function filtro($filtro){
        if($filtro == 1){
            return DB::table('proveedores')
                        ->join('tipo_proveedores','proveedores.tipo_proveedores_id','=','tipo_proveedores.id','left outer')
                        ->select('proveedores.id','proveedores.nombre_compania_p','proveedores.persona_contacto_p',
                                 'tipo_proveedores.tipo','proveedores.telefono_p','proveedores.direccion_p','proveedores.celular_p',
                                 'proveedores.email_p')
                        ->where('proveedores.deleted_at', '=',null)->get();
        }else{
            return DB::table('proveedores')
                        ->join('tipo_proveedores','proveedores.tipo_proveedores_id','=','tipo_proveedores.id','left outer')
                        ->select('proveedores.id','proveedores.nombre_compania_p','proveedores.persona_contacto_p','proveedores.telefono_p','proveedores.direccion_p',
                                 'proveedores.celular_p','proveedores.email_p','tipo_proveedores.tipo')
                        ->where('proveedores.deleted_at','<>',null)->get();
        }
    }

    public static function scopeSearch($query,$search){
        if("" != trim($search)){
          $target = trim($search);
          return DB::table('proveedores') 
                 ->join('tipo_proveedores','proveedores.tipo_proveedores_id','=','tipo_proveedores.id')
                 ->join('marcas_proveedores','proveedores.id','=','marcas_proveedores.proveedores_id')
                 ->select('proveedores.id','proveedores.nombre_compania_p',
                          'proveedores.direccion_p','proveedores.telefono_p',
                          'proveedores.celular_p','proveedores.email_p',
                          'proveedores.persona_contacto_p',
                          'proveedores.tipo_proveedores_id','tipo_proveedores.tipo as tipo',
                          DB::raw('group_concat(DISTINCT marcas_proveedores.marca_prov) as marca_prov'),
                          'proveedores.deleted_at')
                 ->orWhere(function ($query) use ($search) {
                    $query->where("proveedores.nombre_compania_p","rlike",$search)
                          ->orWhere("proveedores.direccion_p","rlike",$search)
                          ->orWhere("proveedores.telefono_p","rlike",$search)
                          ->orWhere("proveedores.celular_p","rlike",$search)
                          ->orWhere("proveedores.email_p","rlike",$search)
                          ->orWhere("proveedores.persona_contacto_p","rlike",$search)
                          ->orWhere("tipo_proveedores.tipo","rlike",$search)
                          ->orWhere("marcas_proveedores.marca_prov","rlike",$search);
                    })
                 ->orderBy('proveedores.updated_at','desc')->distinct();
        } 
    }
    public static function searchProveedores($search){
        if("" != trim($search)){
          $target = trim($search);
          return DB::table('proveedores') 
                 ->join('tipo_proveedores','proveedores.tipo_proveedores_id','=','tipo_proveedores.id')
                 ->join('marcas_proveedores','proveedores.id','=','marcas_proveedores.proveedores_id')
                 ->select('proveedores.id','proveedores.nombre_compania_p',
                          'proveedores.direccion_p','proveedores.telefono_p',
                          'proveedores.celular_p','proveedores.email_p',
                          'proveedores.persona_contacto_p',
                          'proveedores.tipo_proveedores_id','tipo_proveedores.tipo as tipo',
                          'marcas_proveedores.marca_prov','proveedores.deleted_at')
                 ->orWhere(function ($query) use ($search) {
                    $query->where("proveedores.nombre_compania_p","rlike",$search)
                          ->orWhere("proveedores.direccion_p","rlike",$search)
                          ->orWhere("proveedores.telefono_p","rlike",$search)
                          ->orWhere("proveedores.celular_p","rlike",$search)
                          ->orWhere("proveedores.email_p","rlike",$search)
                          ->orWhere("proveedores.persona_contacto_p","rlike",$search)
                          ->orWhere("tipo_proveedores.tipo","rlike",$search)
                          ->orWhere("marcas_proveedores.marca_prov","rlike",$search);
                    })
                 ->orderBy('proveedores.updated_at','desc')->distinct()->get();
        } 
    }
}
