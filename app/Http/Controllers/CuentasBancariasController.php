<?php

namespace PaylessAdmin\Http\Controllers;

use Illuminate\Http\Request;

use PaylessAdmin\Http\Requests;
use PaylessAdmin\CuentasBancarias;
use Session;
use Validator;
class CuentasBancariasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cuentas = CuentasBancarias::getAll();
        return view('cuentas-bancarias.index',['cuentas'=>$cuentas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cuentas-bancarias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
                'banco.min'    => 'El nombre del banco es demasiado corto !',
                'banco.string'    => 'El nombre del banco es inválido !',
                'cuenta_bancaria.min'    => 'El número de cuenta bancaria es inválido !',
                'cuenta_bancaria.string'    => 'El número de cuenta bancaria es inválido !',
                'moneda.string' => 'El tipo de cambio ingresado no es válido !',
        ];
        $validator = Validator::make($request->all(), [
                'banco'     => 'required|min:5',
                'cuenta_bancaria'     => 'required|min:8|string',
                'moneda'    => 'string|required'
        ],$messages);
        if ($validator->fails()) {
          return redirect('cuentas-bancarias/create')
                      ->withErrors($validator)
                      ->withInput();
        }
        CuentasBancarias::create($request->all());
        Session::flash('store-sucess','datos agregados correctamente !');
        return redirect()->route('cuentas-bancarias.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cuenta = CuentasBancarias::find($id);
        return view('cuentas-bancarias.edit',['cuenta'=>$cuenta]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
                'banco.min'    => 'El nombre del banco es demasiado corto !',
                'banco.string'    => 'El nombre del banco es inválido !',
                'cuenta_bancaria.min'    => 'El número de cuenta bancaria es inválido !',
                'cuenta_bancaria.string'    => 'El número de cuenta bancaria es inválido !',
                'moneda.string' => 'El tipo de cambio ingresado no es válido !',
        ];
        $validator = Validator::make($request->all(), [
                'banco'     => 'required|min:5',
                'cuenta_bancaria'     => 'required|min:8|string',
                'moneda'    => 'string|required'
        ],$messages);
        if ($validator->fails()) {
          return redirect('cuentas-bancarias/'.$id.'/edit')
                      ->withErrors($validator)
                      ->withInput();
        }
        $cuenta = CuentasBancarias::find($id);
        $cuenta->banco = $request->banco;
        $cuenta->cuenta_bancaria = $request->cuenta_bancaria;
        $cuenta->moneda = $request->moneda;
        $cuenta->save();
        Session::flash('update-success','datos actualizados correctamente !');
        return redirect()->route('cuentas-bancarias.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cuenta = CuentasBancarias::find($id);
        //$cuenta->delete();
        //Session::flash('delete-success','datos eliminados correctamente !');
        return redirect()->route('cuentas-bancarias.index');
    }
}
