<?php
namespace PaylessAdmin\Http\Controllers;

use Illuminate\Http\Request;

use PaylessAdmin\Http\Requests;
use PaylessAdmin\Paises;
use PaylessAdmin\Cliente;
use PaylessAdmin\Mercaderia;
use PaylessAdmin\Repuesto;
use PaylessAdmin\Proovedor;
use PaylessAdmin\CotizacionRepuesto;
use PaylessAdmin\CotizacionMercaderia;
use PaylessAdmin\Venta;
use PaylessAdmin\Usuario;
use PaylessAdmin\CuentasBancarias;
use PaylessAdmin\DetallePagoEfectivo;
use PaylessAdmin\DetallePagoDeposito;
use PaylessAdmin\VentaConfirmada;
use Illuminate\Support\Facades\Route;
use Session;
use PDF;
use Validator;
use QrCode;

class VentasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        if("" != $request->get('search'))
            $ventas = Venta::search($request->get('search'))->paginate(15); 
        else
            $ventas = Venta::getVentas(1);
        return view('ventas/index',['ventas'=>$ventas]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
      

    public function generarPDF($ventas_id){
      $venta = Venta::find($ventas_id);
      if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$ventas_id)->first();
      $cliente = Cliente::find($venta->clientes_id);
      $usuario = Usuario::find($venta->usuarios_id);
      $cotizacion_rep = CotizacionRepuesto::getRepuestos_cotizacionEstado($ventas_id,1);
      $cotizacion_mer = CotizacionMercaderia::getMercaderias_cotizacionEstado($ventas_id,1);
      $repuestos = array();
      $mercaderias = array();
      if($venta->pago_parcial !=null ){
        $pago_parcial = $venta->pago_parcial;
      }
      if(count($cotizacion_rep) > 0){
        for($i = 0; $i < count($cotizacion_rep); $i ++)
          $repuestos[$i] = CotizacionRepuesto::getVentaDellate_rep($cotizacion_rep[$i]->id);
      }//else return "<b>ERROR 505</b> 2 Ocurrio un error al recuperar los datos de la venta: No existen repuestos cotizados";
      if(count($cotizacion_mer) > 0){
        for($i = 0; $i < count($cotizacion_mer); $i ++)
          $mercaderias[$i] = CotizacionMercaderia::getVentaDellate_mer($cotizacion_mer[$i]->id);
      }//else return "<b>ERROR 505</b> 2 Ocurrio un error al recuperar los datos de la venta: No existen mercaderias cotizados";
      $observaciones = '';
      $plazo_entrega = '';
      $metodo_pago = '';
      if(isset($pago_parcial))
        $pdf = PDF::loadView('pdffile.pdf-template',['pago_parcial'=>$pago_parcial,'venta'=>$venta,'cliente'=>$cliente,'usuario'=>$usuario,'repuestos'=>$repuestos,'mercaderias'=>$mercaderias,'observaciones'=>$observaciones,'plazo_entrega'=>$plazo_entrega,'metodo_pago'=>$metodo_pago]);
      else $pdf = PDF::loadView('pdffile.pdf-template',['venta'=>$venta,'cliente'=>$cliente,'usuario'=>$usuario,'repuestos'=>$repuestos,'mercaderias'=>$mercaderias,'observaciones'=>$observaciones,'plazo_entrega'=>$plazo_entrega,'metodo_pago'=>$metodo_pago]);
      $nombre_archivo = $cliente->nombre_c.'-'.$cliente->apellidos_c.'-cotizacion-'.$venta->created_at.'.pdf';
      $png = '';
      $renderer = new \BaconQrCode\Renderer\Image\Png();
      $renderer->setHeight(256);
      $renderer->setWidth(256);
      $writer = new \BaconQrCode\Writer($renderer);
      $url = 'http://sis.paylessimport.com/administracion/mostrar-cotizacion-principal/show/'.$ventas_id;
      $writer->writeFile($url, base_path().'/administracion/qrcodes/qrcode.png');
      //$writer->writeFile($url, '/home4/boliviac/public_html/sis/administracion/qrcodes/qrcode.png');
      //return view('video.invoice',['venta'=>$venta,'cliente'=>$cliente,'usuario'=>$usuario,'repuestos'=>$repuestos,'mercaderias'=>$mercaderias]);
      return $pdf->download($nombre_archivo);//$pdf = PDF::loadView
        
    }
    public function personalizarPDF($ventas_id){
      $venta = Venta::find($ventas_id);
        if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$ventas_id)->first();
      return view('pdffile/create',['venta'=>$venta]);
    }
    public function generarPDFPersonalizado(Request $request){
      $venta = Venta::find($request->ventas_id);
      if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$request->ventas_id)->first();
      $cliente = Cliente::find($venta->clientes_id);
      $usuario = Usuario::find($venta->usuarios_id);
      $cotizacion_rep = CotizacionRepuesto::getRepuestos_cotizacionEstado($request->ventas_id,1);
      $cotizacion_mer = CotizacionMercaderia::getMercaderias_cotizacionEstado($request->ventas_id,1);
      $repuestos = array();
      $mercaderias = array();
      $observaciones = '';
      $plazo_entrega = '';
      $metodo_pago = '';
      if(isset($request->observaciones)) $observaciones = $request->observaciones;
      if(isset($request->plazo_entrega)) $plazo_entrega = $request->plazo_entrega;
      if(isset($request->metodo_pago)) $metodo_pago = $request->metodo_pago;
      if(count($cotizacion_rep) > 0){
        for($i = 0; $i < count($cotizacion_rep); $i ++)
          $repuestos[$i] = CotizacionRepuesto::getVentaDellate_rep($cotizacion_rep[$i]->id);
      }//else return "<b>ERROR 505</b> 2 Ocurrio un error al recuperar los datos de la venta: No existen repuestos cotizados";
      if(count($cotizacion_mer) > 0){
        for($i = 0; $i < count($cotizacion_mer); $i ++)
          $mercaderias[$i] = CotizacionMercaderia::getVentaDellate_mer($cotizacion_mer[$i]->id);
      }//else return "<b>ERROR 505</b> 2 Ocurrio un error al recuperar los datos de la venta: No existen mercaderias cotizados";
      $pdf = PDF::loadView('pdffile.custom-pdf-template',['venta'=>$venta,'cliente'=>$cliente,'usuario'=>$usuario,'repuestos'=>$repuestos,'mercaderias'=>$mercaderias,'observaciones'=>$observaciones,'plazo_entrega'=>$plazo_entrega,'metodo_pago'=>$metodo_pago]);
      $nombre_archivo = $cliente->nombre_c.'-'.$cliente->apellidos_c.'-cotizacion-'.$venta->created_at.'.pdf';
      $png = '';
      $renderer = new \BaconQrCode\Renderer\Image\Png();
      $renderer->setHeight(256);
      $renderer->setWidth(256);
      $writer = new \BaconQrCode\Writer($renderer);
      $url = 'http://sis.paylessimport.com/administracion/mostrar-cotizacion-principal/show/'.$request->ventas_id;
      $writer->writeFile($url, base_path().'/administracion/qrcodes/qrcode.png');
      //$writer->writeFile($url, '/home4/boliviac/public_html/sis/administracion/qrcodes/qrcode.png');
      //return view('video.invoice',['venta'=>$venta,'cliente'=>$cliente,'usuario'=>$usuario,'repuestos'=>$repuestos,'mercaderias'=>$mercaderias]);$pdf->download($nombre_archivo)
      //Session::flash('download',$pdf->download($nombre_archivo));
      //return redirect()->route('confirmar-venta.detalleVenta',[$request->ventas_id]);
      return $pdf->download($nombre_archivo);
    }
    public function agregarSegundoPago($id){
        $venta = Venta::find($id);
        if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$id)->first();
        $usuario = Usuario::find($venta->usuarios_id);
        $cliente = Cliente::find($venta->clientes_id);
        $venta_confirmada = VentaConfirmada::where('ventas_id','=',$venta->id)->first();
        $cuentas = CuentasBancarias::getCuentas_string();

        if($venta->pago_parcial == 'deposito'){
            $pago_deposito = DetallePagoDeposito::where('ventas_confirmadas_id','=',$venta_confirmada->id)->first();
            $cuenta = CuentasBancarias::find($pago_deposito->cuentas_bancarias_id);
            return view('ventas/agregar-segundo-pago',['venta'=>$venta, 'cliente'=>$cliente, 
                                                   'usuario'=>$usuario,'cuenta'=>$cuenta,
                                                   'pago_deposito'=>$pago_deposito,'cuentas'=>$cuentas]);
        }else if($venta->pago_parcial == 'efectivo'){
            $pago_efectivo = DetallePagoEfectivo::where('ventas_confirmadas_id','=',$venta_confirmada->id)->first();
            return view('ventas/agregar-segundo-pago',['venta'=>$venta, 'cliente'=>$cliente,
          'usuario'=>$usuario,'cuentas'=>$cuentas,'pago_efectivo'=>$pago_efectivo]); 
        }  
    }

    public function guardarSegundoPago(Request $request, $id){
        //dd($request->all());
        $messages = [
                'segundo_pago.regex'=>'El monto ingresado es inválido !'
        ];
        $validator = Validator::make($request->all(), [
                'segundo_pago'=>'regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u'
        ],$messages);
        if ($validator->fails()) {
          $id1 = $request->ventas_id;
          return redirect('ventas/segundo-pago/'.$id1.'/edit')
                      ->withErrors($validator);
        }
        if(isset($request->segundo_pago)){
            $venta = Venta::find($id);
            if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$id)->first();
            if($request->tipo_pago == 'efectivo'){
              $venta_confirmada = VentaConfirmada::where('ventas_id','=',$venta->id)->first();
              $segundo_pago = new DetallePagoEfectivo();
              $segundo_pago->ventas_confirmadas_id = $venta_confirmada->id;
              $segundo_pago->detalle = 'Segundo pago';
              $segundo_pago->codigo_recibo = $request->cod_deposito_recibo;
              $segundo_pago->moneda = $request->moneda;
              if($request->moneda == 'Bolivianos'){
                $segundo_pago->monto = $request->segundo_pago / $venta->tipo_cambio;
                $segundo_pago->monto_bs = $request->segundo_pago;
              }
              if($request->moneda == 'Dolares'){
                $segundo_pago->monto = $request->segundo_pago;
              }
              $segundo_pago->save();
            }
            if($request->tipo_pago == 'deposito'){
              $venta_confirmada = VentaConfirmada::where('ventas_id','=',$venta->id)->first();
              $segundo_pago = new DetallePagoDeposito();
              $segundo_pago->ventas_confirmadas_id = $venta_confirmada->id;
              $segundo_pago->cuentas_bancarias_id = $request->cuenta_bancaria;
              $segundo_pago->detalle = 'Segundo pago';
              $segundo_pago->codigo_recibo = $request->cod_deposito_recibo;
              $segundo_pago->moneda = $request->moneda;
              if($request->moneda == 'Bolivianos'){
                $segundo_pago->monto = $request->segundo_pago / $request->tipo_cambio;
                $segundo_pago->monto_bs = $request->segundo_pago;
              }
              if($request->moneda == 'Dolares'){
                $segundo_pago->monto = $request->segundo_pago;
              }
              $segundo_pago->save();
            }
            $venta->segundo_pago = $request->tipo_pago;
            $venta->estado = 1;
            $venta->finalizado = 1;
            $venta->save();
        }
        Session::flash('store-success','datos guardados correctamente !');
        return redirect()->route('ventas.detalleVentaFinalizada',[$venta->id]);
    }
    public function editarPagos($id){
        $venta = Venta::find($id);
        if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$id)->first();
        $cliente = Cliente::find($venta->clientes_id);
        $usuario = Usuario::find($venta->usuarios_id);
        $cuentas = CuentasBancarias::getCuentas_string();
        $venta_confirmada = VentaConfirmada::where('ventas_id','=',$venta->id)->first();
        if($venta_confirmada != null){
          $pagos_ef_pp = DetallePagoEfectivo::where('ventas_confirmadas_id','=',$venta_confirmada->id)->where('detalle','=','Primer pago')->orderBy('created_at','asc')->get();
          $pagos_ef_sp = DetallePagoEfectivo::where('ventas_confirmadas_id','=',$venta_confirmada->id)->where('detalle','=','Segundo pago')->orderBy('created_at','asc')->get();
          $pagos_dep_pp = DetallePagoDeposito::where('ventas_confirmadas_id','=',$venta_confirmada->id)->where('detalle','=','Primer pago')->orderBy('created_at','asc')->get();
          $pagos_dep_sp = DetallePagoDeposito::where('ventas_confirmadas_id','=',$venta_confirmada->id)->where('detalle','=','Segundo pago')->orderBy('created_at','asc')->get();
          $tipo_cambio = $venta->tipo_cambio;
          if(count($pagos_ef_pp) == 1 && count($pagos_ef_sp) == 1){
            $tipos = 'efectivos';
            $descuento = '';
            if($venta->descuento != null && $venta->pago_total_desc != null)
                $descuento = 'descuento';
            else
                $descuento = 'sin-descuento';
            //return $pagos_ef;
            return view('ventas/edit-pagos',['venta'=>$venta, 'cliente'=>$cliente, 'usuario'=>$usuario,'descuento'=>$descuento,'pagos_ef_pp'=>$pagos_ef_pp,'pagos_ef_sp'=>$pagos_ef_sp,'tipos'=>$tipos,'cuentas'=>$cuentas, 'tipo_cambio'=>$tipo_cambio]);
          }
          if(count($pagos_dep_pp) == 1 && count($pagos_dep_sp) == 1){
            $tipos = 'depositos';
            $descuento = '';
            $cuenta_banco_dep = array();
            $cuenta_banco_dep[0] = CuentasBancarias::find($pagos_dep_pp[0]->cuentas_bancarias_id);
            $cuenta_banco_dep[1] = CuentasBancarias::find($pagos_dep_sp[0]->cuentas_bancarias_id);
            if($venta->descuento != null && $venta->pago_total_desc != null)
                $descuento = 'descuento';
            else
                $descuento = 'sin-descuento';
            return view('ventas/edit-pagos',['venta'=>$venta, 'cliente'=>$cliente, 'usuario'=>$usuario,'descuento'=>$descuento,'pagos_dep_pp'=>$pagos_dep_pp,'pagos_dep_sp'=>$pagos_dep_sp,'tipos'=>$tipos,'cuentas'=>$cuentas,'cuenta_banco_dep'=>$cuenta_banco_dep,'tipo_cambio'=>$tipo_cambio]);
          }
          if(count($pagos_dep_pp) == 1 && count($pagos_ef_sp) == 1){
            $tipos = 'ef_dep';
            $descuento = '';
            $cuenta_banco_dep[0] = CuentasBancarias::find($pagos_dep_pp[0]->cuentas_bancarias_id);
            if($venta->descuento != null && $venta->pago_total_desc != null)
                $descuento = 'descuento';
            else
                $descuento = 'sin-descuento';
            //completar para este
            $primer_pago = 'deposito';
            return view('ventas/edit-pagos',['venta'=>$venta, 'cliente'=>$cliente, 'usuario'=>$usuario,'descuento'=>$descuento,'pagos_ef_sp'=>$pagos_ef_sp,'pagos_dep_pp'=>$pagos_dep_pp,'tipos'=>$tipos,'cuentas'=>$cuentas,'cuenta_banco_dep'=>$cuenta_banco_dep,'tipo_cambio'=>$tipo_cambio,'primer_pago'=>$primer_pago]);
          }
          if(count($pagos_dep_sp) == 1 && count($pagos_ef_pp) == 1){
            $tipos = 'ef_dep';
            $descuento = '';
            $cuenta_banco_dep[0] = CuentasBancarias::find($pagos_dep_sp[0]->cuentas_bancarias_id);
            if($venta->descuento != null && $venta->pago_total_desc != null)
                $descuento = 'descuento';
            else
                $descuento = 'sin-descuento';
            //completar para este
            $primer_pago = 'efectivo';
            return view('ventas/edit-pagos',['venta'=>$venta, 'cliente'=>$cliente, 'usuario'=>$usuario,'descuento'=>$descuento,'pagos_ef_pp'=>$pagos_ef_pp,'pagos_dep_sp'=>$pagos_dep_sp,'tipos'=>$tipos,'cuentas'=>$cuentas,'cuenta_banco_dep'=>$cuenta_banco_dep,'tipo_cambio'=>$tipo_cambio,'primer_pago'=>$primer_pago]);
          }
      }
    }
    
    public function updatePagos(Request $request, $id){
        //dd($request->all());
        $messages = [
                'pago_parcial.regex'=>'El monto del primer pago es inválido !',
                'segundo_pago.regex'=>'El monto del segundo pago es inválido !'
        ];
        $validator = Validator::make($request->all(), [
                'pago_parcial'=>'regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u',
                'segundo_pago'=>'regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u'
        ],$messages);
        if ($validator->fails()) {
          $id1 = $request->ventas_id;
          return redirect('ventas/pagos/'.$id1.'/edit')->withErrors($validator);
        }
        $venta = Venta::find($id);
        if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$id)->first();
        $venta_confirmada = VentaConfirmada::where('ventas_id','=',$venta->id)->first();
        if($request->tipos == 'efectivos'){
          if($request->tipo_pago1 == 'efectivo-pp'){
            $primer_pago = DetallePagoEfectivo::find($request->primer_pago_id);
            $primer_pago->detalle = 'Primer pago';
            $primer_pago->codigo_recibo = $request->codigo_recibo_pp;
            $primer_pago->moneda = $request->moneda_pp;
            if($request->moneda == 'Dolares') $primer_pago->monto = $request->primer_pago;
            if($request->moneda == 'Bolivianos') $primer_pago->monto_bs = $request->primer_pago;
            $primer_pago->save();
            $venta->pago_parcial = 'efectivo';
          }
          if($request->tipo_pago1 == 'deposito-pp'){
            $primer_pago_old = DetallePagoEfectivo::find($request->primer_pago_id);
            $primer_pago = new DetallePagoDeposito();
            $primer_pago->ventas_confirmadas_id = $venta_confirmada->id;
            $primer_pago->cuentas_bancarias_id = $request->cuenta_bancaria_pp;
            $primer_pago->detalle = 'Primer pago';
            $primer_pago->codigo_recibo = $request->codigo_recibo_pp;
            $primer_pago->moneda = $request->moneda_pp;
            if($request->moneda_pp == 'Dolares') $primer_pago->monto = $request->primer_pago;
            if($request->moneda_pp == 'Bolivianos') $primer_pago->monto_bs = $request->primer_pago;
            $primer_pago->save();
            $primer_pago->save();
            $primer_pago_old->delete();
            $venta->pago_parcial = 'deposito';
          }
          if($request->tipo_pago2 == 'efectivo-sp'){
            $segundo_pago = DetallePagoEfectivo::find($request->segundo_pago_id);
            $segundo_pago->detalle = 'Segundo pago';
            $segundo_pago->moneda = $request->moneda_sp;
            if($request->moneda_sp == 'Dolares') $segundo_pago->monto = $request->segundo_pago;
            if($request->moneda_sp == 'Bolivianos') $segundo_pago->monto_bs = $request->segundo_pago;
            $segundo_pago->codigo_recibo = $request->codigo_recibo_sp;
            $segundo_pago->save();
            $venta->segundo_pago = 'efectivo';
          }
          if($request->tipo_pago2 == 'deposito-sp'){
            $segundo_pago_old = DetallePagoEfectivo::find($request->segundo_pago_id);
            $segundo_pago = new DetallePagoDeposito();
            $segundo_pago->ventas_confirmadas_id = $venta_confirmada->id;
            $segundo_pago->cuentas_bancarias_id = $request->cuenta_bancaria_sp;
            $segundo_pago->detalle = 'Segundo pago';
            $segundo_pago->moneda = $request->moneda_sp;
            if($request->moneda_sp == 'Dolares') $segundo_pago->monto = $request->segundo_pago;
            if($request->moneda_sp == 'Bolivianos') $segundo_pago->monto_bs = $request->segundo_pago;
            $segundo_pago->codigo_recibo = $request->codigo_recibo_sp;
            $segundo_pago->save();
            $segundo_pago_old->delete();
            $venta->segundo_pago = 'deposito';
          }
          $venta->estado = 1;
          $venta->finalizado = 1;
          $venta->save();
        }
        //
        if($request->tipos == 'depositos'){
          if($request->tipo_pago1 == 'deposito-pp'){
            $primer_pago = DetallePagoDeposito::find($request->primer_pago_id);
            $primer_pago->detalle = 'Primer pago';
            $primer_pago->cuentas_bancarias_id = $request->cuenta_bancaria_pp;
            $primer_pago->moneda = $request->moneda_pp;
            if($request->moneda_pp == 'Dolares') $primer_pago->monto = $request->primer_pago;
            if($request->moneda_pp == 'Bolivianos') $primer_pago->monto_bs = $request->primer_pago;
            $primer_pago->codigo_recibo = $request->codigo_recibo_pp;
            $primer_pago->save();
            $venta->pago_parcial = 'deposito';
          }
          if($request->tipo_pago1 == 'efectivo-pp'){
            $primer_pago_old = DetallePagoDeposito::find($request->primer_pago_id);
            $primer_pago = new DetallePagoEfectivo();
            $primer_pago->ventas_confirmadas_id = $venta_confirmada->id;
            $primer_pago->detalle = 'Primer pago';
            $primer_pago->moneda = $request->moneda_pp;
            if($request->moneda_pp == 'Dolares') $primer_pago->monto = $request->primer_pago;
            if($request->moneda_pp == 'Bolivianos') $primer_pago->monto_bs = $request->primer_pago;
            $primer_pago->codigo_recibo = $request->codigo_recibo_pp;
            $primer_pago->save();
            $primer_pago_old->delete();
            $venta->pago_parcial = 'efectivo';
          }
          if($request->tipo_pago2 == 'efectivo-sp'){
            $segundo_pago_old = DetallePagoDeposito::find($request->segundo_pago_id);
            $segundo_pago = new DetallePagoEfectivo();
            $segundo_pago->ventas_confirmadas_id = $venta_confirmada->id;
            $segundo_pago->detalle = 'Segundo pago';
            $segundo_pago->moneda = $request->moneda_sp;
            if($request->moneda_sp == 'Dolares') $segundo_pago->monto = $request->segundo_pago;
            if($request->moneda_sp == 'Bolivianos') $segundo_pago->monto_bs = $request->segundo_pago;
            $segundo_pago->codigo_recibo = $request->codigo_recibo_sp;
            $segundo_pago->save();
            $segundo_pago_old->delete();
            $venta->segundo_pago = 'efectivo';
          }
          if($request->tipo_pago2 == 'deposito-sp'){
            $segundo_pago = DetallePagoDeposito::find($request->segundo_pago_id);
            $segundo_pago->detalle = 'Segundo pago';
            $segundo_pago->cuentas_bancarias_id = $request->cuenta_bancaria_sp;
            $segundo_pago->moneda = $request->moneda_sp;
            if($request->moneda_sp == 'Dolares') $segundo_pago->monto = $request->segundo_pago;
            if($request->moneda_sp == 'Bolivianos') $segundo_pago->monto_bs = $request->segundo_pago;
            $segundo_pago->codigo_recibo = $request->codigo_recibo_sp;
            $segundo_pago->save();
            $venta->segundo_pago = 'deposito';
          }
          $venta->estado = 1;
          $venta->finalizado = 1;
          $venta->save();
        }
        //
        if($request->tipos == 'ef_dep'){
          if($request->primer_pago_tipo == 'efectivo'){//si el pago era antes en efectivo
            if($request->tipo_pago1 == 'efectivo-pp'){//y ahora es efectivo
              $primer_pago = DetallePagoEfectivo::find($request->primer_pago_id);
              $primer_pago->detalle = 'Primer pago';
              $primer_pago->moneda = $request->moneda_pp;
              if($request->moneda_pp == 'Dolares') $primer_pago->monto = $request->primer_pago;
              if($request->moneda_pp == 'Bolivianos') $primer_pago->monto_bs = $request->primer_pago;
              $primer_pago->codigo_recibo = $request->codigo_recibo_pp;
              $primer_pago->save();
              $venta->pago_parcial = 'efectivo';
            }

            if($request->tipo_pago1 == 'deposito-pp'){//y ahora es deposito
              $primer_pago_old = DetallePagoEfectivo::find($request->primer_pago_id);
              $primer_pago = new DetallePagoDeposito();
              $primer_pago->ventas_confirmadas_id = $venta_confirmada->id;
              $primer_pago->cuentas_bancarias_id = $request->cuenta_bancaria_pp;
              $primer_pago->detalle = 'Primer pago';
              $primer_pago->moneda = $request->moneda_pp;
              if($request->moneda_pp == 'Dolares') $primer_pago->monto = $request->primer_pago;
              if($request->moneda_pp == 'Bolivianos') $primer_pago->monto_bs = $request->primer_pago;
              $primer_pago->codigo_recibo = $request->codigo_recibo_pp;
              $primer_pago->save();
              $primer_pago_old->delete();
              $venta->pago_parcial = 'deposito';
            }
          }
          if($request->primer_pago_tipo == 'deposito'){//si el pago era antes en deposito
            if($request->tipo_pago1 == 'efectivo-pp'){//y ahora es efectivo
              $primer_pago_old = DetallePagoDeposito::find($request->primer_pago_id);
              $primer_pago = new DetallePagoEfectivo();
              $primer_pago->detalle = 'Primer pago';
              $primer_pago->ventas_confirmadas_id = $venta_confirmada->id;
              $primer_pago->moneda = $request->moneda_pp;
              if($request->moneda_pp == 'Dolares') $primer_pago->monto = $request->primer_pago;
              if($request->moneda_pp == 'Bolivianos') $primer_pago->monto_bs = $request->primer_pago;
              $primer_pago->codigo_recibo = $request->codigo_recibo_pp;
              $primer_pago->save();
              $primer_pago_old->delete();
              $venta->pago_parcial = 'efectivo';
            }
            if($request->tipo_pago1 == 'deposito-pp'){//y ahora es deposito
              $primer_pago = DetallePagoDeposito::find($request->primer_pago_id);
              $primer_pago->ventas_confirmadas_id = $venta_confirmada->id;
              $primer_pago->cuentas_bancarias_id = $request->cuenta_bancaria_pp;
              $primer_pago->detalle = 'Primer pago';
              $primer_pago->moneda = $request->moneda_pp;
              if($request->moneda_pp == 'Dolares') $primer_pago->monto = $request->primer_pago;
              if($request->moneda_pp == 'Bolivianos') $primer_pago->monto_bs = $request->primer_pago;
              $primer_pago->codigo_recibo = $request->codigo_recibo_pp;
              $primer_pago->save();
              $venta->pago_parcial = 'deposito';
            }
          }

          if($request->segundo_pago_tipo == 'efectivo'){//si el pago era antes en efectivo
            if($request->tipo_pago2 == 'efectivo-sp'){//y ahora es efectivo
              $segundo_pago = DetallePagoEfectivo::find($request->segundo_pago_id);
              $segundo_pago->ventas_confirmadas_id = $venta_confirmada->id;
              $segundo_pago->detalle = 'Segundo pago';
              $segundo_pago->moneda = $request->moneda_sp;
              if($request->moneda_sp == 'Dolares') $segundo_pago->monto = $request->segundo_pago;
              if($request->moneda_sp == 'Bolivianos') $segundo_pago->monto_bs = $request->segundo_pago;
              $segundo_pago->codigo_recibo = $request->codigo_recibo_sp;
              $segundo_pago->save();
              $venta->segundo_pago = 'efectivo';
            }
            if($request->tipo_pago2 == 'deposito-sp'){//y ahora es deposito
              $segundo_pago_old = DetallePagoEfectivo::find($request->segundo_pago_id);
              $segundo_pago = new DetallePagoDeposito();
              $segundo_pago->ventas_confirmadas_id = $venta_confirmada->id;
              $segundo_pago->cuentas_bancarias_id = $request->cuenta_bancaria_sp;
              $segundo_pago->detalle = 'Segundo pago';
              $segundo_pago->moneda = $request->moneda_sp;
              if($request->moneda_sp == 'Dolares') $segundo_pago->monto = $request->segundo_pago;
              if($request->moneda_sp == 'Bolivianos') $segundo_pago->monto_bs = $request->segundo_pago;
              $segundo_pago->codigo_recibo = $request->codigo_recibo_sp;
              $segundo_pago->save();
              $segundo_pago_old->delete();
              $venta->segundo_pago = 'deposito';
            }
          }

          if($request->segundo_pago_tipo == 'deposito'){//si el pago era antes en deposito
            if($request->tipo_pago2 == 'efectivo-sp'){//y ahora es efectivo
              $segundo_pago_old = DetallePagoDeposito::find($request->segundo_pago_id);
              $segundo_pago = new DetallePagoEfectivo();
              $segundo_pago->ventas_confirmadas_id = $venta_confirmada->id;
              $segundo_pago->detalle = 'Segundo pago';
              $segundo_pago->moneda = $request->moneda_sp;
              if($request->moneda_sp == 'Dolares') $segundo_pago->monto = $request->segundo_pago;
              if($request->moneda_sp == 'Bolivianos') $segundo_pago->monto_bs = $request->segundo_pago;
              $segundo_pago->codigo_recibo = $request->codigo_recibo_sp;
              $segundo_pago->save();
              $segundo_pago_old->delete();
              $venta->segundo_pago = 'efectivo';
            }
            if($request->tipo_pago2 == 'deposito-sp'){//y ahora es deposito
              $segundo_pago = DetallePagoDeposito::find($request->segundo_pago_id);
              $segundo_pago->ventas_confirmadas_id = $venta_confirmada->id;
              $segundo_pago->cuentas_bancarias_id = $request->cuenta_bancaria_sp;
              $segundo_pago->detalle = 'Segundo pago';
              $segundo_pago->moneda = $request->moneda_sp;
              if($request->moneda_sp == 'Dolares') $segundo_pago->monto = $request->segundo_pago;
              if($request->moneda_sp == 'Bolivianos') $segundo_pago->monto_bs = $request->segundo_pago;
              $segundo_pago->codigo_recibo = $request->codigo_recibo_sp;
              $segundo_pago->save();
              $venta->segundo_pago = 'deposito';
            }
          }
          $venta->estado = 1;
          $venta->finalizado = 1;
          $venta->tipo_cambio = $request->tipo_cambio;
          $venta->save();
        }
        Session::flash('toast-actualizado','datos actualizados correctamente!');
        return redirect()->route('ventas.detalleVentaFinalizada',[$venta->id]);
    }
    public function create($id)
    {
        $venta = Venta::find($id);
        if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$id)->first();
        $cliente = Cliente::find($venta->clientes_id);
        $usuario = Usuario::find($venta->usuarios_id);
        $cuentas = CuentasBancarias::getCuentas_string();
        //return $cuentas;
        return view('ventas/create',['venta'=>$venta,'cliente'=>$cliente,'usuario'=>$usuario,'cuentas'=>$cuentas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)//PRIMER PAGO
    {
        //dd($request->all());
        $messages = [
                'pago_parcial.regex'=>'El pago parcial ingresado es inválido !'
        ];
        $validator = Validator::make($request->all(), [
                'pago_parcial'=>'regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u'
        ],$messages);
        if ($validator->fails()) {
          $id1 = $request->ventas_id;
          return redirect('ventas/'.$id1.'/create')
                      ->withErrors($validator);
        }
        $venta = Venta::find($request->ventas_id);
        if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$request->ventas_id)->first();
        $venta_confirmada = VentaConfirmada::where('ventas_id','=',$venta->id)->first();
        if($venta_confirmada == null){
          $venta_confirmada = new VentaConfirmada();
          $venta_confirmada->ventas_id = $venta->id;
          $venta_confirmada->estado = 0;
          $venta_confirmada->save();
        }
        if($request->tipo_pago == 'efectivo'){
          $pago_efectivo = new DetallePagoEfectivo();
          $pago_efectivo->ventas_confirmadas_id = $venta_confirmada->id;
          $pago_efectivo->detalle = "Primer pago";
          if($request->moneda == "Bolivianos"){//convertir a dolares
            $pago_efectivo->monto = $request->pago_parcial / $request->tipo_cambio;
            $pago_efectivo->monto_bs = $request->pago_parcial;
            $pago_efectivo->moneda = $request->moneda;
            //return $pago_efectivo->monto;
          }
          if($request->moneda == 'Dolares'){
            $pago_efectivo->monto = $request->pago_parcial;
            $pago_efectivo->moneda = $request->moneda;
          }
          $pago_efectivo->codigo_recibo = $request->cod_deposito_recibo;
          $pago_efectivo->save();
        }
        if($request->tipo_pago == 'deposito'){
          $pago_deposito = new DetallePagoDeposito();
          $pago_deposito->ventas_confirmadas_id = $venta_confirmada->id;
          $pago_deposito->cuentas_bancarias_id = $request->cuenta_bancaria;
          $pago_deposito->detalle = "Primer pago";
          if($request->moneda == "Bolivianos"){//convertir a dolares
            $pago_deposito->monto = $request->pago_parcial / $request->tipo_cambio;
            $pago_deposito->monto_bs = $request->pago_parcial;
            $pago_deposito->moneda = $request->moneda;
          }
          if($request->moneda == 'Dolares'){
            $pago_deposito->monto = $request->pago_parcial;
            $pago_deposito->moneda = $request->moneda;
          }
          $pago_deposito->codigo_recibo = $request->cod_deposito_recibo;
          $pago_deposito->save();
        }
        $tipo = $request->tipo_pago;
        $venta->pago_parcial = $request->tipo_pago;
        $venta->estado = 1;
        $venta->finalizado = 0;
        $venta->tipo_cambio = $request->tipo_cambio;
        $venta->pago_total_bs = $venta->pago_total * $request->tipo_cambio;
        $venta->save();
        return redirect()->route('ventas.detalleVentaFinalizada',[$venta->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $venta = Venta::find($id);
        if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$id)->first();
        $cliente = Cliente::find($venta->clientes_id);
        $usuario = Usuario::find($venta->usuarios_id);
        $cuentas = CuentasBancarias::getCuentas_string();
        $venta_confirmada = VentaConfirmada::where('ventas_id','=',$venta->id)->first();
        if($venta_confirmada != null){
          $pagos_ef_pp = DetallePagoEfectivo::where('ventas_confirmadas_id','=',$venta_confirmada->id)->where('detalle','=','Primer pago')->orderBy('created_at','asc')->get();
          $pagos_ef_sp = DetallePagoEfectivo::where('ventas_confirmadas_id','=',$venta_confirmada->id)->where('detalle','=','Segundo pago')->orderBy('created_at','asc')->get();
          $pagos_dep_pp = DetallePagoDeposito::where('ventas_confirmadas_id','=',$venta_confirmada->id)->where('detalle','=','Primer pago')->orderBy('created_at','asc')->get();
          $pagos_dep_sp = DetallePagoDeposito::where('ventas_confirmadas_id','=',$venta_confirmada->id)->where('detalle','=','Segundo pago')->orderBy('created_at','asc')->get();
          if(count($pagos_ef_pp) == 1 && count($pagos_ef_sp) == 1){
            $tipos = 'efectivos';
            $descuento = '';
            if($venta->descuento != null && $venta->pago_total_desc != null)
                $descuento = 'descuento';
            else
                $descuento = 'sin-descuento';
            //return $pagos_ef;
            $tipo_cambio = $venta->tipo_cambio;
            return view('ventas/show',['venta'=>$venta, 'cliente'=>$cliente, 'usuario'=>$usuario,'descuento'=>$descuento,'pagos_ef_pp'=>$pagos_ef_pp,'pagos_ef_sp'=>$pagos_ef_sp,'tipos'=>$tipos,'cuentas'=>$cuentas,'tipo_cambio'=>$tipo_cambio]);
          }
          if(count($pagos_dep_pp) == 1 && count($pagos_dep_sp) == 1){
            $tipos = 'depositos';
            $descuento = '';
            $cuenta_banco_dep = array();
            $cuenta_banco_dep[0] = CuentasBancarias::find($pagos_dep_pp[0]->cuentas_bancarias_id);
            $cuenta_banco_dep[1] = CuentasBancarias::find($pagos_dep_sp[0]->cuentas_bancarias_id);
            if($venta->descuento != null && $venta->pago_total_desc != null)
                $descuento = 'descuento';
            else
                $descuento = 'sin-descuento';
            $tipo_cambio = $venta->tipo_cambio;
            return view('ventas/show',['venta'=>$venta, 'cliente'=>$cliente, 'usuario'=>$usuario,'descuento'=>$descuento,'pagos_dep_pp'=>$pagos_dep_pp,'pagos_dep_sp'=>$pagos_dep_sp,'tipos'=>$tipos,'cuentas'=>$cuentas,'cuenta_banco_dep'=>$cuenta_banco_dep,'tipo_cambio'=>$tipo_cambio]);
          }
          if(count($pagos_dep_pp) == 1 && count($pagos_ef_sp) == 1){
            $tipos = 'ef_dep';
            $descuento = '';
            $cuenta_banco_dep[0] = CuentasBancarias::find($pagos_dep_pp[0]->cuentas_bancarias_id);
            if($venta->descuento != null && $venta->pago_total_desc != null)
                $descuento = 'descuento';
            else
                $descuento = 'sin-descuento';
            //completar para este
            $tipo_cambio = $venta->tipo_cambio;
            $primer_pago = 'deposito';
            return view('ventas/show',['venta'=>$venta, 'cliente'=>$cliente, 'usuario'=>$usuario,'descuento'=>$descuento,'pagos_ef_sp'=>$pagos_ef_sp,'pagos_dep_pp'=>$pagos_dep_pp,'tipos'=>$tipos,'cuentas'=>$cuentas,'cuenta_banco_dep'=>$cuenta_banco_dep,'tipo_cambio'=>$tipo_cambio,'primer_pago'=>$primer_pago]);
          }
          if(count($pagos_dep_sp) == 1 && count($pagos_ef_pp) == 1){
            $tipos = 'ef_dep';
            $descuento = '';
            $cuenta_banco_dep[0] = CuentasBancarias::find($pagos_dep_sp[0]->cuentas_bancarias_id);
            if($venta->descuento != null && $venta->pago_total_desc != null)
                $descuento = 'descuento';
            else
                $descuento = 'sin-descuento';
            //completar para este
            $tipo_cambio = $venta->tipo_cambio;
            $primer_pago = 'efectivo';
            return view('ventas/show',['venta'=>$venta, 'cliente'=>$cliente, 'usuario'=>$usuario,'descuento'=>$descuento,'pagos_ef_pp'=>$pagos_ef_pp,'pagos_dep_sp'=>$pagos_dep_sp,'tipos'=>$tipos,'cuentas'=>$cuentas,'cuenta_banco_dep'=>$cuenta_banco_dep,'tipo_cambio'=>$tipo_cambio,'primer_pago'=>$primer_pago]);
          }
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)//adicionales
    {
        //return redirect()->route('ventas.detalleVentaFinalizada',[$id]);
        $venta = Venta::find($id);
        $tipo_cambio = 0;
        $cuentas = CuentasBancarias::getCuentas_string();
        if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$id)->first();
        $cliente = Cliente::find($venta->clientes_id);
        $usuario = Usuario::find($venta->usuarios_id);
        $venta_confirmada = VentaConfirmada::where('ventas_id','=',$venta->id)->first();
        if($venta_confirmada == null) $venta_confirmada = VentaConfirmada::onlyTrashed()->where('ventas_id','=',$venta->id)->first();
        $tipo_cambio = $venta->tipo_cambio;
        if($venta->pago_parcial == "efectivo"){
          $pago_efectivo = DetallePagoEfectivo::where('ventas_confirmadas_id','=',$venta_confirmada->id)->where('detalle','=','Primer pago')->first();
          return view('ventas.edit-pago-parcial',['venta'=>$venta,'cliente'=>$cliente,'usuario'=>$usuario,'pago_efectivo'=>$pago_efectivo,'cuentas'=>$cuentas,'tipo_cambio'=>$tipo_cambio]);
        }else{
          $pago_deposito = DetallePagoDeposito::where('ventas_confirmadas_id','=',$venta_confirmada->id)->where('detalle','=','Primer pago')->first();
          $cuenta_banco_dep = CuentasBancarias::find($pago_deposito->cuentas_bancarias_id);
          //return $pago_deposito;
          return view('ventas.edit-pago-parcial',['venta'=>$venta,'cliente'=>$cliente,'usuario'=>$usuario,'pago_deposito'=>$pago_deposito,'cuentas'=>$cuentas,'cuenta_banco_dep'=>$cuenta_banco_dep,'tipo_cambio'=>$tipo_cambio]);
        } 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        //return redirect()->route('ventas.detalleVentaFinalizada',[$id]);
        $messages = [
                'pago_parcial.regex'=>'El pago parcial ingresado es inválido !'
        ];
        $validator = Validator::make($request->all(), [
                'pago_parcial'=>'regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u'
        ],$messages);
        if ($validator->fails()) {
          return redirect('ventas/'.$id.'/edit')
                      ->withErrors($validator);
        }
        $venta = Venta::find($id);
        $confirmation = "fail";
        if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$id)->first();
        $venta_confirmada = VentaConfirmada::where('ventas_id','=',$venta->id)->first();
        if(isset($request->pagos_efectivo_id)){
          if($request->tipo_pago1 == 'efectivo-pp'){
            $pago_efectivo = DetallePagoEfectivo::find($request->pagos_efectivo_id);
            if($request->moneda == 'Bolivianos'){
              $pago_efectivo->monto = round(($request->pago_parcial/$request->tipo_cambio),2);
              $pago_efectivo->monto_bs = $request->pago_parcial;
            }
            if($request->moneda == 'Dolares'){
              $pago_efectivo->monto = $request->pago_parcial;
              $pago_efectivo->monto_bs = round(($request->pago_parcial*$request->tipo_cambio),2);
            }
            $pago_efectivo->codigo_recibo = $request->codigo_recibo_pp;
            $pago_efectivo->moneda = $request->moneda;
            $pago_efectivo->save();
            $venta->pago_parcial = 'efectivo';
            $venta->save();
            $confirmation = "pass";
          }
          if($request->tipo_pago1 == 'deposito-pp'){
            $pago_efectivo = DetallePagoEfectivo::find($request->pagos_efectivo_id);
            $pago_efectivo->delete();
            $pago_deposito = new DetallePagoDeposito();
            $pago_deposito->ventas_confirmadas_id = $venta_confirmada->id;
            $pago_deposito->cuentas_bancarias_id = $request->cuenta_bancaria_pp;
            $pago_deposito->detalle = "Primer pago";
            if($request->moneda == 'Bolivianos'){
              $pago_deposito->monto = round(($request->pago_parcial/$request->tipo_cambio),2);
              $pago_deposito->monto_bs = $request->pago_parcial;
            }
            if($request->moneda == 'Dolares'){
              $pago_deposito->monto = $request->pago_parcial;
              $pago_deposito->monto_bs = round(($request->pago_parcial*$request->tipo_cambio),2);
            }
            $pago_deposito->moneda = $request->moneda;
            $pago_deposito->codigo_recibo = $request->codigo_recibo_pp;
            $pago_deposito->save();
            $venta->pago_parcial = 'deposito';
            $venta->tipo_cambio = $request->tipo_cambio;
            $venta->save();

            $confirmation = "pass";
          }
        }
        if(isset($request->pagos_deposito_id)){
          if($request->tipo_pago1 == 'efectivo-pp'){
            $pago_deposito = DetallePagoDeposito::find($request->pagos_deposito_id);
            $pago_deposito->delete();
            $pago_efectivo = new DetallePagoEfectivo();
            $pago_efectivo->ventas_confirmadas_id = $venta_confirmada->id;
            $pago_efectivo->detalle = "Primer pago";
            if($request->moneda == 'Bolivianos'){
              $pago_efectivo->monto = round(($request->pago_parcial/$request->tipo_cambio),2);
              $pago_efectivo->monto_bs = $request->pago_parcial;
            }
            if($request->moneda == 'Dolares'){
              $pago_efectivo->monto = $request->pago_parcial;
              $pago_efectivo->monto_bs = round(($request->pago_parcial*$request->tipo_cambio),2);
            }
            $pago_efectivo->codigo_recibo = $request->codigo_recibo_pp;
            $pago_efectivo->moneda = $request->moneda;
            $pago_efectivo->save();
            $venta->pago_parcial = 'efectivo';
            $venta->save();
            $confirmation = "pass";
          }
          if($request->tipo_pago1 == 'deposito-pp'){
            $pago_deposito = DetallePagoDeposito::find($request->pagos_deposito_id);
            $pago_deposito->cuentas_bancarias_id = $request->cuenta_bancaria_pp;
            if($request->moneda == 'Bolivianos'){
              $pago_deposito->monto = round(($request->pago_parcial/$request->tipo_cambio),2);
              $pago_deposito->monto_bs = $request->pago_parcial;
            }
            if($request->moneda == 'Dolares'){
              $pago_deposito->monto = $request->pago_parcial;
              $pago_deposito->monto_bs = round(($request->pago_parcial*$request->tipo_cambio),2);
            }
            $pago_deposito->moneda = $request->moneda;
            $pago_deposito->codigo_recibo = $request->codigo_recibo_pp;
            $pago_deposito->save();
            $venta->pago_parcial = 'deposito';
            $venta->tipo_cambio = $request->tipo_cambio;
            $venta->save();
            $confirmation = "pass";
          }
        }
        if($confirmation == "pass"){
          Session::flash('toast-actualizado','datos actualizados correctamente !');
          return redirect()->route('ventas.agregarSegundoPago',[$venta->id]);
        }else{
          Session::flash('message-error','ocurri&oacute un error al actualizar los datos! <br> C&oacute;digo de error: 800');
          return redirect()->route('ventas.edit',[$venta->id]);
        } 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
