<?php

namespace PaylessAdmin\Http\Controllers;

use Illuminate\Http\Request;
use PaylessAdmin\Cliente;
use PaylessAdmin\Paises;
use PaylessAdmin\Http\Requests;
use PaylessAdmin\Http\Requests\ClientesCreateRequest;
use PaylessAdmin\Http\Requests\ClientesUpdateRequest;
use Session;
use Validator;
class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        if("" != $request->get('search'))
            $clientes = Cliente::searchClientes($request->get('search'))->paginate(10);
        else 
            $clientes = Cliente::getAll();
        //dd($clientes->all());    
        return view('clientes.index',['clientes'=>$clientes]);
    }

    /*BUSQUEDAS*/
    public function busquedaCliente(Request $request, $search){
        $clientes = Cliente::searchClientes($search)->get();
        return response()->json(['response' => $clientes]); 
    }

    public function validarDuplicadosCel(Request $request, $search){
        //sleep(1);
        $cliente = Cliente::validarDuplicadosCel($search);
        return response()->json(['response' => $cliente]); 
    }
    public function validarDuplicadosTel(Request $request, $search){
        //sleep(1);
        $cliente = Cliente::validarDuplicadosTel($search);
        return response()->json(['response' => $cliente]); 
    }

    public function validarDuplicadosCelEdit(Request $request, $cliente_id, $celular_c){
        //sleep(1);
        $cliente = Cliente::validarDuplicadosCelEdit($cliente_id, $celular_c);
        return response()->json(['response' => $cliente]); 
    }
    public function validarDuplicadosTelEdit(Request $request, $cliente_id, $telefono_c){
        //sleep(1);
        $cliente = Cliente::validarDuplicadosTelEdit($cliente_id, $telefono_c);
        return response()->json(['response' => $cliente]); 
    }
    
    public function filtro($filtro){
        $clientes = Cliente::filtro($filtro);
        return $clientes;
    }
    /*BUSQUEDAS*/
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $paises = Paises::lists('nombre_largo','nombre_largo');
        $accion = "create";
        $origen_contacto = ["Anuncio"=>"Anuncio","Pagina Facebook"=>"Pagina Facebook","WhatsApp"=>"WhatsApp"];
        return view('clientes.create',['paises'=>$paises, 'origen_contacto'=>$origen_contacto,'accion'=>$accion]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $messages = [
                'nombre_c.regex'=>'El campo nombre no puede contener números o símbolos !',
                'apellidos_c.regex'=>'El campo apellidos no puede contener números o símbolos !',
                'email_c.email'=>'El email ingresado no es válido !',
        ];
        $validator = Validator::make($request->all(), [
                'nombre_c'=> 'required|regex:/^[\pL\s\-]+$/u',
                'apellidos_c'=> 'required|regex:/^[\pL\s\-]+$/u',
                'email_c'=>'email',
                'celular_c'=>'required'
        ],$messages);
        if ($validator->fails()) {
          return redirect('clientes/create')
                      ->withErrors($validator)
                      ->withInput();
        }
        $cliente = Cliente::create($request->all());
        Session::flash('store-success','datos agregados correctamente !');
        return redirect()->route('clientes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = Cliente::find($id);
        if($cliente == null) $cliente = Cliente::onlyTrashed()->where('id','=',$id)->first(); 
        $paises = Paises::lists('nombre_largo','nombre_largo');
        $origen_contacto = $this->getOrigenContactos($cliente->origen_contacto);
        $accion = 'editar';
        return view('clientes.edit',['cliente'=>$cliente,'paises'=>$paises,'origen_contacto'=>$origen_contacto,'accion'=>$accion]);
    }
    public function getOrigenContactos($actual){
      $seleccion = array();
      switch($actual){
        case "Anuncio":
          $seleccion = ["Anuncio"=>"Anuncio","Pagina Facebook"=>"Pagina Facebook","WhatsApp"=>"WhatsApp"];
          break;
        case "Pagina Facebook":
          $seleccion = ["Pagina Facebook"=>"Pagina Facebook","Anuncio"=>"Anuncio","WhatsApp"=>"WhatsApp"];
          break;
        case "WhatsApp":
          $seleccion = ["WhatsApp"=>"WhatsApp","Pagina Facebook"=>"Pagina Facebook","Anuncio"=>"Anuncio"];
          break;
      }
      return $seleccion;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $messages = [
                'nombre_c.regex'=>'El campo nombre no puede contener números o símbolos !',
                'apellidos_c.regex'=>'El campo apellidos no puede contener números o símbolos !',
                'email_c.email'=>'El email ingresado no es válido !',
                'celular_c.required'=>'El campo celular es obligatorio !'
        ];
        $validator = Validator::make($request->all(), [
                'nombre_c'=> 'required|regex:/^[\pL\s\-]+$/u',
                'apellidos_c'=> 'required|regex:/^[\pL\s\-]+$/u',
                'email_c'=>'email',
                'celular_c'=>'required'
        ],$messages);
        if ($validator->fails()) {
          return redirect('clientes/create')
                      ->withErrors($validator)
                      ->withInput();
        }
        $cliente = Cliente::find($id);
        if($cliente == null) $cliente = Cliente::onlyTrashed()->where('id','=',$id)->first(); 
        //$cliente->nombre_c = $request->nombre_c;
        //$cliente->apellidos_c = $request->apellidos_c;
        $cliente->direccion_c = $request->direccion_c;
        $cliente->telefono_c = $request->telefono_c;
        $cliente->celular_c = $request->celular_c;
        $cliente->email_c = $request->email_c;
        $cliente->pais = $request->pais;
        $cliente->origen_contacto = $request->origen_contacto;
        $cliente->save();
        Session::flash('update-success','datos actualizados correctamente !');
        return redirect()->route('clientes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyCliente($id)
    {
        $cliente = Cliente::find($id);
        $cliente->delete();
        Session::flash('delete-success','cliente desactivado correctamente !');
        return redirect()->route('clientes.index');
    }

    public function activate($id){
        $cliente = Cliente::withTrashed()->find($id)->restore();
        Session::flash('store-success','cliente activado correctamente !');
        return redirect()->route('clientes.index');
    }
}
