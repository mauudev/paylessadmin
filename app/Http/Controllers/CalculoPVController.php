<?php

namespace PaylessAdmin\Http\Controllers;

use Illuminate\Http\Request;

use PaylessAdmin\Http\Requests;
use PaylessAdmin\Cliente;
use PaylessAdmin\Usuario;
use PaylessAdmin\Proveedor;
use PaylessAdmin\Repuesto;
use PaylessAdmin\PrecioVentaRepuesto;
use PaylessAdmin\CotizacionRepuesto;
use PaylessAdmin\CotizacionMercaderia;
use PaylessAdmin\ProveedoresCotizacionRepuesto;
use PaylessAdmin\ProveedoresCotizacionMercaderia;
use PaylessAdmin\PrecioTransporteRepuestos;
use PaylessAdmin\PrecioTransporteMercaderias;
use PaylessAdmin\Venta;
use PaylessAdmin\PreciosTransporte;
use DB;
use Session;
use Validator;
class CalculoPVController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function calculoRepuesto($repuestos_id,$ventas_id)
    {
        //dd($request->all());
        $cotizacion_repuestos_id = DB::table('cotizacion_repuestos')->where('ventas_id','=',$ventas_id)->where('repuestos_id','=',$repuestos_id)->first();
        $proveedores_cotizacion_rep = ProveedoresCotizacionRepuesto::getProveedores_cotizacion($repuestos_id,$ventas_id);

        $venta = Venta::find($ventas_id);
        $proveedores = Proveedor::lists('nombre_compania_p','id');
        $cliente = DB::table('clientes')->where('id','=',$venta->clientes_id)->first();
        $repuesto = Repuesto::find($repuestos_id);
        $precios_transporte = PreciosTransporte::getPrecios_transporte();

        return view('calculopv.index',['proveedores_cotizacion_rep'=>$proveedores_cotizacion_rep,
                                       'venta'=>$venta, 'repuesto'=>$repuesto,'cliente'=>$cliente,
                                       'proveedores'=>$proveedores,
                                       'cotizacion_repuestos_id'=>$cotizacion_repuestos_id,
                                       'precios_transporte'=>$precios_transporte]); 
    }

    public function calcular(Request $request){
        $messages = [
                'transporte_rep.regex'=>'El precio del transporte ingresado es inválido !',
                'adicional_rep.regex'=>'El precio adicional ingresado es inválido !'
        ];
        $validator = Validator::make($request->all(), [
                'transporte_rep'=>'required|regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u',
                'adicional_rep'=>'regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u'
        ],$messages);
        if ($validator->fails()) {
          $id1 = $request->repuestos_id;
          $id2 = $request->ventas_id;
          return redirect('/calcular-precio-venta/repuesto/'.$id1.'/venta/'.$id2)
                      ->withErrors($validator);
        }
        $repuesto = Repuesto::find($request->repuestos_id);
        $cliente = Cliente::find($request->clientes_id);
        if($cliente == null) $cliente = Cliente::onlyTrashed()->where('id','=',$request->clientes_id)->first();
        $ventas_id = $request->ventas_id;
        $repuestos_id = $request->repuestos_id;
        $proveedores_elegidos = $request->proveedores_all;
        $cotizacion_repuestos_id = DB::table('cotizacion_repuestos')->where('ventas_id','=',$ventas_id)->where('repuestos_id','=',$repuestos_id)->first();
        $proveedores_nombre_compania = array();//nombres ordenados con sus precios
        $proveedores_persona_contacto = array();//contactos ordenados con sus precios
        for($i = 0; $i < count($proveedores_elegidos); $i ++){
            $datos_proveedor = DB::table('proveedores')->select('nombre_compania_p','persona_contacto_p')->where('id','=',$proveedores_elegidos[$i])->first();
            $proveedores_nombre_compania[$i] = $datos_proveedor->nombre_compania_p;
            $proveedores_persona_contacto[$i] = $datos_proveedor->persona_contacto_p;
        }
        $cantidad_rep = $request->cantidad_rep;
        $precios_compra = $request->precios_cot_rep;

        if(isset($request->adicional_rep) && isset($request->transporte_rep)){
            return view('calculopv.create',['repuesto'=>$repuesto,'cliente'=>$cliente,
                                            'proveedores_elegidos'=>$proveedores_elegidos,
                                            'proveedores_nombre_compania'=>$proveedores_nombre_compania,
                                            'precios_compra'=>$precios_compra,
                                            'adicionales'=>$request->adicional_rep,
                                            'proveedores_persona_contacto'=>$proveedores_persona_contacto,
                                            'transportes'=>$request->transporte_rep,
                                            'ventas_id'=>$ventas_id,'repuestos_id'=>$repuestos_id,
                                            'cotizacion_repuestos_id'=>$cotizacion_repuestos_id,
                                            'cantidad_rep'=>$cantidad_rep]);
        }
        return view('calculopv.create',['repuesto'=>$repuesto,'cliente'=>$cliente,
                                        'proveedores_elegidos'=>$proveedores_elegidos,
                                        'precios_compra'=>$precios_compra,
                                        'cotizacion_repuestos_id'=>$cotizacion_repuestos_id]);
    }
    public function array_equal($a, $b) {
    return (
         is_array($a) && is_array($b) && 
         count($a) == count($b) &&
         array_diff($a, $b) === array_diff($b, $a)
        );
    }
    public function getProveedor_barato($array){
      $min = 9999999999999;
      $datos_mayor = array();
      foreach ($array as $key => $value) {
          if($value['total'] < $min){
            $min = $value['total'];
            $datos_mayor = $array[$key];
          }
      }
      return $datos_mayor;
    }
    public function store(Request $request){//ACA GUARDAR EL PRECIO DE TRASNPORTE
        //dd($request->all());
        if(isset($request->proveedores_all) && isset($request->transporte_rep) && isset($request->adicional_rep)){
          $cotizacion_repuestos = DB::table('cotizacion_repuestos')->where('ventas_id','=',$request->ventas_id)->where('repuestos_id','=',$request->repuestos_id)->first();
          $datos_cotizacion = array();
          $total = -1;
          for($i = 0; $i < count($request->precios_cot_rep); $i++){
            $total = (15/100)*$request->precios_cot_rep[$i] 
                   + (30/100)*$request->precios_cot_rep[$i] 
                   + (40/100)*$request->precios_cot_rep[$i] 
                   + $request->transporte_rep + $request->adicional_rep + $request->precios_cot_rep[$i];
            $total *= $cotizacion_repuestos->cantidad;
            $datos_cotizacion[$i] = [
                'proveedor_id'=>$request->proveedores_all[$i],
                'precios_compra'=>$request->precios_cot_rep[$i],
                'transporte'=>$request->transporte_rep,
                'adicionales'=>$request->adicional_rep,
                'total'=>$total
            ];
          }
          $resultado = $this->getProveedor_barato($datos_cotizacion);
          //dd($resultado);
          $proveedor_barato_id = $resultado['proveedor_id'];
          $msg = $this->eliminarProveedoresExcluyendo($proveedor_barato_id,$request->ventas_id,$request->repuestos_id);

          if($msg == 'sin-proveedores')
            return redirect()->route('principal-cot-show.showCotizacion',[$request->ventas_id]);
          else if($msg == 'un-proveedor'){
                  $proveedor_cotizacion_rep = ProveedoresCotizacionRepuesto::where('ventas_id','=',$request->ventas_id)->where('repuestos_id','=',$request->repuestos_id)->where('proveedores_id','=',$resultado['proveedor_id'])->first();
                  $precio_venta_rep = PrecioVentaRepuesto::where('cotizacion_repuestos_id','=',$cotizacion_repuestos->id)->where('proveedores_cotizacion_rep_id','=',$proveedor_cotizacion_rep->id)->where('proveedores_id','=',$resultado['proveedor_id'])->count();
                  if($precio_venta_rep > 0) return redirect()->route('principal-cot-show.showCotizacion',[$request->ventas_id]);
                  else{
                    $cotizacion_repuesto = CotizacionRepuesto::find($cotizacion_repuestos->id);
                    $precio_venta_rep = new PrecioVentaRepuesto();
                    $precio_venta_rep->transporte = $resultado['transporte'];
                    $precio_venta_rep->adicional = $resultado['adicionales'];
                    $precio_venta_rep->precio_total = $resultado['total'];
                    $precio_venta_rep->cotizacion_repuestos_id = $cotizacion_repuesto->id;
                    $precio_venta_rep->proveedores_cotizacion_rep_id = $proveedor_cotizacion_rep->id;  
                    $precio_venta_rep->proveedores_id = $resultado['proveedor_id'];
                    $precio_venta_rep->save();
                    $repuesto = Repuesto::find($request->repuestos_id);
                    if($resultado['precios_compra'] == null) $repuesto->precio_venta_r = 0;
                    else $repuesto->precio_venta_r = $resultado['precios_compra'];
                    $repuesto->save();
                    $cotizacion_repuesto->estado = 1;
                    $cotizacion_repuesto->save();
                    $venta = Venta::find($request->ventas_id);
                    if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$request->ventas_id)->first();
                    $venta->pago_total += $resultado['total'];
                    //$venta->pago_total_bs = round(($venta->pago_total * $request->tipo_cambio),2);
                    $venta->save();
                    Session::flash("store-success",'se han guardado los registros correctamente !');
                  }
                }else if($msg == "eliminados"){
                        $cotizacion_repuesto = CotizacionRepuesto::find($cotizacion_repuestos->id);
                        $proveedor_cotizacion_rep = ProveedoresCotizacionRepuesto::where('ventas_id','=',$request->ventas_id)->where('repuestos_id','=',$request->repuestos_id)->where('proveedores_id','=',$resultado['proveedor_id'])->first();
                        $precio_venta_rep = new PrecioVentaRepuesto();
                        $precio_venta_rep->transporte = $resultado['transporte'];
                        $precio_venta_rep->adicional = $resultado['adicionales'];
                        $precio_venta_rep->precio_total = $resultado['total'];
                        $precio_venta_rep->cotizacion_repuestos_id = $cotizacion_repuesto->id;
                        $precio_venta_rep->proveedores_cotizacion_rep_id = $proveedor_cotizacion_rep->id;  
                        $precio_venta_rep->proveedores_id = $resultado['proveedor_id'];
                        $precio_venta_rep->save();
                        $repuesto = Repuesto::find($request->repuestos_id);
                        if($resultado['precios_compra'] == null) $repuesto->precio_venta_r = 0;
                        else $repuesto->precio_venta_r = $resultado['precios_compra'];
                        $repuesto->save();
                        $cotizacion_repuesto->estado = 1;
                        $cotizacion_repuesto->save();
                        $venta = Venta::find($request->ventas_id);
                        if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$request->ventas_id)->first();
                        $venta->pago_total += $resultado['total'];
                        $venta->save();
                        Session::flash("store-success",'se han guardado los registros correctamente !');
                }
                return redirect()->route('principal-cot-show.showCotizacion',[$request->ventas_id]);
        }else return "<b>ERROR!</b> Ocurri&oacute; un error al enviar los datos del formulario !";
    }

    public static function  eliminarProveedoresExcluyendo($proveedor,$ventas_id,$repuestos_id){
        $msg = 'no-eliminados';
        $proveedores_cotizacion_rep = ProveedoresCotizacionRepuesto::where('ventas_id','=',$ventas_id)
                                                                      ->where('repuestos_id','=',$repuestos_id);
        switch($proveedores_cotizacion_rep->count()){
          case 0:
            $msg = "sin-proveedores";
          break;
          case 1:
            $msg = "un-proveedor";
          break;
          case $proveedores_cotizacion_rep->count() > 1:
            $proveedores_cotizacion_rep = ProveedoresCotizacionRepuesto::where('ventas_id','=',$ventas_id)
                                                                      ->where('repuestos_id','=',$repuestos_id)
                                                                      ->where('proveedores_id','<>',$proveedor);
            $proveedores_cotizacion_rep->delete();
            $msg = 'eliminados';
          break;
        }
        return $msg;
        }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_cotizacion)
    {
        $precio_venta_rep = PrecioVentaRepuesto::getPrecioVentaRep($id_cotizacion);
        if(count($precio_venta_rep) > 0){
            $cotizacion_repuesto = CotizacionRepuesto::find($precio_venta_rep[0]->cotizacion_repuestos_id);
            $repuesto = Repuesto::find($cotizacion_repuesto->repuestos_id);
            $venta = Venta::find($cotizacion_repuesto->ventas_id);
            $cliente = Cliente::find($venta->clientes_id);
            $proveedores = array();
            for($i = 0; $i < count($precio_venta_rep); $i++)
                $proveedores[$i] = Proveedor::find($precio_venta_rep[$i]->proveedores_id);
            $datos_venta = PrecioVentaRepuesto::getPrecioVentaDetalle($id_cotizacion);
        }else{return "<b>ERROR!</b> No se ha enviado el precio de venta";}
        //return $datos_venta;
        if(isset($cotizacion_repuesto) && isset($repuesto) && isset($venta) && isset($cliente) && isset($proveedores) && isset($datos_venta)){
            return view('calculopv.show',['precio_venta_rep'=>$precio_venta_rep, 'cotizacion_repuesto'=>$cotizacion_repuesto,
                                          'repuesto'=>$repuesto,'venta'=>$venta,'cliente'=>$cliente,'proveedores'=>$proveedores,
                                          'datos_venta'=>$datos_venta]);
        }
    }

    public function confirmacionProveedor($cotizacion_repuestos_id,$ventas_id){
      //return $cotizacion_repuestos_id;
      $venta = Venta::find($ventas_id);
      if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$ventas_id)->first();
      $cliente = Cliente::find($venta->clientes_id);
      if($cliente == null) $cliente = Cliente::onlyTrashed()->where('id','=',$venta->clientes_id)->first();
      $cotizacion_repuesto = CotizacionRepuesto::find($cotizacion_repuestos_id);
      $repuesto = Repuesto::find($cotizacion_repuesto->repuestos_id);
      $proveedores_cotizacion_rep = ProveedoresCotizacionRepuesto::where('ventas_id','=',$ventas_id)
                                                                  ->where('repuestos_id','=',$repuesto->id)
                                                                  ->get();
      $proveedores_id = array();
      for($i = 0; $i < count($proveedores_cotizacion_rep); $i ++){
        $proveedores_id[$i] = $proveedores_cotizacion_rep[$i]->proveedores_id;
      }
      $proveedores = Proveedor::findMany($proveedores_id);
      $precio_venta_rep = array();
      for($i = 0; $i < count($proveedores_cotizacion_rep); $i++){
        $precio_venta_collection = PrecioVentaRepuesto::where('cotizacion_repuestos_id','=',$cotizacion_repuesto->id)
                                                      ->where('proveedores_cotizacion_rep_id','=',$proveedores_cotizacion_rep[$i]->id)
                                                      ->first();
        $precio_venta_rep[$i] = ['transporte'=>$precio_venta_collection->transporte, 
                                 'adicional'=>$precio_venta_collection->adicional];
      }
      return view('calculopv.elegir-proveedor',['venta'=>$venta,'cotizacion_repuesto'=>$cotizacion_repuesto,
                                                'proveedores_cotizacion_rep'=>$proveedores_cotizacion_rep,
                                                'repuesto'=>$repuesto,
                                                'cliente'=>$cliente,'proveedores'=>$proveedores,
                                                'precio_venta_rep'=>$precio_venta_rep]);
    }

    public function confirmarProveedor(Request $request){
      //dd($request->all());
      $proveedor_cotizacion_mer = ProveedoresCotizacionRepuesto::where('ventas_id','=',$request->ventas_id)
                                                                 ->where('repuestos_id','=',$request->repuestos_id)
                                                                 ->where('proveedores_id','=',$request->proveedores_id[0])
                                                                 ->first();
      $precio_venta_rep = PrecioVentaRepuesto::where('cotizacion_repuestos_id','=',$request->cotizacion_repuestos_id)
                                               ->where('proveedores_cotizacion_rep_id','<>',$proveedor_cotizacion_mer->id)
                                               ->where('proveedores_id','<>',$request->proveedores_id[0])
                                               ->get();
      $proveedor_cotizacion_mer_target = ProveedoresCotizacionRepuesto::where('ventas_id','=',$request->ventas_id)
                                                                 ->where('repuestos_id','=',$request->repuestos_id)
                                                                 ->where('proveedores_id','<>',$request->proveedores_id[0])
                                                                 ->get();
      
      $confirmation = 'pass';
      for($i = 0; $i < count($precio_venta_rep); $i ++){
        $precio_venta_rep[$i]->delete();
        if(!$precio_venta_rep) $confirmation = 'fail';
      }
        
      for($i = 0; $i < count($proveedor_cotizacion_mer_target); $i ++){
        $proveedor_cotizacion_mer_target[$i]->delete();
        if(!$proveedor_cotizacion_mer_target) $confirmation = 'fail';
      }

      if($confirmation == 'pass'){
        Session::flash('update-success','se ha actualizado la cotizaci&oacute;n correctamente !');
        return redirect()->route('principal-cot-show.showCotizacion',[$request->ventas_id]);
      }else{
        Session::flash('message-error','ha ocurrido un error al actualizar los datos !');
        return redirect()->route('principal-cot-show.showCotizacion',[$request->ventas_id]);
      } 
      //$proveedores_eliminar = ProveedoresCotizacionRepuesto::where('ventas_id','=',$)
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cotizacion_repuesto = CotizacionRepuesto::find($id);
        $proveedores_cotizacion_rep = ProveedoresCotizacionRepuesto::getProveedores_cotizacion_all($cotizacion_repuesto->id,$cotizacion_repuesto->ventas_id);
        $venta = Venta::find($cotizacion_repuesto->ventas_id);
        if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$cotizacion_repuesto->ventas_id)->first();
        $cliente = Cliente::find($venta->clientes_id);
        if($cliente == null) $cliente = Cliente::onlyTrashed()->where('id','=',$venta->clientes_id)->first();
        $accion = "nada";
        if($venta->estado == 1) {
          $accion = "edit-finished";
          $venta->estado = 0;
        }
        $venta->save();
        $proveedores = Proveedor::lists('nombre_compania_p','id');
        $datos_venta = PrecioVentaRepuesto::getPrecioVentaDetalle($cotizacion_repuesto->id);
        $repuesto = Repuesto::find($cotizacion_repuesto->repuestos_id);
        $precios_transporte = PreciosTransporte::getPrecios_transporte();
        return view('calculopv.edit',['proveedores_cotizacion_rep'=>$proveedores_cotizacion_rep,
                                      'venta'=>$venta, 'repuesto'=>$repuesto,'cliente'=>$cliente,
                                      'proveedores'=>$proveedores,'cotizacion'=>$cotizacion_repuesto,
                                      'precios_transporte'=>$precios_transporte,
                                      'datos_venta'=>$datos_venta[0],'accion'=>$accion]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $messages = [
                'transporte_rep.between'=>'El precio del transporte ingresado es inválido !',
                'adicional_rep.between'=>'El precio adicional ingresado es inválido !'
        ];
        $validator = Validator::make($request->all(), [
                'transporte_rep'=>'required|between:0,99.99',
                'adicional_rep'=>'required|between:0,99.99'
        ],$messages);
        if ($validator->fails()) {
          $id1 = $request->cotizacion_id;
          return redirect('calcular-precio-venta/'.$id1.'/edit')
                      ->withErrors($validator);
        }
        $cotizacion_repuesto = CotizacionRepuesto::find($request->cotizacion_id);
        $cotizacion_repuesto->cantidad = $request->cantidad_rep;//aplica a ambos casos
        $cotizacion_repuesto->save();
        $proveedores_cotizacion_rep = ProveedoresCotizacionRepuesto::getProveedores_cotizacion($request->repuestos_id,$request->ventas_id);
        $confirmacion = '';
        if(count($request->precio_venta_rep_id) > 0){
          for($i = 0; $i < count($request->precio_venta_rep_id); $i++){
            $precio_venta = PrecioVentaRepuesto::find($request->precio_venta_rep_id[$i]);
            $precio_venta->transporte = $request->transporte_rep;
            $precio_venta->adicional = $request->adicional_rep;
            $total = $request->precio_cot_rep[$i] + (15/100)*$request->precio_cot_rep[$i] + (30/100)*$request->precio_cot_rep[$i] + (40/100)*$request->precio_cot_rep[$i];
            $total = $total + $request->transporte_rep + $request->adicional_rep;
            $precio_venta->precio_total = $total * $cotizacion_repuesto->cantidad;
            $precio_venta->save();
            $confirmacion = "toast-actualizado";
          }
        }else{return "<b>ERROR!</b> Ha ocurrido un error al recuperar los precios de venta de esta mercader&iacute;a!";}
        Session::flash($confirmacion,'se han actualizado los datos correctamente !');
        return redirect()->route('principal-cot-show.showCotizacion',[$request->ventas_id]);
    }   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
        
}
