<?php

namespace PaylessAdmin\Http\Controllers;

use Illuminate\Http\Request;
use PaylessAdmin\Proveedor;
use PaylessAdmin\Http\Requests;
use PaylessAdmin\Http\Requests\ProveedoresValidationRequest;
use Session;
use PaylessAdmin\TipoProveedor;
use PaylessAdmin\MarcasProveedor;
use DB;
use Validator;
use Redirect;
class ProveedoresController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if("" != $request->get('search')){
            $proveedores = Proveedor::search($request->get('search'))->paginate(10);
        }else
        $proveedores = Proveedor::getProveedores();
        //dd($proveedores->all());
        return view('proveedores.index',['proveedores'=>$proveedores]);
    }
    public function getProveedores(Request $request, $id){
      if($request->ajax()){
        $proveedores = Proveedor::getProveedores_with($id);
        return response()->json($proveedores);
      }
    }
    public function busquedaProveedores($search){
        $proveedores = Proveedor::searchProveedores($search);
        return response()->json(['response' => $proveedores]); 
    }
    public function filtro($filtro){
        $proveedores = Proveedor::filtro($filtro);
        return $proveedores;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipos_proveedor = TipoProveedor::lists('tipo','id');
        $vista = "create";
        return view('proveedores.create',['tipos_proveedor'=>$tipos_proveedor,'vista'=>$vista]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $messages = [
                'persona_contacto_p.regex'=>'El campo nombre no puede contener números o símbolos !',
                'email_p.email'=>'El email ingresado no es válido !',
                'marca_prov.*.alpha_num'=>'El campo marca no puede contener símbolos o caracteres especiales !'
        ];
        $validator = Validator::make($request->all(), [
                'persona_contacto_p'=> 'required|regex:/^[\pL\s\-]+$/u',
                'email_p'=>'email',
                'marca_prov.*'=>'required|alpha_num'
        ],$messages);
        if ($validator->fails()) {
          return redirect('proveedores/create')
                      ->withErrors($validator)
                      ->withInput($request->except('marca_prov'));
        }
        Proveedor::create($request->all());
        // ULTIMO REGISTRO Esto me regresa el campo como un arreglo y puedo accederlo como un string ->first()
        $tupla_proveedor = DB::table('proveedores')->select('id')
                                          ->orderBy('created_at', 'desc')
                                          ->take('1')
                                          ->first();
        // OK
        for($i = 0; $i < count($request->marca_prov); $i++){
            $marca = new MarcasProveedor();
            $marca->marca_prov = $request->marca_prov[$i];
            $marca->proveedores_id = $tupla_proveedor->id;
            $marca->save();
        }
        Session::flash("store-success","datos registrados correctamente !");
        return redirect()->route('proveedores.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    public function getProveedor(Request $request,$id)
    {
        if ($request->isMethod('post')){    
            return response()->json(['response' => 'This is post method']); 
        }
        $proveedor = Proveedor::getProveedor($id);
        return response()->json(['response' => $proveedor]);
    }
    public function getTipoProveedor(Request $request,$id)
    {
        if ($request->isMethod('post')){    
            return response()->json(['response' => 'This is post method']); 
        }
        $tipo_proveedor = TipoProveedor::getTipoProveedor($id);
        return response()->json(['response' => $tipo_proveedor]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $proveedor = Proveedor::find($id);
        if($proveedor == null) $proveedor = Proveedor::onlyTrashed()->where('id','=',$id)->first();
        $marcas_prov = MarcasProveedor::getMarcasProveedor($id);
        $tipos_proveedor = TipoProveedor::lists('tipo','id');
        $vista = "edit";
        $count = count($marcas_prov);
        return view('proveedores.edit',['proveedor'=>$proveedor,
                                        'tipos_proveedor'=>$tipos_proveedor,
                                        'marcas_prov'=>$marcas_prov,
                                        'vista'=>$vista,
                                        'count'=>$count]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $messages = [
                'persona_contacto_p.regex'=>'El campo nombre no puede contener números o símbolos !',
                'email_p.email'=>'El email ingresado no es válido !',
                'marca_prov.*.regex'=>'El campo marca no puede contener símbolos o caracteres especiales !'
        ];
        $validator = Validator::make($request->all(), [
                'persona_contacto_p'=> 'required|regex:/^[\pL\s\-]+$/u',
                'email_p'=>'email',
                'marca_prov.*'=>'required|regex:/^[\pL\s\-]+$/u'
        ],$messages);
        if ($validator->fails()) {
          return redirect()->route('proveedores.edit',[$id])
                      ->withErrors($validator)
                      ->withInput([$request->except('marca_prov_old'),$request->except('marca_prov_id')]);
        }
        //return "mierda";
        $proveedor = Proveedor::find($id);
        if($proveedor == null) $proveedor = Proveedor::onlyTrashed()->where('id','=',$id)->first();
        //$proveedor->nombre_compania_p = $request->nombre_compania_p;
        $proveedor->direccion_p = $request->direccion_p;
        $proveedor->telefono_p = $request->telefono_p;
        $proveedor->celular_p = $request->celular_p;
        $proveedor->email_p = $request->email_p;
        $proveedor->persona_contacto_p = $request->persona_contacto_p;
        $proveedor->tipo_proveedores_id = $request->tipo_proveedores_id;
        $proveedor->save();

        if(isset($request->marcas_prov_id)){
            for($i = 0; $i < count($request->marcas_prov_id); $i++){
                $marca = MarcasProveedor::find($request->marcas_prov_id[$i]);
                $marca->marca_prov = $request->marca_prov_old[$i];
                $marca->proveedores_id = $id;
                $marca->save();
            }
        }
        if(isset($request->marca_prov)){
            for($i = 0; $i < count($request->marca_prov); $i++){
                $marca = new MarcasProveedor();
                $marca->marca_prov = $request->marca_prov[$i];
                $marca->proveedores_id = $id;
                $marca->save();
            }
        }
        Session::flash('update-success','Datos actualizados correctamente !');
        return redirect()->route('proveedores.index');
    }   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function activate($id){
        $proveedor = Proveedor::withTrashed()->find($id)->restore();
        Session::flash('store-success','proveedor activado correctamente !');
        return redirect()->route('proveedores.index');
    }
    public function destroy($id)
    {
        $proveedor = Proveedor::find($id);
        $proveedor->delete();
        Session::flash('delete-success',"proveedor desactivado correctamente !");
        return redirect()->route('proveedores.index');
    }
}
