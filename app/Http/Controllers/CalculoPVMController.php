<?php

namespace PaylessAdmin\Http\Controllers;

use Illuminate\Http\Request;

use PaylessAdmin\Http\Requests;
use PaylessAdmin\Cliente;
use PaylessAdmin\Usuario;
use PaylessAdmin\Proveedor;
use PaylessAdmin\Mercaderia;
use PaylessAdmin\CotizacionMercaderia;
use PaylessAdmin\PrecioVentaMercaderia;
use PaylessAdmin\ProveedoresCotizacionMercaderia;
use PaylessAdmin\PreciosTransporte;
use PaylessAdmin\Venta;
use DB;
use Session;
use Validator;
class CalculoPVMController extends Controller
{
    public function calculoMercaderia($mercaderias_id,$ventas_id)
    {   
        $cotizacion_mercaderias_id = DB::table('cotizacion_mercaderias')->where('ventas_id','=',$ventas_id)->where('mercaderias_id','=',$mercaderias_id)->first();
        $proveedores_cotizacion_mer = ProveedoresCotizacionMercaderia::getProveedores_cotizacion($mercaderias_id,$ventas_id);
        $venta = Venta::find($ventas_id);
        if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$ventas_id)->first();
        $cliente = Cliente::find($venta->clientes_id);
        if($cliente == null) $cliente = Cliente::onlyTrashed()->where('id','=',$venta->clientes_id)->first();
        $precios_transporte = PreciosTransporte::getPrecios_transporte();
        $mercaderia = Mercaderia::find($mercaderias_id);
        return view('calculopvm.index',['proveedores_cotizacion_mer'=>$proveedores_cotizacion_mer,
                                        'venta'=>$venta, 'mercaderia'=>$mercaderia,'cliente'=>$cliente,
                                        'precios_transporte'=>$precios_transporte,
                                        'cotizacion_mercaderias_id'=>$cotizacion_mercaderias_id]); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function calcular(Request $request){
        //dd($request->all());
        $messages = [
                'transporte_mer.regex'=>'El precio del transporte ingresado es inválido !',
                'adicional_mer.regex'=>'El precio adicional ingresado es inválido !'
        ];
        $validator = Validator::make($request->all(), [
                'transporte_mer'=>'required|regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u',
                'adicional_mer'=>'regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u'
        ],$messages);
        if ($validator->fails()) {
          $id1 = $request->mercaderias_id;
          $id2 = $request->ventas_id;
          return redirect('/calcular-precio-venta-mer/mercaderia/'.$id1.'/venta/'.$id2)
                      ->withErrors($validator);
        }
        $mercaderia = Mercaderia::find($request->mercaderias_id);
        $cliente = Cliente::find($request->clientes_id);
        $ventas_id = $request->ventas_id;
        $mercaderias_id = $request->mercaderias_id;
        $proveedores_elegidos = $request->proveedores_all;
        $cotizacion_mercaderias_id = DB::table('cotizacion_mercaderias')->where('ventas_id','=',$ventas_id)->where('mercaderias_id','=',$mercaderias_id)->first();
        $proveedores_nombre_compania = array();//nombres ordenados con sus precios
        $proveedores_persona_contacto = array();//contactos ordenados con sus precios
        for($i = 0; $i < count($proveedores_elegidos); $i ++){
            $datos_proveedor = DB::table('proveedores')->select('nombre_compania_p','persona_contacto_p')->where('id','=',$proveedores_elegidos[$i])->first();
            $proveedores_nombre_compania[$i] = $datos_proveedor->nombre_compania_p;
            $proveedores_persona_contacto[$i] = $datos_proveedor->persona_contacto_p;
        }
        $cantidad_mer = $request->cantidad_mer;
        $precios_compra = $request->precios;
        if(isset($request->adicional_mer) && isset($request->transporte_mer)){
            return view('calculopvm.create',['mercaderia'=>$mercaderia,'cliente'=>$cliente,
                                            'proveedores_elegidos'=>$proveedores_elegidos,
                                            'proveedores_nombre_compania'=>$proveedores_nombre_compania,
                                            'precios_compra'=>$precios_compra,
                                            'adicionales'=>$request->adicional_mer,
                                            'proveedores_persona_contacto'=>$proveedores_persona_contacto,
                                            'transportes'=>$request->transporte_mer,
                                            'ventas_id'=>$ventas_id,'mercaderias_id'=>$mercaderias_id,
                                            'cotizacion_mercaderias_id'=>$cotizacion_mercaderias_id,
                                            'cantidad_mer'=>$cantidad_mer]);
        }
        return view('calculopvm.create',['mercaderia'=>$mercaderia,'cliente'=>$cliente,
                                        'proveedores_elegidos'=>$proveedores_elegidos,
                                        'precios_compra'=>$precios_compra,
                                        'cotizacion_mercaderias_id'=>$cotizacion_mercaderias_id]);
    }

    public function array_equal($a, $b) {
        return (
             is_array($a) && is_array($b) && 
             count($a) == count($b) &&
             array_diff($a, $b) === array_diff($b, $a)
            );
    }
    public function getProveedor_barato($array){
      $min = 9999999999999;
      $datos_mayor = array();
      foreach ($array as $key => $value) {
          if($value['total'] < $min){
            $min = $value['total'];
            $datos_mayor = $array[$key];
          }
      }
      return $datos_mayor;
    }
    public function store(Request $request){
        //dd($request->all()); 
        if(isset($request->proveedores_all) && isset($request->transporte_mer) && isset($request->adicional_mer)){
            $cotizacion_mercaderias = DB::table('cotizacion_mercaderias')->where('ventas_id','=',$request->ventas_id)->where('mercaderias_id','=',$request->mercaderias_id)->first();
            $datos_cotizacion = array();
            $total = -1;
            for($i = 0; $i < count($request->precios); $i++){
              $total = (15/100)*$request->precios[$i] 
                     + (30/100)*$request->precios[$i] 
                     + (40/100)*$request->precios[$i] 
                     + $request->transporte_mer + $request->adicional_mer + $request->precios[$i];
              $total *= $cotizacion_mercaderias->cantidad;
              $datos_cotizacion[$i] = [
                  'proveedor_id'=>$request->proveedores_all[$i],
                  'precios_compra'=>$request->precios[$i],
                  'transporte'=>$request->transporte_mer,
                  'adicionales'=>$request->adicional_mer,
                  'total'=>$total
              ];
            }
            $resultado = $this->getProveedor_barato($datos_cotizacion);
            //dd($resultado);
            $proveedor_barato_id = $resultado['proveedor_id'];
            $msg = $this->eliminarProveedoresExcluyendo($proveedor_barato_id,$request->ventas_id,$request->mercaderias_id);
            if($msg == 'sin-proveedores')
              return redirect()->route('principal-cot-show.showCotizacion',[$request->ventas_id]);
            else if($msg == 'un-proveedor'){
                    $proveedor_cotizacion_mer = ProveedoresCotizacionMercaderia::where('ventas_id','=',$request->ventas_id)->where('mercaderias_id','=',$request->mercaderias_id)->where('proveedores_id','=',$resultado['proveedor_id'])->first();
                    $precio_venta_mer = PrecioVentaMercaderia::where('cotizacion_mercaderias_id','=',$cotizacion_mercaderias->id)->where('proveedores_cotizacion_mer_id','=',$proveedor_cotizacion_mer->id)->where('proveedores_id','=',$resultado['proveedor_id'])->count();
                    if($precio_venta_mer > 0) return redirect()->route('principal-cot-show.showCotizacion',[$request->ventas_id]);
                    else{
                      $cotizacion_mercaderia = CotizacionMercaderia::find($cotizacion_mercaderias->id);
                      $precio_venta_mer = new PrecioVentaMercaderia();
                      $precio_venta_mer->transporte = $resultado['transporte'];
                      $precio_venta_mer->adicional = $resultado['adicionales'];
                      $precio_venta_mer->precio_total = $resultado['total'];
                      $precio_venta_mer->cotizacion_mercaderias_id = $cotizacion_mercaderia->id;
                      $precio_venta_mer->proveedores_cotizacion_mer_id = $proveedor_cotizacion_mer->id;
                      $precio_venta_mer->proveedores_id = $resultado['proveedor_id'];
                      $precio_venta_mer->save();
                      $mercaderia = Mercaderia::find($request->mercaderias_id);
                      if($resultado['precios_compra'] == null) $mercaderia->precio_venta_m = 0;
                      else $mercaderia->precio_venta_m = $resultado['precios_compra'];
                      $mercaderia->save();
                      $cotizacion_mercaderia->estado = 1;
                      $cotizacion_mercaderia->save();
                      $venta = Venta::find($request->ventas_id);
                      if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$request->ventas_id)->first();
                      $venta->pago_total += $resultado['total'];
                      //$venta->pago_total_bs = round(($venta->pago_total * $request->tipo_cambio),2);
                      $venta->save();
                      Session::flash("store-success",'se han guardado los registros correctamente !');
                    }
                  }else if($msg == "eliminados"){
                          $cotizacion_mercaderia = CotizacionMercaderia::find($cotizacion_mercaderias->id);
                          $proveedor_cotizacion_mer = ProveedoresCotizacionMercaderia::where('ventas_id','=',$request->ventas_id)->where('mercaderias_id','=',$request->mercaderias_id)->where('proveedores_id','=',$resultado['proveedor_id'])->first();
                          $precio_venta_mer = new PrecioVentaMercaderia();
                          $precio_venta_mer->transporte = $resultado['transporte'];
                          $precio_venta_mer->adicional = $resultado['adicionales'];
                          $precio_venta_mer->precio_total = $resultado['total'];
                          $precio_venta_mer->cotizacion_mercaderias_id = $cotizacion_mercaderia->id;
                          $precio_venta_mer->proveedores_cotizacion_mer_id = $proveedor_cotizacion_mer->id;
                          $precio_venta_mer->proveedores_id = $resultado['proveedor_id'];
                          $precio_venta_mer->save();
                          $mercaderia = Mercaderia::find($request->mercaderias_id);
                          if($resultado['precios_compra'] == null) $mercaderia->precio_venta_m = 0;
                          else $mercaderia->precio_venta_m = $resultado['precios_compra'];
                          $mercaderia->save();
                          $cotizacion_mercaderia->estado = 1;
                          $cotizacion_mercaderia->save();
                          $venta = Venta::find($request->ventas_id);
                          if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$request->ventas_id)->first();
                          $venta->pago_total += $resultado['total'];
                          $venta->save();
                          Session::flash("store-success",'se han guardado los registros correctamente !');
                  }
                  return redirect()->route('principal-cot-show.showCotizacion',[$request->ventas_id]);
          }else return "<b>ERROR!</b> Ocurri&oacute; un error al enviar los datos del formulario !";
    }
    public static function  eliminarProveedoresExcluyendo($proveedor,$ventas_id,$mercaderias_id){
        $msg = 'no-eliminados';
        $proveedores_cotizacion_mer = ProveedoresCotizacionMercaderia::where('ventas_id','=',$ventas_id)
                                                                      ->where('mercaderias_id','=',$mercaderias_id);
        switch($proveedores_cotizacion_mer->count()){
          case 0:
            $msg = "sin-proveedores";
          break;
          case 1:
            $msg = "un-proveedor";
          break;
          case $proveedores_cotizacion_mer->count() > 1:
            $proveedores_cotizacion_mer = ProveedoresCotizacionMercaderia::where('ventas_id','=',$ventas_id)
                                                                      ->where('mercaderias_id','=',$mercaderias_id)
                                                                      ->where('proveedores_id','<>',$proveedor);
            $proveedores_cotizacion_mer->delete();
            $msg = 'eliminados';
          break;
        }
        return $msg;
        }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id_cotizacion)
    {
        $precio_venta_mer = PrecioVentaMercaderia::getPrecioVentaMer($id_cotizacion);
        if(count($precio_venta_mer) > 0){
            $cotizacion_mercaderia = CotizacionMercaderia::find($precio_venta_mer[0]->cotizacion_mercaderias_id);
            $mercaderia = Mercaderia::find($cotizacion_mercaderia->mercaderias_id);
            $venta = Venta::find($cotizacion_mercaderia->ventas_id);
            if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$cotizacion_mercaderia->ventas_id)->first();
            $cliente = Cliente::find($venta->clientes_id);
            if($cliente == null) $cliente = Cliente::onlyTrashed()->where('id','=',$venta->clientes_id)->first();
            $proveedores = array();
            for($i = 0; $i < count($precio_venta_mer); $i++)
                $proveedores[$i] = Proveedor::find($precio_venta_mer[$i]->proveedores_id);
            $datos_venta = PrecioVentaMercaderia::getPrecioVentaDetalle($id_cotizacion);
        }else{return "<b>ERROR!</b> No se ha podido obtener el precio de venta de la mercaderia ";}
        //return $datos_venta;
        if(isset($cotizacion_mercaderia) && isset($mercaderia) && isset($venta) && isset($cliente) && isset($proveedores) && isset($datos_venta)){
            return view('calculopvm.show',['precio_venta_mer'=>$precio_venta_mer,
                                           'cotizacion_mercaderia'=>$cotizacion_mercaderia,
                                           'mercaderia'=>$mercaderia,'venta'=>$venta,'cliente'=>$cliente,
                                           'proveedores'=>$proveedores,'datos_venta'=>$datos_venta]);
        }
    }

    public function confirmacionProveedor($cotizacion_mercaderias_id,$ventas_id){
      //return $cotizacion_mercaderias_id;
      $venta = Venta::find($ventas_id);
      if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$cotizacion_mercaderia->ventas_id)->first();
      $cliente = Cliente::find($venta->clientes_id);
      if($cliente == null) $cliente = Cliente::onlyTrashed()->where('id','=',$venta->clientes_id)->first();
      $cotizacion_mercaderia = CotizacionMercaderia::find($cotizacion_mercaderias_id);
      $mercaderia = Mercaderia::find($cotizacion_mercaderia->mercaderias_id);
      $proveedores_cotizacion_mer = ProveedoresCotizacionMercaderia::where('ventas_id','=',$ventas_id)
                                                                    ->where('mercaderias_id','=',$mercaderia->id)
                                                                    ->get();
      $proveedores_id = array();
      for($i = 0; $i < count($proveedores_cotizacion_mer); $i ++){
        $proveedores_id[$i] = $proveedores_cotizacion_mer[$i]->proveedores_id;
      }
      $proveedores = Proveedor::findMany($proveedores_id);
      $precio_venta_mer = array();
      for($i = 0; $i < count($proveedores_cotizacion_mer); $i++){
        $precio_venta_collection = PrecioVentaMercaderia::where('cotizacion_mercaderias_id','=',$cotizacion_mercaderia->id)
                                                        ->where('proveedores_cotizacion_mer_id','=',$proveedores_cotizacion_mer[$i]->id)
                                                        ->first();
        //return $precio_venta_collection;
        $precio_venta_mer[$i] = ['transporte'=>$precio_venta_collection->transporte, 
                                 'adicional'=>$precio_venta_collection->adicional];
      }
      //dd($precio_venta_mer);
      return view('calculopvm.elegir-proveedor',['venta'=>$venta,'cotizacion_mercaderia'=>$cotizacion_mercaderia,
                                                 'proveedores_cotizacion_mer'=>$proveedores_cotizacion_mer,'mercaderia'=>$mercaderia,
                                                 'cliente'=>$cliente,'proveedores'=>$proveedores,'precio_venta_mer'=>$precio_venta_mer]);
      
    }

    public function confirmarProveedor(Request $request){
      //dd($request->all());
      $proveedor_cotizacion_mer = ProveedoresCotizacionMercaderia::where('ventas_id','=',$request->ventas_id)
                                                                 ->where('mercaderias_id','=',$request->mercaderias_id)
                                                                 ->where('proveedores_id','=',$request->proveedores_id[0])
                                                                 ->first();
      $precio_venta_mer = PrecioVentaMercaderia::where('cotizacion_mercaderias_id','=',$request->cotizacion_mercaderias_id)
                                               ->where('proveedores_cotizacion_mer_id','<>',$proveedor_cotizacion_mer->id)
                                               ->where('proveedores_id','<>',$request->proveedores_id[0])
                                               ->get();
      $proveedor_cotizacion_mer_target = ProveedoresCotizacionMercaderia::where('ventas_id','=',$request->ventas_id)
                                                                 ->where('mercaderias_id','=',$request->mercaderias_id)
                                                                 ->where('proveedores_id','<>',$request->proveedores_id[0])
                                                                 ->get();
      
      $confirmation = 'pass';
      for($i = 0; $i < count($precio_venta_mer); $i ++){
        $precio_venta_mer[$i]->delete();
        if(!$precio_venta_mer) $confirmation = 'fail';
      }
        
      for($i = 0; $i < count($proveedor_cotizacion_mer_target); $i ++){
        $proveedor_cotizacion_mer_target[$i]->delete();
        if(!$proveedor_cotizacion_mer_target) $confirmation = 'fail';
      }

      if($confirmation == 'pass'){
        Session::flash('update-success','se ha actualizado la cotizaci&oacute;n correctamente !');
        return redirect()->route('principal-cot-show.showCotizacion',[$request->ventas_id]);
      }else{
        Session::flash('message-error','ha ocurrido un error al actualizar los datos !');
        return redirect()->route('principal-cot-show.showCotizacion',[$request->ventas_id]);
      } 
      //$proveedores_eliminar = ProveedoresCotizacionMercaderia::where('ventas_id','=',$)
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cotizacion_mercaderia = CotizacionMercaderia::find($id);
        $proveedores_cotizacion_mer = ProveedoresCotizacionMercaderia::getProveedores_cotizacion_all($cotizacion_mercaderia->id);
        //return $proveedores_cotizacion_mer;
        $accion = "nada";
        $venta = Venta::find($cotizacion_mercaderia->ventas_id);
        if($venta->estado == 1) {
          $accion = "edit-finished";
          $venta->estado = 0;
        }
        $venta->save();
        $proveedores = Proveedor::lists('nombre_compania_p','id');
        $cliente = DB::table('clientes')->where('id','=',$venta->clientes_id)->first();
        $mercaderia = Mercaderia::find($cotizacion_mercaderia->mercaderias_id);
        $datos_venta = PrecioVentaMercaderia::getPrecioVentaDetalle($cotizacion_mercaderia->id);
        $precios_transporte = PreciosTransporte::getPrecios_transporte();
        //return $precios_transporte;

        return view('calculopvm.edit',['proveedores_cotizacion_mer'=>$proveedores_cotizacion_mer,
                                      'venta'=>$venta, 'mercaderia'=>$mercaderia,'cliente'=>$cliente,
                                      'proveedores'=>$proveedores,'cotizacion_mercaderia'=>$cotizacion_mercaderia,'datos_venta'=>$datos_venta[0],
                                      'precios_transporte'=>$precios_transporte,'accion'=>$accion]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $messages = [
                'transporte_mer.between'=>'El precio del transporte ingresado es inválido !',
                'adicional_mer.between'=>'El precio adicional ingresado es inválido !'
        ];
        $validator = Validator::make($request->all(), [
                'transporte_mer'=>'required|between:0,99.99',
                'adicional_mer'=>'required|between:0,99.99'
        ],$messages);
        if ($validator->fails()) {
          $id1 = $request->cotizacion_id;
          return redirect('calcular-precio-venta-mer/'.$id1.'/edit')
                      ->withErrors($validator);
        }
        $cotizacion_mercaderia = CotizacionMercaderia::find($request->cotizacion_id);
        $cotizacion_mercaderia->cantidad = $request->cantidad_mer;//aplica a ambos casos
        $cotizacion_mercaderia->save();
        $proveedores_cotizacion_mer = ProveedoresCotizacionMercaderia::getProveedores_cotizacion($request->mercaderias_id,$request->ventas_id);
        $confirmacion = '';
        if(count($request->precio_venta_mer_id) > 0){
          for($i = 0; $i < count($request->precio_venta_mer_id); $i++){
            $precio_venta = PrecioVentaMercaderia::find($request->precio_venta_mer_id[$i]);
            $precio_venta->transporte = $request->transporte_mer;
            $precio_venta->adicional = $request->adicional_mer;
            $total = $request->precio_cot_mer[$i] + (15/100)*$request->precio_cot_mer[$i] + (30/100)*$request->precio_cot_mer[$i] + (40/100)*$request->precio_cot_mer[$i];
            $total = $total + $request->transporte_mer + $request->adicional_mer;
            $precio_venta->precio_total = $total * $cotizacion_mercaderia->cantidad;
            //return $precio_venta->precio_total;
            $precio_venta->save(); 
            $confirmacion = "toast-actualizado";
          }
        }else{
          return "<b>ERROR!</b> Ha ocurrido un error al recuperar los precios de venta de esta mercader&iacute;a!";
        }
        Session::flash($confirmacion,'se han actualizado los datos correctamente !');
        return redirect()->route('principal-cot-show.showCotizacion',[$request->ventas_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    } 
}
