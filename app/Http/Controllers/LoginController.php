<?php

namespace PaylessAdmin\Http\Controllers;

use PaylessAdmin\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use PaylessAdmin\Http\Requests;
use Auth;
use Session;
use Redirect;


class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('login.login');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * 23527c
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LoginRequest $request)
    {
        //dd($request->all());
        if(Auth::attempt(['user_name'=>$request['user_name'],'password'=>$request['password']])){
            return redirect()->route('cotizaciones.listar',[0]);
        }
        Session::flash('message-error','Username y/o password incorrectos !');
        return Redirect::to('login');
    }

    public function logout(){
        Auth::logout();
        return Redirect::to('login');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
