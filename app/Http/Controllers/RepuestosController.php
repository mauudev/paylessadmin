<?php

namespace PaylessAdmin\Http\Controllers;

use Illuminate\Http\Request;
use PaylessAdmin\Repuesto;
use PaylessAdmin\Http\Requests;
use DB;
use Session;
use Validator;
class RepuestosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if("" != $request->get('search')){
            $repuestos = Repuesto::search($request->get('search'))->paginate(10);
        }else
        $repuestos = Repuesto::getAll();
        return view('repuestos.index',['repuestos'=>$repuestos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('repuestos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
                'anio_r.numeric'=>'El año ingresado es inválido !'
        ];
        $validator = Validator::make($request->all(), [
                'anio_r'=>'required|numeric'
        ],$messages);
        if ($validator->fails()) {
          return redirect('repuestos/create')
                      ->withErrors($validator)->withInput();
        }
        Repuesto::create($request->all());
        Session::flash('store-success','datos agregados correctamente !');
        return redirect()->route('repuestos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $repuesto = Repuesto::find($id);
        return view('repuestos.edit',['repuesto'=>$repuesto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        $messages = [
                'anio_r.numeric'=>'El año ingresado es inválido !',
                'anio_r.digits'=>'El año ingresado es inválido !',
        ];
        $validator = Validator::make($request->all(), [
                'anio_r'=>'required|numeric|digits:4',
        ],$messages);
        if ($validator->fails()) {
          return redirect('repuestos/'.$id.'/edit') 
                      ->withErrors($validator)->withInput();
        }
        $repuesto = Repuesto::find($id);
        $repuesto->marca_r = $request->marca_r;
        $repuesto->modelo_r = $request->modelo_r;
        $repuesto->anio_r = $request->anio_r;
        $repuesto->vin_r = $request->vin_r;
        $repuesto->detalle_r = $request->detalle_r;
        $repuesto->codigo_repuesto = $request->codigo_repuesto;
        $repuesto->save();
        Session::flash('update-success','datos actualizados correctamente !');
        return redirect()->route('repuestos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $repuesto = Repuesto::find($id);
        $repuesto->delete();
        if($request->ajax()){
          //return "Eliminado correctamente!";
        }else{
            Session::flash('delete-success','datos eliminados correctamente !');
            return redirect()->route('repuestos.index');
        }
    }
}
