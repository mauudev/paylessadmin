<?php
//7779 lineas de codigo
namespace PaylessAdmin\Http\Controllers;

use Illuminate\Http\Request;

use PaylessAdmin\Http\Requests;
use PaylessAdmin\Http\Requests\CotizacionCreateRequest;
use PaylessAdmin\Http\Requests\CotizacionUpdateRequest;
use PaylessAdmin\Paises;
use PaylessAdmin\Cliente;
use PaylessAdmin\Mercaderia;
use PaylessAdmin\Repuesto;
use PaylessAdmin\Proovedor;
use PaylessAdmin\CotizacionRepuesto;
use PaylessAdmin\CotizacionMercaderia;
use PaylessAdmin\Venta;
use PaylessAdmin\Usuario;
use PaylessAdmin\TipoProveedor;
use PaylessAdmin\CuentasBancarias;
use PaylessAdmin\PrecioVentaMercaderia;
use PaylessAdmin\PrecioVentaRepuesto;
use PaylessAdmin\ProveedoresCotizacionRepuesto;
use PaylessAdmin\ProveedoresCotizacionMercaderia;
use PaylessAdmin\DetallePagoEfectivo;
use PaylessAdmin\DetallePagoDeposito;
use PaylessAdmin\VentaConfirmada;
use PaylessAdmin\MarcaModelo;
use DB;
use Session;
use Validator;

class CotizacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $flag = 'pendiente';
        if("" != $request->get('search')){
          $usuario_id = -123;
          if(\Auth::user()->type == 'member')
            $usuario_id = \Auth::user()->id;
          $ventas = Venta::searchCotizacion($request->get('search'),$request->get('pointer'),$usuario_id)->paginate(15);
        }
        else{
          $usuario_id = -123;
          if(\Auth::user()->type == 'member')
            $usuario_id = \Auth::user()->id;
          $ventas = Venta::getAll(1,$usuario_id);
        } 
        //dd($ventas->all());
        if(count($ventas) > 0)
          return view('cotizaciones.index',['ventas'=>$ventas, 'flag'=>$flag, 'pointer'=>$request->get('pointer')]);
        else return view('cotizaciones.index',['pointer'=>$request->get('pointer'),'flag'=>$flag]);
    }
    public function listar($estado){
      $usuario_id = -123;
      if(\Auth::user()->type == 'member')
        $usuario_id = \Auth::user()->id;
      $ventas = Venta::getAll($estado,$usuario_id);
      //return $ventas;
      $pointer = $estado;
      $flag = '';
      if($estado == 1) $flag = 'finalizado';
      else $flag = 'pendiente';
      if($ventas->all() == null)
        return view('cotizaciones.index',['pointer'=>$pointer,'flag'=>$flag]);
      else{
        return view('cotizaciones.index',['ventas'=>$ventas, 'flag'=>$flag,'pointer'=>$pointer]);
      } 
    }
    public function detalleVenta($ventas_id){
      $venta = Venta::find($ventas_id);
      if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$ventas_id)->first();
      $cliente = Cliente::find($venta->clientes_id);
      if($cliente == null) $cliente = Cliente::onlyTrashed()->where('id','=',$venta->clientes_id)->first();
      $usuario = Usuario::find($venta->usuarios_id);
      if($usuario == null) $usuario = Usuario::onlyTrashed()->where('id','=',$venta->usuarios_id)->first();
      $cotizacion_rep = CotizacionRepuesto::getRepuestos_cotizacionEstado($ventas_id,1);
      $cotizacion_mer = CotizacionMercaderia::getMercaderias_cotizacionEstado($ventas_id,1);
      $repuestos = array();
      $mercaderias = array();
      $pago_parcial = $venta->pago_parcial;
      if(count($cotizacion_rep) > 0){
        for($i = 0; $i < count($cotizacion_rep); $i ++)
          $repuestos[$i] = CotizacionRepuesto::getVentaDellate_rep($cotizacion_rep[$i]->id);
      }//else return "<b>ERROR 505</b> 1 Ocurrio un error al recuperar los datos de la venta: No existen repuestos cotizados";
      if(count($cotizacion_mer) > 0){
        for($i = 0; $i < count($cotizacion_mer); $i ++)
          $mercaderias[$i] = CotizacionMercaderia::getVentaDellate_mer($cotizacion_mer[$i]->id);
      }//else return "<b>ERROR 505</b> 1 Ocurrio un error al recuperar los datos de la venta: No existen mercaderias cotizados";
      return view('cotizaciones.detalle-venta',['pago_parcial'=>$pago_parcial,'venta'=>$venta,'cliente'=>$cliente,'usuario'=>$usuario,'repuestos'=>$repuestos,'mercaderias'=>$mercaderias]);
    }

    public function detalleVentaFinalizada($ventas_id){
      $venta = Venta::find($ventas_id);
      if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$ventas_id)->first();
      $cliente = Cliente::find($venta->clientes_id);
      if($cliente == null) $cliente = Cliente::onlyTrashed()->where('id','=',$venta->clientes_id)->first();
      $usuario = Usuario::find($venta->usuarios_id);
      if($usuario == null) $usuario = Usuario::onlyTrashed()->where('id','=',$venta->usuarios_id)->first();
      $cotizacion_rep = CotizacionRepuesto::getRepuestos_cotizacionEstado($ventas_id,1);
      $cotizacion_mer = CotizacionMercaderia::getMercaderias_cotizacionEstado($ventas_id,1);
      $repuestos = array();
      $mercaderias = array();
      $pago_parcial = $venta->pago_parcial;
      if(count($cotizacion_rep) > 0){
        for($i = 0; $i < count($cotizacion_rep); $i ++)
          $repuestos[$i] = CotizacionRepuesto::getVentaDellate_rep($cotizacion_rep[$i]->id);
      }//else return "<b>ERROR 505</b> 2 Ocurrio un error al recuperar los datos de la venta: No existen repuestos cotizados";
      if(count($cotizacion_mer) > 0){
        for($i = 0; $i < count($cotizacion_mer); $i ++)
          $mercaderias[$i] = CotizacionMercaderia::getVentaDellate_mer($cotizacion_mer[$i]->id);
      }//else return "<b>ERROR 505</b> 2 Ocurrio un error al recuperar los datos de la venta: No existen mercaderias cotizados";
      return view('cotizaciones.detalle-venta',['pago_parcial'=>$pago_parcial,'venta'=>$venta,'cliente'=>$cliente,'usuario'=>$usuario,'repuestos'=>$repuestos,'mercaderias'=>$mercaderias]);
    }

    public function confirmarVenta(Request $request){
      //dd($request->all());
      $messages = [
                'descuento.regex'=>'El descuento ingresado es inválido !',
        ];
        $validator = Validator::make($request->all(), [
                'descuento'=>'regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u',
        ],$messages);
        if ($validator->fails()) {
          $id1 = $request->ventas_id;
          return redirect('confirmar-venta/'.$id1.'/detalle')
                      ->withErrors($validator);
        }
      $venta = Venta::find($request->ventas_id);
      if($venta == null) $venta = Venta::onlyTrashed()->where('id','=',$request->ventas_id)->first();
      $venta->estado = 1;
      $venta->pago_total = $request->pago_total;
      if(isset($request->descuento)){
        if($request->descuento != null){
          $venta->descuento = $request->descuento;
          $venta->pago_total_desc = $request->pago_total - $request->descuento;
        }
      }
      if($venta->pago_parcial != null && $venta->segundo_pago != null)
        $venta->finalizado = 1;
      $venta->save();
      Session::flash('store-succes','se ha guardado una nueva venta correctamente !');
      return redirect()->route('ventas.detalleVentaFinalizada',[$request->ventas_id]);
    }

    public function deshabilitarVenta($ventas_id){
      $venta = Venta::find($ventas_id);
      $venta->estado = 0;
      $venta->finalizado = 0;
      $venta->save();
      Session::flash('store-succes','se ha deshabilitado la venta correctamente !');
      return redirect()->route('ventas.detalleVentaFinalizada',[$ventas_id]);
    }
    public function filtro($estado,$filtro){
        $ventas = Venta::filtro($estado,$filtro);
        return $ventas;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $paises = Paises::lists('nombre_largo','nombre_largo');
        $vista = 'create';
        $origen_contacto = $this->getOrigenContactos("");
        $marcas = MarcaModelo::lists('marca','marca');
        return view('cotizaciones.create',['paises'=>$paises,'vista'=>$vista,'origen_contacto'=>$origen_contacto,'marcas'=>$marcas]);
    }

    public function corregir(){
        $paises = Paises::lists('nombre_largo','nombre_largo');
        $vista = 'create';
        return view('cotizaciones.create-corregir',['paises'=>$paises,'vista'=>$vista]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $messages = [
          'nombre_c.regex'    => 'El campo nombre no puede contener números o símbolos !',
          'apellidos_c.regex'    => 'El campo apellidos no puede contener números o símbolos !',
          'email_c.email' => 'El email ingresado no es válido !',
          'anio_r.*.numeric' => 'El año ingresado no es válido !'
        ];
        $validator = Validator::make($request->all(), [
          'nombre_c'=> 'regex:/^[\pL\s\-]+$/u',
          'apellidos_c'=> 'regex:/^[\pL\s\-]+$/u',
          'email_c'=> 'email',
          'anio_r.*' => 'numeric',

        ],$messages);
        //dd($validator);
        if ($validator->fails()) {
          $paises = Paises::lists('nombre_largo','nombre_largo');
          if(isset($request->marca_r) && isset($request->nombre_m)){
            $mercaderia = "mercaderia-validar";
            return redirect()->route('cotizaciones.create')
                             ->withErrors($validator)
                             ->withInput(['nombre_c'=>$request->nombre_c,'apellidos_c'=>$request->apellidos_c,
                                          'email_c'=>$request->email_c,'telefono_c'=>$request->telefono_c,
                                          'direccion_c'=>$request->direccion_c,
                                          'celular_c'=>$request->celular_c,'nombre_m'=>$request->nombre_m[0],
                                          'nro_item'=>$request->nro_item[0],'cantidad_mer'=>$request->cantidad_mer[0],
                                          'modelo_r'=>$request->modelo_r[0],'marca_r'=>$request->marca_r[0],
                                          'anio_r'=>$request->anio_r[0],'cantidad_rep'=>$request->cantidad_rep[0],
                                          'vin_r'=>$request->vin_r[0],'detalle_r'=>$request->detalle_r[0],
                                          'codigo_repuesto'=>$request->codigo_repuesto[0]])
                             ->with('mercaderia',$mercaderia);
          }
          if(isset($request->marca_r)){
            $mercaderia = "no";
            return redirect()->route('cotizaciones.create')
                             ->withErrors($validator)
                             ->withInput(['nombre_c'=>$request->nombre_c,'apellidos_c'=>$request->apellidos_c,
                                          'email_c'=>$request->email_c,'telefono_c'=>$request->telefono_c,'direccion_c'=>$request->direccion_c,
                                          'celular_c'=>$request->celular_c,'modelo_r'=>$request->modelo_r[0],'marca_r'=>$request->marca_r[0],
                                          'anio_r'=>$request->anio_r[0],'cantidad_rep'=>$request->cantidad_rep[0],
                                          'vin_r'=>$request->vin_r[0],'detalle_r'=>$request->detalle_r[0],
                                          'codigo_repuesto'=>$request->codigo_repuesto[0]])
                             ->with('mercaderia',$mercaderia);
          }
          if(isset($request->nombre_m)){
            $mercaderia = "mercaderia-validar";
            return redirect()->route('cotizaciones.create')
                             ->withErrors($validator)
                             ->withInput(['nombre_c'=>$request->nombre_c,'apellidos_c'=>$request->apellidos_c,
                                          'email_c'=>$request->email_c,'telefono_c'=>$request->telefono_c,'direccion_c'=>$request->direccion_c,
                                          'celular_c'=>$request->celular_c,'nombre_m'=>$request->nombre_m[0],
                                          'nro_item'=>$request->nro_item[0],'cantidad_mer'=>$request->cantidad_mer[0]])
                             ->with('mercaderia',$mercaderia);
          }
        }//->withInput($request->except(['nombre_m','nro_item','cantidad_mer','modelo_r','marca_r','anio_r','cantidad_rep','vin_r','detalle_r','codigo_repuesto']));
        $flag_antiguos = false;
        $flag_nuevos = false;
        $nueva_venta = null;
        if("Antiguo" == $request->cliente){
          $venta_antiguo = new Venta();
          $venta_antiguo->estado = 0;
          $venta_antiguo->clientes_id = $request->clientes_id;
          $venta_antiguo->usuarios_id = $request->usuarios_id;
          $save_venta = $venta_antiguo->save();
          if($save_venta){
            $nueva_venta = $venta_antiguo;
            $flag_antiguos = true;
          }else return "<b>ERROR 505</b> 1 Ocurrio un error al guardar los datos de la venta, por favor comuniquese con el developer.";
        }// if antiguos
        if("Nuevo" == $request->cliente){
            $cliente = Cliente::create($request->all());
            if(isset($cliente)){
              $id_cliente = $cliente->id;
              $venta_nuevo = new Venta();
              $venta_nuevo->estado = 0;
              $venta_nuevo->clientes_id = $cliente->id;
              $venta_nuevo->usuarios_id = $request->usuarios_id;
              $save_venta = $venta_nuevo->save();
              if($save_venta){
                $nueva_venta = $venta_nuevo;
                $flag_nuevos = true;
              }else return "<b>ERROR 505</b> 2 Ocurrio un error al guardar los datos de la venta, por favor comuniquese con el developer.";
            }else return "<b>ERROR 505</b> 3 Ocurrio un error al guardar los datos del cliente, por favor comuniquese con el developer.";
        }//if nuevos
        if(isset($request->marca_r)){
          for($i = 0; $i < count($request->marca_r); $i++){
            $repuesto = new Repuesto();
            $repuesto->marca_r = $request->marca_r[$i];
            $repuesto->modelo_r = $request->modelo_r[$i];
            $repuesto->anio_r = $request->anio_r[$i];
            $repuesto->vin_r = $request->vin_r[$i];
            $repuesto->detalle_r = $request->detalle_r[$i];
            $repuesto->codigo_repuesto = $request->codigo_repuesto[$i];
            $repuesto->save();
            $cotizacion_rep = new CotizacionRepuesto();
            $cotizacion_rep->estado = 0;
            $cotizacion_rep->ventas_id = $nueva_venta->id;
            $cotizacion_rep->repuestos_id = $repuesto->id;
            $cotizacion_rep->cantidad = $request->cantidad_rep[$i];
            $confirm_rep = $cotizacion_rep->save();  
            if(!$confirm_rep) return "<b>ERROR 505</b> 4 Ocurrio un error al guardar los datos de la venta, por favor comuniquese con el developer.";   
          }
        }

        if(isset($request->nombre_m)){
          for($i = 0; $i < count($request->nombre_m); $i++){
            $mercaderia = new Mercaderia();
            $mercaderia->nombre_m = $request->nombre_m[$i];
            $mercaderia->nro_item = $request->nro_item[$i];
            $mercaderia->save();
            $cotizacion_mer = new CotizacionMercaderia();
            $cotizacion_mer->estado = 0;
            $cotizacion_mer->ventas_id = $nueva_venta->id;
            $cotizacion_mer->mercaderias_id = $mercaderia->id;
            $cotizacion_mer->cantidad = $request->cantidad_mer[$i];
            $confirm_mer = $cotizacion_mer->save();
            if(!$confirm_mer) return "<b>ERROR 505</b> 5 Ocurrio un error al guardar los datos de la venta, por favor comuniquese con el developer.";
          }
        }
        Session::flash('store-success','datos agregados correctamente !');
        return redirect()->route('cotizaciones.show',[$nueva_venta->id]);

    }//metodo store
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public static function checkPagos($ventas_id){
      $venta = Venta::find($ventas_id);
      $primer_pago = $venta->pago_parcial;
      $segundo_pago = $venta->segundo_pago;
      $venta_confirmada = VentaConfirmada::where('ventas_id','=',$ventas_id)->first();
      $pago_efectivo = array();
      $pago_deposito = array();
      $msg = "no-problem";
      $pagos_total = 0;
      if($primer_pago == "efectivo") 
        $pago_efectivo = DetallePagoEfectivo::where('ventas_confirmadas_id','=',$venta_confirmada->id)->get();
      if($segundo_pago == "deposito")
        $pago_deposito = DetallePagoDeposito::where('ventas_confirmadas_id','=',$venta_confirmada->id)->get();
      if(count($pago_efectivo) == 1 && count($pago_deposito) == 1){
        $pagos_total = $pago_efectivo[0]->monto + $pago_deposito[0]->monto;
        if($pagos_total < $venta->pago_total) $msg = "actualizar-pagos";
      }
      if(count($pago_efectivo) == 2){
        for($i = 0; $i < count($pago_efectivo); $i ++){
          $pagos_total += $pago_efectivo[$i]->monto;
        }
        if($pagos_total < $venta->pago_total) $msg = "actualizar-pagos";
      }
      if(count($pago_deposito) == 2){
        for($i = 0; $i < count($pago_deposito); $i ++){
          $pagos_total += $pago_deposito[$i]->monto;
        }
        if($pagos_total < $venta->pago_total) $msg = "actualizar-pagos";
      }
      return $msg;
    }

    public function show($id)//muestra los datos de la cotizacion y los repsuestos y mercaderias
    {
        $mensaje_pagos = "";
        //return $cotizacion_rep;
        $msg_venta_trashed = '';
        $venta = Venta::find($id);
        if($venta == null) {
          $venta = Venta::onlyTrashed()->where('id','=',$id)->first();
          $msg_venta_trashed = 'desactivado';
        }
        if($venta->pago_parcial != null && $venta->segundo_pago != null)
          $mensaje_pagos = $this->checkPagos($venta->id);
        $cotizacion_rep = CotizacionRepuesto::getRepuestos_cotizacion($id);
        $cotizacion_mer = CotizacionMercaderia::getMercaderias_cotizacion($id);
        //return $activados_mer;
        $flag_mercaderias = 'null';
        if(count($cotizacion_mer) > 0){
          $flag_mercaderias = 'pendent';
          for($i =0; $i < count($cotizacion_mer); $i++){
            $proveedores_cot_mer = ProveedoresCotizacionMercaderia::where('ventas_id','=',$venta->id)->where('mercaderias_id','=',$cotizacion_mer[$i]->mercaderia_id)->count();
            $precio_calculado_mer=PrecioVentaMercaderia::where('cotizacion_mercaderias_id','=',$cotizacion_mer[$i]->id_cot)->count();
            //return $cotizacion_mer[$i]->id;
            //return $precio_calculado_mer;
            if($proveedores_cot_mer == 1) $flag_mercaderias = 'pass';
            if($precio_calculado_mer == 0) $flag_mercaderias = 'fail';
            if($proveedores_cot_mer > 1) $flag_mercaderias = 'fail'; 
          }
        }
        
        $flag_repuestos = 'null';
        if(count($cotizacion_rep) > 0){
          $flag_repuestos = 'pendent';
          for($i =0; $i < count($cotizacion_rep); $i++){
            $proveedores_cot_rep = ProveedoresCotizacionRepuesto::where('ventas_id','=',$venta->id)->where('repuestos_id','=',$cotizacion_rep[$i]->repuesto_id)->count();
            $precio_calculado_rep = PrecioVentaRepuesto::where('cotizacion_repuestos_id','=',$cotizacion_rep[$i]->id)->count();
            //return $precio_calculado_rep;
            if($proveedores_cot_rep == 1) $flag_repuestos = 'pass';
            if($precio_calculado_rep == 0) $flag_repuestos = 'fail';
            if($proveedores_cot_rep > 1) $flag_repuestos = 'fail';     
          }
        }
        
        $msg_confirmation = "";

        if($flag_mercaderias != 'null' && $flag_repuestos == 'null'){
          if($flag_mercaderias == 'pass') $msg_confirmation = "finalizado";
          else $msg_confirmation = "pendiente";
        }

        if($flag_mercaderias == 'null' && $flag_repuestos != 'null'){
          if($flag_repuestos == 'pass') $msg_confirmation = "finalizado";
          else $msg_confirmation = "pendiente";
        }

        if($flag_mercaderias != 'null' && $flag_repuestos != 'null'){
          if($flag_mercaderias == 'pass' && $flag_repuestos == "pass") $msg_confirmation = "finalizado";
          else $msg_confirmation = "pendiente";
        }
        if($venta->estado == 1) $msg_confirmation = "confirmado";
        $cliente = Cliente::getCliente($venta->clientes_id);
        $usuario = Usuario::getUsuario($venta->usuarios_id);

        $cliente_check = Cliente::find($venta->clientes_id);
        $msg_cliente = '';
        if($cliente_check == null) $msg_cliente = 'desactivado';
        $usuario_check = Usuario::find($venta->usuarios_id);
        $msg_usuario = '';
        if($usuario_check == null) $msg_usuario = 'desactivado';
        $tipo_proveedores = TipoProveedor::lists('tipo','id');
        return view('cotizaciones.show',['cotizacion_rep'=>$cotizacion_rep,
            'cotizacion_mer'=>$cotizacion_mer,'cliente'=>$cliente,'usuario'=>$usuario,
            'venta'=>$venta,'tipo_proveedores'=>$tipo_proveedores,'msg_confirmation'=>$msg_confirmation,'msg_cliente'=>$msg_cliente,'msg_usuario'=>$msg_usuario,
            'msg_venta_trashed'=>$msg_venta_trashed,'mensaje_pagos'=>$mensaje_pagos]); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    { 
        $venta = Venta::find($id);
        $msg_venta_trashed = '';
        if($venta == null) {
          $venta = Venta::onlyTrashed()->where('id','=',$id)->first();
          $msg_venta_trashed = "desactivado";
        }
        $venta->estado = 0;
        $venta->finalizado = 0;
        $venta->save();
        $cliente = Cliente::find($venta->clientes_id);
        $msg_trashed = "";
        if($cliente == null){
          $cliente = Cliente::onlyTrashed()->where('id','=',$venta->clientes_id)->first();
          $msg_trashed = "desactivado";
        } 
        $origen_contacto = $this->getOrigenContactos($cliente->origen_contacto);
        $repuestos_cot = CotizacionRepuesto::getRepuestos_cotizacion($id);
        $mercaderias_cot = CotizacionMercaderia::getMercaderias_cotizacion($id);
        //return $mercaderias_cot;
        $flag_mercaderias = 'pass';
        $flag_repuestos = 'pass';
        for($i = 0; $i < count($repuestos_cot); $i++){
          $precio_venta_rep = PrecioVentaRepuesto::where('cotizacion_repuestos_id','=',$repuestos_cot[$i]->id)->get();
          if(count($precio_venta_rep) > 0) $flag_repuestos = 'fail';
        }
        for($i = 0; $i < count($mercaderias_cot); $i++){
          $precio_venta_mer = PrecioVentaMercaderia::where('cotizacion_mercaderias_id','=',$mercaderias_cot[$i]->id_cot)->get();
          if(count($precio_venta_mer) > 0) $flag_mercaderias = 'fail';
        }
        $vista = "edit";
        $marcas_edit = MarcaModelo::lists('marca','marca');
        $paises = Paises::lists('nombre_largo','nombre_largo');
        return view('cotizaciones.edit',['venta'=>$venta,'cliente'=>$cliente,
                                         'repuestos_cot'=>$repuestos_cot,
                                         'mercaderias_cot'=>$mercaderias_cot,
                                         'vista'=>$vista,
                                         'cliente'=>$cliente,
                                         'contador_r'=>count($repuestos_cot),
                                         'contador_m'=>count($mercaderias_cot),
                                         'paises'=>$paises,
                                         'origen_contacto'=>$origen_contacto,
                                         'flag_mercaderias'=>$flag_mercaderias,
                                         'flag_repuestos'=>$flag_repuestos,
                                         'msg_trashed'=>$msg_trashed,
                                         'msg_venta_trashed'=>$msg_venta_trashed,'marcas_edit'=>$marcas_edit]);
    }
    public function getOrigenContactos($actual){
      $seleccion = array();
      switch($actual){
        case "Anuncio":
          $seleccion = ["Anuncio"=>"Anuncio","Pagina Facebook"=>"Pagina Facebook","WhatsApp"=>"WhatsApp"];
          break;
        case "Pagina Facebook":
          $seleccion = ["Pagina Facebook"=>"Pagina Facebook","Anuncio"=>"Anuncio","WhatsApp"=>"WhatsApp"];
          break;
        case "WhatsApp":
          $seleccion = ["WhatsApp"=>"WhatsApp","Pagina Facebook"=>"Pagina Facebook","Anuncio"=>"Anuncio"];
          break;
        case "":
          $seleccion = ["Anuncio"=>"Anuncio","Pagina Facebook"=>"Pagina Facebook","WhatsApp"=>"WhatsApp"];
          break;
      }
      return $seleccion;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        //CLIENTE
        $messages = [
          'nombre_c.regex'    => 'El campo nombre no puede contener números o símbolos !',
          'apellidos_c.regex'    => 'El campo apellidos no puede contener números o símbolos !',
          'email_c.email' => 'El email ingresado no es válido !',
          'anio_r.*.numeric' => 'El año ingresado no es válido !',
          'cantidad_rep.*.numeric' => 'Cantidad es inválido !',
          'cantidad_mer.*.numeric' => 'Cantidad es inválido !',
          'anio_r_n.*.numeric' => 'El año ingresado no es válido !',
          'cantidad_rep_n.*.numeric' => 'Cantidad es inválido !',
          'cantidad_mer_n.*.numeric' => 'Cantidad es inválido !',
        ];
        $validator = Validator::make($request->all(), [
          'nombre_c'=> 'regex:/^[\pL\s\-]+$/u',
          'apellidos_c'=> 'regex:/^[\pL\s\-]+$/u',
          'email_c'=> 'email',
          'anio_r.*' => 'numeric',
          'cantidad_rep.*' => 'numeric',
          'cantidad_mer.*' => 'numeric',
          'anio_r_n.*' => 'numeric',
          'cantidad_rep_n.*' => 'numeric',
          'cantidad_mer_n.*' => 'numeric',
        ],$messages);
        if ($validator->fails()) {
          $paises = Paises::lists('nombre_largo','nombre_largo');
          return redirect()->route('cotizaciones.edit',[$id])
                      ->withErrors($validator);
        }
        $cliente = Cliente::find($request->cliente_id);
        $venta = Venta::find($id);
        if($venta == null) {
          $venta = Venta::onlyTrashed()->where('id','=',$id)->first();
        }
        $venta->updated_at = date("Y-m-d H:i:s");
        $venta->save();
        if($cliente == null) $cliente = Cliente::onlyTrashed()->where('id','=',$request->cliente_id)->first();
        $cliente->nombre_c = $request->nombre_c;
        $cliente->apellidos_c = $request->apellidos_c;
        $cliente->direccion_c = $request->direccion_c;
        $cliente->telefono_c = $request->telefono_c;
        $cliente->celular_c = $request->celular_c;
        $cliente->email_c = $request->email_c;
        $cliente->pais = $request->pais;
        $cliente->origen_contacto = $request->origen_contacto;
        $cliente->save();
        
        //REPUESTOS
        if(isset($request->repuestos_id)){
            for ($i=0; $i < count($request->repuestos_id); $i++) { 
                $repuesto = Repuesto::find($request->repuestos_id[$i]);
                $repuesto->marca_r = $request->marca_r[$i];
                $repuesto->modelo_r = $request->modelo_r[$i];
                $repuesto->anio_r = $request->anio_r[$i];
                $repuesto->vin_r = $request->vin_r[$i];
                $repuesto->detalle_r = $request->detalle_r[$i];
                $repuesto->codigo_repuesto = $request->codigo_repuesto[$i];
                $repuesto->save();
                $cotizacion_rep = CotizacionRepuesto::getCotizacionId1($request->repuestos_id[$i],$id);
                if($cotizacion_rep != null){
                  $cotizacion_rep->cantidad = $request->cantidad_rep[$i];
                  $cotizacion_rep->save();
                }
            }
        }
        if(isset($request->marca_r_n)){
            for($i =0; $i < count($request->marca_r_n); $i++){
                    $repuesto = new Repuesto();
                    $repuesto->marca_r = $request->marca_r_n[$i];
                    $repuesto->modelo_r = $request->modelo_r_n[$i];
                    $repuesto->anio_r = $request->anio_r_n[$i];
                    $repuesto->vin_r = $request->vin_r_n[$i];
                    $repuesto->detalle_r = $request->detalle_r_n[$i];
                    $repuesto->codigo_repuesto = $request->codigo_repuesto_n[$i];
                    $repuesto->save();
                    $cotizacion_repuesto = new CotizacionRepuesto();
                    $cotizacion_repuesto->ventas_id = $id;
                    $cotizacion_repuesto->repuestos_id = $repuesto->id;
                    $cotizacion_repuesto->cantidad = $request->cantidad_rep_n[$i];
                    $cotizacion_repuesto->created_at = date("Y-m-d H:i:s");
                    $cotizacion_repuesto->updated_at = date("Y-m-d H:i:s");
                    $cotizacion_repuesto->save();
                }
        }
        //MERCADERIAS
        if(isset($request->nombre_m)){
            $mercaderias = CotizacionMercaderia::getMercaderias_cotizacion($id);
            for ($i=0; $i < count($mercaderias); $i++) { 
                $mercaderia = Mercaderia::find($mercaderias[$i]->mercaderia_id);
                $mercaderia->nombre_m = $request->nombre_m[$i];
                $mercaderia->nro_item = $request->nro_item[$i];
                $mercaderia->save();
                $cotizacion_mer = CotizacionMercaderia::getCotizacionId1($mercaderias[$i]->mercaderia_id,$id);
                if($cotizacion_mer != null){
                  $cotizacion_mer->cantidad = $request->cantidad_mer[$i];
                  $cotizacion_mer->save();
                }
            }
        }
        if(isset($request->nombre_m_n)){
            for($i =0; $i < count($request->nombre_m_n); $i++){
                    $mercaderia = new Mercaderia();
                    $mercaderia->nombre_m = $request->nombre_m_n[$i];
                    $mercaderia->nro_item = $request->nro_item_n[$i];
                    $mercaderia->save();
                    $cotizacion_mercaderia = new CotizacionMercaderia();
                    $cotizacion_mercaderia->ventas_id = $id;
                    $cotizacion_mercaderia->mercaderias_id = $mercaderia->id;
                    $cotizacion_mercaderia->cantidad = $request->cantidad_mer_n[$i];
                    $cotizacion_mercaderia->created_at = date("Y-m-d H:i:s");
                    $cotizacion_mercaderia->updated_at = date("Y-m-d H:i:s");
                    $cotizacion_mercaderia->save();
                }
        }
        
        Session::flash('update-success','datos actualizados correctamente !');
        return redirect()->route('cotizaciones.show',[$id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$flag)
    {
        $venta = Venta::find($id);
        $venta->delete();
        Session::flash("delete-success",'datos eliminados correctamente !');
        if($flag == 0)
          return redirect()->route('cotizaciones.listar',[0]);
        else return redirect()->route('cotizaciones.listar',[1]);
    }
    public function activate($id,$flag){
        $cotizacion = Venta::withTrashed()->find($id)->restore();
        Session::flash('store-success','cotizacion activada correctamente !');
        if($flag == 0)
          return redirect()->route('cotizaciones.listar',[0]);
        else return redirect()->route('cotizaciones.listar',[1]);
    }
}
