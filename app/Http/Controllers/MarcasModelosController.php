<?php

namespace PaylessAdmin\Http\Controllers;

use Illuminate\Http\Request;

use PaylessAdmin\Http\Requests;
use PaylessAdmin\MarcaModelo;
use Session;
class MarcasModelosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if("" != $request->get('search'))
            $datos = MarcaModelo::search($request->get('search'))->paginate(15);
        else 
            $datos = MarcaModelo::orderBy('marca','asc')->paginate(15);        
        return view('marcas-modelos.index',compact('datos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('marcas-modelos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        for($i = 0; $i < count($request->modelo); $i++){
            $nuevo_registro = new MarcaModelo();
            $nuevo_registro->marca = $request->marca;
            $nuevo_registro->modelo = $request->modelo[$i];
            $nuevo_registro->save();
        }
        Session::flash('store-success','datos agregados correctamente !');
        return redirect()->route('marcas-modelos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getModelosPorMarca(Request $request, $marca){
      if($request->ajax()){
        $modelos = MarcaModelo::getModelosPorMarca($marca);
        return response()->json($modelos);
      }else{
        $modelos = MarcaModelo::getModelosPorMarca($marca);
        return $modelos;
      } 
    }
    public function edit($id)
    {
        $context = "edit";
        $objeto = MarcaModelo::find($id);
        return view('marcas-modelos.edit',['objeto'=>$objeto,"context"=>$context]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objeto = MarcaModelo::find($id);
        $objeto->marca = $request->marca;
        $objeto->modelo = $request->modelo;
        $objeto->save();
        Session::flash('store-success','datos actualizados correctamente !');
        return redirect()->route('marcas-modelos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objeto = MarcaModelo::find($id);
        $objeto->delete();
        Session::flash('store-success','datos eliminados correctamente !');
        return redirect()->route('marcas-modelos.index');
    }
}
