<?php

namespace PaylessAdmin\Http\Controllers;

use Illuminate\Http\Request;

use PaylessAdmin\Http\Requests;
use PaylessAdmin\PrecioTransporteMercaderias;
use Session;
use DB;
use Validator;
class PreciosTransporteMerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if("" != $request->get('search'))
            $precios = PrecioTransporteMercaderias::search($request->get('search'))->paginate(10);
        else 
            $precios = PrecioTransporteMercaderias::getAll();
        //dd($precios->all());    
        return view('precios-transporte-mer.index',['precios'=>$precios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mercaderias = PrecioTransporteMercaderias::getAll_mercaderias_sin_precio();
        return view("precios-transporte-mer.create",["mercaderias"=>$mercaderias]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $messages = [
                'transporte.regex'=>'El precio del transporte ingresado es inválido !'
        ];
        $validator = Validator::make($request->all(), [
                'transporte'=>'regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u',
        ],$messages);
        if ($validator->fails()) {
          return redirect('precios-transporte-mercaderias/create')
                      ->withErrors($validator)
                      ->withInput();
        }
        if(count($request->mercaderias_id) > 0){
            for($i = 0; $i < count($request->mercaderias_id); $i++){
                $precio_transporte_mer = new PrecioTransporteMercaderias();
                $precio_transporte_mer->transporte = $request->transporte;
                $precio_transporte_mer->mercaderias_id = $request->mercaderias_id[$i];
                $precio_transporte_mer->save();
            }
        }
        Session::flash('store-success','se han guardado los datos correctamente !');
        return redirect()->route('precios-transporte-mercaderias.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $precio_transporte_mer = PrecioTransporteMercaderias::getPrecio_mercaderia($id);
        return view('precios-transporte-mer.edit',['precio_transporte_mer'=>$precio_transporte_mer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
                'transporte.regex'=>'El precio del transporte ingresado es inválido !'
        ];
        $validator = Validator::make($request->all(), [
                'transporte'=>'regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u',
        ],$messages);
        if ($validator->fails()) {
          return redirect('precios-transporte-mercaderias/'.$id.'/edit')
                      ->withErrors($validator)
                      ->withInput();
        }
        $precio_transporte_mer = PrecioTransporteMercaderias::find($id);
        $precio_transporte_mer->transporte = $request->transporte;
        $precio_transporte_mer->save();
        Session::flash('update-success','datos actualizados correctamente !');
        return redirect()->route('precios-transporte-mercaderias.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $precio_transporte_mer = PrecioTransporteMercaderias::find($id);
        $precio_transporte_mer->delete();
        Session::flash('update-success','datos eliminados correctamente !');
        return redirect()->route('precios-transporte-mercaderias.index');
    }
}
