<?php

namespace PaylessAdmin\Http\Controllers;

use Illuminate\Http\Request;

use PaylessAdmin\Http\Requests;
use PaylessAdmin\PrecioTransporteRepuestos;
use PaylessAdmin\Repuesto;
use DB;
use Session;
use Validator;
class PreciosTransporteRepController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if("" != $request->get('search'))
            $precios = PrecioTransporteRepuestos::search($request->get('search'))->paginate(10);
        else 
            $precios = PrecioTransporteRepuestos::getAll();
        //dd($precios->all());    
        return view('precios-transporte-rep.index',['precios'=>$precios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $repuestos = PrecioTransporteRepuestos::getAll_repuestos_sin_precio();
        return view("precios-transporte-rep.create",["repuestos"=>$repuestos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $messages = [
                'transporte.regex'=>'El precio del transporte ingresado es inválido !'
        ];
        $validator = Validator::make($request->all(), [
                'transporte'=>'regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u',
        ],$messages);
        if ($validator->fails()) {
          return redirect('precios-transporte-repuestos/create')
                      ->withErrors($validator)
                      ->withInput();
        }
        if(count($request->repuestos_id) > 0){
            for($i = 0; $i < count($request->repuestos_id); $i++){
                $precio_transporte_rep = new PrecioTransporteRepuestos();
                $precio_transporte_rep->transporte = $request->transporte;
                $precio_transporte_rep->repuestos_id = $request->repuestos_id[$i];
                $precio_transporte_rep->save();
            }
        }
        Session::flash('store-success','se han guardado los datos correctamente !');
        return redirect()->route('precios-transporte-repuestos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $precio_transporte_rep = PrecioTransporteRepuestos::getPrecio_repuesto($id);
        return view('precios-transporte-rep.edit',['precio_transporte_rep'=>$precio_transporte_rep]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
                'transporte.regex'=>'El precio del transporte ingresado es inválido !'
        ];
        $validator = Validator::make($request->all(), [
                'transporte'=>'regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u',
        ],$messages);
        if ($validator->fails()) {
          return redirect('precios-transporte-repuestos/'.$id.'/edit')
                      ->withErrors($validator)
                      ->withInput();
        }
        $precio_transporte_rep = PrecioTransporteRepuestos::find($id);
        $precio_transporte_rep->transporte = $request->transporte;
        $precio_transporte_rep->save();
        Session::flash('update-success','datos actualizados correctamente !');
        return redirect()->route('precios-transporte-repuestos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $precio_transporte_rep = PrecioTransporteRepuestos::find($id);
        $precio_transporte_rep->delete();
        Session::flash('update-success','datos eliminados correctamente !');
        return redirect()->route('precios-transporte-repuestos.index');
    }
}
