<?php

namespace PaylessAdmin\Http\Controllers;

use Illuminate\Http\Request;

use PaylessAdmin\Http\Requests;
use PaylessAdmin\PreciosTransporte;
use Session;
use Validator;
class PreciosTransporteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if("" != $request->get('search'))
            $precios = PreciosTransporte::search($request->get('search'))->paginate(10);
        else 
            $precios = PreciosTransporte::getAll();
        //dd($precios->all());
        return view('precios-transporte.index',['precios'=>$precios]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('precios-transporte.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
                'precio.regex'=>'El precio del transporte ingresado es inválido !'
        ];
        $validator = Validator::make($request->all(), [
                'precio'=>'regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u',
        ],$messages);
        if ($validator->fails()) {
          return redirect('precios-transporte/create')
                      ->withErrors($validator)
                      ->withInput();
        }
        PreciosTransporte::create($request->all());
        Session::flash('store-success','datos agregados correctamente !');
        return redirect()->route('precios-transporte.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $precio_transporte = PreciosTransporte::find($id);
        return view('precios-transporte.edit',['precio_transporte'=>$precio_transporte]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
                'precio.regex'=>'El precio del transporte ingresado es inválido !'
        ];
        $validator = Validator::make($request->all(), [
                'precio'=>'regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u',
        ],$messages);
        if ($validator->fails()) {
          return redirect('precios-transporte/'.$id.'/edit')
                      ->withErrors($validator)
                      ->withInput();
        }
        $precio_transporte = PreciosTransporte::find($id);
        $precio_transporte->descripcion = $request->descripcion;
        $precio_transporte->precio = $request->precio;
        $precio_transporte->save();
        Session::flash('update-success','datos actualizados correctamente !');
        return redirect()->route('precios-transporte.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $precio_transporte = PreciosTransporte::find($id);
        $precio_transporte->delete();
        Session::flash('update-success','datos eliminados correctamente !');
        return redirect()->route('precios-transporte.index');
    }
}
