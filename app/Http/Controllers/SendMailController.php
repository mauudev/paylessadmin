<?php

namespace PaylessAdmin\Http\Controllers;

use Illuminate\Http\Request;

use PaylessAdmin\Http\Requests;
use PaylessAdmin\Cliente;
use PaylessAdmin\Mercaderia;
use PaylessAdmin\Repuesto;
use PaylessAdmin\Proovedor;
use PaylessAdmin\CotizacionRepuesto;
use PaylessAdmin\CotizacionMercaderia;
use PaylessAdmin\Venta;
use PaylessAdmin\Usuario;
use Mail;
use Session;
use PDF;
use Beautymail;
use Validator;
use App;
use Config;
use Artisan;

class SendMailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $venta = Venta::find($id);
        return view('sendmail.sent',['venta'=>$venta]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $venta = Venta::find($id);
        $cliente = Cliente::find($venta->clientes_id);
        $email_c = "sin-correo";
        if($cliente->email_c != "") $email_c = $cliente->email_c;
        return view('sendmail/sendmail',['venta'=>$venta,'cliente'=>$cliente,'email_c'=>$email_c]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            //+
            //dd($request->all());
            $messages = [
                    'receptor.email'=>'El email ingresado no es válido !',
                    'asunto.min'=>'El asunto es demasiado corto !',
                    'mensaje.min'=>'El mensaje es demasiado corto !'
            ];
            $validator = Validator::make($request->all(), [
                    'receptor'=>'email',
                    'asunto'=>'min:5',
                    'mensaje'=>'min:10'
            ],$messages);
            if ($validator->fails()) {
              return redirect('/sendmail/'.$request->ventas_id.'/create')
                          ->withErrors($validator)
                          ->withInput();
            }
            $venta = Venta::find($request->ventas_id);
            $cliente = Cliente::find($venta->clientes_id);
            $usuario = Usuario::find($venta->usuarios_id);
            $cotizacion_rep = CotizacionRepuesto::getRepuestos_cotizacionEstado($request->ventas_id,1);
            $cotizacion_mer = CotizacionMercaderia::getMercaderias_cotizacionEstado($request->ventas_id,1);
            $repuestos = array();
            $mercaderias = array();
            $pago_parcial = $venta->pago_parcial;
            $observaciones = '';
            $plazo_entrega = '';
            $metodo_pago = '';
            if(isset($request->observaciones)) $observaciones = $request->observaciones;
            if(isset($request->plazo_entrega)) $plazo_entrega = $request->plazo_entrega;
            if(isset($request->metodo_pago)) $metodo_pago = $request->metodo_pago;
            if(count($cotizacion_rep) > 0){
                for($i = 0; $i < count($cotizacion_rep); $i ++)
                  $repuestos[$i] = CotizacionRepuesto::getVentaDellate_rep($cotizacion_rep[$i]->id);
            }//else return "<b>ERROR 505</b> 2 Ocurrio un error al recuperar los datos de la venta: No existen repuestos cotizados";
            if(count($cotizacion_mer) > 0){
                for($i = 0; $i < count($cotizacion_mer); $i ++)
                  $mercaderias[$i] = CotizacionMercaderia::getVentaDellate_mer($cotizacion_mer[$i]->id);
            }
            if(isset($request->personalizado)){
                $pdf = PDF::loadView('pdffile.custom-pdf-template',['pago_parcial'=>$pago_parcial,'venta'=>$venta,'cliente'=>$cliente,'usuario'=>$usuario,'repuestos'=>$repuestos,'mercaderias'=>$mercaderias,'observaciones'=>$observaciones,'plazo_entrega'=>$plazo_entrega,'metodo_pago'=>$metodo_pago]);
            }else $pdf = PDF::loadView('pdffile.pdf-template',['pago_parcial'=>$pago_parcial,'venta'=>$venta,'cliente'=>$cliente,'usuario'=>$usuario,'repuestos'=>$repuestos,'mercaderias'=>$mercaderias,'observaciones'=>$observaciones,'plazo_entrega'=>$plazo_entrega,'metodo_pago'=>$metodo_pago]);
            
            $nombre_archivo = $cliente->nombre_c.'-'.$cliente->apellidos_c.'-cotizacion-'.$venta->created_at.'.pdf';
            if($request->remitente == "es1"){

                $backup = Mail::getSwiftMailer();

                // Setup your gmail mailer
                $transport = \Swift_SmtpTransport::newInstance('box668.bluehost.com', 465, 'ssl');
                $transport->setUsername('info@paylessimport.com');
                $transport->setPassword('Samz1712$');
                // Any other mailer configuration stuff needed...

                $gmail = new \Swift_Mailer($transport);

                // Set the mailer as gmail
                Mail::setSwiftMailer($gmail);

                // Send your message
                Mail::send('sendmail.mail-content-es1',$request->all(), function($message) use($pdf,$nombre_archivo,$request){
                    $message->from("info@paylessimport.com","Payless Import");
                    $message->subject($request->asunto);
                    $message->to($request->receptor);
                    $message->attachData($pdf->output(), $nombre_archivo);
                });

                // Restore your original mailer
                Mail::setSwiftMailer($backup);
            }else if($request->remitente == "es2"){

                $backup = Mail::getSwiftMailer();

                // Setup your gmail mailer
                $transport = \Swift_SmtpTransport::newInstance('box668.bluehost.com', 465, 'ssl');
                $transport->setUsername('cotizaciones@paylessimport.com');
                $transport->setPassword('Alvaro2017$');
                // Any other mailer configuration stuff needed...

                $gmail = new \Swift_Mailer($transport);

                // Set the mailer as gmail
                Mail::setSwiftMailer($gmail);

                // Send your message
                Mail::send('sendmail.mail-content-es2',$request->all(), function($message) use($pdf,$nombre_archivo,$request){
                    $message->from("cotizaciones@paylessimport.com","Payless Import");
                    $message->subject($request->asunto);
                    $message->to($request->receptor);
                    $message->attachData($pdf->output(), $nombre_archivo);
                });

                // Restore your original mailer
                Mail::setSwiftMailer($backup);
            }else if($request->remitente == "en1"){

                // Backup your default mailer
                $backup = Mail::getSwiftMailer();

                // Setup your gmail mailer
                $transport = \Swift_SmtpTransport::newInstance('box668.bluehost.com', 465, 'ssl');
                $transport->setUsername('sales@paylessgroupusa.com');
                $transport->setPassword('Alvaro2017$');
                // Any other mailer configuration stuff needed...

                $gmail = new \Swift_Mailer($transport);

                // Set the mailer as gmail
                Mail::setSwiftMailer($gmail);

                // Send your message
                Mail::send('sendmail.mail-content-en1',$request->all(), function($message) use($pdf,$nombre_archivo,$request){
                    $message->from("sales@paylessgroupusa.com","Payless Group");
                    $message->subject($request->asunto);
                    $message->to($request->receptor);
                    $message->attachData($pdf->output(), $nombre_archivo);
                });

                // Restore your original mailer
                Mail::setSwiftMailer($backup);
            } 
            
            Session::flash('store-success','mensaje enviado correctamente !');
            return redirect()->route('sendmail.index',[$request->ventas_id]);
        }catch(Exception $e){
            return view('sendmail.error-send',['message'=>$e->getMessage()]);
        }
    }

    private function setEnvironmentValue($environmentName, $configKey, $newValue, $flag) {
        $res = '';
        if($flag == 1){
            Artisan::call("config:cache");
            file_put_contents(App::environmentFilePath(), str_replace(
            $environmentName . '="' . Config::get($configKey).'"',
            $environmentName . '="' . $newValue.'"',
            file_get_contents(App::environmentFilePath())
            ));
            Config::set($configKey, $newValue);   
            Artisan::call("config:cache");
            $res = Config::get($configKey);
        }else{
            Artisan::call("config:cache");
            file_put_contents(App::environmentFilePath(), str_replace(
            $environmentName . '=' . Config::get($configKey),
            $environmentName . '=' . $newValue,
            file_get_contents(App::environmentFilePath())
            ));
            Config::set($configKey, $newValue);   
            Artisan::call("config:cache");
        }
        return $res;
    }
}
