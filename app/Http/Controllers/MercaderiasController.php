<?php

namespace PaylessAdmin\Http\Controllers;

use Illuminate\Http\Request;

use PaylessAdmin\Http\Requests;
use PaylessAdmin\Mercaderia;
use DB;
use Session;
use Validator;
class MercaderiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if("" != $request->get('search')){
            $mercaderias = Mercaderia::search($request->get('search'))->paginate(10);
        }else
        $mercaderias = Mercaderia::getAll();
        return view('mercaderias.index',['mercaderias'=>$mercaderias]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('mercaderias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
                'precio_venta_m.regex'=>'El monto del precio de venta es inválido !'
        ];
        $validator = Validator::make($request->all(), [
                'precio_venta_m'=>'regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u'
        ],$messages);
        if ($validator->fails()) {
          return redirect('mercaderias/create') 
                      ->withErrors($validator)->withInput();
        }
        Mercaderia::create($request->all());
        Session::flash('store-success','datos agregados correctamente !');
        return redirect()->route('mercaderias.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mercaderia = Mercaderia::find($id);
        return view('mercaderias.edit',['mercaderia'=>$mercaderia]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
                'precio_venta_m.regex'=>'El monto del precio de venta es inválido !'
        ];
        $validator = Validator::make($request->all(), [
                'precio_venta_m'=>'regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u'
        ],$messages);
        if ($validator->fails()) {
          return redirect('mercaderias/'.$id.'/edit') 
                      ->withErrors($validator)->withInput();
        }
        $mercaderia = Mercaderia::find($id);
        $mercaderia->nombre_m = $request->nombre_m;
        $mercaderia->precio_venta_m= $request->precio_venta_m;
        $mercaderia->nro_item = $request->nro_item;
        $mercaderia->save();
        Session::flash('update-success','datos actualizados correctamente !');
        return redirect()->route('mercaderias.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mercaderia = Mercaderia::find($id);
        $mercaderia->delete();
        if($request->ajax()){
          //return "Eliminado correctamente!";
        }else{
            Session::flash('delete-success','datos eliminados correctamente !');
            return redirect()->route('mercaderias.index');
        }
    }
}
