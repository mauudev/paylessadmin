<?php

namespace PaylessAdmin\Http\Controllers;

use Illuminate\Http\Request;
use PaylessAdmin\Usuario;
use PaylessAdmin\TipoUsuario;
use PaylessAdmin\Http\Requests;
use Session;
use Validator;
class UsuariosController extends Controller
{
    /**no tiene los privilegios suficientes para acceder a esta secci&oacute;n, si cree que este mensaje sali&oacute; por error, cont&aacute;ctese con el administrador.
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
      //$this->middleware('usuarioAdmin',['only'=>['create','store','update','destroy']]);
      //$this->middleware('usuarioStandard',['only'=>['store','update']]);
    }
    public function index(Request $request)
    {
        if("" != $request->get('search'))
            $usuarios = Usuario::search($request->get('search'))->paginate(10);
        else 
            $usuarios = Usuario::getAll();
        //dd($usuarios->all());
        return view('usuarios.index',['usuarios'=>$usuarios]);
    }
    public function filtro($filtro){
        $usuarios = Usuario::filtro($filtro);
        return $usuarios;
    }
    public function unauthorized(){
        return view('usuarios.unauthorized');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipo_usuarios = TipoUsuario::lists('tipo','id');
        $vista = "create";
        //dd($tipo_usuarios->all());
        return view('usuarios.create',compact('tipo_usuarios'),['vista'=>$vista]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
                'nombre_u.regex'    => 'El campo nombre no puede contener números o símbolos !',
                'apellidos_u.regex'    => 'El campo apellidos no puede contener números o símbolos !',
                'email_u.email' => 'El email ingresado no es válido !',
                'user_name.alpha_num'=>'El nombre de usuario no puede contener símbolos o caracteres especiales !',
                'user_name.unique'=>'El nombre de usuario ingresado ya está siendo utilizado !',
                'password.alpha_num'=>'La contraseña no puede contener símbolos o caracteres especiales !'
        ];
        $validator = Validator::make($request->all(), [
                'nombre_u'     => 'required|regex:/^[\pL\s\-]+$/u',
                'apellidos_u'     => 'required|regex:/^[\pL\s\-]+$/u',
                'email_u'    => 'email|unique:usuarios',
                'user_name'=>'required|unique:usuarios|alpha_num',
                'password'=>'required|alpha_num'
        ],$messages);
        if ($validator->fails()) {
          return redirect('usuarios/create')
                      ->withErrors($validator)
                      ->withInput($request->except('password'));
        }
        $usuario = new Usuario();
        $usuario->nombre_u = $request->nombre_u;
        $usuario->apellidos_u = $request->apellidos_u;
        $usuario->telefono_u = $request->telefono_u;
        $usuario->celular_u = $request->celular_u;
        $usuario->email_u = $request->email_u;
        $usuario->user_name = $request->user_name;
        $usuario->password = bcrypt($request->password);
        $usuario->tipo_usuarios_id = $request->tipo_usuarios_id;
        if($request->tipo_usuarios_id == 1) $usuario->type = 'admin';
        else $usuario->type = 'member';
        $usuario->save();
        Session::flash('store-success','usuario registrado correctamente !');
        return redirect()->route('usuarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = Usuario::find($id);
        if($usuario == null) $usuario = Usuario::onlyTrashed()->where('id','=',$id)->first();
        $tipo_usuarios = TipoUsuario::lists('tipo','id');
        $vista = "edit";
        return view('usuarios.edit',['usuario'=>$usuario,
                                     'tipo_usuarios'=>$tipo_usuarios,
                                     'vista'=>$vista]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request->all());
        if(!isset($request->user_name) && !isset($request->password)){
            $messages = [
                'nombre_u.regex'    => 'El campo nombre no puede contener números o símbolos !',
                'apellidos_u.regex'    => 'El campo apellidos no puede contener números o símbolos !',
                'email_u.email' => 'El email ingresado no es válido !',
            ];
            $validator = Validator::make($request->all(), [
                    'nombre_u'     => 'required|regex:/^[\pL\s\-]+$/u',
                    'apellidos_u'     => 'required|regex:/^[\pL\s\-]+$/u',
                    'email_u'    => 'email|unique:users,email',
            ],$messages);
            if ($validator->fails()) {
              return redirect()->route('usuarios.edit',[$id])
                          ->withErrors($validator)
                          ->withInput($request->except('password'));
            }
        }else{
            $messages = [
                'nombre_u.regex'    => 'El campo nombre no puede contener números o símbolos !',
                'apellidos_u.regex'    => 'El campo apellidos no puede contener números o símbolos !',
                'email_u.email' => 'El email ingresado no es válido !',
                'user_name.alpha_num'=>'El nombre de usuario no puede contener símbolos o caracteres especiales !',
                'user_name.unique'=>'El nombre de usuario ingresado ya está siendo utilizado !',
                'password.alpha_num'=>'La contraseña no puede contener símbolos o caracteres especiales !'
            ];
            $validator = Validator::make($request->all(), [
                    'nombre_u'     => 'required|regex:/^[\pL\s\-]+$/u',
                    'apellidos_u'     => 'required|regex:/^[\pL\s\-]+$/u',
                    'email_u'    => 'email|unique:users,email',
                    'user_name'=>'required|alpha_num',
                    'password'=>'required|alpha_num'
            ],$messages);
            if ($validator->fails()) {
              return redirect()->route('usuarios.edit',[$id])
                          ->withErrors($validator)
                          ->withInput($request->except('password'));
            }
        }
        $usuario = Usuario::find($id);
        if($usuario == null) $usuario = Usuario::onlyTrashed()->where('id','=',$id)->first();
        $usuario->nombre_u = $request->nombre_u;
        $usuario->apellidos_u = $request->apellidos_u;
        $usuario->telefono_u = $request->telefono_u;
        $usuario->celular_u = $request->celular_u;
        $usuario->email_u = $request->email_u;
        if(isset($request->user_name) && isset($request->password)){
            $usuario->user_name = $request->user_name;
            $usuario->password = bcrypt($request->password);
        }
        $usuario->tipo_usuarios_id = $request->tipo_usuarios_id;
        if($request->tipo_usuarios_id == 1){
            $usuario->type = 'admin';
        } else $usuario->type = 'member';
        $usuario->save();
        Session::flash('update-success','datos de usuario actualizados correctamente !');
        return redirect()->route('usuarios.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usuario = Usuario::find($id);
        $usuario->delete();
        Session::flash('delete-success','datos eliminados correctamente !');
        return redirect()->route('usuarios.index');
    }

    public function activate($id){
        $usuario = Usuario::withTrashed()->find($id)->restore();
        Session::flash('store-success','usuario activado correctamente !');
        return redirect()->route('usuarios.index');
    }
}
