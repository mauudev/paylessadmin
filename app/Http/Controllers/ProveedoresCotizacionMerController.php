<?php

namespace PaylessAdmin\Http\Controllers;

use Illuminate\Http\Request;

use PaylessAdmin\Http\Requests;
use PaylessAdmin\CotizacionMercaderia;
use PaylessAdmin\Cliente;
use PaylessAdmin\Usuario;
use PaylessAdmin\Venta;
use PaylessAdmin\TipoProveedor;
use PaylessAdmin\Proveedor;
use PaylessAdmin\ProveedoresCotizacionMercaderia;
use Session;
use DB;
use Redirect;
use Validator;
class ProveedoresCotizacionMerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id1,$id2)
    {
        $proveedores_cotizacion = ProveedoresCotizacionMercaderia::getProveedores_cotizacion1($id1);
        $cotizacion_mercaderia = CotizacionMercaderia::getMercaderia_cotizacion($id1);
        $venta_id = $id2;
        $tipo_proveedores = TipoProveedor::lists('tipo','id');
        return view('cotizaciones.add-proveedores-mer',['cotizacion_mercaderia'=>$cotizacion_mercaderia,
                    'tipo_proveedores'=>$tipo_proveedores,
                    'venta_id'=>$venta_id,
                    'proveedores_cotizacion'=>$proveedores_cotizacion]); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $messages = [
                'precio_cot.*.regex'=>'El precio ingresado es inválido !',
                'precio_cot_old.*.regex'=>'El precio ingresado es inválido !'
        ];
        $validator = Validator::make($request->all(), [
                'precio_cot.*'=>'required|regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u',
                'precio_cot_old.*'=>'required|regex:/^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$/u'
        ],$messages);
        if ($validator->fails()) {
          $id1 = $request->mercaderias_id;
          $id2 = $request->venta_id;
          return redirect('agregar-proveedores/'.$id1.'/create-mer/'.$id2)
                      ->withErrors($validator);
        }

        $venta = Venta::find($request->venta_id);
        $venta->updated_at = date("Y-m-d H:i:s");
        $venta->save();
        if(isset($request->id_cot_p_mer)){
            for($i = 0; $i < count($request->id_cot_p_mer); $i++){
                $id = $request->id_cot_p_mer[$i];
                $proveedores_cotizacion_mer = ProveedoresCotizacionMercaderia::find($id);
                $proveedores_cotizacion_mer->precio_cot_mer = $request->precio_cot_old[$i];
                $proveedores_cotizacion_mer->save(); 
            }
        }
        if(isset($request->tipos_p_id)){
            for($i = 0; $i < count($request->tipos_p_id); $i++){
                $proveedores_cotizacion_mer = new ProveedoresCotizacionMercaderia();
                $proveedores_cotizacion_mer->precio_cot_mer = $request->precio_cot[$i];
                $proveedores_cotizacion_mer->ventas_id = $request->venta_id;
                $proveedores_cotizacion_mer->mercaderias_id = $request->mercaderias_id;
                $proveedores_cotizacion_mer->proveedores_id = $request->proveedores[$i];
                $proveedores_cotizacion_mer->save();
            }
        }
        Session::flash('store-success','datos de proveedores agregados !');
        return redirect()->route('principal-cot-show.showCotizacion', $request->venta_id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        $mercaderia_cot = ProveedoresCotizacionMercaderia::find($id);
        $mercaderia_cot->delete();
        if($request->ajax()){
          //return "Eliminado correctamente!";
        }else{
            Session::flash('delete-success','datos eliminados correctamente !');
            return redirect()->route('cotizaciones.listar',[0]);
        }
    }
}
