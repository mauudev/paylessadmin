<?php

namespace PaylessAdmin\Http\Middleware;

use Closure;
use Session;
class MDAdministrador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::check()){
            $usuario_actual = \Auth::user();
            if($usuario_actual->tipo_usuarios_id != 1){//es un usuario standard
                Session::flash('unauthorized','no tiene los privilegios suficientes para acceder a esta secci&oacute;n, si cree que este mensaje sali&oacute; por error, cont&aacute;ctese con el administrador.');
                return redirect()->to('unauthorized');
            }
        }else{
            Session::flash('unauthorized-login','debe iniciar sesi&oacute;n para continuar');
            return redirect()->to('login');
        }
        return $next($request);
    }
}
