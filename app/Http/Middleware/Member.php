<?php

namespace PaylessAdmin\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Session;
use Closure;
class Member
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            if(Auth::user()->type == 'member' || Auth::user()->type == 'admin'){
                //return "see"; 
                return $next($request); 
            }else{
                Session::flash('unauthorized','Usted no tiene los privilegios suficientes para ver esta secci&oacute;n');
                return redirect()->to('unauthorized');
            }
        }else{
            Session::flash('unauthorized-login','Debe iniciar sesi&oacute;n para continuar');
            return redirect()->to('login');
        }
        
    }
}
