<?php

namespace PaylessAdmin\Http\Middleware;

use Closure;
use Session;

class MDStandard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(\Auth::check()){
            $usuario_actual = \Auth::user();
            if($usuario_actual->tipo_usuarios_id != 2){//es un usuario standard
                Session::flash('unauthorized','usted es un kepiri');
                return redirect()->to('unauthorized');
            }
        }else{
            Session::flash('unauthorized-login','debe iniciar sesi&oacute;n para continuar');
            return redirect()->to('login');
        }
        return $next($request);
    }
}
