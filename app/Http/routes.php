<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Support\Facades\Input;
use PaylessAdmin\TipoProveedor;
Route::get('/', function () {
    return view('login.login');
});
Route::resource('videos','VideoController');
Route::group(['middleware'=> 'admin'], function(){
    //admin
    Route::get('/test', function(){
         $beautymail = app()->make(Snowfire\Beautymail\Beautymail::class);
         $beautymail->send('sendmail.mail-content', [], function($message){
            $message->from('mtrigo143@gmail.com')
                    ->to('mtrigo143@gmail.com', 'John Smith')
                    ->subject('Correo de prueba!');
        });
    echo '%PUBLIC%';
    });

    Route::get('/invoice',[
      'uses'=>'VideoController@invoice',
      'as' => 'invoices.invoice']
    );
    Route::get('/invoice/index',[
      'uses'=>'VideoController@index',
      'as' => 'invoices.invoice1']
    );
    Route::resource('marcas-modelos','MarcasModelosController');
    Route::get('/marcas-modelos/{marca}/destroy',[
      'uses'=>'MarcasModelosController@destroy',
      'as' => 'marcas-modelos.destroy'
    ]);

    Route::resource('precios-transporte','PreciosTransporteController');
    Route::get('/precios-transporte/{precio}/destroy',[
      'uses'=>'PreciosTransporteController@destroy',
      'as' => 'precios-transporte.destroy'
    ]);

    Route::get('/administracion/clientes/{clientes}/destroy',[
      'uses'=>'ClientesController@destroyCliente',
      'as' => 'clientes.destroyCliente'
    ]); 
    Route::get('/administracion/clientes/{clientes}/activate',[
      'uses'=>'ClientesController@activate',
      'as' => 'clientes.activate'
    ]);
    Route::get('/administracion/proveedores/{proveedores}/activate',[
      'uses'=>'ProveedoresController@activate',
      'as' => 'proveedores.activate'
    ]);
    Route::resource('usuarios','UsuariosController');
    Route::get('/administracion/usuarios/{usuarios}/destroy',[
      'uses'=>'UsuariosController@destroy',
      'as' => 'usuarios.destroy'
    ]);
    Route::get('/administracion/usuarios/{usuarios}/activate',[
      'uses'=>'UsuariosController@activate',
      'as' => 'usuarios.activate'
    ]);
    //admin
//admin
    Route::resource('precios-transporte-repuestos','PreciosTransporteRepController');
    Route::get('/precios-transporte-repuestos/{precio}/destroy',[
      'uses'=>'PreciosTransporteRepController@destroy',
      'as' => 'precios-transporte-repuestos.destroy'
    ]);

    Route::resource('precios-transporte-mercaderias','PreciosTransporteMerController');
    Route::get('/precios-transporte-mercaderias/{precio}/destroy',[
      'uses'=>'PreciosTransporteMerController@destroy',
      'as' => 'precios-transporte-mercaderias.destroy'
    ]);

    Route::resource('ventas','VentasController');

    Route::get('ventas/{venta}/create',[
      'uses'=>'VentasController@create',
      'as'=>'ventas.create']);

    Route::resource('proveedores','ProveedoresController');
    Route::get('proveedores/{proveedores}/destroy',[
      'uses'=>'ProveedoresController@destroy',
      'as' => 'proveedores.destroy'
    ]);
    Route::resource('repuestos','RepuestosController');
    Route::get('repuestos/{repuestos}/destroy',[
      'uses'=>'RepuestosController@destroy',
      'as' => 'repuestos.destroy'
    ]);
    Route::resource('mercaderias','MercaderiasController');
    Route::get('mercaderias/{mercaderias}/destroy',[
      'uses'=>'MercaderiasController@destroy',
      'as' => 'mercaderias.destroy'
    ]);
    //admin

    Route::get('cotizaciones/{cotizaciones}/destroy',[
      'uses'=>'CotizacionesController@destroy',
      'as' => 'cotizaciones.destroy'
    ]);

    Route::get('confirmar-venta/{venta}/deshabilitar',[
      'uses'=>'CotizacionesController@deshabilitarVenta',
      'as'=>'confirmar-venta.deshabilitarVenta']);

    Route::resource('marcas-prov','MarcasProveedorController');
    
    Route::get('ventas/segundo-pago/{venta}/edit',[
      'uses'=>'VentasController@agregarSegundoPago',
      'as'=>'ventas.agregarSegundoPago']);

    Route::post('ventas/segundo-pago/{venta}/save',[
      'uses'=>'VentasController@guardarSegundoPago',
      'as'=>'ventas.guardarSegundoPago']);

    Route::get('ventas/pagos/{venta}/edit',[
      'uses'=>'VentasController@editarPagos',
      'as'=>'ventas.editarPagos']);

    Route::get('ventas/pagos/{venta}/show',[
      'uses'=>'VentasController@show',
      'as'=>'ventas.editarPagos']);

    Route::post('ventas/pagos/{venta}/save',[
      'uses'=>'VentasController@updatePagos',
      'as'=>'ventas.updatePagos']);

    Route::resource('cuentas-bancarias','CuentasBancariasController');

    Route::get('cuentas-bancarias/{cuenta}/destroy',[
      'uses'=>'CuentasBancariasController@destroy',
      'as' => 'cuentas-bancarias.destroy'
    ]);

});

Route::group(['middleware' => 'member'], function(){
    //mail y pdf
    Route::resource('sendmail','SendMailController');
    Route::get('/sendmail/{venta}/create',[
      'uses'=>'SendMailController@create',
      'as' => 'sendmail.create'
    ]);
    Route::get('/sendmail/{venta}/enviado',[
      'uses'=>'SendMailController@index',
      'as' => 'sendmail.index'
    ]);
    Route::get('ventas/generar-pdf/{venta}','VentasController@generarPDF');
    Route::get('ventas/generar-pdf/{venta}/personalizar','VentasController@personalizarPDF');
    Route::post('ventas/generar-pdf/{venta}/personalizado',[
      'uses'=>'VentasController@generarPDFPersonalizado',
      'as'=>'ventas.generarPDFPersonalizado'
    ]);
    //mail y pdf
    
    Route::get('agregar-proveedores/{id1}/create-rep/{id2}','ProveedoresCotizacionRepController@create');
    Route::get('agregar-proveedores/{id1}/create-mer/{id2}','ProveedoresCotizacionMerController@create');

    Route::resource('agregar-proveedores-rep','ProveedoresCotizacionRepController');
    Route::resource('agregar-proveedores-mer','ProveedoresCotizacionMerController');

    Route::post('agregar-proveedores-rep',[
      'uses'=>'ProveedoresCotizacionRepController@store',
      'as' => 'agregar-proveedores-rep.store'
    ]);
    Route::post('agregar-proveedores-mer',[
      'uses'=>'ProveedoresCotizacionMerController@store',
      'as' => 'agregar-proveedores-mer.store'
    ]);

    Route::resource('calcular-precio-venta','CalculoPVController');
    Route::resource('calcular-precio-venta-mer','CalculoPVMController');

    Route::get('calcular-precio-venta/repuesto/{repuesto}/venta/{venta}',[
      'uses'=>'CalculoPVController@calculoRepuesto',
      'as'=>'calcular-precio-venta.calculoRepuesto']);
    
    Route::post('calcular-precio-venta/calcular',[
      'uses'=>'CalculoPVController@calcular',
      'as' => 'calcular-precio-venta.calcular'
    ]);

    Route::get('calcular-precio-venta/{precioventa}/precioventa',[
      'uses'=>'CalculoPVController@show',
      'as'=>'calcular-precio-venta.show']);

    Route::get('calcular-precio-venta-mer/mercaderia/{mercaderia}/venta/{venta}',[
      'uses'=>'CalculoPVMController@calculoMercaderia',
      'as'=>'calcular-precio-venta-mer.calculoMercaderia']);
    
    Route::post('calcular-precio-venta-mer/calcular',[
      'uses'=>'CalculoPVMController@calcular',
      'as' => 'calcular-precio-venta-mer.calcular'
    ]);

    Route::get('calcular-precio-venta-mer/{precioventa}/precioventa',[
      'uses'=>'CalculoPVMController@show',
      'as'=>'calcular-precio-venta-mer.show']);

    Route::get('ver-precio-venta-rep/cotizacion-rep/{cotizacion}/venta/{venta}',[
      'uses'=>'CalculoPVController@confirmacionProveedor',
      'as'=>'ver-precio-venta-rep.confirmacionProveedor']);

    Route::get('ver-precio-venta-mer/cotizacion-mer/{cotizacion}/venta/{venta}',[
      'uses'=>'CalculoPVMController@confirmacionProveedor',
      'as'=>'ver-precio-venta-mer.confirmacionProveedor']);

    Route::post('confirmar-precio-venta-rep/confirmar',[
      'uses'=>'CalculoPVController@confirmarProveedor',
      'as'=>'confirmar-precio-venta-rep.confirmarProveedor']);

    Route::post('confirmar-precio-venta-mer/confirmar',[
      'uses'=>'CalculoPVMController@confirmarProveedor',
      'as'=>'confirmar-precio-venta-mer.confirmarProveedor']);

    Route::get('confirmar-venta/{venta}/detalle',[
      'uses'=>'CotizacionesController@detalleVenta',
      'as'=>'confirmar-venta.detalleVenta']);

    Route::post('confirmar-venta/confirmar',[
      'uses'=>'CotizacionesController@confirmarVenta',
      'as'=>'confirmar-venta.confirmarVenta']);
    //todos
    Route::get('/administracion/mostrar-cotizacion-principal/show/{show}',[
      'uses' => 'CotizacionesController@show',
      'as' => 'principal-cot-show.showCotizacion']);
    Route::resource('clientes','ClientesController');
    Route::get('/administracion/clientes/search/{search}','ClientesController@busquedaCliente');
    Route::get('ventas/{venta}/detalle',[
      'uses'=>'CotizacionesController@detalleVentaFinalizada',
      'as'=>'ventas.detalleVentaFinalizada']);


    //todos
    Route::resource('cotizaciones','CotizacionesController');
    Route::get('cotizaciones/listar/{estado}',[
      'uses'=>'CotizacionesController@listar',
      'as' => 'cotizaciones.listar'
    ]);
    Route::get('/cotizaciones/create/corregir',[
      'uses'=>'CotizacionesController@corregir',
      'as' => 'cotizaciones.corregir'
    ]);
    Route::get('/administracion/cotizaciones/{cotizaciones}/destroy/{vista}/vista',[
      'uses'=>'CotizacionesController@destroy',
      'as' => 'cotizaciones.destroy'
    ]);
    Route::get('/administracion/cotizaciones/{cotizaciones}/activate/{vista}/vista',[
      'uses'=>'CotizacionesController@activate',
      'as' => 'ventas.activate'
    ]);
    Route::resource('cotizacion-repuestos','CotizacionRepuestosController');
    Route::resource('cotizacion-mercaderias','CotizacionMercaderiasController');
    //todos

    //jquery y ajax
    Route::get('/administracion/clientes/filtrar/{filtrar}','ClientesController@filtro');
    Route::get('/administracion/clientes/check/celular/{check}','ClientesController@validarDuplicadosCel');
    Route::get('/administracion/clientes/check/telefono/{check}','ClientesController@validarDuplicadosTel');
    Route::get('/administracion/clientes/{clientes}/check/celular/{check}/edit','ClientesController@validarDuplicadosCelEdit');
    Route::get('/administracion/clientes/{clientes}/check/telefono/{check}/edit','ClientesController@validarDuplicadosTelEdit');
    Route::get('/administracion/proveedores/{proveedores}','ProveedoresController@getProveedores');
    Route::get('/administracion/proveedor/{proveedor}', 'ProveedoresController@getProveedor');
    Route::get('/administracion/proveedores/filtrar/{filtrar}','ProveedoresController@filtro');
    Route::get('/administracion/proveedores/search/{search}','ProveedoresController@busquedaProveedores');
    Route::get('/administracion/tipo-proveedor/{tipo}', 'ProveedoresController@getTipoProveedor');  
    Route::get('/administracion/usuarios/filtrar/{filtrar}','UsuariosController@filtro');
    Route::get('/administracion/cotizaciones/estado/{estado}/filtro/{filtro}','CotizacionesController@filtro');
     Route::get('/administracion/marcas-modelos/get/{marca}','MarcasModelosController@getModelosPorMarca');
    //jquery y ajax
});

Route::resource('login','LoginController');
Route::get('unauthorized',[
      'uses'=>'UsuariosController@unauthorized',
      'as' => 'usuarios.unauthorized'
    ]);
Route::get('logout','LoginController@logout');