<?php

namespace PaylessAdmin\Http\Requests;

use PaylessAdmin\Http\Requests\Request;

class ClientesCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_c'     => 'required|regex:/^[\pL\s\-]+$/u',
            'apellidos_c'     => 'required|regex:/^[\pL\s\-]+$/u',
            'email_c'    => 'email|unique:users,email',
            'telefono_c'    => 'required|numeric',
            'celular_c'    => 'required|numeric'
        ];
    }
    public function messages(){
        return [
                'nombre_c.regex'    => 'El campo nombre no puede contener números o símbolos !',
                'apellidos_c.regex'    => 'El campo apellidos no puede contener números o símbolos !',
                'email_c.email' => 'El email ingresado no es válido !',
                'telefono_c.numeric'      => 'El teléfono ingresado no es válido !',
                'celular_c.numeric'      => 'El celular ingresado no es válido !',
        ];
    }
}
