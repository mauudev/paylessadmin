<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;
use PaylessAdmin\PrecioTransporteRepuestos;
use PaylessAdmin\Repuesto;
use DB;
class PrecioTransporteRepuestos extends Model
{
    protected $table = 'precio_transporte_repuestos';
    protected $fillable = ['transporte',
                           'repuestos_id'];

    public static function getAll(){
    	return DB::table('precio_transporte_repuestos')
    				->select('precio_transporte_repuestos.id','precio_transporte_repuestos.transporte',
    						 'precio_transporte_repuestos.repuestos_id','repuestos.marca_r',
    						 'repuestos.modelo_r','repuestos.anio_r','repuestos.detalle_r')
    				->join('repuestos','precio_transporte_repuestos.repuestos_id','=','repuestos.id')
                    ->orderBy('repuestos.marca_r')
    				->paginate(15);
    }
    public static function getAll_repuestos_sin_precio(){
    	$con_precios = PrecioTransporteRepuestos::select('precio_transporte_repuestos.repuestos_id')
       			  								->join('repuestos','precio_transporte_repuestos.repuestos_id','=','repuestos.id')->get();
       	$sin_precios = Repuesto::whereNotIn('id',$con_precios)->orderBy('marca_r')->distinct()->get();
        return $sin_precios;
    }
    public static function getPrecio_repuesto($id){
    	return PrecioTransporteRepuestos::select('precio_transporte_repuestos.id',
    											 'precio_transporte_repuestos.transporte',
    											 'precio_transporte_repuestos.repuestos_id',
    											 'repuestos.marca_r','repuestos.modelo_r',
    											 'repuestos.anio_r','repuestos.detalle_r')
    									->join('repuestos','precio_transporte_repuestos.repuestos_id','=','repuestos.id')
    									->where('precio_transporte_repuestos.id','=',$id)->first();
    }

    public static function scopeSearch($query,$search){
        if("" != trim($search)){
          $target = trim($search);
          return DB::table('precio_transporte_repuestos') 
                 ->join('repuestos','precio_transporte_repuestos.repuestos_id','=','repuestos.id')
                 ->select('precio_transporte_repuestos.id','precio_transporte_repuestos.transporte',
                          'repuestos.marca_r','repuestos.modelo_r','repuestos.anio_r','repuestos.vin_r',
                          'repuestos.detalle_r','repuestos.precio_venta_r','repuestos.codigo_repuesto')
                 ->orWhere(function ($query) use ($search) {
                    $query->where("precio_transporte_repuestos.transporte","rlike",$search)
                          ->orWhere("repuestos.marca_r","rlike",$search)
                          ->orWhere("repuestos.modelo_r","rlike",$search)
                          ->orWhere("repuestos.anio_r","rlike",$search)
                          ->orWhere("repuestos.vin_r","rlike",$search)
                          ->orWhere("repuestos.detalle_r","rlike",$search)
                          ->orWhere("repuestos.precio_venta_r","rlike",$search)
                          ->orWhere("repuestos.codigo_repuesto","rlike",$search);
                    })
                 ->orderBy('repuestos.marca_r','desc')->distinct();
        } 
    }

    public static function getRepuestos_precios(){
      return DB::table('precio_transporte_repuestos') 
                 ->join('repuestos','precio_transporte_repuestos.repuestos_id','=','repuestos.id')
                 ->select('precio_transporte_repuestos.id','precio_transporte_repuestos.transporte',
                          'repuestos.marca_r','repuestos.modelo_r','repuestos.anio_r','repuestos.vin_r',
                          'repuestos.detalle_r','repuestos.precio_venta_r','repuestos.codigo_repuesto')
                 ->orderBy('repuestos.marca_r','desc')->get();
    }
}
