<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;
use DB;
class ProveedoresCotizacionMercaderia extends Model
{
    protected $table = 'proveedores_cotizacion_mer';
    protected $fillable = [
    					   'precio_cot_mer',
    					   'ventas_id',
    					   'mercaderias_id',
    					   'proveedores_id',
    					   ];	
    public static function getProveedores_cotizacion($mercaderias_id,$ventas_id){
    	return DB::table('proveedores_cotizacion_mer')
    			   ->join('proveedores','proveedores_cotizacion_mer.proveedores_id','=','proveedores.id')
             ->join('tipo_proveedores','proveedores.tipo_proveedores_id','=','tipo_proveedores.id')
    			   ->select('proveedores_cotizacion_mer.id','proveedores.id as proveedores_id','tipo_proveedores.tipo','proveedores.nombre_compania_p','proveedores.persona_contacto_p',
    			   			    'proveedores.telefono_p','proveedores.celular_p','proveedores_cotizacion_mer.precio_cot_mer')
    			   ->where('proveedores_cotizacion_mer.mercaderias_id','=',$mercaderias_id)
             ->where('proveedores_cotizacion_mer.ventas_id','=',$ventas_id)
    			   ->get();
    }
    public static function getProveedores_cotizacion_all($cotizacion_mercaderias_id){
      return DB::table('precio_venta_mer')
             ->select('precio_venta_mer.id as precio_venta_mer_id','precio_venta_mer.transporte','precio_venta_mer.adicional','precio_venta_mer.precio_total','proveedores.persona_contacto_p','proveedores.nombre_compania_p','proveedores_cotizacion_mer.proveedores_id','proveedores_cotizacion_mer.precio_cot_mer')
             ->join('cotizacion_mercaderias','precio_venta_mer.cotizacion_mercaderias_id','=','cotizacion_mercaderias.id')
             ->join('proveedores_cotizacion_mer','precio_venta_mer.proveedores_cotizacion_mer_id','=','proveedores_cotizacion_mer.id')
             ->join('proveedores','precio_venta_mer.proveedores_id','=','proveedores.id')
             ->where('precio_venta_mer.cotizacion_mercaderias_id','=',$cotizacion_mercaderias_id)
             ->distinct()->get();
    }
    public static function getProveedores_cotizacion1($id){
      return DB::select("SELECT tp.id as tipos_p_id, tp.tipo, p.id as proveedores_id,
      							p.nombre_compania_p, p.persona_contacto_p, p.telefono_p, 
      							p.celular_p, pc.precio_cot_mer, pc.id as proveedores_cot_id
                         FROM proveedores_cotizacion_mer pc, tipo_proveedores tp, proveedores p
                         WHERE pc.proveedores_id = p.id AND p.tipo_proveedores_id = tp.id AND pc.mercaderias_id =".$id);        
    }
}
