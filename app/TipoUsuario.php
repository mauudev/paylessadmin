<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;

class TipoUsuario extends Model
{
    protected $table = 'tipo_usuarios';
    protected $fillable = ['tipo'];
}
