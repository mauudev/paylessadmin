<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;
use DB;
class CotizacionRepuesto extends Model
{
    protected $table = 'cotizacion_repuestos';
    protected $fillable = ['cantidad','precio_venta_cot',
              					   'ventas_id',
              					   'repuestos_id'];

    public static function getAll(){
    	return DB::table('cotizacion_repuestos')
    			   ->select('cotizacion_repuestos.*')
    			   ->paginate(15);
    }
    public static function getRepuestos_cotizacion($id){
        return DB::table('cotizacion_repuestos')
                   ->join('repuestos','cotizacion_repuestos.repuestos_id','=','repuestos.id')
                   ->join('ventas','cotizacion_repuestos.ventas_id','=','ventas.id')
                   ->select('cotizacion_repuestos.id','cotizacion_repuestos.estado','cotizacion_repuestos.cantidad','cotizacion_repuestos.id as id_cot',
                    'repuestos.id as repuesto_id','repuestos.marca_r','repuestos.modelo_r','repuestos.vin_r','repuestos.detalle_r',
                    'repuestos.precio_venta_r','repuestos.anio_r','ventas.id as venta_id','repuestos.codigo_repuesto')
                   ->where('cotizacion_repuestos.ventas_id','=',$id)
                   ->get();
    }
    public static function getRepuesto_cotizacion($id){
        return DB::table('cotizacion_repuestos')
                   ->join('repuestos','cotizacion_repuestos.repuestos_id','=','repuestos.id')
                   ->join('ventas','cotizacion_repuestos.ventas_id','=','ventas.id')
                   ->select('cotizacion_repuestos.id as id_cot','cotizacion_repuestos.cantidad',
                    'repuestos.id as repuesto_id','repuestos.marca_r','repuestos.modelo_r','repuestos.vin_r',
                    'repuestos.detalle_r','repuestos.precio_venta_r','repuestos.anio_r','ventas.id as venta_id','repuestos.codigo_repuesto')
                   ->where('cotizacion_repuestos.repuestos_id','=',$id)
                   ->first();
    }
    public static function getRepuestos_cotizacion1($id){
      return DB::select("SELECT cr.id as id_cot,cr.precio_venta_cot, cr.repuestos_id, r.id as repuesto_id,r.marca_r, r.modelo_r, v.id as venta_id, cr.cantidad
                         FROM cotizacion_repuestos cr, ventas v, repuestos r 
                         WHERE cr.ventas_id = v.id AND cr.repuestos_id = r.id AND cr.ventas_id = ".$id);        
    }
    public static function getCantidad_proveedores_rep($id){
      return DB::select("SELECT DISTINCT COUNT(pr.id) as count
                         FROM ventas v, repuestos r, proveedores_cotizacion_rep pr 
                         WHERE pr.ventas_id = v.id AND pr.repuestos_id = r.id AND pr.repuestos_id = ".$id);
    }
    public static function getCotizacionId($repuestos_id,$ventas_id){
      return DB::table('cotizacion_repuestos')
                 ->select('cotizacion_repuestos.id')
                 ->join('ventas','cotizacion_repuestos.ventas_id','=','ventas.id')
                 ->where('cotizacion_repuestos.repuestos_id','=',$repuestos_id)
                 ->where('cotizacion_repuestos.ventas_id','=',$ventas_id)
                 ->first(); 
    }
    public static function getCotizacionId1($repuestos_id,$ventas_id){
      return CotizacionRepuesto::select('cotizacion_repuestos.id')
                 ->join('ventas','cotizacion_repuestos.ventas_id','=','ventas.id')
                 ->where('cotizacion_repuestos.repuestos_id','=',$repuestos_id)
                 ->where('cotizacion_repuestos.ventas_id','=',$ventas_id)
                 ->first(); 
    }
    public static function getRepuestos_cotizacionEstado($id,$estado){
        return DB::table('cotizacion_repuestos')
                   ->join('repuestos','cotizacion_repuestos.repuestos_id','=','repuestos.id')
                   ->join('ventas','cotizacion_repuestos.ventas_id','=','ventas.id')
                   ->select('cotizacion_repuestos.id')
                   ->where('cotizacion_repuestos.ventas_id','=',$id)
                   ->where('cotizacion_repuestos.estado','=',$estado)
                   ->get();
    }

    public static function getVentaDellate_rep($id){
        return DB::table('cotizacion_repuestos')
                   ->join('repuestos','cotizacion_repuestos.repuestos_id','=','repuestos.id')
                   ->join('precio_venta_rep','precio_venta_rep.cotizacion_repuestos_id','=','cotizacion_repuestos.id')
                   ->join('proveedores','proveedores.id','=','precio_venta_rep.proveedores_id')
                   ->join('tipo_proveedores','tipo_proveedores.id','=','proveedores.tipo_proveedores_id')
                   ->select('cotizacion_repuestos.id as id_cot','repuestos.marca_r','repuestos.modelo_r','repuestos.detalle_r','repuestos.codigo_repuesto','repuestos.anio_r',
                            'precio_venta_rep.precio_total','tipo_proveedores.tipo','cotizacion_repuestos.cantidad')
                   ->where('cotizacion_repuestos.id','=',$id)
                   ->first();
  }
}
