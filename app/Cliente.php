<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Cliente extends Model
{
    use SoftDeletes;
    protected $table = 'clientes';
    protected $dates = ['deleted_at'];
    protected $fillable = ['usuarios_id',
                           'nombre_c',
              					   'apellidos_c',
              					   'direccion_c',
              					   'telefono_c',
              					   'celular_c',
              					   'email_c',
              					   'pais',
                           'origen_contacto',
                           'deleted_at'];

    public static function getAll(){
    	return DB::table('clientes')
    			      ->select('clientes.id','clientes.usuarios_id',
    			   			       'clientes.nombre_c','clientes.apellidos_c',
    			   			       'clientes.direccion_c','clientes.telefono_c',
    			   			       'clientes.celular_c','clientes.email_c',
                         'clientes.origen_contacto','clientes.pais',
    			   			       'clientes.created_at','clientes.updated_at','clientes.deleted_at',
                         'usuarios.nombre_u','usuarios.apellidos_u')
                ->join('usuarios','clientes.usuarios_id','=','usuarios.id','left outer')
                ->where('clientes.deleted_at', '=',null)
                ->orderBy('clientes.updated_at','desc')
			          ->paginate(15);
    }
    public static function getCliente($id){
        return  DB::table('clientes')
                ->select('clientes.nombre_c','clientes.apellidos_c',
                    'clientes.telefono_c','clientes.celular_c',
                    'clientes.email_c','clientes.pais','clientes.origen_contacto')
                ->where('clientes.id','=',$id)
                ->first();
    }
    public static function findClienteTrashedById($id){
        return Cliente::where('deleted_at','=',null)
                ->where('id','=',$id);
    }
    public static function findClienteNonTrashedById($id){
        return Cliente::where('deleted_at','<>',null)
                ->where('id','=',$id);
    }
    public static function filtro($filtro){
        if($filtro == 1){
            return DB::table('clientes')
                        ->select('clientes.id','clientes.usuarios_id',
                         'clientes.nombre_c','clientes.apellidos_c',
                         'clientes.direccion_c','clientes.telefono_c',
                         'clientes.celular_c','clientes.email_c',
                         'clientes.origen_contacto','clientes.pais',
                         'clientes.created_at','clientes.updated_at',
                         'usuarios.nombre_u','usuarios.apellidos_u')
                        ->join('usuarios','clientes.usuarios_id','=','usuarios.id','left outer')
                        ->where('clientes.deleted_at', '=',null)
                        ->orderBy('clientes.updated_at','desc')
                        ->get();
        }else{
            return DB::table('clientes')
                        ->select('clientes.id','clientes.usuarios_id',
                         'clientes.nombre_c','clientes.apellidos_c',
                         'clientes.direccion_c','clientes.telefono_c',
                         'clientes.celular_c','clientes.email_c',
                         'clientes.origen_contacto','clientes.pais',
                         'clientes.created_at','clientes.updated_at',
                         'usuarios.nombre_u','usuarios.apellidos_u')
                        ->join('usuarios','clientes.usuarios_id','=','usuarios.id','left outer')
                        ->where('clientes.deleted_at', '<>',null)
                        ->orderBy('clientes.updated_at','desc')
                        ->get();
        }
    }
    public static function scopeSearch1($query, $search){
        if("" != trim($search)) 
              $query->select('clientes.nombre_c','clientes.apellidos_c',
                             'clientes.telefono_c','clientes.direccion_c',
                             'clientes.celular_c','clientes.email_c',
                             'clientes.pais','clientes.origen_contacto',
                             'clientes.usuarios_id')
                    ->where("clientes.nombre_c","LIKE","%$search%")
                    ->orWhere("clientes.apellidos_c","LIKE","%$search%")
                    ->orWhere("clientes.telefono_c","LIKE","%$search%")
                    ->orWhere("clientes.celular_c","LIKE","%$search%")
                    ->orWhere("clientes.email_c","LIKE","%$search%")
                    ->orWhere("clientes.pais","LIKE","%$search%")
                    ->orWhere("clientes.origen_contacto","LIKE","%$search%")->distinct();
    }
    public static function scopeSearch($query,$search){
        if("" != trim($search)){
          $target = trim($search);
          return DB::table('clientes') 
                 ->select('clientes.nombre_c','clientes.apellidos_c',
                          'clientes.telefono_c','clientes.direccion_c',
                          'clientes.celular_c','clientes.email_c',
                          'clientes.pais','clientes.origen_contacto',
                          'clientes.usuarios_id','clientes.deleted_at')
                 ->orWhere(function ($query) use ($search) {
                    $query->where("clientes.nombre_c","rlike",$search)
                          ->orWhere("clientes.apellidos_c","rlike",$search)
                          ->orWhere("clientes.telefono_c","rlike",$search)
                          ->orWhere("clientes.celular_c","rlike",$search)
                          ->orWhere("clientes.email_c","rlike",$search)
                          ->orWhere("clientes.pais","rlike",$search)
                          ->orWhere("clientes.origen_contacto","rlike",$search);
                    })
                 ->orderBy('clientes.updated_at','desc')->distinct();
        } 
    }
    public static function searchClientes($search){
        if("" != trim($search)){
          $target = trim($search);
          return DB::table('clientes') 
                 ->join('usuarios','clientes.usuarios_id','=','usuarios.id')
                 ->select('clientes.id','clientes.nombre_c','clientes.apellidos_c',
                          'clientes.telefono_c','clientes.direccion_c',
                          'clientes.celular_c','clientes.email_c',
                          'clientes.pais','clientes.origen_contacto',
                          'clientes.usuarios_id','usuarios.nombre_u','usuarios.apellidos_u','clientes.deleted_at')
                 ->orWhere(function ($query) use ($search) {
                    $query->where("clientes.nombre_c", "rlike", $search)
                          ->orWhere("clientes.apellidos_c","rlike",$search)
                          ->orWhere("clientes.telefono_c","rlike",$search)
                          ->orWhere("clientes.celular_c","rlike",$search)
                          ->orWhere("clientes.direccion_c","rlike",$search)
                          ->orWhere("clientes.email_c","rlike",$search)
                          ->orWhere("clientes.pais","rlike",$search)
                          ->orWhere("clientes.origen_contacto","rlike",$search)
                          ->orWhere("usuarios.nombre_u","rlike",$search)
                          ->orWhere("usuarios.apellidos_u","rlike",$search);
                    })
                 ->orderBy('clientes.updated_at','desc');
        } 
    }
    // public static function searchClientes2($search){
    //     if("" != trim($search)) 
    //         return  DB::table('clientes')
    //                 ->select('clientes.id as clientes_id','clientes.nombre_c',
    //                          'clientes.apellidos_c','clientes.direccion_c',
    //                          'clientes.telefono_c','clientes.celular_c',
    //                          'clientes.email_c','clientes.pais',
    //                          'clientes.origen_contacto','clientes.usuarios_id','usuarios.nombre_u',
    //                          'usuarios.apellidos_u')
    //                 ->join('usuarios','clientes.usuarios_id','=','usuarios.id')
    //                 ->where("clientes.nombre_c","LIKE","%$search%")
    //                 ->orWhere("clientes.apellidos_c","LIKE","%$search%")
    //                 ->orWhere("clientes.telefono_c","LIKE","%$search%")
    //                 ->orWhere("clientes.celular_c","LIKE","%$search%")
    //                 ->orWhere("clientes.email_c","LIKE","%$search%")
    //                 ->orWhere("clientes.pais","LIKE","%$search%")
    //                 ->orWhere("clientes.origen_contacto","LIKE","%$search%")
    //                 ->orWhere("usuarios.nombre_u","LIKE","%$search%")
    //                 ->orWhere("usuarios.apellidos_u","LIKE","%$search%")
    //                 ->distinct()->get();
    // }

    public static function validarDuplicadosCel($search){
        if("" != trim($search)) 
            return  DB::table('clientes')
                    ->select('clientes.id')
                    ->where("clientes.celular_c","=", $search)->get();
    }
    public static function validarDuplicadosTel($search){
        if("" != trim($search)) 
            return  DB::table('clientes')
                    ->select('clientes.id')
                    ->where("clientes.telefono_c","=",$search)->get();
    }

    public static function validarDuplicadosCelEdit($cliente_id, $celular_c){
        if("" != trim($celular_c)) 
            return  DB::table('clientes')
                    ->select('clientes.id')
                    ->where('clientes.id','<>',$cliente_id)
                    ->where("clientes.celular_c","=",$celular_c)->get();
    }
    public static function validarDuplicadosTelEdit($cliente_id, $telefono_c){
        if("" != trim($telefono_c)) 
            return  DB::table('clientes')
                    ->select('clientes.id')
                    ->where('clientes.id','<>',$cliente_id)
                    ->where("clientes.telefono_c","=",$telefono_c)->get();
    }

}
