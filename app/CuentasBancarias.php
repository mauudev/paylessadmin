<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;
use DB;

class CuentasBancarias extends Model
{
    protected $table = 'cuentas_bancarias';
    protected $fillable = ['banco',
                           'cuenta_bancaria',
                           'moneda'];

    public static function getAll(){
    	return DB::table('cuentas_bancarias')
    			   ->select('id',
    			   			'banco',
    			   			'cuenta_bancaria',
    			   			'moneda',
    			   			'created_at',
    			   			'updated_at')
                   ->orderBy('cuentas_bancarias.updated_at','desc')
    			   ->paginate(15);
    }

    public static function getCuentas_string(){
        return CuentasBancarias::select('id',DB::raw("CONCAT_WS(' ',banco,cuenta_bancaria,moneda) as string"))
                                 ->lists('string','id');

    }
}
