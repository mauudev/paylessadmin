<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;
use PaylessAdmin\Proveedor;
class TipoProveedor extends Model
{
    protected $table = 'tipo_proveedores';
    protected $fillable = ['tipo'];

	public function proveedores(){
		return $this->hasMany('Proveedor');
	}

	public static function getTipoProveedor($id){
      return TipoProveedor::where('id','=',$id)->get();
    }
}
