<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;

class DetallePagoDeposito extends Model
{
    protected $table = 'detalle_pagos_deposito';
    protected $fillable = ['ventas_confirmadas_id',
    					             'cuentas_bancarias_id',
                           'detalle',
                           'monto',
                           'moneda',
                           'codigo_recibo'];

    public static function getAll(){
    	return DB::table('detalle_pagos_deposito')
    			   ->select('id',
        			   			'ventas_confirmadas_id',
        			   			'cuentas_bancarias_id',
                     	'detalle',
                      'monto',
                      'moneda',
                      'codigo_recibo',
        			   			'created_at',
        			   			'updated_at')
             ->orderBy('detalle_pagos_deposito.updated_at','desc')
    	     ->paginate(15);
    }
}
