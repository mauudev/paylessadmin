<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;

class DetallePagoEfectivo extends Model
{
    protected $table = 'detalle_pagos_efectivo';
    protected $fillable = ['ventas_confirmadas_id',
                           'detalle',
                           'monto',
                           'moneda',
                           'codigo_recibo'];

    public static function getAll(){
    	return DB::table('detalle_pagos_efectivo')
    			   ->select('id',
    			   			    'ventas_confirmadas_id',
                     	'detalle',
                      'monto',
                      'moneda',
                      'codigo_recibo',
        			   			'created_at',
        			   			'updated_at')
             ->orderBy('detalle_pagos_efectivo.updated_at','desc')
    	     ->paginate(15);
    }
}
