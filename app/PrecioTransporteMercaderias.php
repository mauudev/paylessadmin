<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;
use PaylessAdmin\PrecioTransporteMercaderias;
use DB;

class PrecioTransporteMercaderias extends Model
{
    protected $table = 'precio_transporte_mercaderias';
    protected $fillable = ['transporte',
                           'mercaderias_id'];

    public static function getAll(){
    	return DB::table('precio_transporte_mercaderias')
    				->select('precio_transporte_mercaderias.id',
    						 'precio_transporte_mercaderias.transporte',
    						 'precio_transporte_mercaderias.mercaderias_id','mercaderias.nombre_m',
    						 'mercaderias.precio_venta_m','mercaderias.nro_item')
    				->join('mercaderias','precio_transporte_mercaderias.mercaderias_id','=','mercaderias.id')
                    ->orderBy('mercaderias.nombre_m')
    				->paginate(15);
    }
    public static function getAll_mercaderias_sin_precio(){
    	  $con_precios = PrecioTransporteMercaderias::select('precio_transporte_mercaderias.mercaderias_id')
       			  								->join('mercaderias','precio_transporte_mercaderias.mercaderias_id','=','mercaderias.id')->get();
       	$sin_precios = Mercaderia::whereNotIn('id',$con_precios)->orderBy('nombre_m')->distinct()->get();
        return $sin_precios;
    }
    public static function getPrecio_mercaderia($id){
    	return PrecioTransporteMercaderias::select('precio_transporte_mercaderias.id',
    											 'precio_transporte_mercaderias.transporte',
    											 'precio_transporte_mercaderias.mercaderias_id',
    											 'mercaderias.nombre_m','mercaderias.precio_venta_m',
    											 'mercaderias.nro_item')
    									->join('mercaderias','precio_transporte_mercaderias.mercaderias_id','=','mercaderias.id')
    									->where('precio_transporte_mercaderias.id','=',$id)->first();
    }

    public static function scopeSearch($query,$search){
        if("" != trim($search)){
          $target = trim($search);
          return DB::table('precio_transporte_mercaderias') 
                 ->join('mercaderias','precio_transporte_mercaderias.mercaderias_id','=','mercaderias.id')
                 ->select('precio_transporte_mercaderias.id','precio_transporte_mercaderias.transporte',
                          'mercaderias.nombre_m','mercaderias.precio_venta_m','mercaderias.nro_item')
                 ->orWhere(function ($query) use ($search) {
                    $query->where("precio_transporte_mercaderias.transporte","rlike",$search)
                          ->orWhere("mercaderias.nombre_m","rlike",$search)
                          ->orWhere("mercaderias.precio_venta_m","rlike",$search)
                          ->orWhere("mercaderias.nro_item","rlike",$search);
                    })
                 ->orderBy('precio_transporte_mercaderias.updated_at','desc')->distinct();
        } 
    }

    public static function getMercaderias_precios(){
      return DB::table('precio_transporte_mercaderias') 
                 ->join('mercaderias','precio_transporte_mercaderias.mercaderias_id','=','mercaderias.id')
                 ->select('precio_transporte_mercaderias.id','precio_transporte_mercaderias.transporte',
                          'mercaderias.nombre_m','mercaderias.precio_venta_m','mercaderias.nro_item')
                 ->orderBy('mercaderias.nombre_m','desc')->get();
    }
}

