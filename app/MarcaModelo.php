<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;
use DB;
class MarcaModelo extends Model
{
    protected $table = 'marcas_modelos';
	  protected $fillable = [
	        'marca',
	        'modelo'
	    ];

    public static function scopeSearch($query,$search){
        if("" != trim($search)){
          $target = trim($search);
          return DB::table('marcas_modelos') 
                 ->select('marcas_modelos.id','marcas_modelos.marca','marcas_modelos.modelo')
                 ->orWhere(function ($query) use ($search) {
                    $query->where("marcas_modelos.marca","rlike",$search)
                          ->orWhere("marcas_modelos.modelo","rlike",$search);
                    })
                 ->orderBy('marcas_modelos.marca','desc')->distinct();
        } 
    }

    public static function getModelosPorMarca($marca){
      return MarcaModelo::select("modelo")->where("marca","rlike",$marca)->distinct()->orderBy("modelo","asc")->get();
    }

    public static function getMarcas(){
      return MarcaModelo::select('id','marca')->distinct()->orderBy('marca','asc')->get();
    }

    public static function getMarcas_except($marca){
      return MarcaModelo::select('marca')->where('marca','<>',$marca)->distinct()->orderBy('marca','asc')->get();
    }

    public static function getModelos_except($marca,$modelo){
      return MarcaModelo::select('modelo')->where('marca','=',$marca)->where('modelo','<>',$modelo)->distinct()->orderBy('modelo','asc')->get();
    }
}
