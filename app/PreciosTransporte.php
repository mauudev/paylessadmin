<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;
use DB;
class PreciosTransporte extends Model
{
    protected $table = 'precios_transporte';
    protected $fillable = ['descripcion',
                           'precio'];

    public static function getAll(){
    	return DB::table('precios_transporte')
    			->select('precios_transporte.id','precios_transporte.descripcion',
    			   		 'precios_transporte.precio')
                ->orderBy('precios_transporte.created_at','desc')
		        ->paginate(15);
    }

    public static function scopeSearch($query,$search){
        if("" != trim($search)){
          $target = trim($search);
          return DB::table('precios_transporte') 
                 ->select('precios_transporte.id','precios_transporte.descripcion',
                        'precios_transporte.precio')
                 ->orWhere(function ($query) use ($search) {
                    $query->where("precios_transporte.descripcion","rlike",$search)
                          ->orWhere("precios_transporte.precio","rlike",$search);
                    })
                 ->orderBy('precios_transporte.updated_at','desc')->distinct();
        } 
    }

    public static function getPrecios_transporte(){
      return DB::table('precios_transporte') 
                 ->select('precios_transporte.id','precios_transporte.descripcion',
                          'precios_transporte.precio')
                 ->orderBy('precios_transporte.descripcion','desc')->get();
    }
}
