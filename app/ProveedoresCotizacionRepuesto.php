<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;
use PaylessAdmin\Venta;
use DB;
class ProveedoresCotizacionRepuesto extends Model
{
    protected $table = 'proveedores_cotizacion_rep';
    protected $fillable = [
    					   'precio_cot_rep',
    					   'ventas_id',
    					   'repuestos_id',
    					   'proveedores_id',
    					   ];	
    public static function getProveedores_cotizacion($repuestos_id,$ventas_id){
    	return DB::table('proveedores_cotizacion_rep')
    			         ->join('proveedores','proveedores_cotizacion_rep.proveedores_id','=','proveedores.id')
                   ->join('ventas','proveedores_cotizacion_rep.ventas_id','=','ventas.id')
                   ->join('repuestos','proveedores_cotizacion_rep.repuestos_id','=','repuestos.id')
                   ->join('tipo_proveedores','proveedores.tipo_proveedores_id','=','tipo_proveedores.id')
    			         ->select('proveedores_cotizacion_rep.id','proveedores.id as proveedores_id','ventas.id as ventas_id','repuestos.id as repuestos_id','tipo_proveedores.tipo','proveedores.nombre_compania_p',
                      'proveedores.persona_contacto_p','proveedores.telefono_p','proveedores.celular_p','proveedores_cotizacion_rep.precio_cot_rep')
    			         ->where('proveedores_cotizacion_rep.repuestos_id','=',$repuestos_id)
                   ->where('proveedores_cotizacion_rep.ventas_id','=',$ventas_id)
    			         ->get();
    }
    public static function getProveedores_cotizacion_all($cotizacion_repuestos_id){
      return DB::table('precio_venta_rep')
             ->select('precio_venta_rep.id as precio_venta_rep_id','precio_venta_rep.transporte','precio_venta_rep.adicional','precio_venta_rep.precio_total','proveedores.persona_contacto_p','proveedores.nombre_compania_p','proveedores_cotizacion_rep.proveedores_id','proveedores_cotizacion_rep.precio_cot_rep')
             ->join('cotizacion_repuestos','precio_venta_rep.cotizacion_repuestos_id','=','cotizacion_repuestos.id')
             ->join('proveedores_cotizacion_rep','precio_venta_rep.proveedores_cotizacion_rep_id','=','proveedores_cotizacion_rep.id')
             ->join('proveedores','precio_venta_rep.proveedores_id','=','proveedores.id')     
             ->where('precio_venta_rep.cotizacion_repuestos_id','=',$cotizacion_repuestos_id)
             ->distinct()->get();
    }


    public static function getProveedores_cotizacion1($id){
      return DB::select("SELECT tp.id as tipos_p_id, tp.tipo, p.id as proveedores_id,
      							p.nombre_compania_p, p.persona_contacto_p, p.telefono_p, 
      							p.celular_p, pc.precio_cot_rep, pc.id as proveedores_cot_id
                         FROM proveedores_cotizacion_rep pc, tipo_proveedores tp, proveedores p
                         WHERE pc.proveedores_id = p.id AND p.tipo_proveedores_id = tp.id AND pc.repuestos_id =".$id);        
    }
    public static function getProveedoresCotizacionId($ventas_id, $repuestos_id, $proveedores_id){
      return DB::table('proveedores_cotizacion_rep')
             ->join('proveedores','proveedores_cotizacion_rep.proveedores_id','=','proveedores.id')
             ->join('ventas','proveedores_cotizacion_rep.ventas_id','=','ventas.id')
             ->join('repuestos','proveedores_cotizacion_rep.repuestos_id','=','repuestos.id')
             ->select('proveedores_cotizacion_rep.id','precio_cot_rep')
             ->where('proveedores_cotizacion_rep.ventas_id','=',$ventas_id)
             ->where('proveedores_cotizacion_rep.repuestos_id','=',$repuestos_id)
             ->where('proveedores_cotizacion_rep.proveedores_id','=',$proveedores_id)
             ->first();
    }
}
