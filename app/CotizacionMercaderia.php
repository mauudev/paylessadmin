<?php

namespace PaylessAdmin;

use Illuminate\Database\Eloquent\Model;
use DB;
class CotizacionMercaderia extends Model
{
    protected $table = 'cotizacion_mercaderias';
    protected $fillable = ['cantidad','precio_venta_cot',
              					   'ventas_id',
              					   'mercaderias_id'];

    public static function getAll(){
    	return DB::table('cotizacion_mercaderias')
    			   ->select('cotizacion_mercaderias.*')
    			   ->paginate(15);
    }
    public static function getMercaderias_cotizacion($id){
        return DB::table('cotizacion_mercaderias')
                   ->join('mercaderias','cotizacion_mercaderias.mercaderias_id','=','mercaderias.id')
                   ->join('ventas','cotizacion_mercaderias.ventas_id','=','ventas.id')
                   ->select('cotizacion_mercaderias.id as id_cot','cotizacion_mercaderias.cantidad',
                            'mercaderias.id as mercaderia_id','cotizacion_mercaderias.estado','mercaderias.nombre_m','mercaderias.precio_venta_m','ventas.id as venta_id','mercaderias.nro_item')
                   ->where('cotizacion_mercaderias.ventas_id','=',$id)
                   ->get();
    }
    public static function getMercaderia_cotizacion($id){
        return DB::table('cotizacion_mercaderias')
                   ->join('mercaderias','cotizacion_mercaderias.mercaderias_id','=','mercaderias.id')
                   ->join('ventas','cotizacion_mercaderias.ventas_id','=','ventas.id')
                   ->select('cotizacion_mercaderias.id as id_cot','cotizacion_mercaderias.cantidad',
                    'mercaderias.id as mercaderia_id','mercaderias.nombre_m','mercaderias.precio_venta_m','ventas.id as venta_id','mercaderias.nro_item')
                   ->where('cotizacion_mercaderias.mercaderias_id','=',$id)
                   ->first();
    }
    public static function getCantidad_proveedores_mer($id){
      return DB::select("SELECT DISTINCT COUNT(pm.id) as count
                         FROM ventas v, mercaderias r, proveedores_cotizacion_mer pm 
                         WHERE pm.ventas_id = v.id AND pm.mercaderias_id = r.id AND pm.mercaderias_id = ".$id);
    }

    public static function getMercaderias_cotizacionEstado($id,$estado){
        return DB::table('cotizacion_mercaderias')
                   ->join('mercaderias','cotizacion_mercaderias.mercaderias_id','=','mercaderias.id')
                   ->join('ventas','cotizacion_mercaderias.ventas_id','=','ventas.id')
                   ->select('cotizacion_mercaderias.id')
                   ->where('cotizacion_mercaderias.ventas_id','=',$id)
                   ->where('cotizacion_mercaderias.estado','=',$estado)
                   ->get();
    }
    public static function getCotizacionId($mercaderias_id,$ventas_id){
      return DB::table('cotizacion_mercaderias')
                 ->select('cotizacion_mercaderias.id')
                 ->join('ventas','cotizacion_mercaderias.ventas_id','=','ventas.id')
                 ->where('cotizacion_mercaderias.mercaderias_id','=',$mercaderias_id)
                 ->where('cotizacion_mercaderias.ventas_id','=',$ventas_id)
                 ->get(); 
    }
    public static function getCotizacionId1($mercaderias_id,$ventas_id){
      return CotizacionMercaderia::select('cotizacion_mercaderias.id')
                 ->join('ventas','cotizacion_mercaderias.ventas_id','=','ventas.id')
                 ->where('cotizacion_mercaderias.mercaderias_id','=',$mercaderias_id)
                 ->where('cotizacion_mercaderias.ventas_id','=',$ventas_id)
                 ->first(); 
    }
    public static function getVentaDellate_mer($id){
        return DB::table('cotizacion_mercaderias')
                   ->join('mercaderias','cotizacion_mercaderias.mercaderias_id','=','mercaderias.id')
                   ->join('precio_venta_mer','precio_venta_mer.cotizacion_mercaderias_id','=','cotizacion_mercaderias.id')
                   ->join('proveedores','proveedores.id','=','precio_venta_mer.proveedores_id')
                   ->join('tipo_proveedores','tipo_proveedores.id','=','proveedores.tipo_proveedores_id')
                   ->select('cotizacion_mercaderias.id as id_cot','cotizacion_mercaderias.cantidad','mercaderias.nombre_m','mercaderias.nro_item','precio_venta_mer.precio_total','tipo_proveedores.tipo')
                   ->where('cotizacion_mercaderias.id','=',$id)
                   ->first();
  }
}
