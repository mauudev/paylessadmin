var check_telf = 0
var check_cel = 0
$(document).ready(function() {
		$('#celular_c1').blur(function(e){
			e.preventDefault();
		$('#checkCel1').html('<a class="btn btn-accent btn-rounded"><i class="fa fa-spinner fa-spin"></i></a>').fadeOut(2000);
		var celular_data = $(this).val();
	         $.ajax({
		       type: 'GET',
		       url: "/administracion/clientes/check/celular/"+celular_data,
		       dataType: "json", //add this line
		       success: function(data){ //only the data object passed to success handler with JSON dataType
				  var resp = data.response.length.toString()
		          if(resp > 0){
		          	document.getElementById('alertCel1').className = "form-group has-error"
		          	toastr.options = {
		                        "closeButton": true,
		                        "debug": false,
		                        "newestOnTop": false,
		                        "progressBar": false,
		                        "positionClass": "toast-top-center",
		                        "preventDuplicates": true,
		                        "onclick": null,
		                        "showDuration": "300",
		                        "hideDuration": "1000",
		                        "timeOut": "5000",
		                        "extendedTimeOut": "1000",
		                        "showEasing": "swing",
		                        "hideEasing": "linear",
		                        "showMethod": "fadeIn",
		                        "hideMethod": "fadeOut"
		                      };
		        	toastr.error('<b>Error !</b> Ya existe un cliente con este n&uacute;mero de celular PENDEJO');
		        	check_cel = 1
		      	  }else{
		      	  	document.getElementById('alertCel1').className = "form-group has-success"
		      	  	check_cel = 0
		      	  }	
		       }
		   });
		});
	});
	// $(document).ready(function() {
	// 	$('#telefono_c1').blur(function(e){
	// 		//alert($(this).val())
	// 		e.preventDefault();
	// 	$('#checkTelf1').html('<a class="btn btn-accent btn-rounded"><i class="fa fa-spinner fa-spin"></i></a>').fadeOut(2000);
	// 	var telefono_data = $(this).val();
	//          $.ajax({
	// 	       type: 'GET',
	// 	       url: "/administracion/clientes/check/telefono/"+telefono_data,
	// 	       dataType: "json", //add this line
	// 	       success: function(data){ //only the data object passed to success handler with JSON dataType
	// 			  var resp = data.response.length.toString()
	// 	          if(resp > 0){
	// 	          	document.getElementById('alertTelf1').className = "form-group has-error"
	// 	          	toastr.options = {
	// 	                        "closeButton": true,
	// 	                        "debug": false,
	// 	                        "newestOnTop": false,
	// 	                        "progressBar": false,
	// 	                        "positionClass": "toast-top-center",
	// 	                        "preventDuplicates": true,
	// 	                        "onclick": null,
	// 	                        "showDuration": "300",
	// 	                        "hideDuration": "1000",
	// 	                        "timeOut": "5000",
	// 	                        "extendedTimeOut": "1000",
	// 	                        "showEasing": "swing",
	// 	                        "hideEasing": "linear",
	// 	                        "showMethod": "fadeIn",
	// 	                        "hideMethod": "fadeOut"
	// 	                      };
	// 	        	toastr.error('<b>Error !</b> Ya existe un cliente con este n&uacute;mero de tel&eacute;fono');
	// 	        	check_telf = 1
	// 	      	  }else{
	// 	      	  	document.getElementById('alertTelf1').className = "form-group has-success"
	// 	      	  	check_telf = 0
	// 	      	  }	
	// 	       }
	// 	   });
	// 	});
	// });

$(document).ready(function() {
		$('#celular_c1_edit').blur(function(e){
			e.preventDefault();
		$('#checkCel1_edit').html('<a class="btn btn-accent btn-rounded"><i class="fa fa-spinner fa-spin"></i></a>').fadeOut(2000);
		var celular_data = $(this).val();
		var cliente_id = document.getElementById('clientes_id')
	         $.ajax({
		       type: 'GET',
		       url: "/administracion/clientes/"+cliente_id.value+"/check/celular/"+celular_data+"/edit",
		       dataType: "json", //add this line
		       success: function(data){ //only the data object passed to success handler with JSON dataType
				  var resp = data.response.length.toString()
		          if(resp > 0){
		          	document.getElementById('alertCel1_edit').className = "form-group has-error"
		          	toastr.options = {
		                        "closeButton": true,
		                        "debug": false,
		                        "newestOnTop": false,
		                        "progressBar": false,
		                        "positionClass": "toast-top-center",
		                        "preventDuplicates": true,
		                        "onclick": null,
		                        "showDuration": "300",
		                        "hideDuration": "1000",
		                        "timeOut": "5000",
		                        "extendedTimeOut": "1000",
		                        "showEasing": "swing",
		                        "hideEasing": "linear",
		                        "showMethod": "fadeIn",
		                        "hideMethod": "fadeOut"
		                      };
		        	toastr.error('<b>Error !</b> Ya existe un cliente con este n&uacute;mero de celular');
		        	check_cel = 1
		      	  }else{
		      	  	document.getElementById('alertCel1_edit').className = "form-group has-success"
		      	  	check_cel = 0
		      	  }	
		       }
		   });
		});
	});
	// $(document).ready(function() {
	// 	$('#telefono_c1_edit').blur(function(e){
	// 		e.preventDefault();
	// 	$('#checkTelf1_edit').html('<a class="btn btn-accent btn-rounded"><i class="fa fa-spinner fa-spin"></i></a>').fadeOut(2000);
	// 	var telefono_data = $(this).val();
	// 	var cliente_id = document.getElementById('cliente_id1')
	//          $.ajax({
	// 	       type: 'GET',
	// 	       url: "/administracion/clientes/"+cliente_id.value+"/check/telefono/"+telefono_data+"/edit",
	// 	       dataType: "json", //add this line
	// 	       success: function(data){ //only the data object passed to success handler with JSON dataType
	// 			  var resp = data.response.length.toString()
	// 			  console.log()
	// 	          if(resp > 0){
	// 	          	document.getElementById('alertTelf1_edit').className = "form-group has-error"
	// 	          	toastr.options = {
	// 	                        "closeButton": true,
	// 	                        "debug": false,
	// 	                        "newestOnTop": false,
	// 	                        "progressBar": false,
	// 	                        "positionClass": "toast-top-center",
	// 	                        "preventDuplicates": true,
	// 	                        "onclick": null,
	// 	                        "showDuration": "300",
	// 	                        "hideDuration": "1000",
	// 	                        "timeOut": "5000",
	// 	                        "extendedTimeOut": "1000",
	// 	                        "showEasing": "swing",
	// 	                        "hideEasing": "linear",
	// 	                        "showMethod": "fadeIn",
	// 	                        "hideMethod": "fadeOut"
	// 	                      };
	// 	        	toastr.error('<b>Error !</b> Ya existe un cliente con este n&uacute;mero de tel&eacute;fono');
	// 	        	check_telf = 1
	// 	      	  }else{
	// 	      	  	document.getElementById('alertTelf1_edit').className = "form-group has-success"
	// 	      	  	check_telf = 0
	// 	      	  }	
	// 	       }
	// 	   });
	// 	});
	// });
function showMessage(message){
	toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": false,
                    "progressBar": false,
                    "positionClass": "toast-top-center",
                    "preventDuplicates": true,
                    "onclick": null,
                    "showDuration": "300",
                    "hideDuration": "1000",
                    "timeOut": "5000",
                    "extendedTimeOut": "1000",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut"
		             };
	toastr.error(message);
}
var statSend = false;
function checkSubmit() {
	    if (!statSend) {
	        statSend = true;
	        return true;
	    } else {
	        toastr.options = {
		                        "closeButton": true,
		                        "debug": false,
		                        "newestOnTop": false,
		                        "progressBar": false,
		                        "positionClass": "toast-top-center",
		                        "preventDuplicates": true,
		                        "onclick": null,
		                        "showDuration": "300",
		                        "hideDuration": "1000",
		                        "timeOut": "5000",
		                        "extendedTimeOut": "1000",
		                        "showEasing": "swing",
		                        "hideEasing": "linear",
		                        "showMethod": "fadeIn",
		                        "hideMethod": "fadeOut"
		                      };
		    toastr.info('Los datos del formulario se est&aacute;n enviando...');
	        return false;
	    }
}

function checkValidation(){
	if(check_telf == 1 && check_cel == 1){
		showMessage("<b>Error !</b> Existe un cliente asociado al tel&eacute;fono y celular ingresados, verifique los datos")
		return false;
	}
	if(check_telf == 1 && check_cel == 0){
		showMessage("<b>Error !</b> Existe un cliente asociado al tel&eacute;fono ingresado, verifique los datos")
		return false;
	}
	if(check_telf == 0 && check_cel == 1) {
		showMessage("<b>Error !</b> Existe un cliente asociado al celular ingresado, verifique los datos ")
		return false;
	}
	return true;
}

function validateForm(){
  var validation = true;
  validation = checkValidation();
  if(validation == false) return validation
  validation = checkSubmit();
  if(validation == false) return validation
  return validation;
}

$(document).ready(function(){
	$("#btnClear1").on('click',function(){
		document.getElementById('formUsers1').reset()	
	});
});	
