
<div class="row">

	<div class="col-md-12">

		<div class="col-lg-1">
			<div class="form-group">
				<?php echo Form::label('remitente','De: ',['class'=>'control-label']); ?>

			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
				<?php echo Form::select('remitente',["es1"=>"info@paylessimport.com","es2"=>"cotizaciones@paylessimport.com","en1"=>"sales@paylessgroupusa.com"],null,['class'=>'form-control',"required",'placeholder'=>'Seleccione un correo..',"id"=>"rmt1"]); ?><br/>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="col-lg-1">
			<div class="form-group">
				<?php echo Form::label('receptor','Para: ',['class'=>'control-label']); ?>

			</div>
		</div>
		<?php if($email_c == "sin-correo"): ?>
		<div class="col-lg-6">
			<div class="form-group">
				<?php echo Form::text('receptor',null,['class'=>'form-control','required','min'=>5,'placeholder'=>'Este cliente no tiene una cuenta de correo electr&oacute;nico registrada.']); ?><br/>
			</div>
		</div>
		<?php else: ?>
		<div class="col-lg-6">
			<div class="form-group">
				<?php echo Form::text('receptor',$email_c,['class'=>'form-control','required','min'=>5]); ?><br/>
			</div>
		</div>
		<?php endif; ?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="col-lg-1">
			<div class="form-group">
				<?php echo Form::label('asunto','Asunto: ',['class'=>'control-label']); ?>

			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
				<?php echo Form::text('asunto',null,['class'=>'form-control','required','min'=>5,"id"=>"asuntoField1"]); ?><br/>
			</div>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="col-lg-1">
			<div class="form-group">
				<?php echo Form::label('mensaje','Mensaje: ',['class'=>'control-label']); ?>

			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
				<?php echo Form::textarea('mensaje',null,['class'=>'form-control','required','rows'=>5]); ?><br/>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$("#rmt1").on('change',function(){
			codigo_venta = <?php echo json_encode($venta->id); ?>

			if(codigo_venta < 10)
				codigo_venta = "00"+codigo_venta
			if(codigo_venta > 10 && codigo_venta < 100)
				codigo_venta = "0"+codigo_venta	
			correo = $("#rmt1").val()
			if(correo == 'es1'){
				asunto = "Cotización #"+codigo_venta
				$("#asuntoField1").val(asunto)
			}
			if(correo == 'es2'){
				asunto = "Cotización #"+codigo_venta
				$("#asuntoField1").val(asunto)
			}
			if(correo == 'en1'){
				asunto = "Quote #"+codigo_venta
				$("#asuntoField1").val(asunto)
			}
		});
	});
</script>