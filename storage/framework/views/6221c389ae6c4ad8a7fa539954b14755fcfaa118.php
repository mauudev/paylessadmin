<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-filled">
			<div class="panel-heading">
				<div class="panel-tools">
					<div class="col-md-12">
					<?php echo Form::open(['route'=>'ventas.index','method'=>'GET','class'=>'navbar-form navbar-left pull-right','role'=>'search']); ?>

					<div class="form-group">
						<?php echo Form::text('search',null,['class'=>'form-control','placeholder'=>'Buscar..']); ?><br/>
					</div>
					<button type="submit" class="btn btn-accent">Buscar</button>
					<?php echo Form::close(); ?>

					</div>
				</div>
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-note2"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Ventas registradas</h3>
						<small>
						Cotizaci&oacute;n de repuestos/mercader&iacute;as
						</small>
					</div>
				</div>
			</div>
			<div class="panel-body">			
			<?php if(isset($ventas)): ?>
				<?php if(count($ventas) > 0): ?>
				<div class="table-responsive">
					<table class="table table-hover table-striped">
						<thead>
							<tr role="row">
								<th class="sorting_asc left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
									aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" style="width: 50px;">ID
								</th>
								<th class="sorting_asc left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
									aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" style="width: 50px;">Fecha
								</th>
								<th class="sorting_asc left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
									aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" style="width: 100px;">Nombre
								</th>
								<th class="sorting_asc left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
									aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" style="width: 122px;">Apellidos
								</th>
								<th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
									aria-label="Browser: activate to sort column ascending" style="width: 60px;">Estado
								</th>
								<th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
									aria-label="Engine version: activate to sort column ascending" style="width: 85px;">Responsable
								</th>
								<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
									aria-label="CSS grade: activate to sort column ascending" style="width: 100px;">Acci&oacute;n
								</th>
							</tr>
						</thead>
						<tbody id="resultados1">
							<?php if(isset($ventas)): ?>
								<?php foreach($ventas as $venta): ?>
								<tr>
									<td data-toggle="modal" data-target="#myModal" data-id="<?php echo e($venta->id); ?>"><?php echo e($venta->id); ?>

									<?php echo e(Form::hidden('nombre_c',$venta->nombre_c,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('apellidos_c',$venta->apellidos_c,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('nombre_u',$venta->nombre_u,['class'=>'dataVenta'])); ?><?php echo e(Form::hidden('apellidos_u',$venta->apellidos_u,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('marca_r',$venta->marca_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('modelo_r',$venta->modelo_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('anio_r',$venta->anio_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('detalle_r',$venta->detalle_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('nombre_m',$venta->nombre_m,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('created_at',$venta->created_at,['class'=>'dataVenta'])); ?>

									</td>
									<td data-toggle="modal" data-target="#myModal" data-id="<?php echo e($venta->id); ?>"><?php echo e($venta->created_at); ?>

									<?php echo e(Form::hidden('nombre_c',$venta->nombre_c,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('apellidos_c',$venta->apellidos_c,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('nombre_u',$venta->nombre_u,['class'=>'dataVenta'])); ?><?php echo e(Form::hidden('apellidos_u',$venta->apellidos_u,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('marca_r',$venta->marca_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('modelo_r',$venta->modelo_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('anio_r',$venta->anio_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('detalle_r',$venta->detalle_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('nombre_m',$venta->nombre_m,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('created_at',$venta->created_at,['class'=>'dataVenta'])); ?></td>
									<td data-toggle="modal" data-target="#myModal" data-id="<?php echo e($venta->id); ?>"><?php echo e(str_limit($venta->nombre_c,$limit = 25,$end='...')); ?>

									<?php echo e(Form::hidden('nombre_c',$venta->nombre_c,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('apellidos_c',$venta->apellidos_c,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('nombre_u',$venta->nombre_u,['class'=>'dataVenta'])); ?><?php echo e(Form::hidden('apellidos_u',$venta->apellidos_u,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('marca_r',$venta->marca_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('modelo_r',$venta->modelo_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('anio_r',$venta->anio_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('detalle_r',$venta->detalle_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('nombre_m',$venta->nombre_m,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('created_at',$venta->created_at,['class'=>'dataVenta'])); ?></td>
									<td data-toggle="modal" data-target="#myModal" data-id="<?php echo e($venta->id); ?>"><?php echo e(str_limit($venta->apellidos_c,$limit = 25,$end='...')); ?>

									<?php echo e(Form::hidden('nombre_c',$venta->nombre_c,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('apellidos_c',$venta->apellidos_c,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('nombre_u',$venta->nombre_u,['class'=>'dataVenta'])); ?><?php echo e(Form::hidden('apellidos_u',$venta->apellidos_u,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('marca_r',$venta->marca_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('modelo_r',$venta->modelo_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('anio_r',$venta->anio_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('detalle_r',$venta->detalle_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('nombre_m',$venta->nombre_m,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('created_at',$venta->created_at,['class'=>'dataVenta'])); ?></td>
									<?php if($venta->pago_parcial != null && $venta->segundo_pago != null): ?>
										<td data-toggle="modal" data-target="#myModal" data-id="<?php echo e($venta->id); ?>"><b><p class="text-success">COMPLETADO</p></b>
									<?php echo e(Form::hidden('nombre_c',$venta->nombre_c,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('apellidos_c',$venta->apellidos_c,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('nombre_u',$venta->nombre_u,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('apellidos_u',$venta->apellidos_u,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('marca_r',$venta->marca_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('modelo_r',$venta->modelo_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('anio_r',$venta->anio_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('detalle_r',$venta->detalle_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('nombre_m',$venta->nombre_m,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('created_at',$venta->created_at,['class'=>'dataVenta'])); ?></td>
									<?php endif; ?>
									<?php if($venta->pago_parcial != null && $venta->segundo_pago == null): ?>
										<td data-toggle="modal" data-target="#myModal" data-id="<?php echo e($venta->id); ?>"><b><p class="text-accent">UN SOLO PAGO</p></b>
									<?php echo e(Form::hidden('nombre_c',$venta->nombre_c,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('apellidos_c',$venta->apellidos_c,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('nombre_u',$venta->nombre_u,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('apellidos_u',$venta->apellidos_u,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('marca_r',$venta->marca_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('modelo_r',$venta->modelo_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('anio_r',$venta->anio_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('detalle_r',$venta->detalle_r,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('nombre_m',$venta->nombre_m,['class'=>'dataVenta'])); ?>

									<?php echo e(Form::hidden('created_at',$venta->created_at,['class'=>'dataVenta'])); ?></td>
									<?php endif; ?>
									<td data-toggle="modal" data-target="#myModal" data-id="<?php echo e($venta->id); ?>">
										<?php echo e(str_limit($venta->nombre_u.' '.$venta->apellidos_u,$limit = 40,$end='...')); ?>

										<?php echo e(Form::hidden('nombre_c',$venta->nombre_c,['class'=>'dataVenta'])); ?>

										<?php echo e(Form::hidden('apellidos_c',$venta->apellidos_c,['class'=>'dataVenta'])); ?>

										<?php echo e(Form::hidden('nombre_u',$venta->nombre_u,['class'=>'dataVenta'])); ?><?php echo e(Form::hidden('apellidos_u',$venta->apellidos_u,['class'=>'dataVenta'])); ?>

										<?php echo e(Form::hidden('marca_r',$venta->marca_r,['class'=>'dataVenta'])); ?>

										<?php echo e(Form::hidden('modelo_r',$venta->modelo_r,['class'=>'dataVenta'])); ?>

										<?php echo e(Form::hidden('anio_r',$venta->anio_r,['class'=>'dataVenta'])); ?>

										<?php echo e(Form::hidden('detalle_r',$venta->detalle_r,['class'=>'dataVenta'])); ?>

										<?php echo e(Form::hidden('nombre_m',$venta->nombre_m,['class'=>'dataVenta'])); ?>

										<?php echo e(Form::hidden('created_at',$venta->created_at,['class'=>'dataVenta'])); ?>

									</td>
									<?php if($venta->estado == 1): ?>
										<td align="center"><a href="<?php echo e(URL::to('/ventas/'. $venta->id.'/detalle' )); ?>" class="btn btn-w-md btn-info center-block" data-tooltip="Detalle de la venta"><i class="fa fa-eye"></i> Ver detalle</a></td>
									<?php endif; ?>
								</tr>
									<?php endforeach; ?>
							<?php endif; ?>
							</tbody>
						</table>
						<?php echo e($ventas->render()); ?>

						<?php echo $__env->make('alerts.success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					</div>
				<?php else: ?>
				<div class="alert alert-danger alert-dismissable">
					<strong>No se encontraron resultados</strong>
				</div>
				<?php endif; ?>
			<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header center-block">
			
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title text-center">Informaci&oacute;n r&aacute;pida de la venta</h4>
		</div>
		<div class="modal-body" id="infoVenta">
			<!-- CONTENIDO DEL MODAL -->
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
	$("#editBlocked1").on('click',function(){
	toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-center",
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      };
        	toastr.error('<b>Error!</b> Debe deshabilitar la venta para poder editar, haga click en <b>"Ver detalle"</b> y deshabilite la venta');
	});
	$('#myModal').on('show.bs.modal', function (event) {
		    //var getIdFromRow = $(event.relatedTarget).attr('data-id');
		    // if you wnat to take the text of the first cell  
		    //var compania = $(event.relatedTarget).find('td:nth-child(1)').html();
		    //var contacto = $(event.relatedTarget).find('td:nth-child(2)').text();
		    var data = Array()
		    $(event.relatedTarget).find(".dataVenta").each(function(i) {
		        //console.log(String(this.value))
		        data[i] += String(this.value)
	    }); 
		marca = (data[4].replace('undefined','') == "") ? "Sin repuestos" : data[4].replace('undefined','')
		modelo = (data[5].replace('undefined','') == "") ? "Sin repuestos" : data[5].replace('undefined','')
		anio = (data[6].replace('undefined','') == "") ? "Sin repuestos" : data[6].replace('undefined','')
		detalles = (data[7].replace('undefined','') == "") ? "Sin repuestos" : data[7].replace('undefined','')
		nombre_m = (data[8].replace('undefined','') == "") ? "Sin mercaderias" : data[8].replace('undefined','') 
	    var template2 = '<div class="row"><div class="col-md-5"><strong class="c-white">Nombre cliente: </strong></div><div class="col-sm-7">'+data[0].replace('undefined','')+' '+data[1].replace('undefined','')+'</div></div><div class="row"><div class="col-md-5"><strong class="c-white">Nombre responsable: </strong></div><div class="col-md-7">'+data[2].replace('undefined','')+' '+data[3].replace('undefined','')+'</div></div><div class="row"><div class="col-md-5"><strong class="c-white">Marcas de los repuestos: </strong></div><div class="col-md-7">'+marca+'</div></div><div class="row"><div class="col-md-5"><strong class="c-white">Modelos de los repuestos: </strong></div><div class="col-md-7">'+modelo+'</div></div><div class="row"><div class="col-md-5"><strong class="c-white">Año de los repuestos: </strong></div><div class="col-md-7">'+anio+'</div></div><div class="row"><div class="col-md-5"><strong class="c-white">Detalles de los repuestos: </strong></div><div class="col-md-7">'+detalles+'</div></div><div class="row"><div class="col-md-5"><strong class="c-white">Productos varios: </strong></div><div class="col-md-7">'+nombre_m+'</div></div><div class="row"><div class="col-md-5"><strong class="c-white">Fecha de la cotizaci&oacute;n: </strong></div><div class="col-md-7">'+data[9].replace('undefined','')+'</div></div>'

	    $(this).find('#infoVenta').html($(template2))
	})
//13092 lineas
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>