<div class="row">
	<div class="col-lg-2">
		<div class="form-group">
			<?php echo Form::hidden('usuarios_id', Auth::user()->id); ?>

			<?php echo Form::label('cliente','Cliente nuevo ',['class'=>'control-label']); ?>&nbsp;
			<?php echo e(Form::radio("cliente","Nuevo", false, ["class" => "radioButton1","id"=>"radioForm1","required","checked"])); ?>

		</div>
	</div>
	<div class="col-lg-2">
		<div class="form-group">
			<?php echo Form::label('cliente','Cliente antiguo ',['class'=>'control-label']); ?>&nbsp;
			<?php echo e(Form::radio("cliente","Antiguo", false, ["class" => "radioButton1","id"=>"radioForm2","required"])); ?>

		</div>
	</div>
</div>
<div class="form-group" id="cliente_nuevo">
	<div class="container-fluid" id="clienteForm1">
		<div class="row">
			<div class="col-lg-5">
				<div class="form-group">
					<?php echo Form::label('nombre_c','Nombres: ',['class'=>'control-label']); ?>

					<?php echo Form::text('nombre_c',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del cliente','required','min'=>5,'id'=>'nombre_c']); ?><br/>
				</div>
			</div>
			<div class="col-lg-5">
				<div class="form-group">
					<?php echo Form::label('apellidos_c','Apellidos: ',['class'=>'control-label']); ?>

					<?php echo Form::text('apellidos_c',null,['class'=>'form-control','placeholder'=>'Ingrese el apellido del cliente','required','min'=>5,'id'=>'apellidos_c']); ?><br/>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-5">
				<div class="form-group">
					<?php echo Form::label('direccion_c','Direcci&oacute;n: ',['class'=>'control-label']); ?>

					<?php echo Form::text('direccion_c',null,['class'=>'form-control','placeholder'=>'Ingrese la direccion del cliente','min'=>5,'id'=>'direccion_c']); ?><br/>
				</div>
			</div>
			<div class="col-lg-5">
				<div class="form-group" <?php /* id="alertTelf1" , */ ?>>
					<?php echo Form::label('telefono_c','Tel&eacute;fono: ',['class'=>'control-label']); ?>

					<?php echo Form::text('telefono_c',null,['class'=>'form-control','placeholder'=>'Ingrese el telefono del cliente','id'=>'telefono_c1','min'=>5,]); ?><br/>
				</div>
			</div>
			<div class="col-lg-2">
				&nbsp;
			</div>
		</div>
		<div class="row">
			<div class="col-lg-5">
				<div class="form-group">
					<?php echo Form::label('email_c','E-mail: ',['class'=>'control-label']); ?>

					<?php echo Form::text('email_c',null,['class'=>'form-control','placeholder'=>'Ingrese el e-mail del cliente','min'=>5,'id'=>'email_c']); ?><br/>
				</div>
			</div>
			<div class="col-lg-5">
				<div class="form-group" id="alertCel1">
					<?php echo Form::label('celular_c','Celular: ',['class'=>'control-label']); ?>

					<?php echo Form::text('celular_c',null,['class'=>'form-control','placeholder'=>'Ingrese el celular del cliente','id'=>'celular_c1','required','min'=>5]); ?><br/>
				</div>
			</div>
			<div class="col-lg-2">
				&nbsp;
			</div>
			<div class="col-lg-2" id="checkCel1"></div>
		</div>
		<div class="row">
			<div class="col-lg-5">
				<div class="form-group">
					<?php echo Form::label('origen_contacto','Origen contacto: ',['class'=>'control-label']); ?><br/>
					<select name="origen_contacto" class="form-control">
						<?php foreach($origen_contacto as $key => $value): ?>
						<option value="<?php echo e($key); ?>"><?php echo e($value); ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col-lg-5">
				<div class="form-group">
					<?php echo Form::label('pais','Localidad: ',['class'=>'control-label']); ?>

					<?php echo Form::select('pais',$paises,null,['class'=>'form-control']); ?><br>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="form-group" id="cliente_antiguo">
</div>
<div class="row" id="begin1">
	<div class="col-lg-2" >
		<div class="form-group" >
			<button class="btn btn-success btn-add-repuesto"><i class="fa fa-plus-square-o"></i> Agregar repuesto</button>
		</div>
	</div>
	<div class="col-lg-2" >
		<div class="form-group" >
			<button class="btn btn-primary btn-add-mercaderia"><i class="fa fa-plus-square-o"></i> Agregar productos varios</button>
		</div>
	</div>
</div>
<?php echo $__env->make('alerts.validation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="form-group option-container">
	<div class="row">
		<div class="panel-heading">
			<h4 class="page-header">Datos del repuesto </h4>
			<button class="btn btn-danger btn-remove-repuesto pull-right" type="button" data-tooltip="Quitar elemento"><i class="fa fa-times"></i> Quitar</button>
		</div>
	</div>
	<div class="row marcasDropdown">
		<div class="col-lg-4">
			<div class="form-group">
				<?php echo Form::label("marca_r","Marca: ",["class"=>"control-label"]); ?>

				<?php echo Form::select("marca_r[]",$marcas,null,["class"=>"form-control marca_r_1","placeholder"=>"Seleccione una marca..","required"]); ?><br/>
			</div>
		</div>
		<div class="col-lg-3 modelosSelect">
			<div class="form-group">
				<?php echo Form::label("modelo_r","Modelo: ",["class"=>"control-label"]); ?>

				<?php echo Form::select("modelo_r[]",["placeholder"=>"Seleccione un modelo.."],null,["class"=>"form-control modelo_r_1"]); ?><br/>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="form-group">
				<?php echo Form::label("vin_r","VIN: ",["class"=>"control-label"]); ?>

				<?php echo Form::text("vin_r[]",null,["class"=>"form-control vin_r_1","placeholder"=>"Ingrese el VIN del repuesto","min"=>5]); ?><br/>
			</div>
		</div>
		<div class="col-lg-2">
			<div class="form-group">
				<?php echo Form::label("anio_r","Año: ",["class"=>"control-label"]); ?>

				<?php 
					$template = '<select name="anio_r[]" class="form-control anio_r_1">';
					for($i = 2018; $i >= 1990; $i--)
						$template .= '<option value="'.$i.'">'.$i.'</option>';
					$template .= '</select>';
					echo $template;
				?>
				<br/>
			</div>
		</div>
	</div>
	<div class="row divDetalles1">
		<div class="col-lg-4">
			<div class="form-group">
				<?php echo Form::label("detalle_r","Detalle: ",["class"=>"control-label"]); ?>

				<?php echo Form::text("detalle_r[]",null,["class"=>"form-control", "placeholder"=>"Detalles","required","min"=>10]); ?><br>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="form-group">
				<?php echo Form::label("codigo_repuesto","C&oacute;digo del repuesto: ",["class"=>"control-label"]); ?>

				<?php echo Form::text("codigo_repuesto[]",null,["class"=>"form-control", "placeholder"=>"Ingrese el c&oacute;digo del repuesto","min"=>5]); ?><br>
			</div>
		</div>
		<div class="col-lg-2">
			<div class="form-group">
				<?php echo Form::label("cantidad_rep","Cantidad: ",["class"=>"control-label"]); ?>

				<?php echo Form::number("cantidad_rep[]",1,["class"=>"form-control","placeholder"=>"Ingrese la cantidad","required","min"=>1]); ?><br/>
			</div>
		</div>
		<div class="col-lg-2">
			<div class="form-group">
				<?php echo Form::label("boton","Opci&oacute;n: ",["class"=>"control-label"]); ?><br>
				<a class="btn btn-success btnAddDetalleR1"><i class="fa fa-plus"></i> Agregar</a>
			</div>
		</div>
	</div>
</div>
<?php if(isset($mercaderia)): ?>
<?php if($mercaderia == "mercaderia-validar"): ?>
<div class="form-group option-container">
	<div class="row">
		<div class="panel-heading">
			<h4 class="page-header">Datos del producto </h4>
			<button class="btn btn-danger btn-remove-mercaderia pull-right" type="button" data-tooltip="Quitar elemento"><i class="fa fa-times"></i> Quitar</button>
		</div>
	</div>
	<div class="row divDetallesM1">
		<div class="col-lg-6">
			<div class="form-group">
				<?php echo Form::label("nombre_m","Detalle: ",["class"=>"control-label"]); ?>

				<?php echo Form::text("nombre_m[]",null,["class"=>"form-control","placeholder"=>"Ingrese el nombre de la mercaderia","required","min"=>5]); ?><br/>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="form-group">
				<?php echo Form::label("nro_item","N&uacute;mero de &iacute;tem: ",["class"=>"control-label"]); ?>

				<?php echo Form::text("nro_item[]",null,["class"=>"form-control","placeholder"=>"Ingrese el nro. de item","min"=>5]); ?><br/>
			</div>
		</div>
		<div class="col-lg-2">
			<div class="form-group">
				<?php echo Form::label("cantidad_mer","Cantidad: ",["class"=>"control-label"]); ?>

				<?php echo Form::number("cantidad_mer[]",1,["class"=>"form-control","placeholder"=>"Ingrese la cantidad","required","min"=>1]); ?><br/>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
<?php endif; ?>
<script type="text/javascript">
$(document).ready(function(){
	marca = ''
	modelo = ''
	anio = ''
	vin = ''
	$(document).on('click', '.btnAddDetalleR1', function(){
		marca = $(this).parents(".option-container").find('.marca_r_1').val()
		modelo = $(this).parents(".option-container").find('.modelo_r_1').val()
		anio = $(this).parents(".option-container").find('.anio_r_1').val()
		vin = $(this).parents(".option-container").find('.vin_r_1').val()
		if(marca == "" || modelo == "" || anio == ""){
			toastr.options = {
						"closeButton": true,
						"debug": false,
						"newestOnTop": false,
						"progressBar": false,
						"positionClass": "toast-top-center",
						"preventDuplicates": true,
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
						};
			toastr.error('<b>Error!</b> debe llenar los campos <b>marca, modelo y anio</b> para agregar mas detalles !');
		}else{
			template = '<div class="row detallesAdded1"><div class="col-lg-4"><div class="form-group"><input class="marca_r_2" type="hidden" name="marca_r[]" value="'+marca+'"><input class="modelo_r_2" type="hidden" name="modelo_r[]" value="'+modelo+'"><input class="vin_r_2"  type="hidden" name="vin_r[]" value="'+vin+'"><input class="anio_r_2" type="hidden" name="anio_r[]" value="'+anio+'"><?php echo Form::label("detalle_r","Detalle: ",["class"=>"control-label"]); ?><?php echo Form::text("detalle_r[]",null,["class"=>"form-control", "placeholder"=>"Detalles","required","min"=>10]); ?><br></div></div><div class="col-lg-4"><div class="form-group"><?php echo Form::label("codigo_repuesto","C&oacute;digo del repuesto: ",["class"=>"control-label"]); ?><?php echo Form::text("codigo_repuesto[]",null,["class"=>"form-control", "placeholder"=>"Ingrese el c&oacute;digo del repuesto","min"=>5]); ?><br></div></div><div class="col-lg-2"><div class="form-group"><?php echo Form::label("cantidad_rep","Cantidad: ",["class"=>"control-label"]); ?><?php echo Form::number("cantidad_rep[]",1,["class"=>"form-control","placeholder"=>"Ingrese la cantidad","required","min"=>1]); ?></div></div><div class="col-lg-2"><div class="form-group"><?php echo Form::label("boton","Opci&oacute;n: ",["class"=>"control-label"]); ?><br><a class="btn btn-danger btnRemoveDetalle1"><i class="fa fa-times"></i> Quitar</a></div></div></div>'
			$(this).parents('.divDetalles1').after(template);
		}
	});
	$(document).on('click','.btnRemoveDetalle1',function(){
		$(this).parents('.detallesAdded1').remove();
	});

	$(document).on({
    'focus': function () {
        //hacer algo aca
    },
    'blur': function () {
        marca_nueva = $(this).val()
        marca_antigua = $(this).parents('.option-container').find(".marca_r_2").val()
        if(marca_antigua != marca_nueva){
        	$(this).parents('.option-container').find('.marca_r_2').val(marca_nueva)
        }
    }
	}, 'input.marca_r_1');

	$(document).on({
	    'focus': function () {
	        //hacer algo aca
	    },
	    'blur': function () {
	        modelo_nuevo = $(this).val()
	        modelo_antiguo = $(this).parents('.option-container').find(".modelo_r_2").val()
	        if(modelo_antiguo != modelo_nuevo){
	        	$(this).parents('.option-container').find('.modelo_r_2').val(modelo_nuevo)
	        }
	    }
		}, 'input.modelo_r_1');

	$(document).on({
	    'focus': function () {
	        //hacer algo aca
	    },
	    'blur': function () {
	        vin_nuevo = $(this).val()
	        vin_antiguo = $(this).parents('.option-container').find(".vin_r_2").val()
	        if(vin_antiguo != vin_nuevo){
	        	$(this).parents('.option-container').find('.vin_r_2').val(vin_nuevo)
	        }
	    }
		}, 'input.vin_r_1');

	$(document).on({
	    'focus': function () {
	        //hacer algo aca
	    },
	    'blur': function () {
	        anio_nuevo = $(this).val()
	        anio_antiguo = $(this).parents('.option-container').find(".anio_r_2").val()
	        if(anio_antiguo != anio_nuevo){
	        	$(this).parents('.option-container').find('.anio_r_2').val(anio_nuevo)
	        }
	    }
		}, 'input.anio_r_1');
});
//REPUESTOS

//MERCADERIAS
$(document).ready(function(){
	nombre_m = ''
	$(document).on('click', '.btnAddDetalleM1', function(){
		nombre_m = $(this).parents(".option-container").find('.nombre_m_1').val()
		if(nombre_m == ""){
			toastr.options = {
						"closeButton": true,
						"debug": false,
						"newestOnTop": false,
						"progressBar": false,
						"positionClass": "toast-top-center",
						"preventDuplicates": true,
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
						};
			toastr.error('<b>Error!</b> debe llenar el campo <b>detalle</b> para agregar mas campos !');
		}else{
			template = '<div class="row detallesAddedM1"><div class="col-lg-4"></div><div class="col-lg-4"><div class="form-group"><input class="nombre_m_2" type="hidden" name="nombre_m[]" value="'+nombre_m+'"><?php echo Form::label("nro_item","N&uacute;mero de &iacute;tem/URL: ",["class"=>"control-label"]); ?><?php echo Form::text("nro_item[]",null,["class"=>"form-control", "placeholder"=>"Ingrese el numero de item o URL","required","min"=>10]); ?><br></div></div><div class="col-lg-2"><div class="form-group"><?php echo Form::label("cantidad_mer","Cantidad: ",["class"=>"control-label"]); ?><?php echo Form::number("cantidad_mer[]",1,["class"=>"form-control","placeholder"=>"Ingrese la cantidad","required","min"=>1]); ?><br></div></div><div class="col-lg-2"><div class="form-group"><?php echo Form::label("boton","Opci&oacute;n: ",["class"=>"control-label"]); ?><br><a class="btn btn-danger btnRemoveDetalleM1"><i class="fa fa-times"></i> Quitar</a></div></div></div>'
			$(this).parents('.divDetallesM1').after(template);
		}
	});
	$(document).on('click','.btnRemoveDetalleM1',function(){
		$(this).parents('.detallesAddedM1').remove();
	});

	$(document).on({
    'focus': function () {
        //hacer algo aca
    },
    'blur': function () {
        nombre_nuevo = $(this).val()
        nombre_antiguo = $(this).parents('.option-container').find(".nombre_m_2").val()
        if(nombre_antiguo != nombre_nuevo){
        	$(this).parents('.option-container').find('.nombre_m_2').val(nombre_nuevo)
        }
    }
	}, 'input.nombre_m_1');
});
//MERCADERIAS
function showForm(){
	$("#cliente_nuevo").show()
	var inputs = ['nombre_c','apellidos_c','celular_c1','origen_contacto','pais_c']
	for(i = 0; i < inputs.length; i++){
		$("#"+inputs[i]).prop('required', true);
	}
}
function hideForm(){
	$("#cliente_nuevo").hide()
	var inputs = ['nombre_c','apellidos_c','celular_c1','origen_contacto','pais_c']
	for(i = 0; i < inputs.length; i++){
		$("#"+inputs[i]).prop('required', false);
	}
}
$("#radioForm1").on('click',function(){
	document.getElementById('searchResults1').innerHTML = "";
	$("#cliente_antiguo").hide()
	$("#search1").prop('required', false);
	showForm();
});
$("#radioForm2").on('click',function(){
	hideForm();
	$("#cliente_antiguo").show()
	var divCliente_antiguo = document.getElementById('cliente_antiguo')
	divCliente_antiguo.innerHTML = '<div class="row"><div class="col-md-4"><div class="form-group"><?php echo Form::text("search",null,["class"=>"form-control ","placeholder"=>"Buscar cliente..","id"=>"search1","required"]); ?></div></div><div class="col-md-2"><button id="search-cliente1" type="button" class="btn btn-accent">Buscar</button></div></div><div class="form-group" id="searchResults1"></div>'
	$("#search-cliente1").on('click',function(){
	var search = document.getElementById('search1').value
	if("" != search){
		$.get('/administracion/clientes/search/'+search+'', function(response){
			console.log(response)
		if(response.response.length > 0){
			var table = '<div class="table-responsive"><table class="table table-hover table-striped"><thead><tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" style="width: 80px;">Nombre</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"aria-label="Browser: activate to sort column ascending" style="width: 80px;">Apellidos</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"aria-label="Platform(s): activate to sort column ascending" style="width: 85px;">Direcci&oacute;n</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"aria-label="Engine version: activate to sort column ascending" style="width: 85px;">Tel&eacute;fono</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"aria-label="CSS grade: activate to sort column ascending" style="width: 80px;">Celular</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"aria-label="CSS grade: activate to sort column ascending" style="width: 20px;">Contacto</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"aria-label="CSS grade: activate to sort column ascending" style="width: 20px;">E-mail</th><th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"aria-label="CSS grade: activate to sort column ascending" style="width: 10px;">Pa&iacute;s</th><th class="sorting_asc" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" style="width: 10px;">Selecci&oacute;n</th></tr></thead>'
			var data = ''
			for(i = 0; i < response.response.length; i++){
				data += '<tr>'
					data += '<td>'+response.response[i].nombre_c+'</td>'
					data += '<td>'+response.response[i].apellidos_c+'</td>'
					data += '<td>'+response.response[i].direccion_c.substring(0,15)+'</td>'
					data += '<td>'+response.response[i].telefono_c+'</td>'
					data += '<td>'+response.response[i].celular_c+'</td>'
					data += '<td>'+response.response[i].origen_contacto.substring(0,15)+'</td>'
					data += '<td>'+response.response[i].email_c+'...</td>'
					data += '<td>'+response.response[i].pais+'</td>'
					data += '<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="clientes_id" value="'+response.response[i].id+'" required></td>'
					}
			data += '</table></div>'
			var template = table + data
			document.getElementById('searchResults1').innerHTML = "";
			document.getElementById('searchResults1').innerHTML = template;
			toastr.options = {
						"closeButton": true,
						"debug": false,
						"newestOnTop": false,
						"progressBar": false,
						"positionClass": "toast-top-center",
						"preventDuplicates": true,
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
						};
			toastr.success('<b>Se han encontrado '+response.response.length+' registros !</b><br/> Por favor seleccione un cliente para crear la cotizaci&oacute;n');
		}else{
			toastr.options = {
					"closeButton": true,
					"debug": false,
					"newestOnTop": false,
					"progressBar": false,
					"positionClass": "toast-top-center",
					"preventDuplicates": true,
					"onclick": null,
					"showDuration": "300",
					"hideDuration": "1000",
					"timeOut": "5000",
					"extendedTimeOut": "1000",
					"showEasing": "swing",
					"hideEasing": "linear",
					"showMethod": "fadeIn",
					"hideMethod": "fadeOut"
					};
			toastr.error('<b>No se han encontrado resultados !</b><br/> Por favor verifique los datos ingresados');
		}
	});
	}else{
		toastr.options = {
				"closeButton": true,
				"debug": false,
				"newestOnTop": false,
				"progressBar": false,
				"positionClass": "toast-top-center",
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
				};
			toastr.warning('<b>Datos vac&iacute;os!</b><br/>Debe ingresar los datos para la b&uacute;squeda');
					}
				});
	});

  $(document).on({//SIRVE PARA INPUTS CREADOS DINAMICAMENTE PUTO PROBLEMA -.- !
      'focus': function () {
          //hacer algo aca
      },
      'change': function (e) {
          marca_selected = $(this).parents('.option-container').find('.modelo_r_1')
          console.log(marca_selected)
          if(e.target.value != null){
		    $.get("/administracion/marcas-modelos/get/"+e.target.value+"",function(response){
		      if(response.length > 0){
		        marca_selected.empty();
		        for(i=0; i<response.length; i++){
		          marca_selected.append("<option value='"+response[i].modelo+"'>"+response[i].modelo+"</option>");
		        }
		      } else {
		        marca_selected.empty();
		        marca_selected.append("<option value='#'>No se encontraron resultados..</option>");
		      }  
		    });
	  	  }
	  	}
    }, '.marca_r_1');
</script>