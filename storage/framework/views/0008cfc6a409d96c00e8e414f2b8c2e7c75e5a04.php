<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			<?php echo Form::label('banco','Banco: ',['class'=>'control-label']); ?>

			<?php echo Form::text('banco',null,['class'=>'form-control','placeholder'=>'Entidad financiera','required','min'=>5]); ?><br/>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			<?php echo Form::label('cuenta_bancaria','N&uacute;mero de cuenta: ',['class'=>'control-label']); ?>

			<?php echo Form::text('cuenta_bancaria',null,['class'=>'form-control','placeholder'=>'Ingrese el n&uacute;mero de cuenta','required','min'=>5]); ?><br/>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			<?php echo Form::label('moneda','Moneda: ',['class'=>'control-label']); ?>

			<?php echo Form::select("moneda",["Dolares"=>"D&oacute;lares","Bolivianos"=>"Bolivianos"],null,['class'=>'form-control','required']); ?><br/>
		</div>
	</div>
</div>