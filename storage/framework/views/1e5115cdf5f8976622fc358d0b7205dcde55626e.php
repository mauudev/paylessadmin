<?php $__env->startSection('content'); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-filled">
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-next-2"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Personalizar PDF</h3>
						<small>
						Generar invoice PDF
						</small>
					</div>
				</div>
				<div class="panel-body">
					<?php echo Form::model($venta,['route'=>['ventas.generarPDFPersonalizado',$venta->id],'method'=>'POST']); ?>

					<?php echo Form::hidden("ventas_id",$venta->id); ?>

					<?php echo e(csrf_field()); ?>

					<?php echo $__env->make('pdffile.form.form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					<?php echo $__env->make('alerts.validation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					<div class="form-group">
						
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-7" >
			<?php echo Form::submit('Generar PDF',['class'=>'btn btn-w-md btn-accent','id'=>'generarBtn']); ?> <a class="btn btn-w-md btn-default" href="<?php echo url('ventas/'.$venta->id.'/detalle'); ?>">Atr&aacute;s</a>
		</div>
		<?php echo Form::close(); ?>

	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$(".observacionesCB").change(function(){
			if(this.checked){
				template = '<?php echo e(Form::textarea("observaciones",null,["class"=>"form-control observacionesInput","placeholder"=>"Ingrese observaciones en el campo","required","min"=>5,"rows"=>5])); ?>'
				$(".observacionesDiv").append(template)
			}else {
				$(".observacionesInput").remove()
			}
		});
		$(".metodo_pagoCB").change(function(){
			if(this.checked){
				template = '<?php echo e(Form::textarea("metodo_pago",null,["class"=>"form-control metodo_pagoInput","placeholder"=>"Ingrese metodo de pago en el campo","required","min"=>5,"rows"=>5])); ?>'
				$(".metodo_pagoDiv").append(template)
			}else {
				$(".metodo_pagoInput").remove()
			}
		});
		$(".plazo_entregaCB").change(function(){
			if(this.checked){
				template = '<?php echo e(Form::textarea("plazo_entrega",null,["class"=>"form-control plazo_entregaInput","placeholder"=>"Ingrese el plazo entrega en el campo","required","min"=>5,"rows"=>5])); ?>'
				$(".plazo_entregaDiv").append(template)
			}else {
				$(".plazo_entregaInput").remove()
			}
		});
	});
	$("#generarBtn").on('click',function(){
		if($(".observacionesInput").val().length < 10) {
			toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-center",
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      };
                    toastr.error("<b>Error!</b> El campo debe tener un m&iacute;nimo de 10 caracteres para generar el PDF. ");
			return false;
		}
		if($(".metodo_pagoInput").val().length < 10) {
			toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-center",
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      };
                    toastr.error("<b>Error!</b> El campo debe tener un m&iacute;nimo de 10 caracteres para generar el PDF. ");
			return false;
		}
		if($(".plazo_entregaInput").val().length < 10) {
			toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-center",
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      };
                    toastr.error("<b>Error!</b> El campo debe tener un m&iacute;nimo de 10 caracteres para generar el PDF. ");
			return false;
		}
		var referrer =  document.referrer;
	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>