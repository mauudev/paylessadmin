<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-filled">
			<div class="view-header">
				<div class="header-icon">
					<i class="pe page-header-icon pe-7s-note2"></i>
				</div>
				<div class="header-title">
					<h3 class="page-header">Nueva cotizaci&oacute;n</h3>
					<small>
					Cotizaci&oacute;n de repuestos/mercader&iacute;as
					</small>
				</div>
			</div>
			<div class="panel-body">
				<?php echo Form::open(['route'=>'cotizaciones.store','method'=>'POST','class'=>'form-group',"onsubmit"=>"return validateForm();",'id'=>'myForm']); ?>

				<?php echo $__env->make('cotizaciones.form.form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				<br/><br/>
			</div>
		</div>
		<div class="row">
			<div class="form-group">
				<div class="col-md-7" >
					<?php echo Form::submit('Guardar',['class'=>'btn btn-w-md btn-accent']); ?> <a class="btn btn-w-md btn-default" href="<?php echo url('/cotizaciones/listar/0'); ?>">Cancelar</a>
				</div>
			</div>
			<?php echo Form::close(); ?>

		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<?php echo $__env->make('cotizaciones.dinamic-form-create', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<script type="text/javascript">
	jQuery(function($){
	$("#date").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
	$("#telefono_c1").mask("999-9999");
	$("#celular_c1").mask("999-99999");
	$("#phone").mask("99-9999999");
	$("#ssn").mask("999-99-9999");
	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>