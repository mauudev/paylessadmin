<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-filled">
			<div class="panel-heading">
				<div class="panel-tools">
					<div class="col-md-12">
						<?php echo Form::open(['route'=>'repuestos.index','method'=>'GET','class'=>'navbar-form navbar-left pull-right','role'=>'search']); ?>

						<div class="form-group">
							<?php echo Form::text('search',null,['class'=>'form-control','placeholder'=>'Buscar..']); ?>

						</div>
						<button type="submit" class="btn btn-accent">Buscar</button>
						<?php echo Form::close(); ?>

					</div>
				</div>
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-car"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Repuestos registrados</h3>
						<small>
						Gesti&oacute;n de repuestos
						</small>
					</div>
				</div>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<b>NOTA:</b> Los precios de compra de estos repuestos son referenciales, pertenecientes a cotizaciones realizadas (precio de proveedor).
					</div>
				</div><br>
				<div class="table-responsive">
					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th class="left-align">Marca</th>
								<th class="left-align">Modelo</th>
								<th class="left-align">Año</th>
								<th class="left-align">VIN</th>
								<th class="left-align">Detalle</th>
								<th align="center">Precio de compra</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($repuestos as $repuesto): ?>
							<tr>
								<td><?php echo e($repuesto->marca_r); ?></td>
								<td><?php echo e($repuesto->modelo_r); ?></td>
								<td><?php echo e($repuesto->anio_r); ?></td>
								<td><?php echo e($repuesto->vin_r); ?></td>
								<td><?php echo e($repuesto->detalle_r); ?></td>
								<td align="center"><?php echo e($repuesto->precio_venta_r); ?></td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
					<?php echo e($repuestos->render()); ?>

					<?php echo $__env->make('alerts.success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>