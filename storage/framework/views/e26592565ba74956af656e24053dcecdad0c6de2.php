<!DOCTYPE html>
<html lang='en'>
    <!-- Mirrored from webapplayers.com/luna_admin-v1.1/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Aug 2016 21:36:21 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>-->
        <link rel="shortcut icon" href="<?php echo e(asset('favicon.ico')); ?>" >
        <!-- Page title -->

        <title>Payless | Administraci&oacute;n</title>
        <!-- Vendor styles -->
        <?php echo Html::style('css/jquery-ui.min.css'); ?>

        <?php echo Html::style('vendor/fontawesome/css/font-awesome.css'); ?>

        <?php echo Html::style('vendor/animate.css/animate.css'); ?>

        <?php echo Html::style('vendor/bootstrap/css/bootstrap.css'); ?>

        <?php echo Html::style('vendor/toastr/toastr.min.css'); ?>

        <!-- App styles -->
        <?php echo Html::style('styles/pe-icons/pe-icon-7-stroke.css'); ?>

        <?php echo Html::style('styles/pe-icons/helper.css'); ?>

        <?php echo Html::style('styles/stroke-icons/style.css'); ?>

        <?php echo Html::style('styles/style.css'); ?>

        <?php echo Html::style('css/tooltips.css'); ?>

        <?php echo Html::style('css/select2.min.css'); ?>

        
        <?php echo Html::script('js/jquery-3.1.1.js'); ?>

        <?php echo Html::script('js/jquery-ui.min.js'); ?>

        <?php echo Html::script('vendor/pacejs/pace.min.js'); ?>

        <?php echo Html::script('vendor/jquery/dist/jquery.min.js'); ?>

        
        <?php echo Html::script('vendor/bootstrap/js/bootstrap.min.js'); ?>

        <?php echo Html::script('vendor/toastr/toastr.min.js'); ?>

        <?php echo Html::script('vendor/sparkline/index.js'); ?>

        <!-- App scripts -->
        <?php echo Html::script('js/bootbox.min.js'); ?>

        <?php echo Html::script('scripts/luna.js'); ?>

        <?php echo Html::script('js/cliente.js'); ?>


        <?php echo Html::script('js/jquery.maskedinput.js'); ?>

        <?php echo Html::script('js/onSubmits.js'); ?>

        <?php echo Html::script('js/validator.js'); ?>

        <?php echo Html::script('js/select2.min.js'); ?>

        <?php echo Html::script('js/money.min.js'); ?>

        
        <?php echo $__env->yieldContent('scripts'); ?>
    </head>
    <body>
        <!-- Wrapper-->
        <div class="wrapper">
            <!-- Header-->
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <div id="mobile-menu">
                            <div class="left-nav-toggle">
                                <a href="#">
                                    <i class="stroke-hamburgermenu"></i>
                                </a>
                            </div>
                        </div>
                        <a class="navbar-brand" >
                            <?php /* Payless
                            <span>Sistema de cotizaciones </span> */ ?>
                        </a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <div class="left-nav-toggle">
                            <a href="#">
                                <i class="stroke-hamburgermenu"></i>
                            </a>
                        </div>
                        <ul class="nav navbar-nav navbar-right">
                        <?php if(Auth::check()): ?>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#"><?php echo e(Auth::user()->nombre_u); ?>

                                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="<?php echo URL::to('logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                            </ul>
                        </li>
                        <?php else: ?>
                        <li class=" profil-link">
                            <a href="<?php echo URL::to('login'); ?>">
                                <span class="profile-address">Login</span>
                                <img src="images/user.png" class="img-circle" alt="">
                            </a>
                        </li>
                        <?php endif; ?>
                    </ul>
                    </div>
                </div>
            </nav>
            <!-- End header-->
            <!-- Navigation-->

            <aside class="navigation">
                <nav>
                    <ul class="nav luna-nav">
                        <li class="active nav-category">
                            <b>Men&uacute;</b>
                        </li>
                        <?php if(Auth::user()->type == 'admin'): ?>
                        <li class="active">
                            <a href="<?php echo URL::to('usuarios'); ?>"><i class="fa fa-user"></i> Usuarios</a>
                        </li>
                        <li class="active">
                            <a href="<?php echo URL::to('cuentas-bancarias'); ?>"><i class="fa fa-money"></i> Cuentas bancarias</a>
                        </li>
                        <li class="active">
                            <a href="<?php echo URL::to('proveedores'); ?>"><i class="fa fa-truck"></i> Proveedores</a>
                        </li>
                        <li class="active">
                            <a href="<?php echo URL::to('marcas-modelos'); ?>"><i class="fa fa-car"></i> Marcas y modelos</a>
                        </li>
                        <li class="active">
                            <a href="<?php echo URL::to('repuestos'); ?>"><i class="fa fa-dashboard"></i> Repuestos</a>
                        </li>
                        <li class="active">
                            <a href="<?php echo URL::to('mercaderias'); ?>"><i class="fa fa-headphones"></i> Productos varios</a>
                        </li>
                        <li class="active">
                            <a href="<?php echo URL::to('precios-transporte'); ?>"><i class="fa fa-money"></i> Precios Transporte</a>
                        </li>
                        <li class="active">
                            <a href="<?php echo URL::to('ventas'); ?>"><i class="fa fa-check-circle"></i> Ventas</a>
                        </li>
                        <?php endif; ?>
                        <li class="active">
                            <a href="#monitoring" data-toggle="collapse" aria-expanded="false">
                                <i class="fa fa-clipboard"></i> Cotizaciones<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                            </a>
                            <ul id="monitoring" class="nav nav-second collapse">
                                <li class="active"><a href="<?php echo e(url('/cotizaciones/listar/1')); ?>"> Finalizadas</a></li>
                                <li class="active"><a href="<?php echo e(url('/cotizaciones/listar/0')); ?>"> Pendientes</a></li>
                            </ul>
                        </li>
                        <li class="active">
                            <a href="<?php echo URL::to('clientes'); ?>"><i class="fa fa-users"></i> Clientes</a>
                        </li>
                        <?php if(Auth::check()): ?>
                        <li id="loginLi" style="display: none;" >
                             <a href="#monitoring1" data-toggle="collapse" aria-expanded="false">
                                <i class="fa fa-user"></i> <?php echo e(Auth::user()->nombre_u); ?><span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                            </a>
                            <ul id="monitoring1" class="nav nav-second collapse">
                                <li class="active"><a href="<?php echo URL::to('logout'); ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                            </ul>
                        </li>
                        <?php else: ?>
                        <li class="profil-link" id="loginLi" style="display: none;">
                            <a href="<?php echo URL::to('login'); ?>">
                                <span class="profile-address">Login</span>
                                <img src="images/user.png" class="img-circle" alt="">
                            </a>
                        </li>
                        <?php endif; ?> 
                    </ul>
                </nav>
            </aside>
            
            <!-- End navigation-->
            <!-- Main content-->
            <section class="content">
                <?php echo $__env->yieldContent('content'); ?>
            </section>
            <!-- End main content-->
            
        </div>
        <!-- End wrapper-->
        <!-- Vendor scripts -->
        <!-- Vendor scripts -->
<script type="text/javascript">
    $(".js-example-theme-multiple").select2({
        placeholder: "Seleccione uno o varios repuestos.."
    });
    if(navigator.userAgent.match(/iPad/i)){
     //code for iPad here 
     $("#loginLi").show()
    }
    if(navigator.userAgent.match(/iPhone/i)){
     //code for iPhone here 
     $("#loginLi").show()
    }
    if(navigator.userAgent.match(/Android/i)){
     //code for Android here 
     $("#loginLi").show()
    }
    if(navigator.userAgent.match(/BlackBerry/i)){
     //code for BlackBerry here 
     $("#loginLi").show()
    }
    if(navigator.userAgent.match(/webOS/i)){
     //code for webOS here 
     $("#loginLi").show()
    }
</script>