<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="view-header">
                <div class="header-icon">
                    <i class="pe page-header-icon pe-7s-note2"></i>
                </div>
                <div class="header-title">
                    <h3 class="page-header">Detalle de los pagos</h3>
                    <small>
                    Cotizaci&oacute;n de repuestos/mercader&iacute;as
                    </small>
                </div>
            </div>
            <div class="panel-body">
                <?php echo e(csrf_field()); ?>

                <div class="row">
                    <div class="col-md-6 pull-right">
                        <div class="panel panel-c-success panel-collapse">
                            <div class="panel-heading">
                                <div class="panel-tools">
                                    <a class="panel-toggle"><i class="fa fa-chevron-down"></i></a>
                                </div>
                                Datos del responsable de venta
                            </div>
                            <div class="panel-body" style="display: none;">
                                <div class="col-md-2">
                                    <strong class="c-white">Nombre: </strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo e($usuario->nombre_u); ?>

                                </div><br>
                                <div class="col-md-2">
                                    <strong class="c-white">Apellidos: </strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo e($usuario->apellidos_u); ?>

                                </div><br>
                                <div class="col-md-2">
                                    <strong class="c-white">Tipo: </strong>
                                </div>
                                <div class="col-md-8">
                                    <?php if($usuario->type == 'admin'): ?>
                                    Administrador
                                    <?php else: ?>
                                    Usuario
                                    <?php endif; ?>
                                </div><br>
                                <div class="col-md-2">
                                    <strong class="c-white">Teléfono: </strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo e($usuario->telefono_u); ?>

                                </div><br>
                                <div class="col-md-2">
                                    <strong class="c-white">Celular: </strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo e($usuario->celular_u); ?>

                                </div><br>
                                <div class="col-md-2">
                                    <strong class="c-white">E-mail: </strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo e($usuario->email_u); ?>

                                </div><br>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-c-danger panel-collapse">
                            <div class="panel-heading">
                                <div class="panel-tools">
                                    <a class="panel-toggle"><i class="fa fa-chevron-down"></i></a>
                                </div>
                                Datos del cliente
                            </div>
                            <div class="panel-body" style="display: none;">
                                <div class="row">
                                    <div class="col-md-2"><strong class="c-white">Nombre: </strong></div><div class="col-md-9"> <?php echo e($cliente->nombre_c); ?></div><br>
                                    <div class="col-md-2"><strong class="c-white">Apellidos: </strong></div><div class="col-md-9"> <?php echo e($cliente->apellidos_c); ?></div><br>
                                    <div class="col-md-2"><strong class="c-white">Dirección: </strong></div><div class="col-md-9"> <?php echo e($cliente->direccion_c); ?></div><br>
                                    <div class="col-md-2"><strong class="c-white">Teléfono: </strong></div><div class="col-md-9"> <?php echo e($cliente->telefono_c); ?></div><br>
                                    <div class="col-md-2"><strong class="c-white">Celular: </strong></div><div class="col-md-9"> <?php echo e($cliente->celular_c); ?></div><br>
                                    <div class="col-md-2"><strong class="c-white">E-mail: </strong></div><div class="col-md-9"> <?php echo e($cliente->email_c); ?></div><br>
                                    <div class="col-md-2"><strong class="c-white">Localidad: </strong></div><div class="col-md-9"> <?php echo e($cliente->pais); ?></div><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><hr>
                <div class="row">
                    <div class="col-md-8">
                        <div class="panel panel-filled">
                            <div class="panel-heading">
                                Total y saldos
                            </div>
                            <div class="panel-body">
                                <div class="bs-example">
                                    <strong class="c-white">Total USD:</strong> <b class="text-info"><?php echo e($venta->pago_total); ?></b>&nbsp;&nbsp;<strong class="c-white">Total Bs:</strong> <b class="text-info"><?php echo e($venta->pago_total_bs); ?></b>
                                    <?php if($venta->descuento != null): ?>
                                    <?php endif; ?>
                                    <?php echo e(Form::hidden('pago_total_usd',$venta->pago_total,["id"=>"pago_total_usd_1"])); ?>

                                    <?php echo e(Form::hidden('pago_total_bs',$venta->pago_total_bs,["id"=>"pago_total_bs_1"])); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-filled">
                            <div class="panel-heading">
                                Tipo de cambio de la venta 
                            </div>
                            <div class="panel-body">
                                <div class="bs-example">
                                    <strong class="c-white">Tipo de cambio:</strong> <b class="text-info"><?php echo e($venta->tipo_cambio); ?></b>
                                    <?php echo e(Form::hidden('tipo_cambio',$venta->tipo_cambio,['class'=>'tipo_cambio_value'])); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo $__env->make('alerts.validation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                <?php if($tipos == 'efectivos'): ?>
                <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            <b>Detalle de los pagos</b>
                        </div>
                        <div class="panel-body">
                            <div class="v-timeline vertical-container">
                                <div class="vertical-timeline-block">
                                    <div class="vertical-timeline-icon">
                                        <i class="fa fa-check text-success"></i>
                                    </div>
                                    <div class="vertical-timeline-content">
                                        <div class="p-sm">
                                            <span class="vertical-date pull-right">
                                            <?php if($pagos_ef_pp[0]->detalle == 'Primer pago'): ?>
                                            <b class="text-info"><?php echo e($pagos_ef_pp[0]->created_at); ?></b>
                                            <?php endif; ?>
                                            <br></span>
                                            <h2>Detalle del primer pago</h2><br>
                                            <?php if($pagos_ef_pp[0]->detalle == 'Primer pago'): ?>
                                            <?php endif; ?>
                                            <div class="row show-grid">
                                                <div class="col-md-12" id="cambiarTipoPagoDiv1">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Tipo de pago:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php if($pagos_ef_pp[0]->detalle == 'Primer pago'): ?>
                                                            Pago en efectivo</b>
                                                        <?php endif; ?>
                                                    </div>
                                                </div><br><br class="cuenta_bancaria_pp_header"> 
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Monto:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php if($pagos_ef_pp[0]->detalle == 'Primer pago'): ?>
                                                            <?php if($pagos_ef_pp[0]->moneda == 'Bolivianos'): ?>
                                                            <?php echo e($pagos_ef_pp[0]->monto_bs); ?> Bs</b>
                                                            <?php elseif($pagos_ef_pp[0]->moneda == 'Dolares'): ?>
                                                            <?php echo e($pagos_ef_pp[0]->monto); ?> USD</b>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div><br><br>
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Cod. recibo/dep:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php echo e($pagos_ef_pp[0]->codigo_recibo); ?>

                                                    </div>
                                                </div><br><br class="tipo-cambio-pp-field">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="vertical-timeline-block">
                                    <div class="vertical-timeline-icon">
                                        <i class="fa fa-check text-success"></i>
                                    </div>
                                    <div class="vertical-timeline-content">
                                        <div class="p-sm">
                                            <span class="vertical-date pull-right">
                                            <?php if($pagos_ef_sp[0]->detalle == 'Segundo pago'): ?>
                                            <b class="text-info"><?php echo e($pagos_ef_sp[0]->created_at); ?></b>
                                            <?php endif; ?>
                                            <br></span>
                                            <h2>Detalle del segundo pago</h2><br>
                                            <div class="row show-grid">
                                                <div class="col-md-12" id="cambiarTipoPagoDiv2">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Tipo de pago:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php if($pagos_ef_sp[0]->detalle == 'Segundo pago'): ?>
                                                            Pago en efectivo</b>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <br><br class="cuenta_bancaria_sp_header"> 
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Monto:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php if($pagos_ef_sp[0]->detalle == 'Segundo pago'): ?>
                                                            <?php if($pagos_ef_sp[0]->moneda == 'Bolivianos'): ?>
                                                            <?php echo e($pagos_ef_sp[0]->monto_bs); ?> Bs</b>
                                                            <?php elseif($pagos_ef_sp[0]->moneda == 'Dolares'): ?>
                                                            <?php echo e($pagos_ef_sp[0]->monto); ?> USD</b>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div><br><br>
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Cod. recibo/dep:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php echo e($pagos_ef_sp[0]->codigo_recibo); ?>

                                                    </div>
                                                </div><br><br class="tipo-cambio-sp-field">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <?php if($tipos == 'depositos'): ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            <b>Detalle de los pagos</b>
                        </div>
                        <div class="panel-body">
                            <div class="v-timeline vertical-container">
                                <div class="vertical-timeline-block">
                                    <div class="vertical-timeline-icon">
                                        <i class="fa fa-check text-success"></i>
                                    </div>
                                    <div class="vertical-timeline-content">
                                        <div class="p-sm">
                                            <span class="vertical-date pull-right">
                                            <?php if($pagos_dep_pp[0]->detalle == 'Primer pago'): ?>
                                            <b class="text-info"><?php echo e($pagos_dep_pp[0]->created_at); ?></b>
                                            <?php endif; ?>
                                            <br></span>
                                            <h2>Detalle del primer pago</h2><br>
                                            <div class="row show-grid">
                                                <div class="col-md-12" id="cambiarTipoPagoDiv1">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Tipo de pago:</strong>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php if($pagos_dep_pp[0]->detalle == 'Primer pago'): ?>
                                                            Dep&oacute;sito a cuenta bancaria</b>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <br><br class="cuenta_bancaria_pp_header">
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Cuenta actual:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <b class="text-success cuenta_bancaria_1"><?php echo e($cuenta_banco_dep[0]->banco." ".$cuenta_banco_dep[0]->cuenta_bancaria." ".$cuenta_banco_dep[0]->moneda); ?></b>
                                                    </div>
                                                </div><br><br> 
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Monto:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php if($pagos_dep_pp[0]->detalle == 'Primer pago'): ?>
                                                            <?php if($pagos_dep_pp[0]->moneda == 'Bolivianos'): ?>
                                                            <?php echo e($pagos_dep_pp[0]->monto_bs); ?> Bs</b>
                                                            <?php elseif($pagos_dep_pp[0]->moneda == 'Dolares'): ?>
                                                            <?php echo e($pagos_dep_pp[0]->monto); ?> USD</b>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div><br><br>
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Cod. recibo/dep:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php echo e($pagos_dep_pp[0]->codigo_recibo); ?>

                                                    </div>
                                                </div><br><br class="tipo-cambio-pp-field">
                                                <div class="col-md-12 tipo-cambio-pp-div">
                                                    <?php if($pagos_dep_pp[0]->detalle == 'Primer pago'): ?>
                                                        <?php if($pagos_dep_pp[0]->moneda == 'Bolivianos'): ?>
                                                        <div class="col-md-2">
                                                            <strong class="c-white">Tipo de cambio:</strong>
                                                        </div>
                                                        <div class="col-md-10">
                                                            <?php echo e($pagos_dep_pp[0]->tipo_cambio); ?>

                                                        </div>
                                                        <?php else: ?>
                                                        <div class="col-md-2">
                                                            <strong class="c-white">Tipo de cambio: </strong>
                                                        </div>
                                                        <div class="col-md-10">
                                                            <?php echo e($pagos_dep_pp[0]->tipo_cambio); ?>

                                                        </div>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="vertical-timeline-block">
                                    <div class="vertical-timeline-icon">
                                        <i class="fa fa-check text-success"></i>
                                    </div>
                                    <div class="vertical-timeline-content">
                                        <div class="p-sm">
                                            <span class="vertical-date pull-right">
                                            <?php if($pagos_dep_sp[0]->detalle == 'Segundo pago'): ?>
                                            <b class="text-info"><?php echo e($pagos_dep_sp[0]->created_at); ?></b>
                                            <?php endif; ?>
                                            <br></span>
                                            <h2>Detalle del segundo pago</h2><br>
                                            <div class="row show-grid">
                                                <div class="col-md-12" id="cambiarTipoPagoDiv2">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Tipo de pago:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php if($pagos_dep_sp[0]->detalle == 'Segundo pago'): ?>
                                                            Dep&oacute;sito a cuenta bancaria</b>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <br><br class="cuenta_bancaria_sp_header">
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Cuenta actual:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <b class="text-success cuenta_bancaria_2"><?php echo e($cuenta_banco_dep[1]->banco." ".$cuenta_banco_dep[1]->cuenta_bancaria." ".$cuenta_banco_dep[1]->moneda); ?></b>
                                                    </div>
                                                </div><br><br> 
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Monto:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php if($pagos_dep_sp[0]->detalle == 'Segundo pago'): ?>
                                                            <?php if($pagos_dep_sp[0]->moneda == 'Bolivianos'): ?>
                                                            <?php echo e($pagos_dep_sp[0]->monto_bs); ?> Bs</b>
                                                            <?php elseif($pagos_dep_sp[0]->moneda == 'Dolares'): ?>
                                                            <?php echo e($pagos_dep_sp[0]->monto); ?> USD</b>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div><br><br>
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Cod. recibo/dep:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php echo e($pagos_dep_sp[0]->codigo_recibo); ?></b
                                                    </div>
                                                </div><br><br class="tipo-cambio-sp-field">
                                                <div class="col-md-12 tipo-cambio-sp-div">
                                                    <?php if($pagos_dep_sp[0]->detalle == 'Segundo pago'): ?>
                                                        <?php if($pagos_dep_sp[0]->moneda == 'Bolivianos'): ?>
                                                        <div class="col-md-2">
                                                            <strong class="c-white">Tipo de cambio:</strong>
                                                        </div>
                                                        <div class="col-md-10">
                                                            <?php echo e($pagos_dep_sp[0]->tipo_cambio); ?></b
                                                        </div> 
                                                        <?php else: ?>
                                                        <div class="col-md-2">
                                                            <strong class="c-white">Tipo de cambio: </strong>
                                                        </div>
                                                        <div class="col-md-10">
                                                            <?php echo e($pagos_dep_sp[0]->tipo_cambio); ?></b
                                                        </div>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <?php if($tipos == 'ef_dep'): ?>
            <?php if(isset($primer_pago)): ?>
                <?php if($primer_pago == 'efectivo'): ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            <b>Detalle de los pagos</b>
                        </div>
                        <div class="panel-body">
                            <div class="v-timeline vertical-container">
                                <div class="vertical-timeline-block">
                                    <div class="vertical-timeline-icon">
                                        <i class="fa fa-check text-success"></i>
                                    </div>
                                    <div class="vertical-timeline-content">
                                        <div class="p-sm">
                                            <span class="vertical-date pull-right">
                                            <?php if($pagos_ef_pp[0]->detalle == 'Primer pago'): ?>
                                            <b class="text-info"><?php echo e($pagos_ef_pp[0]->created_at); ?></b>
                                            <?php endif; ?>
                                            <br></span>
                                            <h2>Detalle del primer pago</h2><br>
                                            <div class="row show-grid">
                                                <div class="col-md-12" id="cambiarTipoPagoDiv1">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Tipo de pago:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php if($pagos_ef_pp[0]->detalle == 'Primer pago'): ?>
                                                            Pago en efectivo</b>
                                                        <?php endif; ?>
                                                    </div>
                                                </div><br><br class="cuenta_bancaria_pp_header"> 
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Monto:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php if($pagos_ef_pp[0]->detalle == 'Primer pago'): ?>
                                                            <?php if($pagos_ef_pp[0]->moneda == 'Bolivianos'): ?>
                                                            <?php echo e($pagos_ef_pp[0]->monto_bs); ?> Bs</b>
                                                            <?php elseif($pagos_ef_pp[0]->moneda == 'Dolares'): ?>
                                                            <?php echo e($pagos_ef_pp[0]->monto); ?> USD</b>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div><br><br>
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Cod. recibo/dep:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php echo e($pagos_ef_pp[0]->codigo_recibo); ?> Bs</b>
                                                    </div>
                                                </div><br><br class="tipo-cambio-pp-field">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="vertical-timeline-block">
                                    <div class="vertical-timeline-icon">
                                        <i class="fa fa-check text-success"></i>
                                    </div>
                                    <div class="vertical-timeline-content">
                                        <div class="p-sm">
                                            <span class="vertical-date pull-right">
                                            <?php if($pagos_dep_sp[0]->detalle == 'Segundo pago'): ?>
                                            <b class="text-info"><?php echo e($pagos_dep_sp[0]->created_at); ?></b>
                                            <?php endif; ?>
                                            <br></span>
                                            <h2>Detalle del segundo pago</h2><br>
                                            <div class="row show-grid">
                                                <div class="col-md-12" id="cambiarTipoPagoDiv2">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Tipo de pago:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php if($pagos_dep_sp[0]->detalle == 'Segundo pago'): ?>
                                                            Dep&oacute;sito a cuenta bancaria</b>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <br><br class="cuenta_bancaria_sp_header">
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Cuenta actual:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <b class="text-success cuenta_bancaria_2"><?php echo e($cuenta_banco_dep[0]->banco." ".$cuenta_banco_dep[0]->cuenta_bancaria." ".$cuenta_banco_dep[0]->moneda); ?></b>
                                                    </div>
                                                </div><br><br> 
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Monto:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php if($pagos_dep_sp[0]->detalle == 'Segundo pago'): ?>
                                                            <?php if($pagos_dep_sp[0]->moneda == 'Bolivianos'): ?>
                                                            <?php echo e($pagos_dep_sp[0]->monto_bs); ?> Bs</b>
                                                            <?php elseif($pagos_dep_sp[0]->moneda == 'Dolares'): ?>
                                                            <?php echo e($pagos_dep_sp[0]->monto); ?> USD</b>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div><br><br>
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Cod. recibo/dep:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php echo e($pagos_dep_sp[0]->codigo_recibo); ?>

                                                    </div>
                                                </div><br><br class="tipo-cambio-sp-field">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif; ?>
            <?php if($primer_pago == 'deposito'): ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-tools">
                                <a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
                            </div>
                            <b>Detalle de los pagos</b>
                        </div>
                        <div class="panel-body">
                            <div class="v-timeline vertical-container">
                                <div class="vertical-timeline-block">
                                    <div class="vertical-timeline-icon">
                                        <i class="fa fa-check text-success"></i>
                                    </div>
                                    <div class="vertical-timeline-content">
                                        <div class="p-sm">
                                            <span class="vertical-date pull-right">
                                            <?php if($pagos_dep_pp[0]->detalle == 'Primer pago'): ?>
                                            <b class="text-info"><?php echo e($pagos_dep_pp[0]->created_at); ?></b>
                                            <?php endif; ?>
                                            <br></span>
                                            <h2>Detalle del primer pago</h2><br>
                                            <div class="row show-grid">
                                                <div class="col-md-12" id="cambiarTipoPagoDiv1">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Tipo de pago:</strong>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php if($pagos_dep_pp[0]->detalle == 'Primer pago'): ?>
                                                            Dep&oacute;sito a cuenta bancaria
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <br><br class="cuenta_bancaria_pp_header">
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Cuenta actual:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <b class="text-success cuenta_bancaria_1"><?php echo e($cuenta_banco_dep[0]->banco." ".$cuenta_banco_dep[0]->cuenta_bancaria." ".$cuenta_banco_dep[0]->moneda); ?></b>
                                                    </div>
                                                </div><br><br> 
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Monto:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php if($pagos_dep_pp[0]->detalle == 'Primer pago'): ?>
                                                            <?php if($pagos_dep_pp[0]->moneda == 'Bolivianos'): ?>
                                                            <?php echo e($pagos_dep_pp[0]->monto_bs); ?> Bs</b>
                                                            <?php elseif($pagos_dep_pp[0]->moneda == 'Dolares'): ?>
                                                            <?php echo e($pagos_dep_pp[0]->monto); ?> USD</b>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div><br><br>
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Cod. recibo/dep:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php echo e($pagos_dep_pp[0]->codigo_recibo); ?> </b>
                                                    </div>
                                                </div><br><br class="tipo-cambio-pp-field">
                                                <div class="col-md-12 tipo-cambio-pp-div">
                                                    <?php if($pagos_dep_pp[0]->detalle == 'Primer pago'): ?>
                                                        <?php if($pagos_dep_pp[0]->moneda == 'Bolivianos'): ?>
                                                        <div class="col-md-2">
                                                            <strong class="c-white">Tipo de cambio:</strong>
                                                        </div>
                                                        <div class="col-md-10">
                                                            <?php echo e($pagos_dep_pp[0]->tipo_cambio); ?>

                                                        </div>
                                                        <?php else: ?>
                                                        <div class="col-md-2">
                                                            <strong class="c-white">Tipo de cambio: </strong>
                                                        </div>
                                                        <div class="col-md-10">
                                                            <?php echo e($pagos_dep_pp[0]->tipo_cambio); ?>

                                                        </div>
                                                        <?php endif; ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="vertical-timeline-block">
                                    <div class="vertical-timeline-icon">
                                        <i class="fa fa-check text-success"></i>
                                    </div>
                                    <div class="vertical-timeline-content">
                                        <div class="p-sm">
                                            <span class="vertical-date pull-right">
                                            <?php if($pagos_ef_sp[0]->detalle == 'Segundo pago'): ?>
                                            <b class="text-info"><?php echo e($pagos_ef_sp[0]->created_at); ?></b>
                                            <?php endif; ?>
                                            <br></span>
                                            <h2>Detalle del segundo pago</h2><br>
                                            <div class="row show-grid">
                                                <div class="col-md-12" id="cambiarTipoPagoDiv2">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Tipo de pago:</strong>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php if($pagos_ef_sp[0]->detalle == 'Segundo pago'): ?>
                                                            Pago en efectivo</b>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                                <br><br class="cuenta_bancaria_sp_header"> 
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Monto:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php if($pagos_ef_sp[0]->detalle == 'Segundo pago'): ?>
                                                            <?php if($pagos_ef_sp[0]->moneda == 'Bolivianos'): ?>
                                                            <?php echo e($pagos_ef_sp[0]->monto_bs); ?> Bs</b>
                                                            <?php elseif($pagos_ef_sp[0]->moneda == 'Dolares'): ?>
                                                            <?php echo e($pagos_ef_sp[0]->monto); ?> USD</b>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div><br><br>
                                                <div class="col-md-12">
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Cod. recibo/dep:</strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <?php echo e($pagos_ef_sp[0]->codigo_recibo); ?>

                                                    </div>
                                                </div><br><br class="tipo-cambio-sp-field">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <?php endif; ?>
            <?php endif; ?>
        <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="form-group">
                <div class="col-md-7" >
                     <a class="btn btn-w-md btn-accent" href="<?php echo url('ventas/'.$venta->id.'/detalle'); ?>">Regresar</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    saldo_bs_verificado = 0
    saldo_usd_verificado = 0
    function verificarSaldosCorrectos(){
        correcto = true
        if(saldo_bs_verificado < 0 && saldo_usd_verificado < 0){
            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-center",
                                "preventDuplicates": true,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                              };
                toastr.error('<b>Error!</b> El saldo no puede ser negativo, verifique los valores ingresados !');
        correcto = false
        }
        if(saldo_bs_verificado > 0 && saldo_usd_verificado > 0){
            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-center",
                                "preventDuplicates": true,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                              };
                toastr.error('<b>Error!</b> El saldo debe ser igual a CERO, por favor verifique los valores ingresados !');
            correcto = false
        }
        if(saldo_usd_verificado < 0 || saldo_bs_verificado < 0){
            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-center",
                                "preventDuplicates": true,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                              };
                toastr.error('<b>Error!</b> Los saldos deben ser igual a CERO, por favor verifique los valores ingresados !');
            correcto = false
        }
        return correcto   
    }
    function actualizarSaldos(){
        texto_saldo_usd = $("#strong_saldo_usd")
        texto_saldo_bs = $("#strong_saldo_bs")
        valor_saldo_usd = $("#b_saldo_usd")
        valor_saldo_bs = $("#b_saldo_bs")
        total_bs = $("#pago_total_bs_1").val()
        total_bs = parseFloat(total_bs)
        total_usd = $("#pago_total_usd_1").val()
        total_usd = parseFloat(total_usd)
        monto_1 = $("#monto_1").val()
        monto_1 = parseFloat(monto_1)
        monto_2 = $("#monto_2").val()
        monto_2 = parseFloat(monto_2)
        moneda_1 = $(".monedaSelectSaldo1 option:selected").val()
        moneda_2 = $(".monedaSelectSaldo2 option:selected").val()
        tipo_cambio = $(".tipo_cambio_value").val()
        tipo_cambio = parseFloat(tipo_cambio)
        if(moneda_1 == 'Bolivianos'){
            monto_1_en_usd = monto_1 / tipo_cambio
            if(moneda_2 == 'Bolivianos'){
                monto_2_en_usd = monto_2 / tipo_cambio
                monto_total_bs = monto_1 + monto_2
                monto_total_usd = monto_1_en_usd + monto_2_en_usd
                saldo_total_bs = total_bs - monto_total_bs
                saldo_total_usd = total_usd - monto_total_usd
                saldo_total_bs = roundNumber(saldo_total_bs,2)
                saldo_total_usd = roundNumber(saldo_total_usd,2)
                texto_saldo_bs.html("Saldo en Bs:")
                valor_saldo_bs.html(saldo_total_bs)
                texto_saldo_usd.html("Saldo en USD:")
                valor_saldo_usd.html(saldo_total_usd)
                saldo_bs_verificado = saldo_total_bs
                saldo_usd_verificado = saldo_total_usd
            }
            if(moneda_2 == 'Dolares'){
                monto_2_en_bs = monto_2 * tipo_cambio
                monto_total_bs = monto_1 + monto_2_en_bs
                monto_total_usd = monto_1_en_usd + monto_2
                saldo_total_bs = total_bs - monto_total_bs
                saldo_total_usd = total_usd - monto_total_usd
                saldo_total_usd = roundNumber(saldo_total_usd,2)
                saldo_total_bs = roundNumber(saldo_total_bs,2)
                texto_saldo_bs.html("Saldo en Bs:")
                valor_saldo_bs.html(saldo_total_bs)
                texto_saldo_usd.html("Saldo en USD:")
                valor_saldo_usd.html(saldo_total_usd)
                saldo_bs_verificado = saldo_total_bs
                saldo_usd_verificado = saldo_total_usd
            }
            
        }
        if(moneda_1 == 'Dolares'){
            monto_1_en_bs = monto_1 * tipo_cambio
            if(moneda_2 == 'Dolares'){
                monto_2_en_bs = monto_2 * tipo_cambio
                monto_total_bs = monto_1_en_bs + monto_2_en_bs
                monto_total_usd = monto_1 + monto_2
                saldo_total_bs = total_bs - monto_total_bs
                saldo_total_usd = total_usd - monto_total_usd
                saldo_total_usd = roundNumber(saldo_total_usd,2)
                saldo_total_bs = roundNumber(saldo_total_bs,2)
                texto_saldo_bs.html("Saldo en Bs:")
                valor_saldo_bs.html(saldo_total_bs)
                texto_saldo_usd.html("Saldo en USD:")
                valor_saldo_usd.html(saldo_total_usd)
                saldo_bs_verificado = saldo_total_bs
                saldo_usd_verificado = saldo_total_usd
            }
            if(moneda_2 == 'Bolivianos'){
                monto_2_en_usd = monto_2 / tipo_cambio
                monto_total_usd = monto_1 + monto_2_en_usd
                monto_total_bs = monto_1_en_bs + monto_2
                saldo_total_usd = total_usd - monto_total_usd
                saldo_total_bs = total_bs - monto_total_bs
                saldo_total_usd = roundNumber(saldo_total_usd,2)
                saldo_total_bs = roundNumber(saldo_total_bs,2)
                texto_saldo_bs.html("Saldo en Bs:")
                valor_saldo_bs.html(saldo_total_bs)
                texto_saldo_usd.html("Saldo en USD:")
                valor_saldo_usd.html(saldo_total_usd)
                saldo_bs_verificado = saldo_total_bs
                saldo_usd_verificado = saldo_total_usd
            }
        }
    }
    $(document).ready(function(){
        actualizarSaldos()
    });
    function roundNumber(num, scale) {
      var number = Math.round(num * Math.pow(10, scale)) / Math.pow(10, scale);
      if(num - number > 0) {
        return (number + Math.floor(2 * Math.round((num - number) * Math.pow(10, (scale + 1))) / 10) / Math.pow(10, scale));
      } else {
        return number;
      }
    }
</script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>