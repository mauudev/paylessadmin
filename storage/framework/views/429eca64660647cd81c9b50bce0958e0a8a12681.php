<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-filled">
			<div class="panel-heading">
				<div class="panel-tools">
					<div class="col-md-12"><br>
						<a href="<?php echo URL::to('cuentas-bancarias/create'); ?>" class="btn btn-w-md btn-accent"><i class="fa fa-plus"></i> Nueva cuenta</a>
					</div>
				</div>
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-cash"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Cuentas bancarias registradas</h3>
						<small>
						Gesti&oacute;n de cuentas bancarias
						</small>
					</div>
				</div>
			</div>
			<div class="panel-body">
				<?php if(count($cuentas) > 0): ?>
				<div class="table-responsive">
					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th class="left-align">Banco</th>
								<th class="left-align">Nro. cuenta</th>
								<th class="left-align">Moneda</th>
								<th class="left-align">Acci&oacute;n</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($cuentas as $cuenta): ?>
							<tr>
								<td><?php echo e($cuenta->banco); ?></td>
								<td><?php echo e($cuenta->cuenta_bancaria); ?></td>
								<td><?php echo e($cuenta->moneda); ?></td>
								<td class="center-block">
									<a class="btn btn-accent" href="<?php echo route('cuentas-bancarias.edit',$parameters = $cuenta->id); ?>" data-tooltip="Editar cuenta"><i class="fa fa-pencil"></i> Editar</a>&nbsp;</td>
							</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
					<?php echo e($cuentas->render()); ?>

					<?php echo $__env->make('alerts.success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					<?php echo $__env->make('alerts.error', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div>
				<?php else: ?>
					<div class="alert alert-danger alert-dismissable">
						<strong>No se encontraron resultados</strong> 
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>