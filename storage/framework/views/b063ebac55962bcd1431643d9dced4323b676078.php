<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="view-header">
                <div class="header-icon">
                    <i class="pe page-header-icon pe-7s-note2"></i>
                </div>
                <div class="header-title">
                    <h3 class="page-header">Agregar segundo pago</h3>
                    <small>
                    Cotizaci&oacute;n de repuestos/mercader&iacute;s
                    </small>
                </div>
            </div>
            <div class="panel-body">
                <?php echo Form::model($venta,['route'=>['ventas.guardarSegundoPago',$venta->id],'method'=>'POST',"onsubmit"=>"return validateForm();"]); ?>

                <?php echo Form::hidden("ventas_id",$venta->id); ?>

                <?php echo e(csrf_field()); ?>

                <div class="row">
                    <div class="col-md-6 pull-right">
                        <div class="panel panel-c-success panel-collapse">
                            <div class="panel-heading">
                                <div class="panel-tools">
                                    <a class="panel-toggle"><i class="fa fa-chevron-down"></i></a>
                                </div>
                                Datos del responsable de venta
                            </div>
                            <div class="panel-body" style="display: none;">
                                <div class="col-md-2">
                                    <strong class="c-white">Nombre: </strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo e($usuario->nombre_u); ?>

                                </div><br>
                                <div class="col-md-2">
                                    <strong class="c-white">Apellidos: </strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo e($usuario->apellidos_u); ?>

                                </div><br>
                                <div class="col-md-2">
                                    <strong class="c-white">Tipo: </strong>
                                </div>
                                <div class="col-md-8">
                                    <?php if($usuario->type == 'admin'): ?>
                                    Administrador
                                    <?php else: ?>
                                    Usuario
                                    <?php endif; ?>
                                </div><br>
                                <div class="col-md-2">
                                    <strong class="c-white">Teléfono: </strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo e($usuario->telefono_u); ?>

                                </div><br>
                                <div class="col-md-2">
                                    <strong class="c-white">Celular: </strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo e($usuario->celular_u); ?>

                                </div><br>
                                <div class="col-md-2">
                                    <strong class="c-white">E-mail: </strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo e($usuario->email_u); ?>

                                </div><br>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-c-danger panel-collapse">
                            <div class="panel-heading">
                                <div class="panel-tools">
                                    <a class="panel-toggle"><i class="fa fa-chevron-down"></i></a>
                                </div>
                                Datos del cliente
                            </div>
                            <div class="panel-body" style="display: none;">
                                <div class="row">
                                    <div class="col-md-2"><strong class="c-white">Nombre: </strong></div><div class="col-md-9">  <?php echo e($cliente->nombre_c); ?></div><br>
                                    <div class="col-md-2"><strong class="c-white">Apellidos: </strong></div><div class="col-md-9"> <?php echo e($cliente->apellidos_c); ?></div><br>
                                    <div class="col-md-2"><strong class="c-white">Dirección: </strong></div><div class="col-md-9"> <?php echo e($cliente->direccion_c); ?></div><br>
                                    <div class="col-md-2"><strong class="c-white">Teléfono: </strong></div><div class="col-md-9"> <?php echo e($cliente->telefono_c); ?></div><br>
                                    <div class="col-md-2"><strong class="c-white">Celular: </strong></div><div class="col-md-9"> <?php echo e($cliente->celular_c); ?></div><br>
                                    <div class="col-md-2"><strong class="c-white">E-mail: </strong></div><div class="col-md-9"> <?php echo e($cliente->email_c); ?></div><br>
                                    <div class="col-md-2"><strong class="c-white">Localidad: </strong></div><div class="col-md-9"> <?php echo e($cliente->pais); ?></div><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><hr>
                <div class="row">
                        <div class="panel">
                            <div class="panel-heading">
                            </div>
                            <div class="panel-body">
                                <div class="v-timeline vertical-container">
                                    <div class="vertical-timeline-block">
                                        <div class="vertical-timeline-icon">
                                            <i class="fa fa-check text-success"></i>
                                        </div>
                                        <div class="vertical-timeline-content">
                                            <div class="p-sm">
                                                <span class="vertical-date pull-right"> <small><b class="text-info"><?php echo e($venta->created_at); ?></b></small> </span>
                                                <h2>Datos del pago parcial</h2><br>
                                                <div class="row show-grid">
                                                    <?php if(isset($venta)): ?>
                                                    <?php echo e(Form::hidden('tipo_cambio_venta',$venta->tipo_cambio,['id'=>'tipo_cambio_venta1'])); ?>

                                                    <?php if(isset($pago_deposito)): ?>
                                                    <?php
                                                    $saldo_bs = $venta->pago_total_bs - $pago_deposito->monto_bs;
                                                    $saldo_usd = $venta->pago_total - $pago_deposito->monto;
                                                    ?>
                                                    <?php echo e(Form::hidden('saldo_bs',$saldo_bs,["id"=>"saldo_bs1"])); ?>

                                                    <?php echo e(Form::hidden('saldo_usd',$saldo_usd,["id"=>"saldo_usd1"])); ?>

                                                    <div class="col-md-2">
                                                        <strong class="c-white">Tipo de pago: </strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <a class="btn btn-accent btn-xs  pull-right" href="<?php echo url('ventas/'.$venta->id.'/edit'); ?>"><i class="fa fa-edit"></i> Editar</a>
                                                        <b>Dep&oacute;sito a cuenta</b>
                                                    </div><br/>
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Banco: </strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <b><?php echo $cuenta->banco; ?></b>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Cuenta bancaria: </strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <b class="text-info"><?php echo $cuenta->cuenta_bancaria. ' ' .$cuenta->moneda; ?></b>
                                                    </div><br/>
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Total a pagar: </strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <b class="text-success"><?php echo $venta->pago_total; ?> USD - <?php echo $venta->pago_total_bs; ?> Bs</b>
                                                    </div><br/>
                                                    <?php if($venta->descuento != null && $venta->pago_total_desc != null): ?>
                                                    <?php /* ACA PONER PARA DESCUENTOS !!! */ ?>
                                                    <?php else: ?>
                                                    <?php if($pago_deposito->moneda == 'Bolivianos'): ?>
                                                    <div class="col-lg-2">
                                                        <strong class="c-white">Monto: </strong>
                                                    </div>
                                                    <div class="col-md-10"> <b class="text-primary"><?php echo $pago_deposito->monto; ?> USD - <?php echo $pago_deposito->monto_bs; ?> Bs</b>
                                                    </div><br/>
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Saldo: </strong>
                                                    </div>
                                                    <div class="col-md-10"><b><b class="text-accent"><?php echo round($saldo_usd,2); ?> USD - <?php echo round($saldo_bs,2); ?> Bs</b></b></div><br/>
                                                    <?php endif; ?>
                                                    <?php if($pago_deposito->moneda == 'Dolares'): ?>
                                                    <div class="col-lg-2">
                                                        <strong class="c-white">Monto: </strong>
                                                    </div>
                                                    <div class="col-md-10"> <b class="text-primary"><?php echo $pago_deposito->monto; ?> USD - <?php echo $pago_deposito->monto_bs; ?> Bs </b>
                                                    </div><br/>
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Saldo: </strong>
                                                    </div>
                                                    <div class="col-md-10"><b><b class="text-accent"><?php echo round($saldo_usd,2); ?> USD - <?php echo round($saldo_bs,2); ?> Bs</b></b></div><br/>
                                                    <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php elseif(isset($pago_efectivo)): ?>
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Tipo de pago: </strong>
                                                    </div>
                                                    <?php if($pago_efectivo->moneda == 'Bolivianos'): ?>
                                                    <?php
                                                    $tipo_cambio = $venta->tipo_cambio;
                                                    $saldo_bs = $venta->pago_total_bs - $pago_efectivo->monto_bs;
                                                    $monto_en_usd = $pago_efectivo->monto_bs / $tipo_cambio;
                                                    $monto_en_usd = round($monto_en_usd,2);
                                                    $saldo_usd = $venta->pago_total - $monto_en_usd;
                                                    ?>
                                                    <div class="col-md-10">
                                                        <b>Efectivo en Bolivianos</b>
                                                        <?php echo e(Form::hidden('saldo_bs',$saldo_bs,["id"=>"saldo_bs1"])); ?>

                                                        <?php echo e(Form::hidden('saldo_usd',$saldo_usd,["id"=>"saldo_usd1"])); ?>

                                                        <a class="btn btn-accent btn-xs  pull-right" href="<?php echo url('ventas/'.$venta->id.'/edit'); ?>"><i class="fa fa-edit"></i> Editar</a>
                                                    </div><br/>
                                                    <?php endif; ?>
                                                    <?php if($pago_efectivo->moneda == 'Dolares'): ?>
                                                    <?php
                                                    $tipo_cambio = $venta->tipo_cambio;
                                                    $monto_en_bs = $pago_efectivo->monto * $tipo_cambio;
                                                    $monto_en_bs = round($monto_en_bs,2);
                                                    $saldo_bs = $venta->pago_total_bs - $monto_en_bs;
                                                    $saldo_usd = $venta->pago_total - $pago_efectivo->monto;
                                                    ?>
                                                    <div class="col-md-10">
                                                        <b>Efectivo en D&oacute;lares</b>
                                                        <?php echo e(Form::hidden('saldo_bs',$saldo_bs,["id"=>"saldo_bs1"])); ?>

                                                        <?php echo e(Form::hidden('saldo_usd',$saldo_usd,["id"=>"saldo_usd1"])); ?>

                                                        <a class="btn btn-accent btn-xs  pull-right" href="<?php echo url('ventas/'.$venta->id.'/edit'); ?>"><i class="fa fa-edit"></i> Editar</a>
                                                    </div>
                                                    <?php endif; ?>
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Cod. dep/recibo: </strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <b class="text-info"><?php echo $pago_efectivo->codigo_recibo; ?></b>
                                                    </div><br/>
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Total a pagar: </strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <b class="text-success"><?php echo $venta->pago_total; ?> USD - <?php echo $venta->pago_total_bs; ?> Bs</b>
                                                    </div><br/>
                                                    <?php if($venta->descuento != null && $venta->pago_total_desc != null): ?>
                                                    <?php /* <div class="col-md-2">
                                                        <strong class="c-white">Descuento: </strong>
                                                    </div>
                                                    <div class="col-md-10">
                                                        <b class="text-danger"><?php echo $venta->descuento; ?></b>
                                                    </div><br/>
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Total con descuento: </strong>
                                                    </div>
                                                    <div class="col-md-10"> <b class="text-success"><?php echo $venta->pago_total_desc; ?></b>
                                                    </div><br/>
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Monto en Bs: </strong>
                                                    </div>
                                                    <div class="col-md-10"> <b class="text-primary"><?php echo $pago_deposito->monto; ?></b>
                                                    </div><br/>
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Saldo: </strong>
                                                    </div>
                                                    <div class="col-md-10"> <b class="text-accent"><?php echo round($venta->pago_total_desc - $pago_deposito->monto,2); ?></b>
                                                    </div><br/> */ ?>
                                                    <?php else: ?>
                                                    <?php if($pago_efectivo->moneda == 'Bolivianos'): ?>
                                                    <div class="col-lg-2">
                                                        <strong class="c-white">Monto: </strong>
                                                    </div>
                                                    <div class="col-md-10"> <b class="text-primary"><?php echo $pago_efectivo->monto_bs; ?> Bs</b>
                                                    </div><br/>
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Saldo: </strong>
                                                    </div>
                                                    <div class="col-md-10"><b class="text-accent"><?php echo round($saldo_usd,2); ?> USD - <?php echo round($saldo_bs,2); ?> Bs</b></div><br/>
                                                    <?php endif; ?>
                                                    <?php if($pago_efectivo->moneda == 'Dolares'): ?>
                                                    <div class="col-lg-2">
                                                        <strong class="c-white">Monto: </strong>
                                                    </div>
                                                    <div class="col-md-10"> <b class="text-primary"><?php echo $pago_efectivo->monto; ?> USD</b>
                                                    </div><br/>
                                                    <div class="col-md-2">
                                                        <strong class="c-white">Saldo: </strong>
                                                    </div>
                                                    <div class="col-md-10"><b class="text-accent"><?php echo round($saldo_usd,2); ?> USD - <?php echo round($saldo_bs,2); ?> Bs</b></div><br/>
                                                    <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php endif; ?>
                                                    <?php endif; ?>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <h4><b>Segundo pago</b></h4>
                                                    </div>
                                                </div><br>
                                                <div class="row">
                                                    <div class="col-md-5">
                                                        <?php echo Form::label('tipo_pago','Tipo de pago: '); ?>

                                                        <?php echo Form::select('tipo_pago',['efectivo'=>'Pago en efectivo','deposito'=>'Dep&oacute;sito a cuenta bancaria'],null ,['class'=>'form-control','placeholder'=>'Seleccione el tipo de pago..','id'=>'tipo_pago1','required']); ?>

                                                    </div>
                                                </div><br>
                                                <div class="row" id="datosPago1">
                                                    
                                                </div><br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <?php echo $__env->make('alerts.validation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group">
        <div class="col-md-7" >
            <?php echo Form::submit('Guardar',['class'=>'btn btn-w-md btn-accent','id'=>'submit1']); ?> <a class="btn btn-w-md btn-default" href="<?php echo url('ventas/'.$venta->id.'/detalle'); ?>">Cancelar</a>
        </div>
    </div>
    <?php echo Form::close(); ?>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#tipo_pago1').on('change', function(){
            var template =''
            var value = $("#tipo_pago1").val()
            var tipo_cambio = $("#tipo_cambio_venta1").val()
            if(value == 'efectivo'){
                template = '<div class="col-md-12"><div class="panel panel-filled panel-c-warning"><div class="panel-heading"><?php echo Form::hidden("monto",$venta->pago_total,["id"=>"montoTotal"]); ?></div><div class="panel-body"><div class="row"><div class="col-md-4"><?php echo Form::label("cod_deposito_recibo","C&oacute;digo dep&oacute;sito/recibo: ",["class"=>"control-label"]); ?><?php echo Form::text("cod_deposito_recibo",null,["class"=>"form-control","placeholder"=>"Ingrese el c&oacute;digo..","required"]); ?></div><div class="col-md-3"><?php echo Form::label("pago_parcial","Monto: ",["class"=>"control-label"]); ?><?php echo Form::text('segundo_pago',null,['class'=>'form-control','placeholder'=>'Ingrese el monto..','required','id'=>'segundoPago1']); ?></div><div class="col-md-2 monedaDiv"><?php echo Form::label("moneda","Moneda: ",["class"=>"control-label"]); ?><?php echo Form::select("moneda",["Dolares"=>"D&oacute;lares","Bolivianos"=>"Bolivianos"],null,['class'=>'form-control monedaSelect1','required']); ?></div><div class="col-md-2 tipo-cambio-div"><?php echo Form::label("tipo_cambio","Tipo cambio: ",["class"=>"control-label"]); ?><input class="form-control" required="" name="tipo_cambio" type="text" value="'+tipo_cambio+'" id="tipo_cambio"></div></div></div></div></div>'
                $("#datosPago1").html(template)
            }
            if(value == 'deposito'){
                template = '<div class="col-md-12"><div class="panel panel-filled panel-c-warning"><div class="panel-heading"><?php echo Form::hidden("monto",$venta->pago_total,["id"=>"montoTotal"]); ?></div><div class="panel-body"><div class="row"><div class="col-md-3 cuentaBancariaDiv"><?php echo Form::label("cuenta_bancaria","Cuenta bancaria: ",["class"=>"control-label"]); ?><?php echo Form::select("cuenta_bancaria",$cuentas,null,["class"=>"form-control cuentaBancaria1","placeholder"=>"Seleccione una cuenta..","required"]); ?></div><div class="col-md-3"><?php echo Form::label("cod_deposito_recibo","Cod. dep&oacute;sito/recibo: ",["class"=>"control-label"]); ?><?php echo Form::text("cod_deposito_recibo",null,["class"=>"form-control","placeholder"=>"Ingrese el c&oacute;digo..","required"]); ?></div><div class="col-md-2"><?php echo Form::label("pago_parcial","Monto: ",["class"=>"control-label"]); ?><?php echo Form::text('segundo_pago',null,['class'=>'form-control','placeholder'=>'Ingrese el monto..','required','id'=>'segundoPago1']); ?></div><div class="col-md-2 monedaDiv"><?php echo Form::label("moneda","Moneda: ",["class"=>"control-label"]); ?><?php echo Form::select("moneda",["Dolares"=>"D&oacute;lares","Bolivianos"=>"Bolivianos"],null,['class'=>'form-control monedaSelect1','required']); ?></div><div class="col-md-2 tipo-cambio-div"><?php echo Form::label("tipo_cambio","Tipo cambio: ",["class"=>"control-label"]); ?><input class="form-control" required="" name="tipo_cambio" type="text" value="'+tipo_cambio+'" id="tipo_cambio"></div></div></div></div></div>'
                $("#datosPago1").html(template)
            }
        }); 
    });
    res = 1; //variable para verificar si existe incoherencia en la cuenta bancaria y el tipo de moneda
    $(document).on({//SIRVE PARA INPUTS CREADOS DINAMICAMENTE PUTO PROBLEMA -.- !
          'focus': function () {
              //hacer algo aca
          },
          'blur': function (e) {

          },//SIGUIENTE PASO OBTENER EL TIPO DE CAMBIO SI EXISTE !
          'change':function(){
            str = $(".cuentaBancaria1 option:selected").text()
            moneda = $(".monedaSelect1 option:selected").val()
            if(str != ''){
                index = str.indexOf(moneda)
                if(index == -1){
                    $(".cuentaBancariaDiv").attr("class","col-md-3 cuentaBancariaDiv has-error")
                    $(".monedaDiv").attr("class","col-md-2 monedaDiv has-error")
                    res = -1;
                }else{
                    $(".cuentaBancariaDiv").attr("class","col-md-3 cuentaBancariaDiv")
                    $(".monedaDiv").attr("class","col-md-2 monedaDiv")
                    res = 1;   
                }
            }
          },
        }, '.monedaSelect1');
        $(document).on({//SIRVE PARA INPUTS CREADOS DINAMICAMENTE PUTO PROBLEMA -.- !
          'focus': function () {
              //hacer algo aca
          },
          'blur': function (e) {

          },//SIGUIENTE PASO OBTENER EL TIPO DE CAMBIO SI EXISTE !
          'change':function(){
            str = $(".cuentaBancaria1 option:selected").text()
            moneda = $(".monedaSelect1 option:selected").val()
            if(str != ''){
                index = str.indexOf(moneda)
                if(index == -1){
                    $(".cuentaBancariaDiv").attr("class","col-md-3 cuentaBancariaDiv has-error")
                    $(".monedaDiv").attr("class","col-md-2 monedaDiv has-error")
                    res = -1;
                }else{
                    $(".cuentaBancariaDiv").attr("class","col-md-3 cuentaBancariaDiv")
                    $(".monedaDiv").attr("class","col-md-2 monedaDiv")
                    res = 1;    
                }
            }
          },
        }, '.cuentaBancaria1');
    $("#submit1").on('click',function(){
        var pago = $("#segundoPago1").val() 
        var moneda = $(".monedaSelect1").val()
        var saldo = 0;
        if(moneda == 'Bolivianos'){
            saldo = $("#saldo_bs1").val()
            saldo = parseFloat(saldo)
        }
        if(moneda == 'Dolares'){
            saldo = $("#saldo_usd1").val()
            saldo = parseFloat(saldo)
        }
        // if(saldo == 0){
        //     alert("Saldo es cero !")//revisar si esto sale
        //     return false;
        // }
        var diferencia = saldo - pago
        if(diferencia < 0){
            toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-center",
                            "preventDuplicates": true,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                          };
            toastr.error('<b>Error!</b> el saldo no debe ser negativo !');
            return false;
        }
        if(diferencia > 0){
            toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-center",
                            "preventDuplicates": true,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                          };
            toastr.error('<b>Error!</b> el saldo debe ser igual a cero para finalizar la venta !');
            return false;
        }
        return true;
    });
function roundNumber(num, scale) {
  var number = Math.round(num * Math.pow(10, scale)) / Math.pow(10, scale);
  if(num - number > 0) {
    return (number + Math.floor(2 * Math.round((num - number) * Math.pow(10, (scale + 1))) / 10) / Math.pow(10, scale));
  } else {
    return number;
  }
}
</script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>