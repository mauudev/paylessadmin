<?php $__env->startSection('content'); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-filled">
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-user"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Actualizar datos del usuario</h3>
						<small>
						Gesti&oacute;n de usuarios
						</small>
					</div>
				</div>
				<div class="panel-body">	
				<?php echo Form::model($usuario,['route'=>['usuarios.update',$usuario->id],'method'=>'PUT']); ?>

					<?php echo $__env->make('usuarios.form.form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?><br>
					<?php echo $__env->make('alerts.validation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					<div class="form-group">
					<div class="col-md-7" >
						<?php echo Form::submit('Actualizar',['class'=>'btn btn-w-md btn-accent']); ?> <a class="btn btn-w-md btn-default" href="<?php echo URL::to('usuarios'); ?>">Cancelar</a>
					</div>
				</div>
				<?php echo Form::close(); ?>

				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(function($){
	   $("#date").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
	   $("#telefono_u_1").mask("999-9999");
	   $("#celular_u_1").mask("999-99999");
	   $("#phone").mask("99-9999999");
	   $("#ssn").mask("999-99-9999");
	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>