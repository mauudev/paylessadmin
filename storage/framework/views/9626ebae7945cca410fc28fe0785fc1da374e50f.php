<?php $__env->startSection('content'); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-filled">
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-users"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Actualizar datos del cliente</h3>
						<small>
						Gesti&oacute;n de clientes
						</small>
					</div>
				</div>
				<div class="panel-body">	
				<?php echo Form::model($cliente,['route'=>['clientes.update',$cliente->id],'method'=>'PUT']); ?>

				<?php echo Form::hidden('clientes_id',$cliente->id,["id"=>"clientes_id"]); ?>

					<?php echo $__env->make('clientes.form.form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					<?php echo $__env->make('alerts.validation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					<div class="form-group">
					<div class="col-md-7" >
						<?php echo Form::submit('Actualizar',['class'=>'btn btn-w-md btn-accent']); ?> <a class="btn btn-w-md btn-default" href="<?php echo URL::to('clientes'); ?>">Cancelar</a>
					</div>
				</div>
				<?php echo Form::close(); ?>

				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	jQuery(function($){
	   $("#date").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
	   $("#telefono_c_1").mask("999-9999");
	   $("#celular_c1_edit").mask("999-99999");
	   $("#phone").mask("99-9999999");
	   $("#ssn").mask("999-99-9999");
	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>