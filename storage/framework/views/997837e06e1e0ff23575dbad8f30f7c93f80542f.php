<?php $__env->startSection('content'); ?>
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-filled">
      <div class="panel-heading">
        <div class="panel-tools">
          <div class="col-md-12">
            <?php $estados = [1=>'Activos',0=>'Inactivos']?>
            <?php echo Form::open(['route'=>'proveedores.index','method'=>'GET','class'=>'navbar-form navbar-left pull-right','role'=>'search']); ?>

            <div class="form-group">
              <?php echo Form::select('estado', $estados,null,['class'=>'form-control','placeholder'=>'Filtrar registros..','id'=>'filtro2']); ?>

              <?php echo Form::text('search',null,['class'=>'form-control','placeholder'=>'Buscar..']); ?>

            </div>
            <button type="submit" class="btn btn-accent">Buscar</button>
            <a href="<?php echo URL::to('proveedores/create'); ?>" class="btn btn-w-md btn-accent"><i class="fa fa-plus"></i> Nuevo proveedor</a>
            <?php echo Form::close(); ?>

          </div>
        </div>
        <div class="view-header">
          <div class="header-icon">
            <i class="pe page-header-icon pe-7s-plane"></i>
          </div>
          <div class="header-title">
            <h3 class="page-header">Proveedores registrados</h3>
            <small>
            Gesti&oacute;n de proveedores
            </small>
          </div>
        </div>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-hover table-striped" id="proveedoresTable1">
          <thead>
                            <tr class="titlerow" role="row">
                              <th class="sorting_asc left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" style="width: 120px;">Compañia
                              </th>
                              <th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="Browser: activate to sort column ascending" style="width: 80px;">Tipo
                              </th>
                              <th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="Browser: activate to sort column ascending" style="width: 100px;">Contacto
                              </th>
                              <th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="Platform(s): activate to sort column ascending" style="width: 80px;">Tel&eacute;fono
                              </th>
                              <th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="Engine version: activate to sort column ascending" style="width: 80px;">Celular
                              </th>
                              <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="CSS grade: activate to sort column ascending" style="width: 60px;">Acci&oacute;n
                              </th>
                            </tr>
                        </thead>
            <tbody id="resultados2">
              <?php if(count($proveedores) > 0): ?>
              <?php foreach($proveedores as $proveedor): ?>
              <tr>
                <td data-toggle="modal" data-target="#myModal" data-id="<?php echo e($proveedor->id); ?>"><?php echo e(str_limit($proveedor->nombre_compania_p,$limit = 20,$end='...')); ?>

                <?php echo e(Form::hidden('compania',$proveedor->nombre_compania_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('tipo',$proveedor->tipo,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('contacto',$proveedor->persona_contacto_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('telefono',$proveedor->telefono_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('celular',$proveedor->celular_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('direccion',$proveedor->direccion_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('email',$proveedor->email_p,['class'=>'dataProveedor'])); ?>

                </td>
                <td data-toggle="modal" data-target="#myModal" data-id="<?php echo e($proveedor->id); ?>"><?php echo e($proveedor->tipo); ?>

                <?php echo e(Form::hidden('compania',$proveedor->nombre_compania_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('tipo',$proveedor->tipo,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('contacto',$proveedor->persona_contacto_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('telefono',$proveedor->telefono_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('celular',$proveedor->celular_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('direccion',$proveedor->direccion_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('email',$proveedor->email_p,['class'=>'dataProveedor'])); ?></td>
                <td data-toggle="modal" data-target="#myModal" data-id="<?php echo e($proveedor->id); ?>"><?php echo e(str_limit($proveedor->persona_contacto_p,$limit = 25,$end='...')); ?>

                <?php echo e(Form::hidden('compania',$proveedor->nombre_compania_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('tipo',$proveedor->tipo,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('contacto',$proveedor->persona_contacto_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('telefono',$proveedor->telefono_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('celular',$proveedor->celular_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('direccion',$proveedor->direccion_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('email',$proveedor->email_p,['class'=>'dataProveedor'])); ?></td>
                <td data-toggle="modal" data-target="#myModal" data-id="<?php echo e($proveedor->id); ?>"><?php echo e(str_limit($proveedor->telefono_p,$limit = 50,$end='')); ?>

                <?php echo e(Form::hidden('compania',$proveedor->nombre_compania_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('tipo',$proveedor->tipo,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('contacto',$proveedor->persona_contacto_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('telefono',$proveedor->telefono_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('celular',$proveedor->celular_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('direccion',$proveedor->direccion_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('email',$proveedor->email_p,['class'=>'dataProveedor'])); ?></td>
                <td data-toggle="modal" data-target="#myModal" data-id="<?php echo e($proveedor->id); ?>"><?php echo e(str_limit($proveedor->celular_p,$limit = 50,$end='')); ?>

                <?php echo e(Form::hidden('compania',$proveedor->nombre_compania_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('tipo',$proveedor->tipo,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('contacto',$proveedor->persona_contacto_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('telefono',$proveedor->telefono_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('celular',$proveedor->celular_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('direccion',$proveedor->direccion_p,['class'=>'dataProveedor'])); ?>

                <?php echo e(Form::hidden('email',$proveedor->email_p,['class'=>'dataProveedor'])); ?></td>
                <td align="center">
                  <a class="btn btn-accent" href="<?php echo route('proveedores.edit',$parameters = $proveedor->id); ?>"><i class="fa fa-pencil"></i> Editar</a>&nbsp;
                  <?php if($proveedor->deleted_at == null ): ?>
                  <a class="btn btn-danger" href="<?php echo route('proveedores.destroy',$parameters = $proveedor->id); ?>"><i class="fa fa-times"></i> Desactivar</a>
                  <?php else: ?> 
                  <a class="btn btn-success" href="<?php echo route('proveedores.activate',$parameters = $proveedor->id); ?>"><i class="fa fa-times"></i> Activar</a>
                    <a href=""><i class="fa fa-check"></i></a>
                  <?php endif; ?>
              </td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
          <?php echo e($proveedores->render()); ?>

          <?php echo $__env->make('alerts.success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
          <?php echo $__env->make('alerts.error', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        </div>
        <?php else: ?>
        <div class="col-md-12" id="msg1">
            <div class="alert alert-danger alert-dismissable">
              <strong>No se encontraron resultados</strong> 
            </div>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header center-block">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Informaci&oacute;n del proveedor</h4>
            </div>
            <div class="modal-body" id="infoProveedores">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  $('#filtro2').on('change',function(){
    var filtro = $('#filtro2').val()
    var target = document.getElementById('resultados2')
    var vaciar_msg = document.getElementById('msg1')
    $.get('/administracion/proveedores/filtrar/'+filtro+'',function(response){
      var proveedores = response
      var template = ''
      target.innerHTML = ''
      if(vaciar_msg) vaciar_msg.innerHTML = ''
      var url_edit = '/administracion/proveedores/'
      var url_destroy = '/administracion/proveedores/'
      var url_activate = '/administracion/proveedores/'
      for(i = 0; i < proveedores.length; i++){
        template += '<tr><td data-toggle="modal" data-target="#myModal" data-id="'+proveedores[i].id+'"><input class="dataProveedor" name="compania" type="hidden" value="'+proveedores[i].nombre_compania_p+'"><input class="dataProveedor" name="tipo" type="hidden" value="'+proveedores[i].tipo+'"><input class="dataProveedor" name="contacto" type="hidden" value="'+proveedores[i].persona_contacto_p+'"><input class="dataProveedor" name="telefono" type="hidden" value="'+proveedores[i].telefono_p+'"><input class="dataProveedor" name="celular" type="hidden" value="'+proveedores[i].celular_p+'"><input class="dataProveedor" name="email" type="hidden" value="'+proveedores[i].email_p+'"><input class="dataProveedor" name="direccion_p" type="hidden" value="'+proveedores[i].direccion_p+'">'+proveedores[i].nombre_compania_p.substring(0, 20)+'..</td><td data-toggle="modal" data-target="#myModal" data-id="'+proveedores[i].id+'"><input class="dataProveedor" name="compania" type="hidden" value="'+proveedores[i].nombre_compania_p+'"><input class="dataProveedor" name="tipo" type="hidden" value="'+proveedores[i].tipo+'"><input class="dataProveedor" name="contacto" type="hidden" value="'+proveedores[i].persona_contacto_p+'"><input class="dataProveedor" name="telefono" type="hidden" value="'+proveedores[i].telefono_p+'"><input class="dataProveedor" name="celular" type="hidden" value="'+proveedores[i].celular_p+'"><input class="dataProveedor" name="email" type="hidden" value="'+proveedores[i].email_p+'"><input class="dataProveedor" name="direccion_p" type="hidden" value="'+proveedores[i].direccion_p+'">'+proveedores[i].tipo+'</td><td data-toggle="modal" data-target="#myModal" data-id="'+proveedores[i].id+'"><input class="dataProveedor" name="compania" type="hidden" value="'+proveedores[i].nombre_compania_p+'"><input class="dataProveedor" name="tipo" type="hidden" value="'+proveedores[i].tipo+'"><input class="dataProveedor" name="contacto" type="hidden" value="'+proveedores[i].persona_contacto_p+'"><input class="dataProveedor" name="telefono" type="hidden" value="'+proveedores[i].telefono_p+'"><input class="dataProveedor" name="celular" type="hidden" value="'+proveedores[i].celular_p+'"><input class="dataProveedor" name="email" type="hidden" value="'+proveedores[i].email_p+'"><input class="dataProveedor" name="direccion_p" type="hidden" value="'+proveedores[i].direccion_p+'">'+proveedores[i].persona_contacto_p.substring(0, 20)+'..</td><td data-toggle="modal" data-target="#myModal" data-id="'+proveedores[i].id+'"><input class="dataProveedor" name="compania" type="hidden" value="'+proveedores[i].nombre_compania_p+'"><input class="dataProveedor" name="tipo" type="hidden" value="'+proveedores[i].tipo+'"><input class="dataProveedor" name="contacto" type="hidden" value="'+proveedores[i].persona_contacto_p+'"><input class="dataProveedor" name="telefono" type="hidden" value="'+proveedores[i].telefono_p+'"><input class="dataProveedor" name="celular" type="hidden" value="'+proveedores[i].celular_p+'"><input class="dataProveedor" name="email" type="hidden" value="'+proveedores[i].email_p+'"><input class="dataProveedor" name="direccion_p" type="hidden" value="'+proveedores[i].direccion_p+'">'+proveedores[i].telefono_p+'</td><td data-toggle="modal" data-target="#myModal" data-id="'+proveedores[i].id+'"><input class="dataProveedor" name="compania" type="hidden" value="'+proveedores[i].nombre_compania_p+'"><input class="dataProveedor" name="tipo" type="hidden" value="'+proveedores[i].tipo+'"><input class="dataProveedor" name="contacto" type="hidden" value="'+proveedores[i].persona_contacto_p+'"><input class="dataProveedor" name="telefono" type="hidden" value="'+proveedores[i].telefono_p+'"><input class="dataProveedor" name="celular" type="hidden" value="'+proveedores[i].celular_p+'"><input class="dataProveedor" name="email" type="hidden" value="'+proveedores[i].email_p+'"><input class="dataProveedor" name="direccion_p" type="hidden" value="'+proveedores[i].direccion_p+'">'+proveedores[i].celular_p+'</td><td align="center"><div style="text-align: center"><a class="btn btn-accent" href="'+url_edit+proveedores[i].id+'/edit"><i class="fa fa-pencil"></i> Editar</a>&nbsp;'
        if(filtro == 1){
          template += '<a class="btn btn-danger" href="'+url_destroy+proveedores[i].id+'/destroy"><i class="fa fa-times"></i> Desactivar</a></div></td></tr>'
        }else{
          template += '<a class="btn btn-success" href="'+url_activate+proveedores[i].id+'/activate"><i class="fa fa-check"></i> Activar</a></div></td></tr>'
        }
      }
      target.innerHTML = template;
    });
  });
  $('#myModal').on('show.bs.modal', function (event) {
    //var getIdFromRow = $(event.relatedTarget).attr('data-id');
    // if you wnat to take the text of the first cell  
    //var compania = $(event.relatedTarget).find('td:nth-child(1)').html();
    //var contacto = $(event.relatedTarget).find('td:nth-child(2)').text();
    var data = Array()
    $(event.relatedTarget).find(".dataProveedor").each(function(i) {
      console.log(String(this.value))
      data[i] += String(this.value)
  }); 
    var template2 = '<div class="row"><div class="col-md-2"><strong class="c-white">Compañia: </strong></div><div class="col-md-10">'+data[0].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">Contacto: </strong></div><div class="col-md-10">'+data[1].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">Tipo: </strong></div><div class="col-md-10">'+data[2].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">Tel&eacute;fono: </strong></div><div class="col-md-10">'+data[3].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">Celular: </strong></div><div class="col-md-10">'+data[4].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">Direcci&oacute;n: </strong></div><div class="col-md-10">'+data[5].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">E-mail: </strong></div><div class="col-md-10">'+data[6].replace('undefined','')+'</div></div>'

    $(this).find('#infoProveedores').html($(template2));
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>