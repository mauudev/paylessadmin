<?php $__env->startSection('content'); ?>
<style type="text/css">
    #checkPPago1 {
    color: #0fef00;
    text-shadow: 1px 1px 1px;
    font-size: 1.5em;
}
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-filled">
                <div class="col-md-11">
                    <div class="view-header">
                        <div class="header-icon">
                            <i class="pe page-header-icon pe-7s-note2"></i>
                        </div>
                        <div class="header-title">
                            <?php if($venta->estado == 1 && $venta->finalizado == 1): ?>
                                <h3 class="page-header">Detalles de la venta</h3>
                            <?php else: ?>
                                <h3 class="page-header">Detalles de la cotizaci&oacute;n</h3>
                            <?php endif; ?>
                            <small>
                            Cotizaci&oacute;n de repuestos/mercader&iacute;as
                            </small>
                        </div>
                    </div>
                </div>
                <?php if($venta->estado == 1): ?>
                <div class="col-md-1"><br><br><br>
                    <a class="btn btn-accent" href="<?php echo URL::to('cotizaciones/'.$venta->id.'/edit'); ?>" data-tooltip="Editar cotizaci&oacute;n" onclick='return confirm("El estado de la venta cambiar&aacute; ¿Desea continuar?")'><i class="fa fa-edit"></i></a>
                </div>
                <?php else: ?>
                <div class="col-md-1"><br><br><br>
                    <a class="btn btn-accent" href="<?php echo URL::to('cotizaciones/'.$venta->id.'/edit'); ?>" data-tooltip="Editar cotizaci&oacute;n"><i class="fa fa-edit"></i></a>
                </div>
                <?php endif; ?>
                <?php echo Form::open(['route'=>'confirmar-venta.confirmarVenta','method'=>'POST','class'=>'form-group']); ?>

                <?php echo Form::hidden("ventas_id",$venta->id); ?>

                <?php echo e(csrf_field()); ?>

                <?php $usuario_auth = \Auth::user()->id; ?>
                <!-- Content -->

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="panel panel-filled panel-c-primary">
                                    <div class="panel-heading">
                                        <div class="panel-tools">
                                            <a class="panel-toggle"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <b>Datos del cliente</b>
                                    </div>
                                    <div class="panel-body" style="display: none;">
                                        <dl class="dl-horizontal">
                                                <dt>Cliente:</dt>
                                                <dd><?php echo e($cliente->nombre_c); ?></dd>
                                                <dt>Teléfono:</dt>
                                                <dd><?php echo e($cliente->telefono_c); ?></dd>
                                                <dt>Celular:</dt>
                                                <dd><?php echo e($cliente->celular_c); ?></dd>
                                                <dt>E-mail:</dt>
                                                <dd><?php echo e($cliente->email_c); ?></dd>
                                                <dt>Localidad:</dt>
                                                <dd><?php echo e($cliente->pais); ?></dd>
                                                <dt>Vía contacto:</dt>
                                                <dd><?php echo e($cliente->origen_contacto); ?></dd>
                                            </dl>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-filled panel-c-info">
                                    <div class="panel-heading">
                                        <div class="panel-tools">
                                            <a class="panel-toggle"><i class="fa fa-chevron-down"></i></a>
                                        </div>
                                        <b>Datos del responsable de venta</b>
                                    </div>
                                    <div class="panel-body" style="display: none;">
                                        <dl class="dl-horizontal">
                                            <dl class="dl-horizontal">
                                                <dt>Usuario:</dt>
                                                <dd><?php echo e($usuario->nombre_u); ?></dd>
                                                <dt>Teléfono:</dt>
                                                <dd><?php echo e($usuario->telefono_u); ?></dd>
                                                <dt>Celular:</dt>
                                                <dd><?php echo e($usuario->celular_u); ?></dd>
                                                <dt>E-mail:</dt>
                                                <dd><?php echo e($usuario->email_u); ?></dd>
                                                <dt>Tipo usuario:</dt>
                                                <?php if($usuario->type == 'admin'): ?>
                                                <dd>Administrador</dd>
                                                <?php else: ?>
                                                <dd>Usuario</dd>
                                                <?php endif; ?>
                                            </dl>
                                    </dl></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" id="descripcionTable">
                        <div class="panel panel-c-info">
                            <div class="panel-heading">
                                <h4>Descripci&oacute;n</h4>
                            </div>
                            <div class="panel-body" >  
                                <div class="table-responsive">
                                    <table class="table" id="sum_table">
                                        <thead>
                                            <tr class="titlerow">
                                                <th class="left-align">Item</th>
                                                <th class="left-align">Tipo</th>
                                                <th class="center">C&oacute;digo</th>
                                                <th class="center">Cantidad</th>
                                                <th align="center" id="precioUnitarioSinDTH">Precio Unitario</th>
                                                <th align="center">Precio Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if(isset($repuestos)): ?>
                                            <?php for($i = 0; $i < count($repuestos); $i++): ?>
                                            <tr>
                                                <td><?php echo $repuestos[$i]->marca_r." ".$repuestos[$i]->modelo_r." ".$repuestos[$i]->detalle_r; ?></td>
                                                <td><?php echo $repuestos[$i]->tipo; ?></td>
                                                <td align="center"><p class="text-accent"><?php echo $repuestos[$i]->codigo_repuesto; ?></p></td>
                                                <td class="cantidadPDescuento"><p class="text-info" align="center"><?php echo $repuestos[$i]->cantidad; ?></p></td>
                                                <td class="precioUnitarioSinDTD" align="center"><p class="text-success precioUnitarioSDValue"><?php echo e(round($repuestos[$i]->precio_total / $repuestos[$i]->cantidad,2)); ?> </p></td>
                                                <td class="totalValue" align="center"><p class="rowDataSd text-success"><?php echo e($repuestos[$i]->precio_total); ?> </p></td>
                                            </tr>
                                            <?php endfor; ?>
                                            <?php endif; ?>
                                            <?php if(isset($mercaderias)): ?>
                                            <?php for($i = 0; $i < count($mercaderias); $i++): ?>
                                            <tr>
                                                <td><?php echo $mercaderias[$i]->nombre_m; ?></td>
                                                <td><?php echo $mercaderias[$i]->tipo; ?></td>
                                                <?php if(filter_var($mercaderias[$i]->nro_item, FILTER_VALIDATE_URL) == true): ?> 
                                                    <td align="center"><a href="<?php echo e($mercaderias[$i]->nro_item); ?>" target="_blank"><?php echo e(str_limit($mercaderias[$i]->nro_item,$limit = 20,$end='..')); ?></a></td>
                                                <?php else: ?>
                                                <td align="center"><p class="text-accent"><?php echo e(str_limit($mercaderias[$i]->nro_item,$limit = 20,$end='..')); ?></p></td>
                                                <?php endif; ?>
                                                <td class="cantidadPDescuento"><p class="text-info" align="center"><?php echo $mercaderias[$i]->cantidad; ?></p></td>
                                                <td class="precioUnitarioSinDTD" align="center"><p class="text-success precioUnitarioSDValue"><?php echo e(round($mercaderias[$i]->precio_total / $mercaderias[$i]->cantidad, 2)); ?> </p></td>
                                                <td class="totalValue" align="center"><p class="rowDataSd text-success"><?php echo e($mercaderias[$i]->precio_total); ?> </p></td>
                                            </tr>
                                            <?php endfor; ?>
                                            <?php endif; ?>
                                            <tr class="totalColumn">
                                                <td>
                                                    <b>TOTAL<b>
                                                </td>
                                                <td></td>
                                                <td id="descuentoBtnTd">
                                                    <button type="button" class="btn btn-wd-md btn-accent" id="descuentoBtn"><i class="fa fa-money"></i> Aplicar descuento</button>
                                                </td>
                                                <td></td>
                                                <td></td>
                                                <td class="totalCol"></td>  
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <ul>
                                <?php if($venta->pago_parcial != null): ?>
                                <li>
                                    Primer pago realizado <i id="checkPPago1" class="fa fa-check"></i>
                                </li>
                                <?php endif; ?>
                                <?php if($venta->segundo_pago != null): ?>
                                <li>
                                    Segundo pago realizado <i id="checkPPago1" class="fa fa-check"></i>
                                </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <!-- aca habia pa descuento -->
                    </div>
                </div>
                <!-- aca habia pa descuento -->

            </div>
        </div>
    </div>
    <?php echo $__env->make('alerts.validation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('alerts.success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('alerts.error', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php if($venta->estado == 1 && $venta->finalizado == 1): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-warning" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <strong>Atenci&oacute;n!</strong> el estado de la venta cambiar&aacute; si deshabilita la cotizaci&oacute;n !
            </div>
        </div>
    </div>
    <?php endif; ?>
    <div class="row">
    <div class="form-group">
        <div class="col-md-12" >
            <?php if($venta->pago_parcial != null && $venta->segundo_pago != null): ?>
                <a class="btn btn-w-md btn-accent" href="<?php echo e(URL::to('/administracion/mostrar-cotizacion-principal/show/'. $venta->id)); ?>">Elementos de la cotizaci&oacute;n</a>
            <?php else: ?>
                <a class="btn btn-w-md btn-accent" href="<?php echo e(URL::to('/administracion/mostrar-cotizacion-principal/show/'. $venta->id)); ?>">Elementos de la cotizaci&oacute;n</a>
            <?php endif; ?>
            <?php if($venta->estado == 1): ?>
                    <?php if(\Auth::user()->type == 'admin'): ?>
                        <a class="btn btn-w-md btn-danger" href="<?php echo e(URL::to('/confirmar-venta/'.$venta->id .'/deshabilitar')); ?>"><i class="fa fa-times"></i> Deshabilitar cotizaci&oacute;n</a>
                    <?php endif; ?>
            <?php else: ?>
                <?php echo Form::submit('Finalizar cotizaci&oacute;n',['class'=>'btn btn-w-md btn-success']); ?>

                <script type="text/javascript">
                toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-center",
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      };
                    toastr.info("Presione <b>'Finalizar cotizaci&oacute;n'</b> para guardar los precios de la cotizaci&oacute;n ");
                </script>
            <?php endif; ?>
            <?php if($venta->estado == 1 && $venta->pago_parcial == null): ?>
                <?php if(\Auth::user()->type == 'admin'): ?>
                    <a class="btn btn-w-md btn-primary" href="<?php echo URL::to('/ventas/'.$venta->id.'/create'); ?>"><i class="fa fa-money"></i> Convertir a venta</a>
                <?php endif; ?>
            <?php endif; ?>
            <?php if($venta->pago_parcial != null & $venta->segundo_pago == null && $venta->estado != 0): ?>
                <a class="btn btn-w-md btn-primary" href="<?php echo URL::to('/ventas/segundo-pago/'.$venta->id.'/edit'); ?>"><i class="fa fa-money"></i> Agregar segundo pago</a>
            <?php elseif($venta->pago_parcial != null & $venta->segundo_pago != null && $venta->estado != 0): ?>
                <div class="btn-group dropup">
                    <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false"><i class="fa fa-money"></i> Pagos de la venta <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo e(URL::to('ventas/pagos/'.$venta->id.'/show')); ?>"><i class="fa fa-eye"></i> Ver detalle</a></li>
                        <li><a href="<?php echo URL::to('/ventas/pagos/'.$venta->id.'/edit'); ?>"><i class="fa fa-edit"></i> Editar pagos</a></li>
                    </ul>
                </div>
            <?php endif; ?>
            <?php if($venta->pago_total != null && $venta->estado != 0): ?>
                <div class="btn-group dropup">
                    <button data-toggle="dropdown" class="btn btn-success dropdown-toggle" aria-expanded="false"><i class="fa fa-file-pdf-o"></i> Generar PDF <span class="caret"></span></button>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo e(URL::to('ventas/generar-pdf/'.$venta->id)); ?>"><i class="fa fa-file-pdf-o"></i> Generar PDF por defecto</a></li>
                        <li><a href="<?php echo e(URL::to('ventas/generar-pdf/'.$venta->id.'/personalizar')); ?>"><i class="fa pe-7s-tools"></i> Personalizar PDF</a></li>
                    </ul>
                </div>
                <a href="<?php echo e(URL::to('sendmail/'.$venta->id.'/create')); ?>" class="btn btn-info btn-w-md"><i class="fa fa-send"></i> Enviar correo</a>
            <?php endif; ?>
        </div>
    </div>
</div><br>
</div>

<script type="text/javascript">
    actualizarTotal();
    function roundNumber(num, scale) {
      var number = Math.round(num * Math.pow(10, scale)) / Math.pow(10, scale);
      if(num - number > 0) {
        return (number + Math.floor(2 * Math.round((num - number) * Math.pow(10, (scale + 1))) / 10) / Math.pow(10, scale));
      } else {
        return number;
      }
    }
    $("#descuentoBtn").on("click",function(){
        template = '<div class="col-md-12"><div class="panel panel-filled panel-c-warning"><div class="panel-heading">Aplicar descuento</div><div class="panel-body"><div class="col-md-2">Monto a aplicar:</div><div class="col-md-4"><?php echo e(Form::text("descuento",null,["class"=>"form-control descuentoValue","placeholder"=>"Ingrese el monto de descuento","required","min"=>5])); ?></div><div class="col-md-4"><button type="button" class="btn btn-wd-md btn-accent aplicarDescBtn" ><i class="fa fa-check"></i> Aplicar descuento</button></div></div></div></div>'
        $("#descuentoBtnTd").html("")
        target = $("#descripcionTable")
        target.after(template)
    });
    $(document).on("click",".aplicarDescBtn",function(){
        descuento = $(".descuentoValue").val()
        if(!isNaN(descuento)){
            descuento = parseFloat(descuento)
            colTemplateTH = '<th class="left-align conDescTH">Con descuento</th>'
            colTempalteTD = '<td class="conDescTD cantidadPDescuento" align="center"><p class="text-success total-descuento"></p></td>'
            $(".conDescTH").remove()
            $(".conDescTD").remove()
            $(".adicional1").remove()
            $("#precioUnitarioSinDTH").after(colTemplateTH)
            $(".precioUnitarioSinDTD").after(colTempalteTD)
            $(".totalCol").before("<td class='adicional1'></td>")
            $('tr').each(function () {
                //the value of sum needs to be reset for each row, so it has to be set inside the row loop
                var sum = 0
                //find the combat elements in the current row and sum it 
                $(this).find('.precioUnitarioSinDTD').each(function () {
                    //a much shorter version to sum the values, using unary operator
                    sum += +$(this).text() || 0;
                });
                //set the value of currents rows sum to the total-combat element in the current row
                total = sum - descuento
                total = roundNumber(total,2)
                $('.total-descuento', this).html(total);
            });
            $('tr').each(function () {
                //the value of sum needs to be reset for each row, so it has to be set inside the row loop
                total = 1;
                $(this).find('.cantidadPDescuento').each(function () {
                    
                    total *= +$(this).text() || 0;
                    //set the value of currents rows total to the total-combat element in the current row
                    
                });
                $('.totalValue', this).html('<p class="rowDataSd text-success">'+total+'</td>');
            });
            actualizarTotal();
        }else{
            toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-center",
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      };
                    toastr.error("<b>Error !</b> ingrese un valor v&aacute;lido para el descuento. ");
        }
    });

function actualizarTotal(){
    var totals=[0,0,0];//para 3 columnas pero solo usaremos 1 <input type="hidden" name="country" value="Norway">
    $(document).ready(function(){
        var $dataRows=$("#sum_table tr:not('.totalColumn, .titlerow')");
        $dataRows.each(function() {
            $(this).find('.rowDataSd').each(function(i){        
                totals[i]+=parseFloat( $(this).html());
            });
        });
        $("#sum_table td.totalCol").each(function(i){  
            total = roundNumber(totals[i],2)
            $(this).html('<p class="text-info" align="center">'+total+' $<input type="hidden" name="pago_total" value="'+total+'"></p>');
        });
    });
}
    

</script>
<?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>