<div class="container-fluclass">
	<div class="row">
        <div class="col-lg-12">
            <div class="panel panel-filled">
                <div class="panel-heading">
                    Agregar detalles al invoice
                </div>
                <div class="panel-body">
                    <p>Puede agregar los siguientes elementos al PDF: <code>Observaciones</code>, <code>M&eacute;todo de pago</code>, <code>Plazo de entrega</code></p><br><b>Elementos:</b><br><br>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group observacionesDiv">
                            	<?php echo Form::checkbox('observacionesCB',1,false,['class'=>'observacionesCB']); ?>

                                <?php echo Form::label('observaciones',' Observaciones ',['class'=>'control-label']); ?><br>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group metodo_pagoDiv">
                            	<?php echo Form::checkbox('metodo_pagoCB',1,false,['class'=>'metodo_pagoCB']); ?>

                                <?php echo Form::label('metodo_pago','M&eacute;todo de pago ',['class'=>'control-label']); ?><br>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group plazo_entregaDiv">
                            	<?php echo Form::checkbox('plazo_entregaCB',1,false,['class'=>'plazo_entregaCB']); ?>

                                <?php echo Form::label('plazo_entrega','Plazo de entrega',['class'=>'control-label']); ?><br>
                            </div>
                        </div>
                    </div><br><br>
                </div>
            </div>
        </div>
    </div>
</div>