<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="view-header">
                <div class="header-icon">
                    <i class="pe page-header-icon pe-7s-note2"></i>
                </div>
                <div class="header-title">
                    <h3 class="page-header">Editar pago parcial</h3>
                    <small>
                    Cotizaci&oacute;n de repuestos/mercader&iacute;as
                    </small>
                </div>
            </div>
            <div class="panel-body">
                <?php echo Form::model($venta,['route'=>['ventas.update',$venta->id],'method'=>'PUT',"onsubmit"=>"return checkSubmit();"]); ?>

                <?php echo Form::hidden("ventas_id",$venta->id); ?>

                <?php echo e(csrf_field()); ?>

                <div class="row">
                    <div class="col-md-6 pull-right">
                        <div class="panel panel-c-success panel-collapse">
                            <div class="panel-heading">
                                <div class="panel-tools">
                                    <a class="panel-toggle"><i class="fa fa-chevron-down"></i></a>
                                </div>
                                Datos del responsable de venta
                            </div>
                            <div class="panel-body" style="display: none;">
                                <div class="col-md-2">
                                    <strong class="c-white">Nombre: </strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo e($usuario->nombre_u); ?>

                                </div><br>
                                <div class="col-md-2">
                                    <strong class="c-white">Apellidos: </strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo e($usuario->apellidos_u); ?>

                                </div><br>
                                <div class="col-md-2">
                                    <strong class="c-white">Tipo: </strong>
                                </div>
                                <div class="col-md-8">
                                    <?php if($usuario->type == 'admin'): ?>
                                    Administrador
                                    <?php else: ?>
                                    Usuario
                                    <?php endif; ?>
                                </div><br>
                                <div class="col-md-2">
                                    <strong class="c-white">Teléfono: </strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo e($usuario->telefono_u); ?>

                                </div><br>
                                <div class="col-md-2">
                                    <strong class="c-white">Celular: </strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo e($usuario->celular_u); ?>

                                </div><br>
                                <div class="col-md-2">
                                    <strong class="c-white">E-mail: </strong>
                                </div>
                                <div class="col-md-8">
                                    <?php echo e($usuario->email_u); ?>

                                </div><br>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-c-danger panel-collapse">
                            <div class="panel-heading">
                                <div class="panel-tools">
                                    <a class="panel-toggle"><i class="fa fa-chevron-down"></i></a>
                                </div>
                                Datos del cliente
                            </div>
                            <div class="panel-body" style="display: none;">
                                <div class="row">
                                    <div class="col-md-2"><strong class="c-white">Nombre: </strong></div><div class="col-md-9"> <?php echo e($cliente->nombre_c); ?></div><br>
                                    <div class="col-md-2"><strong class="c-white">Apellidos: </strong></div><div class="col-md-9"> <?php echo e($cliente->apellidos_c); ?></div><br>
                                    <div class="col-md-2"><strong class="c-white">Dirección: </strong></div>-<div class="col-md-9"> <?php echo e($cliente->direccion_c); ?></div><br>
                                    <div class="col-md-2"><strong class="c-white">Teléfono: </strong></div><div class="col-md-9"> <?php echo e($cliente->telefono_c); ?></div><br>
                                    <div class="col-md-2"><strong class="c-white">Celular: </strong></div><div class="col-md-9"> <?php echo e($cliente->celular_c); ?></div><br>
                                    <div class="col-md-2"><strong class="c-white">E-mail: </strong></div><div class="col-md-9"> <?php echo e($cliente->email_c); ?></div><br>
                                    <div class="col-md-2"><strong class="c-white">Localidad: </strong></div><div class="col-md-9"> <?php echo e($cliente->pais); ?></div><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><hr>
                <div class="row">
                    <div class="col-md-8">
                        <div class="panel panel-filled">
                            <div class="panel-heading">
                                Total y saldos
                            </div>
                            <div class="panel-body">
                                <div class="bs-example">
                                    <strong class="c-white">Total USD:</strong> <b class="text-info"><?php echo e($venta->pago_total); ?></b>&nbsp;&nbsp;<strong class="c-white">Total Bs:</strong> <b class="text-info"><?php echo e($venta->pago_total_bs); ?></b>
                                    <?php if($venta->descuento != null): ?>
                                    <?php endif; ?>
                                    <?php echo e(Form::hidden('pago_total_usd',$venta->pago_total,["id"=>"pago_total_usd_1"])); ?>

                                    <?php echo e(Form::hidden('pago_total_bs',$venta->pago_total_bs,["id"=>"pago_total_bs_1"])); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-filled">
                            <div class="panel-heading">
                                Tipo de cambio de la venta 
                            </div>
                            <div class="panel-body">
                                <div class="bs-example">
                                    <strong class="c-white">Tipo de cambio:</strong> <b class="text-info"><?php echo e($venta->tipo_cambio); ?></b>
                                    <?php echo e(Form::hidden('tipo_cambio',$venta->tipo_cambio,['class'=>'tipo_cambio_value'])); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="panel">
                            <div class="panel-heading">
                                Detalle del pago parcial
                                <?php if(isset($pago_efectivo)): ?>
                                <?php echo Form::hidden('pagos_efectivo_id',$pago_efectivo->id); ?>

                                <?php elseif(isset($pago_deposito)): ?>
                                <?php echo Form::hidden('pagos_deposito_id',$pago_deposito->id); ?>

                                <?php endif; ?>
                            </div>
                            <div class="panel-body">
                                <div class="v-timeline vertical-container">
                                    <div class="vertical-timeline-block">
                                        <div class="vertical-timeline-icon">
                                            <i class="fa fa-check text-success"></i>
                                        </div>
                                        <div class="vertical-timeline-content">
                                            <div class="p-sm">
                                                <?php if(isset($pago_efectivo)): ?>
                                                <span class="vertical-date pull-right"> <b class="text-info"><?php echo e($pago_efectivo->created_at); ?></b> </span>
                                                <?php endif; ?>
                                                <?php if(isset($pago_deposito)): ?>
                                                <span class="vertical-date pull-right"> <b class="text-info"><?php echo e($pago_deposito->created_at); ?></b> </span>
                                                <?php endif; ?><br><br>
                                                <div class="row">
                                                    <div class="col-md-12" id="cambiarTipoPagoDiv1">
                                                        <div class="col-md-3">
                                                            <strong class="c-white">Tipo de pago: </strong>
                                                            <?php if(isset($pago_efectivo)): ?>
                                                            <?php echo Form::hidden('tipo_pago1','efectivo-pp'); ?>

                                                            <?php elseif(isset($pago_deposito)): ?>
                                                            <?php echo Form::hidden('tipo_pago1','deposito-pp'); ?>

                                                            <?php echo Form::hidden('cuenta_bancaria_pp',$cuenta_banco_dep->id); ?>

                                                            <?php endif; ?>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <?php if(isset($pago_efectivo)): ?>
                                                            <b class="text-accent">Pago en efectivo</b>
                                                            <?php elseif(isset($pago_deposito)): ?>
                                                            <b class="text-accent">Dep&oacute;sito a cuenta</b>
                                                            <?php endif; ?>
                                                            
                                                        </div>
                                                        <div class="col-md-5">
                                                            <a class="btn btn-accent btn-xs pull-right" id="cambiarTipoPagoBtn1"><i class="fa fa-edit"></i> Cambiar</a>
                                                        </div>
                                                    </div><br>
                                                    <div class="col-md-12" id="cuentaBancariaPP">
                                                        
                                                    </div><br>
                                                    <?php if(isset($pago_deposito)): ?>
                                                    <div class="col-md-12">
                                                        <div class="col-md-3">
                                                            <strong class="c-white">Cuenta actual:</strong>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <b class="text-success" id="cuenta_actual_texto"><?php echo $cuenta_banco_dep->banco.' '.$cuenta_banco_dep->cuenta_bancaria.' '.$cuenta_banco_dep->moneda; ?></b>
                                                        </div>
                                                    </div>
                                                    <?php endif; ?>
                                                    <div class="col-md-12">
                                                        <div class="col-md-3">
                                                            <strong class="c-white">C&oacute;digo recibo/dep&oacute;sito:</strong>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <?php if(isset($pago_efectivo)): ?>
                                                            <?php echo Form::text('codigo_recibo_pp',$pago_efectivo->codigo_recibo,['class'=>'form-control','placeholder'=>'Ingrese el monto','required']); ?>

                                                            <?php elseif(isset($pago_deposito)): ?>
                                                            <?php echo Form::text('codigo_recibo_pp',$pago_deposito->codigo_recibo,['class'=>'form-control','placeholder'=>'Ingrese el monto','required']); ?>

                                                            <?php endif; ?>
                                                        </div>
                                                    </div><br><br>
                                                    <div class="col-md-12 before-tipo-cambio">
                                                        <div class="col-md-3">
                                                            <strong class="c-white">Monto cancelado: </strong>
                                                        </div>
                                                        <?php if(isset($pago_efectivo)): ?>
                                                        <?php if($pago_efectivo->moneda == 'Bolivianos'): ?>
                                                        <div class="col-md-5">
                                                            <?php echo Form::text('pago_parcial',$pago_efectivo->monto_bs,['class'=>'form-control','placeholder'=>'Ingrese el monto','required','id'=>'pagoParcial']); ?>

                                                        </div>
                                                        <div class="col-md-4 monedaDiv">
                                                            <?php echo Form::select("moneda",["Bolivianos"=>"Bolivianos","Dolares"=>"D&oacute;lares"],null,['class'=>'form-control monedaSelect1','required']); ?>

                                                        </div>
                                                    </div><br><br>
                                                    <div class="col-md-12 tipo-cambio-div">
                                                        <div class="col-md-3">
                                                            <strong class="c-white">Tipo de cambio: </strong>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <?php echo Form::text('tipo_cambio',$pago_efectivo->tipo_cambio,['class'=>'form-control','placeholder'=>'Ingrese el monto','required','id'=>'tipo_cambio_input']); ?>

                                                        </div>
                                                    </div>
                                                    <?php endif; ?>
                                                    <?php if($pago_efectivo->moneda == 'Dolares'): ?>
                                                    <div class="col-md-5">
                                                        <?php echo Form::text('pago_parcial',$pago_efectivo->monto,['class'=>'form-control','placeholder'=>'Ingrese el monto','required','id'=>'pagoParcial']); ?>

                                                    </div>
                                                    <div class="col-md-4 monedaDiv">
                                                        <?php echo Form::select("moneda",["Dolares"=>"D&oacute;lares","Bolivianos"=>"Bolivianos"],null,['class'=>'form-control monedaSelect1','required']); ?>

                                                    </div>
                                                    </div><br><br>
                                                    <div class="col-md-12 tipo-cambio-div">
                                                        <div class="col-md-3">
                                                            <strong class="c-white">Tipo de cambio: </strong>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <?php echo Form::text('tipo_cambio',$pago_efectivo->tipo_cambio,['class'=>'form-control','placeholder'=>'Ingrese el monto','required','id'=>'tipo_cambio_input']); ?>

                                                        </div>
                                                    </div>
                                                    <?php endif; ?>
                                                    <?php elseif(isset($pago_deposito)): ?>
                                                    <?php if($pago_deposito->moneda == 'Bolivianos'): ?>
                                                    <div class="col-md-5">
                                                        <?php echo Form::text('pago_parcial',$pago_deposito->monto_bs,['class'=>'form-control','placeholder'=>'Ingrese el monto','required','id'=>'pagoParcial']); ?>

                                                    </div>
                                                    <div class="col-md-4 monedaDiv">
                                                        <?php echo Form::select("moneda",["Bolivianos"=>"Bolivianos","Dolares"=>"D&oacute;lares"],null ,['class'=>'form-control monedaSelect1','required']); ?>

                                                    </div>
                                                </div><br>
                                                <div class="col-md-12 tipo-cambio-div">
                                                    <div class="col-md-3">
                                                        <strong class="c-white">Tipo de cambio: </strong>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <?php echo Form::text('tipo_cambio',$pago_deposito->tipo_cambio,['class'=>'form-control','placeholder'=>'Ingrese el monto','required','id'=>'tipo_cambio_input']); ?>

                                                    </div>
                                                </div>
                                                <?php endif; ?>
                                                <?php if($pago_deposito->moneda == 'Dolares'): ?>
                                                <div class="col-md-5">
                                                    <?php echo Form::text('pago_parcial',$pago_deposito->monto,['class'=>'form-control','placeholder'=>'Ingrese el monto','required','id'=>'pagoParcial']); ?>

                                                </div>
                                                <div class="col-md-4 monedaDiv">
                                                    <?php echo Form::select("moneda",["Dolares"=>"D&oacute;lares","Bolivianos"=>"Bolivianos"],null,['class'=>'form-control monedaSelect1','required']); ?>

                                                </div>
                                            </div><br>
                                            <div class="col-md-12 tipo-cambio-div">
                                                <div class="col-md-3">
                                                    <strong class="c-white">Tipo de cambio: </strong>
                                                </div>
                                                <div class="col-md-4">
                                                    <?php echo Form::text('tipo_cambio',$pago_deposito->tipo_cambio,['class'=>'form-control','placeholder'=>'Ingrese el monto','required','id'=>'tipo_cambio_input']); ?>

                                                </div>
                                            </div>
                                            <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $__env->make('alerts.validation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->make('alerts.error', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="row">
    <div class="form-group">
        <div class="col-md-7" >
            <?php echo Form::submit('Guardar',['class'=>'btn btn-w-md btn-accent','id'=>'submit1']); ?> <a class="btn btn-w-md btn-default" href="<?php echo url('confirmar-venta/'.$venta->id.'/detalle'); ?>">Cancelar</a>
        </div>
    </div>
    <?php echo Form::close(); ?>

</div>
</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
    res = 1; //variable para verificar si existe incoherencia en la cuenta bancaria y el tipo de moneda
    $(document).on({//SIRVE PARA INPUTS CREADOS DINAMICAMENTE PUTO PROBLEMA -.- !
          'focus': function () {
              //hacer algo aca
          },
          'blur': function (e) {

          },//SIGUIENTE PASO OBTENER EL TIPO DE CAMBIO SI EXISTE !
          'change':function(){
            str = $(".cuentaBancaria1 option:selected").text()
            moneda = $(".monedaSelect1 option:selected").val()
            tipo_cambio = <?php echo json_encode($tipo_cambio); ?>

            if(tipo_cambio == '') tipo_cambio = 6.96
            if(str != ''){
                index = str.indexOf(moneda)
                if(index == -1){
                    $(".cuentaBancariaDiv").attr("class","col-md-9 cuentaBancariaDiv has-error")
                    $(".monedaDiv").attr("class","col-md-4 monedaDiv has-error")
                    res = -1;
                }else{
                    $(".cuentaBancariaDiv").attr("class","col-md-9 cuentaBancariaDiv")
                    $(".monedaDiv").attr("class","col-md-4 monedaDiv")
                    res = 1;   
                }
            }
          },
        }, '.monedaSelect1');
    $(document).on({//SIRVE PARA INPUTS CREADOS DINAMICAMENTE PUTO PROBLEMA -.- !
          'focus': function () {
              //hacer algo aca
          },
          'blur': function (e) {

          },//SIGUIENTE PASO OBTENER EL TIPO DE CAMBIO SI EXISTE !
          'change':function(){
            str = $(".cuentaBancaria1 option:selected").text()
            moneda = $(".monedaSelect1 option:selected").val()
            tipo_cambio = <?php echo json_encode($tipo_cambio); ?>

            if(tipo_cambio == '') tipo_cambio = 6.96
            if(str != ''){
                index = str.indexOf(moneda)
                if(index == -1){
                    $(".cuentaBancariaDiv").attr("class","col-md-9 cuentaBancariaDiv has-error")
                    $(".monedaDiv").attr("class","col-md-4 monedaDiv has-error")
                    res = -1;
                }else{
                    $(".cuentaBancariaDiv").attr("class","col-md-9 cuentaBancariaDiv")
                    $(".monedaDiv").attr("class","col-md-4 monedaDiv")
                    res = 1;    
                }
            }
          },
        }, '.cuentaBancaria1');

        $("#cambiarTipoPagoBtn1").on('click',function(){
            template = '<div class="col-md-3"><strong class="c-white">Tipo de pago: </strong></div><div class="col-md-9"><?php echo Form::select("tipo_pago1", ["efectivo-pp"=>"Pago en efectivo","deposito-pp"=>"Deposito a cuenta"],null,["class"=>"form-control tipo_pago1","required"]); ?><br></div>'
            $("#cambiarTipoPagoDiv1").html(template)
            $("#cuenta_actual_texto").removeAttr('id')
        });
        $(document).on('change', '.tipo_pago1', function(){
            if($(".tipo_pago1").val() == 'deposito-pp'){
                template = '<div class="col-md-3"><strong class="c-white">Cuenta bancaria:</strong></div><div class="col-md-9 cuentaBancariaDiv"><?php echo Form::select("cuenta_bancaria_pp", $cuentas,null,["class"=>"form-control cuentaBancaria1","required",'placeholder'=>'Seleccione una cuenta bancaria..']); ?><br></div>'
                $("#cuentaBancariaPP").html(template)
            }else{
                $("#cuentaBancariaPP").html('')
            }
        }); 
        $("#submit1").on('click',function(){
            cuenta_text = $("#cuenta_actual_texto").text()
            moneda = $(".monedaSelect1 option:selected").val()
            //alert(moneda)
            if(cuenta_text != ''){
                if(cuenta_text.indexOf(moneda) == -1){
                    toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-center",
                                "preventDuplicates": true,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                              };
                toastr.error('<b>Error!</b> El tipo de moneda de la cuenta bancaria debe ser igual al ingresado.');
                $(".monedaDiv").attr("class","col-md-4 monedaDiv has-error")
                return false; //error de tipo de moneda
                }
            }
            if(res < 0){
                toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-center",
                                "preventDuplicates": true,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                              };
                toastr.error('<b>Error!</b> El tipo de moneda de la cuenta bancaria debe ser igual al ingresado.');
                return false; //error de tipo de moneda
            }
            
            moneda = $(".monedaSelect1").val()
            pagoTotal = 0
            pagoParcial = 0
            if(moneda == 'Bolivianos'){ 
                pagoTotal = $('#pago_total_bs_1').val()
                pagoParcial = $("#pagoParcial").val()
            } 
            if(moneda == 'Dolares'){
                pagoTotal = $('#pago_total_usd_1').val()
                pagoParcial = $("#pagoParcial").val()
            } 
            pagoTotal = parseFloat(pagoTotal)
            pagoParcial = parseFloat(pagoParcial)
            if(pagoParcial >= pagoTotal){
                toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-center",
                            "preventDuplicates": true,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                          };
                toastr.error('<b>Error!</b> el pago parcial debe ser menor al pago total !');
                return false;
            }
        });
    });
</script>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>