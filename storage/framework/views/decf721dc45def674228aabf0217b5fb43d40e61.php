<?php $__env->startSection('content'); ?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-filled">
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-plane"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Actualizar datos del proveedor</h3>
						<small>
						Gesti&oacute;n de proveedores
						</small>
					</div>
				</div>
				<div class="panel-body">
					<?php echo Form::model($proveedor,['route'=>['proveedores.update',$proveedor->id],'method'=>'PUT']); ?>

					<?php echo Html::script('js/jquery.maskedinput.js'); ?>

					<?php echo $__env->make('proveedores.form.form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $__env->make('alerts.validation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="form-group">
	<div class="col-md-7" >
		<?php echo Form::submit('Actualizar',['class'=>'btn btn-w-md btn-accent']); ?> <a class="btn btn-w-md btn-default" href="<?php echo URL::to('proveedores'); ?>">Cancelar</a>
	</div>
</div>
<?php echo Form::close(); ?>

<?php echo Form::open(['route'=>['marcas-prov.destroy',':OPTION_ID_PM'],'method'=>'DELETE','id'=>'form-delete-pm']); ?>

<script type="text/javascript">
	var limite = <?php echo json_encode($count); ?>

	$(document).on('click','.removeMarca1-edit',function(e){
		e.preventDefault();
		if(limite > 1){
	if(confirm('Está seguro de eliminar? Esta opción NO se puede deshacer !')){
	var id = $(this).parent('span').attr('opcion-id-pm');
	console.log(id)
	var form = $('#form-delete-pm');
	var url = form.attr('action').replace(':OPTION_ID_PM',id);
	var data = form.serialize();
	$.post(url, data, function(result){
	//alert(result);
	});
	$(this).parents('.marca-container-edit').remove();
	limite --;
	}
	}else{
		toastr.options = {
	"closeButton": true,
	"debug": false,
	"newestOnTop": false,
	"progressBar": false,
	"positionClass": "toast-top-center",
	"preventDuplicates": true,
	"onclick": null,
	"showDuration": "300",
	"hideDuration": "1000",
	"timeOut": "5000",
	"extendedTimeOut": "1000",
	"showEasing": "swing",
	"hideEasing": "linear",
	"showMethod": "fadeIn",
	"hideMethod": "fadeOut"
	};
	toastr.error('<b>No se puede eliminar este elemento !</b> Debe existir al menos una marca que el proveedor distribuya');
				}
	});
	$(document).on('click','.addMarca1',function(){
		var template = '<div class="row marca-container"><div class="col-lg-4" ><div class="form-group"><?php echo Form::label("marca_prov","Marca del proveedor: ",["class"=>"control-label"]); ?><?php echo Form::text("marca_prov[]",null,["class"=>"form-control","placeholder"=>"Ingrese la marca que distribuye el proveedor","required","min"=>5]); ?></div></div><div class="col-lg-2"><div class="form-group"><?php echo Form::label("opcion","Quitar:",["class"=>"control-label"]); ?><br/><button type="button" class="btn btn-danger fa fa-trash block-center removeMarca1"></button></div></div></div>'
		$('#boton-add1').before(template)
	});
	$(document).on('click','.removeMarca1',function(e){
		e.preventDefault();
		$(this).parents('.marca-container').remove();
	});
	jQuery(function($){
	   $("#date").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
	   $("#telefono_p_1").mask("(999) 999-9999? x9999");
	   $("#celular_p_1").mask("(999) 999-9999");
	   $("#phone").mask("99-9999999");
	   $("#ssn").mask("999-99-9999");
	});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>