
  <?php if(Session::has('store-success')): ?>
    <div class="alert alert-success" role="alert" id="msg_confirmation1">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong><i class="fa fa-check"></i> Exito!</strong> <?php echo session('store-success'); ?>

    </div>
  <?php elseif(Session::has('update-success')): ?>
    <div class="alert alert-success" role="alert" id="msg_confirmation1">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong><i class="fa fa-check"></i> Exito!</strong> <?php echo session('update-success'); ?>

    </div>
  <?php elseif(Session::has('edit-warning')): ?>
    <div class="alert alert-warning" role="alert" id="msg_confirmation1">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong><i class="fa fa-warning"></i> NOTA:</strong> <?php echo session('edit-warning'); ?>

    </div>
  <?php elseif(Session::has('delete-success')): ?>
    <div class="alert alert-success" role="alert" id="msg_confirmation1">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong><i class="fa fa-check"></i> Exito!</strong> <?php echo session('delete-success'); ?>

    </div>
  <?php elseif(Session::has('toast-actualizado')): ?>
  <script type="text/javascript">
    toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-center",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
    };
    toastr.success('<b>Exito!</b> <?php echo session("toast-actualizado"); ?> ');
  </script>
  <?php endif; ?>
  <script type="text/javascript">
    $(document).ready(function(){
        $("#msg_confirmation1").fadeOut(6000)
    });
  </script>