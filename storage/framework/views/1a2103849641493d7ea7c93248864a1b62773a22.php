<!DOCTYPE html>
<html>
    <!-- Mirrored from webapplayers.com/luna_admin-v1.1/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Aug 2016 21:37:16 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>
        <!-- Page title -->
        <title>Payless | Iniciar sesi&oacute;n</title>
        <!-- Vendor styles -->
        <?php echo Html::style("vendor/fontawesome/css/font-awesome.css"); ?>

        <?php echo Html::style("vendor/animate.css/animate.css"); ?>

        <?php echo Html::style("vendor/bootstrap/css/bootstrap.css"); ?>

        <!-- App styles -->
        <?php echo Html::style("styles/pe-icons/pe-icon-7-stroke.css"); ?>

        <?php echo Html::style("styles/pe-icons/helper.css"); ?>

        <?php echo Html::style("styles/stroke-icons/style.css"); ?>

        <?php echo Html::style("styles/style.css"); ?>


        <?php echo Html::script("vendor/pacejs/pace.min.js"); ?>

        <?php echo Html::script("vendor/jquery/dist/jquery.min.js"); ?>

        <?php echo Html::script("vendor/bootstrap/js/bootstrap.min.js"); ?>

        <!-- App scripts -->
        <?php echo Html::script("scripts/luna.js"); ?>

    </head>
    <body class="blank" background="<?php echo e(asset('images/payless.jpg')); ?>">
        <!-- Wrapper-->
        <div class="wrapper">
            <!-- Main content-->
            <section class="content">
                <div class="container-center animated slideInDown">
                    <div class="view-header">
                        <div class="header-icon">
                            <i class="pe page-header-icon pe-7s-unlock"></i>
                        </div>
                        <div class="header-title">
                            <h3>Area restringida</h3>
                            <small>
                            Porfavor inicie sesi&oacute;n para continuar..
                            </small>
                        </div>
                    </div>
                    <div class="panel panel-filled">
                        <div class="panel-body">
                            <?php echo Form::open(['route'=>'login.store','method'=>'POST']); ?>

                            <fieldset>
                                <div class="form-group">
                                    <label class="control-label" for="user_name">Username</label>
                                    <?php echo Form::text('user_name',null,['class'=>'form-control','placeholder'=>'Username', 'required','autofocus']); ?>

                                    <span class="help-block small">Ingrese su nombre de usuario</span>
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="password">Password</label>
                                    <?php echo Form::password('password', ['class' => 'form-control','placeholder'=>'Password','required']); ?>

                                    <span class="help-block small">Ingrese su contrasena</span>
                                </div>
                                <div>
                                    <?php echo Form::submit('Login',['class'=>'btn btn-w-md btn-accent btn-block']); ?>

                                </div>
                            </fieldset>
                            <?php echo Form::close(); ?>

                        </div>
                    </div>
                    <?php echo $__env->make('alerts.success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('alerts.error', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('alerts.validation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    <?php echo $__env->make('alerts.unauthorized', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                </div>
            </section>
            <!-- End main content-->
        </div>
        <!-- End wrapper-->
        <!-- Vendor scripts -->

    </body>
    <!-- Mirrored from webapplayers.com/luna_admin-v1.1/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Aug 2016 21:37:16 GMT -->
</html>