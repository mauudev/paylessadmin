<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Payless Import - Invoice</title>
        <?php echo Html::style('css/invoice-style.css'); ?>

        <style type="text/css">
        #watermark {
        opacity: .9;
        position: absolute;
        top: 90%;
        }
        #brFixed {
            display: block; /* makes it have a width */
            content: ""; /* clears default height */
            margin-top: 0px; /* change this to whatever height you want it */
        }
        table hr{
        position: absolute;
        left: 0;
        bottom: 0;
        width: 100%;
        margin: 0;
        top:89%;
        }
        #totalMark1
        {
         font-size: 10px;
         padding-left: 87%;
         padding-right: 0%;
         padding-bottom: 0%;
         padding-top: 0%;
         color:black;
        }
        #smallfixed{
            display: block;
            font-size: 0.83em;
            padding-right: 0%;
            padding-bottom: 0%;
            padding-top: 0%;
            
        }

        </style>
    </head>
    <body>
        
        <div class="invoice-box">
            <table class="qrtable" cellpadding="0" cellspacing="0" border="0" style="empty-cells: hide;border-collapse: separate; ">
                <tr class="information">
                    <td align="left" class="heading"><br>
                        <img src="<?php echo e(asset('/images/payless.jpg')); ?>" style="width:120px; max-width:300px;">
                    </td>
                    <td align="right">
                        <h3><b>COTIZACION # PG<?php echo e($venta->id); ?></b><br>
                        FECHA: <?php if(isset($venta)): ?><?php echo e(date('d-m-Y')); ?><?php endif; ?></h3>
                    </td>
                    <td class="qrcode">
                        <img src="<?php echo e(asset('/qrcodes/qrcode.png')); ?>" style="width:140px; max-width:300px;">
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" style="empty-cells: hide;border-collapse: separate; " >
                <tr class="heading header-text">
                    <td>CLIENTE</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>ASESOR DE VENTA</td>
                </tr>
                <?php if(isset($cliente) && isset($usuario)): ?>
                <tr class="datos-cliente-usuario">
                    <td>
                        <?php if($cliente->nombre_c != null): ?><b>NOMBRE: </b><?php echo e($cliente->nombre_c.' '.$cliente->apellidos_c); ?><?php endif; ?><br>
                        <?php if($cliente->direccion_c != null): ?><b>DIRECCION: </b><?php echo e($cliente->direccion_c); ?><?php endif; ?><br>
                        <?php if($cliente->telefono_c != null): ?><b>TELEFONO: </b><?php echo e($cliente->telefono_c); ?><?php endif; ?><br>
                        <?php if($cliente->celular_c != null): ?><b>CELULAR: </b><?php echo e($cliente->celular_c); ?><?php endif; ?><br>
                        <?php /* <?php if($cliente->email_c != null): ?><b>EMAIL: </b><?php echo e($cliente->email_c); ?><?php endif; ?> */ ?>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <?php if($usuario->nombre_u != null): ?><b>NOMBRE: </b><?php echo e($usuario->nombre_u.' '.$usuario->apellidos_u); ?><?php endif; ?><br>
                        <?php if($usuario->telefono_u != null): ?><b>TELEFONO: </b><?php echo e($usuario->telefono_u); ?><?php endif; ?><br>
                        <?php if($usuario->celular_u != null): ?><b>CELULAR: </b><?php echo e($usuario->celular_u); ?><?php endif; ?><br>
                        <?php if($usuario->email_u != null): ?><b>EMAIL: </b><?php echo e($usuario->email_u); ?><?php endif; ?>
                    </td>
                </tr>
                <?php endif; ?>
            </table>
            <table class="elementos-invoice" style="width:100%; border-collapse: collapse;border-spacing: 0;margin: 0;padding: 0;" border="1">
                <tr class="heading header-text">
                    <td style="border:none;" class="col-details-headers-text"><small>ITEM</small></td>
                    <td style="border:none;" class="col-details-headers-text"><small>DESCRIPCION</small></td>
                    <td style="border:none;" class="col-details-headers-text"><small>CANTIDAD</small></td>
                    <td style="border:none;" class="col-details-headers-text"><small>PRECIO UNITARIO</small></td>
                    <td style="border:none;" class="col-details-headers-text"><small>PRECIO TOTAL</small></td>
                </tr>
                <?php $contador = 1;?>
                <?php if(isset($repuestos)): ?>
                <?php for($i = 0; $i < count($repuestos); $i++): ?>
                <tr class="item">
                    <td class="td-details-item"><?php echo e($contador); ?></td>
                    <td class="td-details-desc"><?php echo $repuestos[$i]->marca_r." ".$repuestos[$i]->modelo_r." ".$repuestos[$i]->anio_r." ".$repuestos[$i]->detalle_r; ?> </td>
                    <td class="td-details-prices"><?php echo $repuestos[$i]->cantidad; ?></td>
                    <td class="td-details-prices"><?php echo e(round($repuestos[$i]->precio_total / $repuestos[$i]->cantidad,2)); ?> $</td>
                    <td class="td-details-prices"><?php echo e($repuestos[$i]->precio_total); ?> $</td>
                </tr>
                <?php $contador += 1;?>
                <?php endfor; ?>
                <?php endif; ?>
                <?php if(isset($mercaderias)): ?>
                <?php for($i = 0; $i < count($mercaderias); $i++): ?>
                <tr class="item">
                    <td class="td-details-item"><?php echo e($contador); ?></td>
                    <td class="td-details-desc"><?php echo $mercaderias[$i]->nombre_m; ?></td>
                    <td class="td-details-prices"><?php echo $mercaderias[$i]->cantidad; ?></td>
                    <td class="td-details-prices"><?php echo e(round($mercaderias[$i]->precio_total / $mercaderias[$i]->cantidad, 2)); ?> $</td>
                    <td class="td-details-prices"><?php echo e($mercaderias[$i]->precio_total); ?> $</td>
                </tr>
                <?php $contador += 1;?>
                <?php endfor; ?>
                <?php endif; ?>
            </table><br>
            
            <table style="width:100%; border-collapse: collapse;border-spacing: 0;margin: 0;" border="1" class="observaciones">
                <tr>
                    <td style="border:none;">
                        <b>OBSERVACIONES<small id="smallfixed">El precio cotizado es antes de IVA</small>
                        <?php if($observaciones != ''): ?>)
                        <small><?php echo e($observaciones); ?></small>
                        <?php endif; ?>
                        <b id="totalMark1">TOTAL: <?php echo e($venta->pago_total); ?> $</b>
                    </td>
                </tr>

            </table>
            <h5 id="terminosCond1"><b><u>TERMINOS Y CONDICIONES DE LA OFERTA</u></b><br>
            <?php if($metodo_pago != ''): ?>
            <b><u>M&Eacute;TODO DE PAGO:</u></b><small> <?php echo e($metodo_pago); ?></small><br>
            <?php else: ?>
            <b><u>M&Eacute;TODO DE PAGO:</u></b><small> 50% del total por adelantado, 50% del total contra-entrega. Los pagos deben realizarse en la cuenta corriente de la empresa.</small><br>
            <?php endif; ?>
            <?php if($plazo_entrega != ''): ?>
            <b><u>PLAZO DE ENTREGA:</u></b><small> <?php echo e($plazo_entrega); ?></small><br>
            <?php else: ?>
            <b><u>PLAZO DE ENTREGA:</u></b><small> 15-20 d&iacute;as a partir del adelanto.</small><br>
            <?php endif; ?>
            <b><u>DATOS BANCARIOS:</u></b><br>
            NOMBRE: Payless Import<br> 
            BANCO UNIÓN<br>
            CUENTA CORRIENTE BOLIVIANOS No. 1-23026368<br>
            CUENTA CORRIENTE DOL&Aacute;RES No. 2-23026384<br>
            </h5>
            <table id="watermark" class="contact" cellpadding="0" cellspacing="0" border="0" style="empty-cells: hide;border-collapse: separate; " ><hr>
                <tr>
                    <td class="contact-td-padding"><b>COCHABAMBA - BOLIVIA</b></td>
                    <td class="contact-miami contact-td-padding"><b>MIAMI - USA</b></td>
                </tr>
                <tr>
                    <td class="contact-td-padding" style="border:none;">
                        Av. Blanco galindo casi Av. per&uacute;<br>
                        EDIFICIO SUPERMALL<br>
                        MEZANINE, OF 33<br>
                        Telf:(591)4-4067755<br>
                        Cel:(591)76400111<br>
                    </td>
                    <td class="contact-miami contact-td-padding" style="border:none;">
                        8285 NW 65th Street<br>
                        Suite #2<br>
                        MIAMI, FL 33166<br>
                        Telf:(305)848-0095<br>
                        Cel:(786)856-3907<br>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>