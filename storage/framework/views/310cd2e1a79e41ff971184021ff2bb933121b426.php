<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			<?php echo Form::label('nombre_u','Nombres: ',['class'=>'control-label']); ?>

			<?php echo Form::text('nombre_u',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del usuario','required','min'=>5]); ?><br/>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			<?php echo Form::label('apellidos_u','Apellidos: ',['class'=>'control-label']); ?>

			<?php echo Form::text('apellidos_u',null,['class'=>'form-control','placeholder'=>'Ingrese el apellido del usuario','required','min'=>5]); ?><br/>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			<?php echo Form::label('telefono_u','Tel&eacute;fono: ',['class'=>'control-label']); ?>

			<?php echo Form::text('telefono_u',null,['class'=>'form-control','placeholder'=>'Ingrese el telefono del usuario','min'=>5,"id"=>"telefono_u_1"]); ?><br/>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			<?php echo Form::label('celular_u','Celular: ',['class'=>'control-label']); ?>

			<?php echo Form::text('celular_u',null,['class'=>'form-control','placeholder'=>'Ingrese el celular del usuario','required','min'=>5,"id"=>"celular_u_1"]); ?><br/>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			<?php echo Form::label('email_u','E-mail: ',['class'=>'control-label']); ?>

			<?php echo Form::text('email_u',null,['class'=>'form-control','placeholder'=>'Ingrese el e-mail del usuario','min'=>5]); ?><br/>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			<?php echo Form::label('tipo_usuarios_id','Tipo de usuario: ',['class'=>'control-label']); ?>

			<?php echo Form::select('tipo_usuarios_id', $tipo_usuarios,null,['class'=>'form-control','placeholder'=>'Seleccione una opcion..','required']); ?><br>
		</div>
	</div>
</div>
<?php if($vista == "edit"): ?>
<div class="row" id="change1">
	<div class="col-lg-4"></div>
	<div class="col-lg-4">
		<button id="changeButton1" type="button" class="btn btn-w-md btn-primary btn-block">Cambiar nombre de usuario y password</button>
	</div>
	<div class="col-lg-4"></div>
</div>
<script type="text/javascript">
	$('#changeButton1').on('click',function(){
		var target = document.getElementById('change1')
		target.innerHTML = ''
		target.innerHTML = '<div class="col-lg-6"><div class="form-group"><?php echo Form::label("user_name","Nombre de usuario: ",["class"=>"control-label"]); ?><?php echo Form::text("user_name",null,["class"=>"form-control","placeholder"=>"Ingrese un nombre de usuario","required","min"=>5]); ?><br/></div></div><div class="col-lg-6"><div class="form-group"><?php echo Form::label("password","Password: ",["class"=>"control-label"]); ?><?php echo Form::password("password",["class"=>"form-control","placeholder"=>"Ingrese una contrasena","required","min"=>5]); ?><br/></div></div>'
	});
</script>
<?php else: ?>
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			<?php echo Form::label('user_name','Username: ',['class'=>'control-label']); ?>

			<?php echo Form::text('user_name',null,['class'=>'form-control','placeholder'=>'Ingrese un nombre de usuario','required','min'=>5,'id'=>'username1']); ?><br/>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			<?php echo Form::label('password','Password: ',['class'=>'control-label']); ?>

			<?php echo Form::password('password',['class'=>'form-control','placeholder'=>'Ingrese una contrasena','required','min'=>5,'id'=>'pass1']); ?><br/>
		</div>
	</div>
</div>

<?php endif; ?>
