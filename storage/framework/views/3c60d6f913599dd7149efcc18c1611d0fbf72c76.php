<div class="row">
	<?php if(isset($vista)): ?>
		<?php if($vista == "edit"): ?>
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo Form::label('nombre_compania_p','Compañia: ',['class'=>'control-label']); ?>

					<?php echo Form::text('nombre_compania_p',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre de la compania','required','min'=>5,'readonly']); ?><br/>
				</div>
			</div>
			<div class="col-lg-6">
				<div class="form-group">
					<?php echo Form::label('persona_contacto_p','Persona contacto: ',['class'=>'control-label']); ?>

					<?php echo Form::text('persona_contacto_p',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del contacto','required','min'=>5]); ?><br/>
				</div>
			</div>
		<?php else: ?>
			<div class="col-lg-6">
			<div class="form-group">
				<?php echo Form::label('nombre_compania_p','Compañia: ',['class'=>'control-label']); ?>

				<?php echo Form::text('nombre_compania_p',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre de la compania','required','min'=>5]); ?><br/>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group">
				<?php echo Form::label('persona_contacto_p','Persona contacto: ',['class'=>'control-label']); ?>

				<?php echo Form::text('persona_contacto_p',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del contacto','required','min'=>5]); ?><br/>
			</div>
		</div>
		<?php endif; ?>
	<?php endif; ?>
</div>
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			<?php echo Form::label('direccion_p','Direcci&oacute;n: ',['class'=>'control-label']); ?>

			<?php echo Form::text('direccion_p',null,['class'=>'form-control','placeholder'=>'Ingrese la direccion de la compania','min'=>5]); ?><br/>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			<?php echo Form::label('telefono_p','Tel&eacute;fono: ',['class'=>'control-label']); ?>

			<?php echo Form::text('telefono_p',null,['class'=>'form-control','placeholder'=>'Ingrese el telefono de la compania','required','min'=>5,'id'=>'telefono_p_1']); ?><br/>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			<?php echo Form::label('celular_p','Celular: ',['class'=>'control-label']); ?>

			<?php echo Form::text('celular_p',null,['class'=>'form-control','placeholder'=>'Ingrese el celular de la compania','min'=>5,'id'=>'celular_p_1']); ?><br/>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			<?php echo Form::label('email_p','E-mail: ',['class'=>'control-label']); ?>

			<?php echo Form::text('email_p',null,['class'=>'form-control','placeholder'=>'Ingrese el e-mail de la compania','min'=>5]); ?>

		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			<?php echo Form::label('tipo_proveedores_id','Tipo de proveedor: ',['class'=>'control-label']); ?>

			<?php echo Form::select('tipo_proveedores_id', $tipos_proveedor,null,['class'=>'form-control','placeholder'=>'Seleccione un tipo de proveedor..','required']); ?><br>
		</div>
	</div>
</div>
<?php if("edit" == $vista): ?>
<?php for($i = 0; $i < count($marcas_prov); $i++): ?>
	<div class="row marca-container-edit" id="marcasProv1">
		<div class="col-lg-4" >
			<div class="form-group">
				<?php echo Form::hidden('marcas_prov_id[]',$marcas_prov[$i]->id); ?>

				<?php echo Form::label('marca_prov','Marca del proveedor: ',['class'=>'control-label']); ?>

				<?php echo Form::text('marca_prov_old[]',$marcas_prov[$i]->marca_prov,['class'=>'form-control','placeholder'=>'Ingrese la marca que distribuye el proveedor','required','min'=>5]); ?>

			</div>
		</div>
		<div class="col-lg-2">
			<div class="form-group">
				<span opcion-id-pm="<?php echo e($marcas_prov[$i]->id); ?>">
					<?php echo Form::label('opcion','Quitar:',['class'=>'control-label']); ?><br/>
					<button type="button" class="btn btn-danger fa fa-trash block-center removeMarca1-edit"></button>
			</div>
		</div>
	</div>
<?php endfor; ?>
	<div class="row" id="boton-add1">
		<div class="col-lg-4">
			<div class="form-group">
				<a class="btn btn-w-md btn-success btn-block block-center addMarca1">Agregar otra marca</a>
			</div>
		</div>
	</div>
<?php else: ?>
<div class="row">
		
	</div>
	<div class="row marca-container" id="marcasProv1">
		<div class="col-lg-4" >
			<div class="form-group">
				<?php echo Form::label('marca_prov','Marca o tipo de producto: ',['class'=>'control-label']); ?>

				<?php echo Form::text('marca_prov[]',null,['class'=>'form-control','placeholder'=>'Ingrese la marca que distribuye el proveedor','required','min'=>5]); ?>

			</div>
		</div>
		<div class="col-lg-2">
			<div class="form-group">
				<?php echo Form::label('addProv','Agregar: ',['class'=>'control-label']); ?><br>
				<a class="btn btn-success fa fa-plus block-center addMarca1"></a>
			</div>
		</div>
	</div>
	
	<?php endif; ?>
