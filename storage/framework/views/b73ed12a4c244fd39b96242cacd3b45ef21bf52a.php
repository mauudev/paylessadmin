<div class="container-fluid" id="clienteForm1">
	<div class="row">
		<div class="col-lg-5">
		<?php if(isset($accion)): ?>
			<?php if($accion == "editar"): ?>
				<div class="form-group">
						<?php echo Form::label('nombre_c','Nombres: ',['class'=>'control-label']); ?>

						<?php echo Form::text('nombre_c',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del cliente','required','min'=>5,'readonly']); ?>

						<?php echo Form::hidden('usuarios_id',\Auth::user()->id); ?><br/>
					</div>
				</div>
				<div class="col-lg-5">
					<div class="form-group">
						<?php echo Form::label('apellidos_c','Apellidos: ',['class'=>'control-label']); ?>

						<?php echo Form::text('apellidos_c',null,['class'=>'form-control','placeholder'=>'Ingrese el apellido del cliente','required','min'=>5,'readonly']); ?><br/>
					</div>
				</div>
			<?php else: ?>
				<div class="form-group">
						<?php echo Form::label('nombre_c','Nombres: ',['class'=>'control-label']); ?>

						<?php echo Form::text('nombre_c',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del cliente','required','min'=>5]); ?>

						<?php echo Form::hidden('usuarios_id',\Auth::user()->id); ?><br/>
					</div>
				</div>
				<div class="col-lg-5">
					<div class="form-group">
						<?php echo Form::label('apellidos_c','Apellidos: ',['class'=>'control-label']); ?>

						<?php echo Form::text('apellidos_c',null,['class'=>'form-control','placeholder'=>'Ingrese el apellido del cliente','required','min'=>5]); ?><br/>
					</div>
				</div>
			<?php endif; ?>
		<?php else: ?>
		<div class="form-group">
				<?php echo Form::label('nombre_c','Nombres: ',['class'=>'control-label']); ?>

				<?php echo Form::text('nombre_c',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del cliente','required','min'=>5]); ?>

				<?php echo Form::hidden('usuarios_id',\Auth::user()->id); ?><br/>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="form-group">
				<?php echo Form::label('apellidos_c','Apellidos: ',['class'=>'control-label']); ?>

				<?php echo Form::text('apellidos_c',null,['class'=>'form-control','placeholder'=>'Ingrese el apellido del cliente','required','min'=>5]); ?><br/>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<div class="row">
		<div class="col-lg-5">
			<div class="form-group">
				<?php echo Form::label('direccion_c','Direcci&oacute;n: ',['class'=>'control-label']); ?>

				<?php echo Form::text('direccion_c',null,['class'=>'form-control','placeholder'=>'Ingrese la direccion del cliente','min'=>5]); ?><br/>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="form-group" <?php /* id="alertTelf1" ,'id'=>'telefono_c1'*/ ?>>
				<?php echo Form::label('telefono_c','Tel&eacute;fono: ',['class'=>'control-label']); ?>

				<?php echo Form::text('telefono_c',null,['class'=>'form-control','placeholder'=>'Ingrese el telefono del cliente','min'=>5,'id'=>"telefono_c_1"]); ?><br/>
			</div>
		</div>
		<div class="col-lg-2">
			&nbsp;
		</div>
		<div class="col-lg-2" id="checkTelf1"></div>
	</div>
	<div class="row">
		<div class="col-lg-5">
			<div class="form-group">
				<?php echo Form::label('email_c','E-mail: ',['class'=>'control-label']); ?>

				<?php echo Form::text('email_c',null,['class'=>'form-control','placeholder'=>'Ingrese el e-mail del cliente','min'=>5]); ?><br/>
			</div>
		</div>
		<?php if($accion == "editar"): ?>
		<div class="col-lg-5">
			<div class="form-group" id="alertCel1_edit">
				<?php echo Form::label('celular_c','Celular: ',['class'=>'control-label']); ?>

				<?php echo Form::text('celular_c',null,['class'=>'form-control','placeholder'=>'Ingrese el celular del cliente','id'=>'celular_c1_edit','required','min'=>5]); ?><br/>
			</div>
		</div>
		<div class="col-lg-2">
			&nbsp;
		</div>
		<div class="col-lg-2" id="checkCel1"></div>
		<?php else: ?>
		<div class="col-lg-5">
			<div class="form-group" id="alertCel1">
				<?php echo Form::label('celular_c','Celular: ',['class'=>'control-label']); ?>

				<?php echo Form::text('celular_c',null,['class'=>'form-control','placeholder'=>'Ingrese el celular del cliente','id'=>'celular_c1','required','min'=>5]); ?><br/>
			</div>
		</div>
		<div class="col-lg-2">
			&nbsp;
		</div>
		<div class="col-lg-2" id="checkCel1"></div>
		<?php endif; ?> 
		
	</div>
	<div class="row">
		<div class="col-lg-5">
			<div class="form-group">
				<?php echo Form::label('origen_contacto','Origen contacto: ',['class'=>'control-label']); ?><br/>
				<select name="origen_contacto" class="form-control">
				  <?php foreach($origen_contacto as $key => $value): ?>
					  <option value="<?php echo e($key); ?>"><?php echo e($value); ?></option>
				  <?php endforeach; ?>
				</select>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="form-group">
				<?php echo Form::label('pais','Localidad: ',['class'=>'control-label']); ?>

				<?php echo Form::select('pais', $paises,null,['class'=>'form-control','placeholder'=>'Seleccione una localidad..','required']); ?><br>
			</div>
		</div>
	</div>
</div>