<?php $__env->startSection('content'); ?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-filled">
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-cash"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Registrar nueva cuenta bancaria</h3>
						<small>
						Gesti&oacute;n de cuentas bancarias
						</small>
					</div>
				</div>
				<div class="panel-body">	
				<?php echo Form::open(['route'=>'cuentas-bancarias.store','method'=>'POST','class'=>'form-group',"onsubmit"=>"return validateForm();",'id'=>'myForm']); ?>

					<?php echo $__env->make('cuentas-bancarias.form.form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					<?php echo $__env->make('alerts.validation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="form-group" >
		<?php echo Form::submit('Guardar',['class'=>'btn btn-w-md btn-accent']); ?> <a class="btn btn-w-md btn-default" href="<?php echo URL::to('cuentas-bancarias'); ?>">Cancelar</a>
		<?php echo Form::close(); ?>

	</div>
		</div>
	</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>