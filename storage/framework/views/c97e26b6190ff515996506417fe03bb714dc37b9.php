<?php $__env->startSection('content'); ?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-filled">
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-mail"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Enviar nuevo mensaje</h3>
						<small>
							Env&iacute;os de mensajes
						</small>
					</div>
				</div>

				<div class="panel-body">
					<?php echo Form::open(['route'=>'sendmail.store','method'=>'POST','class'=>'form-group',"onsubmit"=>"return checkSubmit();"]); ?>

					<?php echo Form::hidden('ventas_id',$venta->id); ?>

					<?php echo Form::hidden('nombre_c',$cliente->nombre_c); ?>

					<?php echo Form::hidden('apellidos_c',$cliente->apellidos_c); ?>

					<?php echo $__env->make('sendmail.form.email-form', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					<?php echo $__env->make('alerts.validation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
					<div class="col-lg-12 personalizarFormDiv">
						<b>NOTA:</b> por defecto se env&iacute;a el invoice con el contenido por defecto, haga click en <button type="button" class="btn btn-success btn-sm personalizarBtn"> Personalizar PDF</button> para cambiar los datos.
					</div>
				</div>

			</div>
		</div>
	</div>
<?php echo $__env->make('alerts.success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<div class="row">
<div class="form-group">
	<div class="col-md-7" >
		<?php echo Form::submit('Enviar',['class'=>'btn btn-w-md btn-accent']); ?> <a class="btn btn-w-md btn-default" href="<?php echo URL::to('ventas/'.$venta->id.'/detalle'); ?>">Cancelar</a>
	</div>
</div>
</div>
<?php echo Form::close(); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$(".personalizarBtn").on("click",function(){
			template = '<p>Puede agregar los siguientes elementos al PDF: <code>Observaciones</code>, <code>M&eacute;todo de pago</code>, <code>Plazo de entrega</code></p><br><b>Elementos:</b><br><br><div class="row"><div class="col-lg-4"><div class="form-group observacionesDiv"><?php echo Form::checkbox("observacionesCB",1,false,["class"=>"observacionesCB"]); ?><?php echo Form::label("observaciones"," Observaciones ",["class"=>"control-label"]); ?><br></div></div><div class="col-lg-4"><div class="form-group metodo_pagoDiv"><?php echo Form::checkbox("metodo_pagoCB",1,false,["class"=>"metodo_pagoCB"]); ?><?php echo Form::label("metodo_pago","M&eacute;todo de pago ",["class"=>"control-label"]); ?><br></div></div><div class="col-lg-4"><div class="form-group plazo_entregaDiv"><?php echo Form::checkbox("plazo_entregaCB",1,false,["class"=>"plazo_entregaCB"]); ?><?php echo Form::label("plazo_entrega","Plazo de entrega",["class"=>"control-label"]); ?><?php echo Form::hidden('personalizado',1); ?><br></div></div></div><br><br>'
			$(".personalizarFormDiv").html(template)
		});
		$(document).on("click",".holaBtn",function(){
			alert("alo")
		});
		$(document).on("change",".observacionesCB",function(){
			if(this.checked){
				template = '<?php echo e(Form::textarea("observaciones",null,["class"=>"form-control observacionesInput","placeholder"=>"Ingrese observaciones en el campo","required","min"=>5,"rows"=>5])); ?>'
				$(".observacionesDiv").append(template)
			}else {
				$(".observacionesInput").remove()
			}
		});
		$(document).on("change",".metodo_pagoCB",function(){
			if(this.checked){
				template = '<?php echo e(Form::textarea("metodo_pago",null,["class"=>"form-control metodo_pagoInput","placeholder"=>"Ingrese metodo de pago en el campo","required","min"=>5,"rows"=>5])); ?>'
				$(".metodo_pagoDiv").append(template)
			}else {
				$(".metodo_pagoInput").remove()
			}
		});
		$(document).on("change",".plazo_entregaCB",function(){
			if(this.checked){
				template = '<?php echo e(Form::textarea("plazo_entrega",null,["class"=>"form-control plazo_entregaInput","placeholder"=>"Ingrese el plazo entrega en el campo","required","min"=>5,"rows"=>5])); ?>'
				$(".plazo_entregaDiv").append(template)
			}else {
				$(".plazo_entregaInput").remove()
			}
		});	
	});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>