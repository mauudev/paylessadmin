<?php if(Session::has('unauthorized')): ?>
<br/>
<div class="alert alert-danger" role="alert">
    <button type="button" class="close" ></button>
    <strong><i class="fa fa-ban"></i> Error!</strong> <?php echo session('unauthorized'); ?>

</div>
<?php elseif(Session::has('unauthorized-login')): ?>
 <div class="alert alert-danger" role="alert">
    <button type="button" class="close" ></button>
    <strong><i class="fa fa-ban"></i> Error!</strong> <?php echo session('unauthorized-login'); ?>

</div>
 <?php endif; ?>
