<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <div class="panel-tools">
                    <div class="col-md-12">
                        <?php $estados = [1=>'Activos',0=>'Inactivos']?>
                        <?php echo Form::open(['route'=>'clientes.index','method'=>'GET','class'=>'navbar-form navbar-left pull-right','role'=>'search']); ?>

                        <div class="form-group">
                            <?php echo Form::select('estado', $estados,null,['class'=>'form-control','placeholder'=>'Filtrar registros..','id'=>'filtro2']); ?>

                            <?php echo Form::text('search',null,['class'=>'form-control','placeholder'=>'Buscar..']); ?>

                        </div>
                        <button type="submit" class="btn btn-accent">Buscar</button>
                        <a href="<?php echo URL::to('clientes/create'); ?>" class="btn btn-w-md btn-accent"><i class="fa fa-plus"></i> Nuevo cliente</a>
                        <?php echo Form::close(); ?>

                    </div>
                </div>
                <div class="view-header">
                    <div class="header-icon">
                        <i class="pe page-header-icon pe-7s-users"></i>
                    </div>
                    <div class="header-title">
                        <h3 class="page-header">Clientes registrados</h3>
                        <small>
                        Gesti&oacute;n de clientes
                        </small>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                            <tr role="row">
                                <th class="sorting_asc left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                    aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" ">Nombre
                                </th>
                                <th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                    aria-label="Browser: activate to sort column ascending" ">Apellidos
                                </th>
                                <th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending" ">Celular
                                </th>
                                <th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending" ">Contacto
                                </th>
                                <th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending" >Pa&iacute;s
                                </th>
                                <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                    aria-label="CSS grade: activate to sort column ascending" style="width: 240px;">Acci&oacute;n
                                </th>
                            </tr>
                        </thead>
                        <tbody id="resultados2"><!-- nombre-apellido-celular-contacto-pais -->
                            <?php if(count($clientes) > 0): ?>
                            <?php foreach($clientes as $cliente): ?>
                            <tr>
                                <td data-toggle="modal" data-target="#myModal" data-id="<?php echo e($cliente->id); ?>"><?php echo e(str_limit($cliente->nombre_c,$limit = 25,$end='..')); ?>

                                    <?php echo e(Form::hidden('nombre',$cliente->nombre_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('apellidos',$cliente->apellidos_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('direccion',$cliente->direccion_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('telefono',$cliente->telefono_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('celular',$cliente->celular_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('origen_contacto',$cliente->origen_contacto,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('email',$cliente->email_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('pais',$cliente->pais,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('nombre_u',$cliente->nombre_u,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('apellidos_u',$cliente->apellidos_u,['class'=>'dataCliente'])); ?>

                                    </td>
                                <td data-toggle="modal" data-target="#myModal" data-id="<?php echo e($cliente->id); ?>"><?php echo e(str_limit($cliente->apellidos_c,$limit = 25,$end='..')); ?>

                                    <?php echo e(Form::hidden('nombre',$cliente->nombre_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('apellidos',$cliente->apellidos_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('direccion',$cliente->direccion_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('telefono',$cliente->telefono_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('celular',$cliente->celular_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('origen_contacto',$cliente->origen_contacto,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('email',$cliente->email_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('pais',$cliente->pais,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('nombre_u',$cliente->nombre_u,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('apellidos_u',$cliente->apellidos_u,['class'=>'dataCliente'])); ?></td>
                                <td data-toggle="modal" data-target="#myModal" data-id="<?php echo e($cliente->id); ?>"><?php echo e(str_limit($cliente->celular_c,$limit = 25,$end='..')); ?>

                                    <?php echo e(Form::hidden('nombre',$cliente->nombre_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('apellidos',$cliente->apellidos_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('direccion',$cliente->direccion_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('telefono',$cliente->telefono_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('celular',$cliente->celular_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('origen_contacto',$cliente->origen_contacto,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('email',$cliente->email_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('pais',$cliente->pais,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('nombre_u',$cliente->nombre_u,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('apellidos_u',$cliente->apellidos_u,['class'=>'dataCliente'])); ?></td>
                                <td data-toggle="modal" data-target="#myModal" data-id="<?php echo e($cliente->id); ?>"><?php echo e(str_limit($cliente->origen_contacto,$limit = 25,$end='..')); ?>

                                    <?php echo e(Form::hidden('nombre',$cliente->nombre_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('apellidos',$cliente->apellidos_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('direccion',$cliente->direccion_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('telefono',$cliente->telefono_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('celular',$cliente->celular_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('origen_contacto',$cliente->origen_contacto,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('email',$cliente->email_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('pais',$cliente->pais,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('nombre_u',$cliente->nombre_u,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('apellidos_u',$cliente->apellidos_u,['class'=>'dataCliente'])); ?></td>
                                <td data-toggle="modal" data-target="#myModal" data-id="<?php echo e($cliente->id); ?>"><?php echo e(str_limit($cliente->pais,$limit = 20,$end='..')); ?>

                                    <?php echo e(Form::hidden('nombre',$cliente->nombre_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('apellidos',$cliente->apellidos_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('direccion',$cliente->direccion_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('telefono',$cliente->telefono_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('celular',$cliente->celular_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('origen_contacto',$cliente->origen_contacto,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('email',$cliente->email_c,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('pais',$cliente->pais,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('nombre_u',$cliente->nombre_u,['class'=>'dataCliente'])); ?>

                                    <?php echo e(Form::hidden('apellidos_u',$cliente->apellidos_u,['class'=>'dataCliente'])); ?>

                                </td>
                                <td align="center">
                                    <div align="center">
                                        <a class="btn btn-accent" href="<?php echo route('clientes.edit',$parameters = $cliente->id); ?>"><i class="fa fa-pencil"></i> Editar</a>&nbsp;
                                        <?php if($cliente->deleted_at == null): ?>
                                        <a class="btn btn-danger" href="<?php echo route('clientes.destroyCliente',$parameters = $cliente->id); ?>" ><i class="fa fa-times" onclick='return confirm("¿Estas seguro de dar de baja este elemento?")'></i> Desactivar</a>
                                        <?php else: ?>
                                        <a class="btn btn-danger" href="<?php echo route('clientes.activate',$parameters = $cliente->id); ?>"><i class="fa fa-check"></i> Activar</a>
                                        <?php endif; ?>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php echo e($clientes->render()); ?>

                        <?php echo $__env->make('alerts.success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                    <?php else: ?>
                    <div class="col-md-12" id="msg1">
                        <div class="alert alert-danger alert-dismissable">
                            <strong>No se encontraron resultados</strong> 
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header center-block">
                
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Informaci&oacute;n del cliente</h4>
            </div>
            <div class="modal-body" id="infoCliente">
                <!-- CONTENIDO DEL MODAL -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div><input class="dataCliente" name="nombre" type="hidden" value="'+clientes[i].nombre_u+'">
<script type="text/javascript">
    $('#filtro2').on('change',function(){
        var filtro = $('#filtro2').val()
        var target = document.getElementById('resultados2')
        var vaciar_msg = document.getElementById('msg1')
        $.get('/administracion/clientes/filtrar/'+filtro+'',function(response){
            var clientes = response
            var template = ''
            target.innerHTML = ''
            if(vaciar_msg) vaciar_msg.innerHTML = ''
            var url_edit = '/administracion/clientes/'//cambiar para server
            var url_destroy = '/administracion/clientes/'
            var url_activate = '/administracion/clientes/'
            for(i = 0; i < clientes.length; i++){
                template += '<tr><td data-toggle="modal" data-target="#myModal" data-id="'+clientes[i].id+'"><input class="dataCliente" name="nombre" type="hidden" value="'+clientes[i].nombre_c+'"><input class="dataCliente" name="apellidos" type="hidden" value="'+clientes[i].apellidos_c+'"><input class="dataCliente" name="direccion" type="hidden" value="'+clientes[i].direccion_c+'"><input class="dataCliente" name="telefono" type="hidden" value="'+clientes[i].telefono_c+'"><input class="dataCliente" name="celular" type="hidden" value="'+clientes[i].celular_c+'"><input class="dataCliente" name="email" type="hidden" value="'+clientes[i].email_c+'"><input class="dataCliente" name="origen_contacto" type="hidden" value="'+clientes[i].origen_contacto+'"><input class="dataCliente" name="pais" type="hidden" value="'+clientes[i].pais+'"><input class="dataCliente" name="nombre_u" type="hidden" value="'+clientes[i].nombre_u+'"><input class="dataCliente" name="apellidos_u" type="hidden" value="'+clientes[i].apellidos_u+'">'+clientes[i].nombre_c.substring(0, 20)+'</td><td data-toggle="modal" data-target="#myModal" data-id="'+clientes[i].id+'">'+clientes[i].apellidos_c.substring(0, 20)+'<input class="dataCliente" name="nombre" type="hidden" value="'+clientes[i].nombre_c+'"><input class="dataCliente" name="apellidos" type="hidden" value="'+clientes[i].apellidos_c+'"><input class="dataCliente" name="direccion" type="hidden" value="'+clientes[i].direccion_c+'"><input class="dataCliente" name="telefono" type="hidden" value="'+clientes[i].telefono_c+'"><input class="dataCliente" name="celular" type="hidden" value="'+clientes[i].celular_c+'"><input class="dataCliente" name="email" type="hidden" value="'+clientes[i].email_c+'"><input class="dataCliente" name="origen_contacto" type="hidden" value="'+clientes[i].origen_contacto+'"><input class="dataCliente" name="pais" type="hidden" value="'+clientes[i].pais+'"><input class="dataCliente" name="nombre_u" type="hidden" value="'+clientes[i].nombre_u+'"><input class="dataCliente" name="apellidos_u" type="hidden" value="'+clientes[i].apellidos_u+'"></td><td data-toggle="modal" data-target="#myModal" data-id="'+clientes[i].id+'">'+clientes[i].celular_c+'<input class="dataCliente" name="nombre" type="hidden" value="'+clientes[i].nombre_c+'"><input class="dataCliente" name="apellidos" type="hidden" value="'+clientes[i].apellidos_c+'"><input class="dataCliente" name="direccion" type="hidden" value="'+clientes[i].direccion_c+'"><input class="dataCliente" name="telefono" type="hidden" value="'+clientes[i].telefono_c+'"><input class="dataCliente" name="celular" type="hidden" value="'+clientes[i].celular_c+'"><input class="dataCliente" name="email" type="hidden" value="'+clientes[i].email_c+'"><input class="dataCliente" name="origen_contacto" type="hidden" value="'+clientes[i].origen_contacto+'"><input class="dataCliente" name="pais" type="hidden" value="'+clientes[i].pais+'"><input class="dataCliente" name="nombre_u" type="hidden" value="'+clientes[i].nombre_u+'"><input class="dataCliente" name="apellidos_u" type="hidden" value="'+clientes[i].apellidos_u+'"></td><td data-toggle="modal" data-target="#myModal" data-id="'+clientes[i].id+'">'+clientes[i].origen_contacto+'<input class="dataCliente" name="nombre" type="hidden" value="'+clientes[i].nombre_c+'"><input class="dataCliente" name="apellidos" type="hidden" value="'+clientes[i].apellidos_c+'"><input class="dataCliente" name="direccion" type="hidden" value="'+clientes[i].direccion_c+'"><input class="dataCliente" name="telefono" type="hidden" value="'+clientes[i].telefono_c+'"><input class="dataCliente" name="celular" type="hidden" value="'+clientes[i].celular_c+'"><input class="dataCliente" name="email" type="hidden" value="'+clientes[i].email_c+'"><input class="dataCliente" name="origen_contacto" type="hidden" value="'+clientes[i].origen_contacto+'"><input class="dataCliente" name="pais" type="hidden" value="'+clientes[i].pais+'"><input class="dataCliente" name="nombre_u" type="hidden" value="'+clientes[i].nombre_u+'"><input class="dataCliente" name="apellidos_u" type="hidden" value="'+clientes[i].apellidos_u+'"></td><td data-toggle="modal" data-target="#myModal" data-id="'+clientes[i].id+'">'+clientes[i].pais+'<input class="dataCliente" name="nombre" type="hidden" value="'+clientes[i].nombre_c+'"><input class="dataCliente" name="apellidos" type="hidden" value="'+clientes[i].apellidos_c+'"><input class="dataCliente" name="direccion" type="hidden" value="'+clientes[i].direccion_c+'"><input class="dataCliente" name="telefono" type="hidden" value="'+clientes[i].telefono_c+'"><input class="dataCliente" name="celular" type="hidden" value="'+clientes[i].celular_c+'"><input class="dataCliente" name="email" type="hidden" value="'+clientes[i].email_c+'"><input class="dataCliente" name="origen_contacto" type="hidden" value="'+clientes[i].origen_contacto+'"><input class="dataCliente" name="pais" type="hidden" value="'+clientes[i].pais+'"><input class="dataCliente" name="nombre_u" type="hidden" value="'+clientes[i].nombre_u+'"><input class="dataCliente" name="apellidos_u" type="hidden" value="'+clientes[i].apellidos_u+'"></td>'+'<td align="center"><div><a class="btn btn-accent" href="'+url_edit+clientes[i].id+'/edit" data-tooltip="Editar usuario"><i class="fa fa-pencil"></i> Editar</a>&nbsp;'
                if(filtro == 1){
                    template += '<a class="btn btn-danger" href="'+url_destroy+clientes[i].id+'/destroy" data-tooltip="Desactivar usuario"><i class="fa fa-times"></i> Desactivar</a></div></td></tr>'
                }else{
                    template += '<a class="btn btn-success" href="'+url_activate+clientes[i].id+'/activate" data-tooltip="Activar usuario"><i class="fa fa-check"></i> Activar</a></div></td></tr>'
                }
            }
            target.innerHTML = template;
        });
    });
    $('#myModal').on('show.bs.modal', function (event) {
    //var getIdFromRow = $(event.relatedTarget).attr('data-id');
    // if you wnat to take the text of the first cell  
    //var compania = $(event.relatedTarget).find('td:nth-child(1)').html();
    //var contacto = $(event.relatedTarget).find('td:nth-child(2)').text();
    var data = Array()
    $(event.relatedTarget).find(".dataCliente").each(function(i) {
        console.log(String(this.value))
        data[i] += String(this.value)
    });
    var template2 = '<div class="row"><div class="col-md-2"><strong class="c-white">Nombres: </strong></div><div class="col-sm-10">'+data[0].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">Apellidos: </strong></div><div class="col-md-10">'+data[1].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">Direcci&oacute;n: </strong></div><div class="col-md-10">'+data[2].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">Tel&eacute;fono: </strong></div><div class="col-md-10">'+data[3].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">Celular: </strong></div><div class="col-md-10">'+data[4].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">Origen: </strong></div><div class="col-md-10">'+data[5].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">E-mail: </strong></div><div class="col-md-10">'+data[6].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">Pa&iacute;s: </strong></div><div class="col-md-10">'+data[7].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">Registro: </strong></div><div class="col-md-10">'+data[8].replace('undefined','')+' '+data[9].replace('undefined','')+'</div></div>'

    $(this).find('#infoCliente').html($(template2))
})
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>