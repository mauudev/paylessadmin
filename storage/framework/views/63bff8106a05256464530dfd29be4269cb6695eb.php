<?php $__env->startSection('content'); ?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-filled">
			<div class="panel-heading">
				<div class="panel-tools">
					<div class="col-md-12">
						<?php echo Form::open(['route'=>'mercaderias.index','method'=>'GET','class'=>'navbar-form navbar-left pull-right','role'=>'search']); ?>

						<div class="form-group">
							<?php echo Form::text('search',null,['class'=>'form-control','placeholder'=>'Buscar..']); ?>

						</div>
						<button type="submit" class="btn btn-accent">Buscar</button>
						<?php echo Form::close(); ?>

					</div>
				</div>
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-phone"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Productos varios registrados</h3>
						<small>
						Gesti&oacute;n de registros de productos varios
						</small>
					</div>
				</div>
			</div>
			
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<b>NOTA:</b> Los precios de compra de estos productos son referenciales, pertenecientes a cotizaciones realizadas (precio de proveedor).
					</div>
				</div><br>
				<div class="table-responsive">
					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th class="left-align">Detalle</th>
								<th class="left-align">N&oacute;mero de &iacute;tem</th>
								<th align="center">Precio de compra</th>
							</tr>
						</thead>
						<tbody>
							<?php for($i = 0; $i < count($mercaderias); $i++): ?>
							<tr>
								<td><?php echo e($mercaderias[$i]->nombre_m); ?></td>
								<td>
									<?php if(filter_var($mercaderias[$i]->nro_item, FILTER_VALIDATE_URL) == true): ?>	
									<a href="<?php echo e($mercaderias[$i]->nro_item); ?>" target="_blank"><?php echo e(str_limit($mercaderias[$i]->nro_item,$limit = 50,$end='..')); ?></a></p>
								<?php else: ?>
									<?php echo e($mercaderias[$i]->nro_item); ?></p>
								<?php endif; ?>
								</td>
								<td align="center"><?php echo e($mercaderias[$i]->precio_venta_m); ?></td>
							</tr>
							<?php endfor; ?>
						</tbody>
					</table>
					<?php echo e($mercaderias->render()); ?>

					<?php echo $__env->make('alerts.success', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.principal', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>