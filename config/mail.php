<?php

return [

    'driver' => env('MAIL_DRIVER', 'smtp'),

    'host' => env('MAIL_HOST', 'box668.bluehost.com'),

    'port' => env('MAIL_PORT', 465),

    'from' => ['address' => env('MAIL_USERNAME'), 'name' => env('MAIL_NAME')],

    'encryption' => env('MAIL_ENCRYPTION', 'ssl'),

    'username' => env('MAIL_USERNAME'),

    'password' => env('MAIL_PASSWORD'),

    'sendmail' => '/usr/sbin/sendmail -bs',

];
