<?php

use Illuminate\Database\Seeder;

class CotizacionRepuestosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cotizacion_repuestos')->delete();
        
        \DB::table('cotizacion_repuestos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'estado' => 1,
                'ventas_id' => 1,
                'repuestos_id' => 1,
                'created_at' => '2017-12-01 01:58:32',
                'updated_at' => '2017-12-01 01:59:22',
                'cantidad' => 1,
            ),
        ));
        
        
    }
}
