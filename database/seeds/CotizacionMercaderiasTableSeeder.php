<?php

use Illuminate\Database\Seeder;

class CotizacionMercaderiasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cotizacion_mercaderias')->delete();
        
        \DB::table('cotizacion_mercaderias')->insert(array (
            0 => 
            array (
                'id' => 1,
                'estado' => 1,
                'ventas_id' => 1,
                'mercaderias_id' => 1,
                'created_at' => '2017-12-01 01:58:32',
                'updated_at' => '2017-12-01 01:59:46',
                'cantidad' => 1,
            ),
        ));
        
        
    }
}
