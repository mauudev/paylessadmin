<?php

use Illuminate\Database\Seeder;

class ProveedoresTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('proveedores')->delete();
        
        \DB::table('proveedores')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre_compania_p' => 'Proveedores',
                'direccion_p' => 'Av. Santa Cruz #4433',
            'telefono_p' => '(239) 843-2782 x3788',
            'celular_p' => '(983) 487-3873',
                'email_p' => 'mail@mail.com',
                'persona_contacto_p' => 'Jose alvarez',
                'tipo_proveedores_id' => 2,
                'created_at' => '2017-07-29 19:52:12',
                'updated_at' => '2017-07-29 19:54:01',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre_compania_p' => 'Proveedor 2',
                'direccion_p' => 'askdjjas',
            'telefono_p' => '(983) 247-9324 x7238',
            'celular_p' => '(237) 848-9349',
                'email_p' => '',
                'persona_contacto_p' => 'asd',
                'tipo_proveedores_id' => 1,
                'created_at' => '2017-07-29 19:54:27',
                'updated_at' => '2017-07-29 19:54:27',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre_compania_p' => 'Proveedor 3',
                'direccion_p' => 'ksadjksakdla',
            'telefono_p' => '(293) 478-3924 x7392',
            'celular_p' => '(823) 749-2374',
                'email_p' => '',
                'persona_contacto_p' => 'asjdkl',
                'tipo_proveedores_id' => 3,
                'created_at' => '2017-07-29 19:54:57',
                'updated_at' => '2017-07-29 19:54:57',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}
