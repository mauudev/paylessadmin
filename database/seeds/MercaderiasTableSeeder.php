<?php

use Illuminate\Database\Seeder;

class MercaderiasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('mercaderias')->delete();
        
        \DB::table('mercaderias')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre_m' => 'Vibrador XL',
                'precio_venta_m' => 350.0,
                'nro_item' => '443432',
                'created_at' => '2017-12-01 01:58:32',
                'updated_at' => '2017-12-01 01:59:46',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}
