<?php

use Illuminate\Database\Seeder;

class ProveedoresCotizacionMerTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('proveedores_cotizacion_mer')->delete();
        
        \DB::table('proveedores_cotizacion_mer')->insert(array (
            0 => 
            array (
                'id' => 1,
                'precio_cot_mer' => 350.0,
                'ventas_id' => 1,
                'mercaderias_id' => 1,
                'proveedores_id' => 2,
                'created_at' => '2017-12-01 01:59:36',
                'updated_at' => '2017-12-01 01:59:36',
            ),
        ));
        
        
    }
}
