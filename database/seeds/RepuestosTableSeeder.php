<?php

use Illuminate\Database\Seeder;

class RepuestosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('repuestos')->delete();
        
        \DB::table('repuestos')->insert(array (
            0 => 
            array (
                'id' => 1,
                'marca_r' => 'BMW',
                'modelo_r' => 'X1',
                'anio_r' => '2018',
                'vin_r' => '7347387',
                'detalle_r' => 'Parachoques Renegade',
                'precio_venta_r' => 500.0,
                'codigo_repuesto' => '',
                'created_at' => '2017-12-01 01:58:32',
                'updated_at' => '2017-12-01 01:59:22',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}
