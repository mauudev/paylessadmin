<?php

use Illuminate\Database\Seeder;

class MarcasProveedoresTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('marcas_proveedores')->delete();
        
        \DB::table('marcas_proveedores')->insert(array (
            0 => 
            array (
                'id' => 1,
                'marca_prov' => 'Toyota',
                'proveedores_id' => 1,
                'created_at' => '2017-07-29 19:52:12',
                'updated_at' => '2017-07-29 19:52:12',
            ),
            1 => 
            array (
                'id' => 2,
                'marca_prov' => 'Nissan',
                'proveedores_id' => 1,
                'created_at' => '2017-07-29 19:52:12',
                'updated_at' => '2017-07-29 19:52:12',
            ),
            2 => 
            array (
                'id' => 3,
                'marca_prov' => 'Nissan',
                'proveedores_id' => 2,
                'created_at' => '2017-07-29 19:54:27',
                'updated_at' => '2017-07-29 19:54:27',
            ),
            3 => 
            array (
                'id' => 4,
                'marca_prov' => 'Jeep',
                'proveedores_id' => 3,
                'created_at' => '2017-07-29 19:54:57',
                'updated_at' => '2017-07-29 19:54:57',
            ),
            4 => 
            array (
                'id' => 5,
                'marca_prov' => 'Honda',
                'proveedores_id' => 3,
                'created_at' => '2017-07-29 19:54:57',
                'updated_at' => '2017-07-29 19:54:57',
            ),
        ));
        
        
    }
}
