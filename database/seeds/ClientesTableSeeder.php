<?php

use Illuminate\Database\Seeder;

class ClientesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('clientes')->delete();
        
        \DB::table('clientes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre_c' => 'Vanesa Belen',
                'apellidos_c' => 'Villarroel Flores',
                'direccion_c' => 'Av. America #4470',
                'telefono_c' => '784-7378',
                'celular_c' => '394-83877',
                'email_c' => 'mtrigo143@gmail.com',
                'pais' => 'Cochabamba',
                'created_at' => '2017-12-01 01:58:32',
                'updated_at' => '2017-12-01 01:58:32',
                'origen_contacto' => 'Anuncio',
                'deleted_at' => NULL,
                'usuarios_id' => 2,
            ),
        ));
        
        
    }
}
