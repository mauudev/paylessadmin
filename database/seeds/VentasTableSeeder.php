<?php

use Illuminate\Database\Seeder;

class VentasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('ventas')->delete();
        
        \DB::table('ventas')->insert(array (
            0 => 
            array (
                'id' => 1,
                'estado' => 1,
                'finalizado' => 0,
                'pago_parcial' => NULL,
                'segundo_pago' => NULL,
                'pago_total' => 1657.5,
                'descuento' => NULL,
                'pago_total_desc' => NULL,
                'clientes_id' => 1,
                'usuarios_id' => 2,
                'deleted_at' => NULL,
                'created_at' => '2017-12-01 01:58:32',
                'updated_at' => '2017-12-01 01:59:54',
                'pago_total_bs' => 0,
            ),
        ));
        
        
    }
}
