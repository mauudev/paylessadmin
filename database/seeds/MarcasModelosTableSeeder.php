<?php

use Illuminate\Database\Seeder;

class MarcasModelosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('marcas_modelos')->delete();
        
        \DB::table('marcas_modelos')->insert(array (
            0 => 
            array (
                'id' => 7,
                'marca' => 'CHEVROLET',
                'modelo' => 'BLAZER FULL SIZE',
                'created_at' => '2017-10-12 21:22:49',
                'updated_at' => '2017-10-12 21:22:49',
            ),
            1 => 
            array (
                'id' => 8,
                'marca' => 'CHEVROLET',
                'modelo' => 'TRUCK SILVERADO 3500',
                'created_at' => '2017-10-12 21:22:49',
                'updated_at' => '2017-10-12 21:22:49',
            ),
            2 => 
            array (
                'id' => 9,
                'marca' => 'CHEVROLET',
                'modelo' => 'TRUCK SILVERADO 2500',
                'created_at' => '2017-10-12 21:22:49',
                'updated_at' => '2017-10-12 21:22:49',
            ),
            3 => 
            array (
                'id' => 10,
                'marca' => 'CHEVROLET',
                'modelo' => 'TRUCK SILVERADO 1500',
                'created_at' => '2017-10-12 21:22:49',
                'updated_at' => '2017-10-12 21:22:49',
            ),
            4 => 
            array (
                'id' => 11,
                'marca' => 'CHEVROLET',
                'modelo' => 'TRUCK COLORADO',
                'created_at' => '2017-10-12 21:22:49',
                'updated_at' => '2017-10-12 21:22:49',
            ),
            5 => 
            array (
                'id' => 12,
                'marca' => 'CHEVROLET',
                'modelo' => 'TRAX',
                'created_at' => '2017-10-12 21:22:49',
                'updated_at' => '2017-10-12 21:22:49',
            ),
            6 => 
            array (
                'id' => 13,
                'marca' => 'CHEVROLET',
                'modelo' => 'TRAVERSE',
                'created_at' => '2017-10-12 21:22:49',
                'updated_at' => '2017-10-12 21:22:49',
            ),
            7 => 
            array (
                'id' => 14,
                'marca' => 'CHEVROLET',
                'modelo' => 'TAHOE',
                'created_at' => '2017-10-12 21:22:49',
                'updated_at' => '2017-10-12 21:22:49',
            ),
            8 => 
            array (
                'id' => 15,
                'marca' => 'CHEVROLET',
                'modelo' => 'SUBURBAN-3500',
                'created_at' => '2017-10-12 21:22:50',
                'updated_at' => '2017-10-12 21:22:50',
            ),
            9 => 
            array (
                'id' => 16,
                'marca' => 'CHEVROLET',
                'modelo' => 'SUBURBAN-2500',
                'created_at' => '2017-10-12 21:22:50',
                'updated_at' => '2017-10-12 21:22:50',
            ),
            10 => 
            array (
                'id' => 17,
                'marca' => 'CHEVROLET',
                'modelo' => 'SUBURBAN-1500',
                'created_at' => '2017-10-12 21:22:50',
                'updated_at' => '2017-10-12 21:22:50',
            ),
            11 => 
            array (
                'id' => 18,
                'marca' => 'CHEVROLET',
                'modelo' => 'SUBURBAN-1000',
                'created_at' => '2017-10-12 21:22:50',
                'updated_at' => '2017-10-12 21:22:50',
            ),
            12 => 
            array (
                'id' => 19,
                'marca' => 'CHEVROLET',
                'modelo' => 'SUBURBAN-30',
                'created_at' => '2017-10-12 21:22:50',
                'updated_at' => '2017-10-12 21:22:50',
            ),
            13 => 
            array (
                'id' => 20,
                'marca' => 'CHEVROLET',
                'modelo' => 'SUBURBAN-20',
                'created_at' => '2017-10-12 21:22:50',
                'updated_at' => '2017-10-12 21:22:50',
            ),
            14 => 
            array (
                'id' => 21,
                'marca' => 'CHEVROLET',
                'modelo' => 'SUBURBAN-10',
                'created_at' => '2017-10-12 21:22:50',
                'updated_at' => '2017-10-12 21:22:50',
            ),
            15 => 
            array (
                'id' => 22,
                'marca' => 'CHEVROLET',
                'modelo' => 'SPARK',
                'created_at' => '2017-10-12 21:22:50',
                'updated_at' => '2017-10-12 21:22:50',
            ),
            16 => 
            array (
                'id' => 23,
                'marca' => 'CHEVROLET',
                'modelo' => 'SONIC',
                'created_at' => '2017-10-12 21:22:50',
                'updated_at' => '2017-10-12 21:22:50',
            ),
            17 => 
            array (
                'id' => 24,
                'marca' => 'CHEVROLET',
                'modelo' => 'EQUINOX',
                'created_at' => '2017-10-12 21:22:50',
                'updated_at' => '2017-10-12 21:22:50',
            ),
            18 => 
            array (
                'id' => 25,
                'marca' => 'CHEVROLET',
                'modelo' => 'CRUZE',
                'created_at' => '2017-10-12 21:22:50',
                'updated_at' => '2017-10-12 21:22:50',
            ),
            19 => 
            array (
                'id' => 26,
                'marca' => 'CHEVROLET',
                'modelo' => 'CORVETTE',
                'created_at' => '2017-10-12 21:22:50',
                'updated_at' => '2017-10-12 21:22:50',
            ),
            20 => 
            array (
                'id' => 27,
                'marca' => 'CHEVROLET',
                'modelo' => 'CAVALIER',
                'created_at' => '2017-10-12 21:22:50',
                'updated_at' => '2017-10-12 21:22:50',
            ),
            21 => 
            array (
                'id' => 28,
                'marca' => 'CHEVROLET',
                'modelo' => 'CAPTIVA SPORT',
                'created_at' => '2017-10-12 21:22:50',
                'updated_at' => '2017-10-12 21:22:50',
            ),
            22 => 
            array (
                'id' => 29,
                'marca' => 'CHEVROLET',
                'modelo' => 'CAMARO',
                'created_at' => '2017-10-12 21:22:50',
                'updated_at' => '2017-10-12 21:22:50',
            ),
            23 => 
            array (
                'id' => 30,
                'marca' => 'CHEVROLET',
                'modelo' => 'BLAZER S10/S15',
                'created_at' => '2017-10-12 21:22:50',
                'updated_at' => '2017-10-12 21:22:50',
            ),
            24 => 
            array (
                'id' => 31,
                'marca' => 'AUDI',
                'modelo' => 'A3',
                'created_at' => '2017-10-12 21:44:07',
                'updated_at' => '2017-10-12 21:44:07',
            ),
            25 => 
            array (
                'id' => 32,
                'marca' => 'AUDI',
                'modelo' => 'TT',
                'created_at' => '2017-10-12 21:44:07',
                'updated_at' => '2017-10-12 21:44:07',
            ),
            26 => 
            array (
                'id' => 33,
                'marca' => 'AUDI',
                'modelo' => 'RS7',
                'created_at' => '2017-10-12 21:44:07',
                'updated_at' => '2017-10-12 21:44:07',
            ),
            27 => 
            array (
                'id' => 34,
                'marca' => 'AUDI',
                'modelo' => 'RS6',
                'created_at' => '2017-10-12 21:44:07',
                'updated_at' => '2017-10-12 21:44:07',
            ),
            28 => 
            array (
                'id' => 35,
                'marca' => 'AUDI',
                'modelo' => 'RS5',
                'created_at' => '2017-10-12 21:44:07',
                'updated_at' => '2017-10-12 21:44:07',
            ),
            29 => 
            array (
                'id' => 36,
                'marca' => 'AUDI',
                'modelo' => 'RS4',
                'created_at' => '2017-10-12 21:44:07',
                'updated_at' => '2017-10-12 21:44:07',
            ),
            30 => 
            array (
                'id' => 37,
                'marca' => 'AUDI',
                'modelo' => 'RS3',
                'created_at' => '2017-10-12 21:44:08',
                'updated_at' => '2017-10-12 21:44:08',
            ),
            31 => 
            array (
                'id' => 38,
                'marca' => 'AUDI',
                'modelo' => 'Q7',
                'created_at' => '2017-10-12 21:44:08',
                'updated_at' => '2017-10-12 21:44:08',
            ),
            32 => 
            array (
                'id' => 39,
                'marca' => 'AUDI',
                'modelo' => 'Q5',
                'created_at' => '2017-10-12 21:44:08',
                'updated_at' => '2017-10-12 21:44:08',
            ),
            33 => 
            array (
                'id' => 40,
                'marca' => 'AUDI',
                'modelo' => 'Q3',
                'created_at' => '2017-10-12 21:44:08',
                'updated_at' => '2017-10-12 21:44:08',
            ),
            34 => 
            array (
                'id' => 41,
                'marca' => 'AUDI',
                'modelo' => 'A8',
                'created_at' => '2017-10-12 21:44:08',
                'updated_at' => '2017-10-12 21:44:08',
            ),
            35 => 
            array (
                'id' => 42,
                'marca' => 'AUDI',
                'modelo' => 'A7',
                'created_at' => '2017-10-12 21:44:08',
                'updated_at' => '2017-10-12 21:44:08',
            ),
            36 => 
            array (
                'id' => 43,
                'marca' => 'AUDI',
                'modelo' => 'A6',
                'created_at' => '2017-10-12 21:44:08',
                'updated_at' => '2017-10-12 21:44:08',
            ),
            37 => 
            array (
                'id' => 44,
                'marca' => 'AUDI',
                'modelo' => 'A5',
                'created_at' => '2017-10-12 21:44:08',
                'updated_at' => '2017-10-12 21:44:08',
            ),
            38 => 
            array (
                'id' => 45,
                'marca' => 'AUDI',
                'modelo' => 'A4',
                'created_at' => '2017-10-12 21:44:08',
                'updated_at' => '2017-10-12 21:44:08',
            ),
            39 => 
            array (
                'id' => 46,
                'marca' => 'BMW',
                'modelo' => 'X1',
                'created_at' => '2017-10-12 21:44:48',
                'updated_at' => '2017-10-12 21:44:48',
            ),
            40 => 
            array (
                'id' => 47,
                'marca' => 'BMW',
                'modelo' => 'X6M',
                'created_at' => '2017-10-12 21:44:48',
                'updated_at' => '2017-10-12 21:44:48',
            ),
            41 => 
            array (
                'id' => 48,
                'marca' => 'BMW',
                'modelo' => 'X6',
                'created_at' => '2017-10-12 21:44:48',
                'updated_at' => '2017-10-12 21:44:48',
            ),
            42 => 
            array (
                'id' => 49,
                'marca' => 'BMW',
                'modelo' => 'X5M',
                'created_at' => '2017-10-12 21:44:48',
                'updated_at' => '2017-10-12 21:44:48',
            ),
            43 => 
            array (
                'id' => 50,
                'marca' => 'BMW',
                'modelo' => 'X5',
                'created_at' => '2017-10-12 21:44:48',
                'updated_at' => '2017-10-12 21:44:48',
            ),
            44 => 
            array (
                'id' => 51,
                'marca' => 'BMW',
                'modelo' => 'X4',
                'created_at' => '2017-10-12 21:44:48',
                'updated_at' => '2017-10-12 21:44:48',
            ),
            45 => 
            array (
                'id' => 52,
                'marca' => 'BMW',
                'modelo' => 'X3',
                'created_at' => '2017-10-12 21:44:49',
                'updated_at' => '2017-10-12 21:44:49',
            ),
            46 => 
            array (
                'id' => 53,
                'marca' => 'BMW',
                'modelo' => 'X2',
                'created_at' => '2017-10-12 21:44:49',
                'updated_at' => '2017-10-12 21:44:49',
            ),
            47 => 
            array (
                'id' => 54,
                'marca' => 'CADILLAC',
                'modelo' => 'ESCALADE',
                'created_at' => '2017-10-12 21:45:12',
                'updated_at' => '2017-10-12 21:45:12',
            ),
            48 => 
            array (
                'id' => 55,
                'marca' => 'CADILLAC',
                'modelo' => 'ESCALADE EXT',
                'created_at' => '2017-10-12 21:45:12',
                'updated_at' => '2017-10-12 21:45:12',
            ),
            49 => 
            array (
                'id' => 56,
                'marca' => 'CADILLAC',
                'modelo' => 'ESCALADE ESV',
                'created_at' => '2017-10-12 21:45:12',
                'updated_at' => '2017-10-12 21:45:12',
            ),
            50 => 
            array (
                'id' => 57,
                'marca' => 'DODGE',
                'modelo' => 'CHALLENGER ',
                'created_at' => '2017-10-12 21:45:50',
                'updated_at' => '2017-10-12 21:45:50',
            ),
            51 => 
            array (
                'id' => 58,
                'marca' => 'DODGE',
                'modelo' => 'NITRO',
                'created_at' => '2017-10-12 21:45:51',
                'updated_at' => '2017-10-12 21:45:51',
            ),
            52 => 
            array (
                'id' => 59,
                'marca' => 'DODGE',
                'modelo' => 'JOURNEY',
                'created_at' => '2017-10-12 21:45:51',
                'updated_at' => '2017-10-12 21:45:51',
            ),
            53 => 
            array (
                'id' => 60,
                'marca' => 'DODGE',
                'modelo' => 'DURANGO',
                'created_at' => '2017-10-12 21:45:51',
                'updated_at' => '2017-10-12 21:45:51',
            ),
            54 => 
            array (
                'id' => 61,
                'marca' => 'DODGE',
                'modelo' => 'DART',
                'created_at' => '2017-10-12 21:45:51',
                'updated_at' => '2017-10-12 21:45:51',
            ),
            55 => 
            array (
                'id' => 62,
                'marca' => 'DODGE',
                'modelo' => 'DAKOTA',
                'created_at' => '2017-10-12 21:45:51',
                'updated_at' => '2017-10-12 21:45:51',
            ),
            56 => 
            array (
                'id' => 63,
                'marca' => 'DODGE',
                'modelo' => 'CHARGER',
                'created_at' => '2017-10-12 21:45:51',
                'updated_at' => '2017-10-12 21:45:51',
            ),
            57 => 
            array (
                'id' => 64,
                'marca' => 'FORD',
                'modelo' => 'ECOSPORT',
                'created_at' => '2017-10-12 21:47:33',
                'updated_at' => '2017-10-12 21:47:33',
            ),
            58 => 
            array (
                'id' => 65,
                'marca' => 'FORD',
                'modelo' => 'WINDSTAR',
                'created_at' => '2017-10-12 21:47:33',
                'updated_at' => '2017-10-12 21:47:33',
            ),
            59 => 
            array (
                'id' => 66,
                'marca' => 'FORD',
                'modelo' => 'TRUCK RANGER',
                'created_at' => '2017-10-12 21:47:33',
                'updated_at' => '2017-10-12 21:47:33',
            ),
            60 => 
            array (
                'id' => 67,
                'marca' => 'FORD',
                'modelo' => 'TRUCK F550 SUPERDUTY',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            61 => 
            array (
                'id' => 68,
                'marca' => 'FORD',
                'modelo' => 'TRUCK F450 SUPERDUTY',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            62 => 
            array (
                'id' => 69,
                'marca' => 'FORD',
                'modelo' => 'TRUCK F450 NOT SUPERDUTY',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            63 => 
            array (
                'id' => 70,
                'marca' => 'FORD',
                'modelo' => 'TRUCK F350 SUPERDUTY',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            64 => 
            array (
                'id' => 71,
                'marca' => 'FORD',
                'modelo' => 'TRUCK F350 NOT SUPERDUTY',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            65 => 
            array (
                'id' => 72,
                'marca' => 'FORD',
                'modelo' => 'TRUCK F250 SUPERDUTY',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            66 => 
            array (
                'id' => 73,
                'marca' => 'FORD',
                'modelo' => 'TRUCK F150 RAPTOR',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            67 => 
            array (
                'id' => 74,
                'marca' => 'FORD',
                'modelo' => 'TRUCK F150',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            68 => 
            array (
                'id' => 75,
                'marca' => 'FORD',
                'modelo' => 'TRUCK F100',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            69 => 
            array (
                'id' => 76,
                'marca' => 'FORD',
                'modelo' => 'TRANSIT 350',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            70 => 
            array (
                'id' => 77,
                'marca' => 'FORD',
                'modelo' => 'TRANSIT 250',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            71 => 
            array (
                'id' => 78,
                'marca' => 'FORD',
                'modelo' => 'TRANSIT 150',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            72 => 
            array (
                'id' => 79,
                'marca' => 'FORD',
                'modelo' => 'RANGER',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            73 => 
            array (
                'id' => 80,
                'marca' => 'FORD',
                'modelo' => 'MUSTANG',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            74 => 
            array (
                'id' => 81,
                'marca' => 'FORD',
                'modelo' => 'GT',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            75 => 
            array (
                'id' => 82,
                'marca' => 'FORD',
                'modelo' => 'FUSION',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            76 => 
            array (
                'id' => 83,
                'marca' => 'FORD',
                'modelo' => 'FOCUS RS',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            77 => 
            array (
                'id' => 84,
                'marca' => 'FORD',
                'modelo' => 'FOCUS',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            78 => 
            array (
                'id' => 85,
                'marca' => 'FORD',
                'modelo' => 'FLEX',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            79 => 
            array (
                'id' => 86,
                'marca' => 'FORD',
                'modelo' => 'FIESTA',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            80 => 
            array (
                'id' => 87,
                'marca' => 'FORD',
                'modelo' => 'EXPLORER',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            81 => 
            array (
                'id' => 88,
                'marca' => 'FORD',
                'modelo' => 'EXPEDITION',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            82 => 
            array (
                'id' => 89,
                'marca' => 'FORD',
                'modelo' => 'ESCORT',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            83 => 
            array (
                'id' => 90,
                'marca' => 'FORD',
                'modelo' => 'ESCAPE',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            84 => 
            array (
                'id' => 91,
                'marca' => 'FORD',
                'modelo' => 'EDGE',
                'created_at' => '2017-10-12 21:47:34',
                'updated_at' => '2017-10-12 21:47:34',
            ),
            85 => 
            array (
                'id' => 92,
                'marca' => 'HONDA',
                'modelo' => 'ACCORD',
                'created_at' => '2017-10-12 21:48:17',
                'updated_at' => '2017-10-12 21:48:17',
            ),
            86 => 
            array (
                'id' => 93,
                'marca' => 'HONDA',
                'modelo' => 'RIDGELINE',
                'created_at' => '2017-10-12 21:48:17',
                'updated_at' => '2017-10-12 21:48:17',
            ),
            87 => 
            array (
                'id' => 94,
                'marca' => 'HONDA',
                'modelo' => 'PRELUDE',
                'created_at' => '2017-10-12 21:48:17',
                'updated_at' => '2017-10-12 21:48:17',
            ),
            88 => 
            array (
                'id' => 95,
                'marca' => 'HONDA',
                'modelo' => 'PILOT',
                'created_at' => '2017-10-12 21:48:17',
                'updated_at' => '2017-10-12 21:48:17',
            ),
            89 => 
            array (
                'id' => 96,
                'marca' => 'HONDA',
                'modelo' => 'HRV',
                'created_at' => '2017-10-12 21:48:17',
                'updated_at' => '2017-10-12 21:48:17',
            ),
            90 => 
            array (
                'id' => 97,
                'marca' => 'HONDA',
                'modelo' => 'FIT',
                'created_at' => '2017-10-12 21:48:17',
                'updated_at' => '2017-10-12 21:48:17',
            ),
            91 => 
            array (
                'id' => 98,
                'marca' => 'HONDA',
                'modelo' => 'CRZ',
                'created_at' => '2017-10-12 21:48:18',
                'updated_at' => '2017-10-12 21:48:18',
            ),
            92 => 
            array (
                'id' => 99,
                'marca' => 'HONDA',
                'modelo' => 'CRX',
                'created_at' => '2017-10-12 21:48:18',
                'updated_at' => '2017-10-12 21:48:18',
            ),
            93 => 
            array (
                'id' => 100,
                'marca' => 'HONDA',
                'modelo' => 'CRV',
                'created_at' => '2017-10-12 21:48:18',
                'updated_at' => '2017-10-12 21:48:18',
            ),
            94 => 
            array (
                'id' => 101,
                'marca' => 'HONDA',
                'modelo' => 'CIVIC',
                'created_at' => '2017-10-12 21:48:18',
                'updated_at' => '2017-10-12 21:48:18',
            ),
            95 => 
            array (
                'id' => 102,
                'marca' => 'HUMMER',
                'modelo' => 'H1',
                'created_at' => '2017-10-12 21:48:34',
                'updated_at' => '2017-10-12 21:48:34',
            ),
            96 => 
            array (
                'id' => 103,
                'marca' => 'HUMMER',
                'modelo' => 'H3',
                'created_at' => '2017-10-12 21:48:34',
                'updated_at' => '2017-10-12 21:48:34',
            ),
            97 => 
            array (
                'id' => 104,
                'marca' => 'HUMMER',
                'modelo' => 'H2',
                'created_at' => '2017-10-12 21:48:34',
                'updated_at' => '2017-10-12 21:48:34',
            ),
            98 => 
            array (
                'id' => 105,
                'marca' => 'HYUNDAI',
                'modelo' => 'ACCENT',
                'created_at' => '2017-10-12 21:49:19',
                'updated_at' => '2017-10-12 21:49:19',
            ),
            99 => 
            array (
                'id' => 106,
                'marca' => 'HYUNDAI',
                'modelo' => 'VELOSTER',
                'created_at' => '2017-10-12 21:49:20',
                'updated_at' => '2017-10-12 21:49:20',
            ),
            100 => 
            array (
                'id' => 107,
                'marca' => 'HYUNDAI',
                'modelo' => 'TUCSON',
                'created_at' => '2017-10-12 21:49:20',
                'updated_at' => '2017-10-12 21:49:20',
            ),
            101 => 
            array (
                'id' => 108,
                'marca' => 'HYUNDAI',
                'modelo' => 'TIBURON',
                'created_at' => '2017-10-12 21:49:20',
                'updated_at' => '2017-10-12 21:49:20',
            ),
            102 => 
            array (
                'id' => 109,
                'marca' => 'HYUNDAI',
                'modelo' => 'SANTA FE',
                'created_at' => '2017-10-12 21:49:20',
                'updated_at' => '2017-10-12 21:49:20',
            ),
            103 => 
            array (
                'id' => 110,
                'marca' => 'HYUNDAI',
                'modelo' => 'SONATA',
                'created_at' => '2017-10-12 21:49:20',
                'updated_at' => '2017-10-12 21:49:20',
            ),
            104 => 
            array (
                'id' => 111,
                'marca' => 'HYUNDAI',
                'modelo' => 'GENESIS',
                'created_at' => '2017-10-12 21:49:20',
                'updated_at' => '2017-10-12 21:49:20',
            ),
            105 => 
            array (
                'id' => 112,
                'marca' => 'HYUNDAI',
                'modelo' => 'ELANTRA',
                'created_at' => '2017-10-12 21:49:20',
                'updated_at' => '2017-10-12 21:49:20',
            ),
            106 => 
            array (
                'id' => 113,
                'marca' => 'JEEP',
                'modelo' => 'COMPASS',
                'created_at' => '2017-10-12 21:49:54',
                'updated_at' => '2017-10-12 21:49:54',
            ),
            107 => 
            array (
                'id' => 114,
                'marca' => 'JEEP',
                'modelo' => 'WRANGLER',
                'created_at' => '2017-10-12 21:49:54',
                'updated_at' => '2017-10-12 21:49:54',
            ),
            108 => 
            array (
                'id' => 115,
                'marca' => 'JEEP',
                'modelo' => 'RENEGADE',
                'created_at' => '2017-10-12 21:49:54',
                'updated_at' => '2017-10-12 21:49:54',
            ),
            109 => 
            array (
                'id' => 116,
                'marca' => 'JEEP',
                'modelo' => 'PATRIOT',
                'created_at' => '2017-10-12 21:49:55',
                'updated_at' => '2017-10-12 21:49:55',
            ),
            110 => 
            array (
                'id' => 117,
                'marca' => 'JEEP',
                'modelo' => 'LIBERTY',
                'created_at' => '2017-10-12 21:49:55',
                'updated_at' => '2017-10-12 21:49:55',
            ),
            111 => 
            array (
                'id' => 118,
                'marca' => 'JEEP',
                'modelo' => 'GRAND WAGONEER',
                'created_at' => '2017-10-12 21:49:55',
                'updated_at' => '2017-10-12 21:49:55',
            ),
            112 => 
            array (
                'id' => 119,
                'marca' => 'JEEP',
                'modelo' => 'GRAND CHEROKEE',
                'created_at' => '2017-10-12 21:49:55',
                'updated_at' => '2017-10-12 21:49:55',
            ),
            113 => 
            array (
                'id' => 120,
                'marca' => 'KIA',
                'modelo' => 'FORTE',
                'created_at' => '2017-10-12 21:50:15',
                'updated_at' => '2017-10-12 21:50:15',
            ),
            114 => 
            array (
                'id' => 121,
                'marca' => 'KIA',
                'modelo' => 'SPORTAGE',
                'created_at' => '2017-10-12 21:50:15',
                'updated_at' => '2017-10-12 21:50:15',
            ),
            115 => 
            array (
                'id' => 122,
                'marca' => 'KIA',
                'modelo' => 'SOUL',
                'created_at' => '2017-10-12 21:50:15',
                'updated_at' => '2017-10-12 21:50:15',
            ),
            116 => 
            array (
                'id' => 123,
                'marca' => 'KIA',
                'modelo' => 'RIO',
                'created_at' => '2017-10-12 21:50:15',
                'updated_at' => '2017-10-12 21:50:15',
            ),
            117 => 
            array (
                'id' => 124,
                'marca' => 'KIA',
                'modelo' => 'OPTIMA',
                'created_at' => '2017-10-12 21:50:15',
                'updated_at' => '2017-10-12 21:50:15',
            ),
            118 => 
            array (
                'id' => 125,
                'marca' => 'LAND ROVER',
                'modelo' => 'DEFENDER',
                'created_at' => '2017-10-12 21:50:33',
                'updated_at' => '2017-10-12 21:50:33',
            ),
            119 => 
            array (
                'id' => 126,
                'marca' => 'LAND ROVER',
                'modelo' => 'RANGE ROVER SPORT',
                'created_at' => '2017-10-12 21:50:33',
                'updated_at' => '2017-10-12 21:50:33',
            ),
            120 => 
            array (
                'id' => 127,
                'marca' => 'LAND ROVER',
                'modelo' => 'RANGE ROVER EVOQUE',
                'created_at' => '2017-10-12 21:50:33',
                'updated_at' => '2017-10-12 21:50:33',
            ),
            121 => 
            array (
                'id' => 128,
                'marca' => 'LAND ROVER',
                'modelo' => 'RANGE ROVER',
                'created_at' => '2017-10-12 21:50:33',
                'updated_at' => '2017-10-12 21:50:33',
            ),
            122 => 
            array (
                'id' => 129,
                'marca' => 'LAND ROVER',
                'modelo' => 'DISCOVERY',
                'created_at' => '2017-10-12 21:50:33',
                'updated_at' => '2017-10-12 21:50:33',
            ),
            123 => 
            array (
                'id' => 130,
                'marca' => 'MAZDA',
                'modelo' => '2',
                'created_at' => '2017-10-12 21:51:29',
                'updated_at' => '2017-10-12 21:51:29',
            ),
            124 => 
            array (
                'id' => 131,
                'marca' => 'MAZDA',
                'modelo' => 'TRIBUTE',
                'created_at' => '2017-10-12 21:51:29',
                'updated_at' => '2017-10-12 21:51:29',
            ),
            125 => 
            array (
                'id' => 132,
                'marca' => 'MAZDA',
                'modelo' => 'CX9',
                'created_at' => '2017-10-12 21:51:29',
                'updated_at' => '2017-10-12 21:51:29',
            ),
            126 => 
            array (
                'id' => 133,
                'marca' => 'MAZDA',
                'modelo' => 'CX7',
                'created_at' => '2017-10-12 21:51:29',
                'updated_at' => '2017-10-12 21:51:29',
            ),
            127 => 
            array (
                'id' => 134,
                'marca' => 'MAZDA',
                'modelo' => 'CX5',
                'created_at' => '2017-10-12 21:51:29',
                'updated_at' => '2017-10-12 21:51:29',
            ),
            128 => 
            array (
                'id' => 135,
                'marca' => 'MAZDA',
                'modelo' => 'CX3',
                'created_at' => '2017-10-12 21:51:29',
                'updated_at' => '2017-10-12 21:51:29',
            ),
            129 => 
            array (
                'id' => 136,
                'marca' => 'MAZDA',
                'modelo' => '6',
                'created_at' => '2017-10-12 21:51:29',
                'updated_at' => '2017-10-12 21:51:29',
            ),
            130 => 
            array (
                'id' => 137,
                'marca' => 'MAZDA',
                'modelo' => '5',
                'created_at' => '2017-10-12 21:51:29',
                'updated_at' => '2017-10-12 21:51:29',
            ),
            131 => 
            array (
                'id' => 138,
                'marca' => 'MAZDA',
                'modelo' => '3',
                'created_at' => '2017-10-12 21:51:29',
                'updated_at' => '2017-10-12 21:51:29',
            ),
            132 => 
            array (
                'id' => 139,
                'marca' => 'MERCEDES',
                'modelo' => 'AMG GT',
                'created_at' => '2017-10-12 21:52:32',
                'updated_at' => '2017-10-12 21:52:32',
            ),
            133 => 
            array (
                'id' => 140,
                'marca' => 'MERCEDES',
                'modelo' => 'SLR',
                'created_at' => '2017-10-12 21:52:32',
                'updated_at' => '2017-10-12 21:52:32',
            ),
            134 => 
            array (
                'id' => 141,
                'marca' => 'MERCEDES',
                'modelo' => 'CLK',
                'created_at' => '2017-10-12 21:52:32',
                'updated_at' => '2017-10-12 21:52:32',
            ),
            135 => 
            array (
                'id' => 142,
                'marca' => 'MERCEDES',
                'modelo' => 'GLA CLASS',
                'created_at' => '2017-10-12 21:52:32',
                'updated_at' => '2017-10-12 21:52:32',
            ),
            136 => 
            array (
                'id' => 143,
                'marca' => 'MERCEDES',
                'modelo' => 'CLA CLASS',
                'created_at' => '2017-10-12 21:52:32',
                'updated_at' => '2017-10-12 21:52:32',
            ),
            137 => 
            array (
                'id' => 144,
                'marca' => 'MERCEDES',
                'modelo' => 'CL CLASS',
                'created_at' => '2017-10-12 21:52:32',
                'updated_at' => '2017-10-12 21:52:32',
            ),
            138 => 
            array (
                'id' => 145,
                'marca' => 'MERCEDES',
                'modelo' => 'C CLASS',
                'created_at' => '2017-10-12 21:52:32',
                'updated_at' => '2017-10-12 21:52:32',
            ),
            139 => 
            array (
                'id' => 146,
                'marca' => 'MERCEDES',
                'modelo' => 'B CLASS',
                'created_at' => '2017-10-12 21:52:32',
                'updated_at' => '2017-10-12 21:52:32',
            ),
            140 => 
            array (
                'id' => 147,
                'marca' => 'MITSUBISHI',
                'modelo' => 'ENDEAVOR',
                'created_at' => '2017-10-12 21:52:49',
                'updated_at' => '2017-10-12 21:52:49',
            ),
            141 => 
            array (
                'id' => 148,
                'marca' => 'MITSUBISHI',
                'modelo' => 'OUTLANDER',
                'created_at' => '2017-10-12 21:52:49',
                'updated_at' => '2017-10-12 21:52:49',
            ),
            142 => 
            array (
                'id' => 149,
                'marca' => 'MITSUBISHI',
                'modelo' => 'MIRAGE',
                'created_at' => '2017-10-12 21:52:49',
                'updated_at' => '2017-10-12 21:52:49',
            ),
            143 => 
            array (
                'id' => 150,
                'marca' => 'MITSUBISHI',
                'modelo' => 'LANCER',
                'created_at' => '2017-10-12 21:52:49',
                'updated_at' => '2017-10-12 21:52:49',
            ),
            144 => 
            array (
                'id' => 151,
                'marca' => 'NISSAN',
                'modelo' => '350Z',
                'created_at' => '2017-10-12 21:53:46',
                'updated_at' => '2017-10-12 21:53:46',
            ),
            145 => 
            array (
                'id' => 152,
                'marca' => 'NISSAN',
                'modelo' => 'XTERRA',
                'created_at' => '2017-10-12 21:53:46',
                'updated_at' => '2017-10-12 21:53:46',
            ),
            146 => 
            array (
                'id' => 153,
                'marca' => 'NISSAN',
                'modelo' => 'VERSA',
                'created_at' => '2017-10-12 21:53:46',
                'updated_at' => '2017-10-12 21:53:46',
            ),
            147 => 
            array (
                'id' => 154,
                'marca' => 'NISSAN',
                'modelo' => 'TRUCK TITAN XD',
                'created_at' => '2017-10-12 21:53:46',
                'updated_at' => '2017-10-12 21:53:46',
            ),
            148 => 
            array (
                'id' => 155,
                'marca' => 'NISSAN',
                'modelo' => 'TRUCK TITAN',
                'created_at' => '2017-10-12 21:53:46',
                'updated_at' => '2017-10-12 21:53:46',
            ),
            149 => 
            array (
                'id' => 156,
                'marca' => 'NISSAN',
                'modelo' => 'SENTRA',
                'created_at' => '2017-10-12 21:53:46',
                'updated_at' => '2017-10-12 21:53:46',
            ),
            150 => 
            array (
                'id' => 157,
                'marca' => 'NISSAN',
                'modelo' => 'ROGUE SPORT',
                'created_at' => '2017-10-12 21:53:46',
                'updated_at' => '2017-10-12 21:53:46',
            ),
            151 => 
            array (
                'id' => 158,
                'marca' => 'NISSAN',
                'modelo' => 'ROGUE',
                'created_at' => '2017-10-12 21:53:46',
                'updated_at' => '2017-10-12 21:53:46',
            ),
            152 => 
            array (
                'id' => 159,
                'marca' => 'NISSAN',
                'modelo' => 'PATROL',
                'created_at' => '2017-10-12 21:53:46',
                'updated_at' => '2017-10-12 21:53:46',
            ),
            153 => 
            array (
                'id' => 160,
                'marca' => 'NISSAN',
                'modelo' => 'PATHFINDER',
                'created_at' => '2017-10-12 21:53:46',
                'updated_at' => '2017-10-12 21:53:46',
            ),
            154 => 
            array (
                'id' => 161,
                'marca' => 'NISSAN',
                'modelo' => 'MURANO',
                'created_at' => '2017-10-12 21:53:46',
                'updated_at' => '2017-10-12 21:53:46',
            ),
            155 => 
            array (
                'id' => 162,
                'marca' => 'NISSAN',
                'modelo' => 'JUKE',
                'created_at' => '2017-10-12 21:53:46',
                'updated_at' => '2017-10-12 21:53:46',
            ),
            156 => 
            array (
                'id' => 163,
                'marca' => 'NISSAN',
                'modelo' => 'GTR',
                'created_at' => '2017-10-12 21:53:46',
                'updated_at' => '2017-10-12 21:53:46',
            ),
            157 => 
            array (
                'id' => 164,
                'marca' => 'NISSAN',
                'modelo' => 'FRONTIER',
                'created_at' => '2017-10-12 21:53:46',
                'updated_at' => '2017-10-12 21:53:46',
            ),
            158 => 
            array (
                'id' => 165,
                'marca' => 'NISSAN',
                'modelo' => 'CUBE',
                'created_at' => '2017-10-12 21:53:46',
                'updated_at' => '2017-10-12 21:53:46',
            ),
            159 => 
            array (
                'id' => 166,
                'marca' => 'NISSAN',
                'modelo' => 'ARMADA',
                'created_at' => '2017-10-12 21:53:46',
                'updated_at' => '2017-10-12 21:53:46',
            ),
            160 => 
            array (
                'id' => 167,
                'marca' => 'NISSAN',
                'modelo' => '370Z',
                'created_at' => '2017-10-12 21:53:46',
                'updated_at' => '2017-10-12 21:53:46',
            ),
            161 => 
            array (
                'id' => 168,
                'marca' => 'SUZUKI',
                'modelo' => 'EQUATOR',
                'created_at' => '2017-10-12 21:54:08',
                'updated_at' => '2017-10-12 21:54:08',
            ),
            162 => 
            array (
                'id' => 169,
                'marca' => 'SUZUKI',
                'modelo' => 'XL7',
                'created_at' => '2017-10-12 21:54:08',
                'updated_at' => '2017-10-12 21:54:08',
            ),
            163 => 
            array (
                'id' => 170,
                'marca' => 'SUZUKI',
                'modelo' => 'SX4',
                'created_at' => '2017-10-12 21:54:08',
                'updated_at' => '2017-10-12 21:54:08',
            ),
            164 => 
            array (
                'id' => 171,
                'marca' => 'SUZUKI',
                'modelo' => 'SWIFT',
                'created_at' => '2017-10-12 21:54:08',
                'updated_at' => '2017-10-12 21:54:08',
            ),
            165 => 
            array (
                'id' => 172,
                'marca' => 'SUZUKI',
                'modelo' => 'KIZASHI',
                'created_at' => '2017-10-12 21:54:08',
                'updated_at' => '2017-10-12 21:54:08',
            ),
            166 => 
            array (
                'id' => 173,
                'marca' => 'TOYOTA',
                'modelo' => '86',
                'created_at' => '2017-10-12 21:55:10',
                'updated_at' => '2017-10-12 21:55:10',
            ),
            167 => 
            array (
                'id' => 174,
                'marca' => 'TOYOTA',
                'modelo' => 'YAIRS Ia',
                'created_at' => '2017-10-12 21:55:10',
                'updated_at' => '2017-10-12 21:55:10',
            ),
            168 => 
            array (
                'id' => 175,
                'marca' => 'TOYOTA',
                'modelo' => 'YARIS',
                'created_at' => '2017-10-12 21:55:10',
                'updated_at' => '2017-10-12 21:55:10',
            ),
            169 => 
            array (
                'id' => 176,
                'marca' => 'TOYOTA',
                'modelo' => 'TUNDRA',
                'created_at' => '2017-10-12 21:55:10',
                'updated_at' => '2017-10-12 21:55:10',
            ),
            170 => 
            array (
                'id' => 177,
                'marca' => 'TOYOTA',
                'modelo' => 'TACOMA',
                'created_at' => '2017-10-12 21:55:10',
                'updated_at' => '2017-10-12 21:55:10',
            ),
            171 => 
            array (
                'id' => 178,
                'marca' => 'TOYOTA',
                'modelo' => 'STARLET',
                'created_at' => '2017-10-12 21:55:10',
                'updated_at' => '2017-10-12 21:55:10',
            ),
            172 => 
            array (
                'id' => 179,
                'marca' => 'TOYOTA',
                'modelo' => 'SIENNA',
                'created_at' => '2017-10-12 21:55:10',
                'updated_at' => '2017-10-12 21:55:10',
            ),
            173 => 
            array (
                'id' => 180,
                'marca' => 'TOYOTA',
                'modelo' => 'SEQUOIA',
                'created_at' => '2017-10-12 21:55:11',
                'updated_at' => '2017-10-12 21:55:11',
            ),
            174 => 
            array (
                'id' => 181,
                'marca' => 'TOYOTA',
                'modelo' => 'RAV4',
                'created_at' => '2017-10-12 21:55:11',
                'updated_at' => '2017-10-12 21:55:11',
            ),
            175 => 
            array (
                'id' => 182,
                'marca' => 'TOYOTA',
                'modelo' => 'HIGHLANDER',
                'created_at' => '2017-10-12 21:55:11',
                'updated_at' => '2017-10-12 21:55:11',
            ),
            176 => 
            array (
                'id' => 183,
                'marca' => 'TOYOTA',
                'modelo' => 'FJ CRUISER',
                'created_at' => '2017-10-12 21:55:11',
                'updated_at' => '2017-10-12 21:55:11',
            ),
            177 => 
            array (
                'id' => 184,
                'marca' => 'TOYOTA',
                'modelo' => 'COROLLA SEDAN',
                'created_at' => '2017-10-12 21:55:11',
                'updated_at' => '2017-10-12 21:55:11',
            ),
            178 => 
            array (
                'id' => 185,
                'marca' => 'TOYOTA',
                'modelo' => 'COROLLA Im',
                'created_at' => '2017-10-12 21:55:11',
                'updated_at' => '2017-10-12 21:55:11',
            ),
            179 => 
            array (
                'id' => 186,
                'marca' => 'TOYOTA',
                'modelo' => 'CHR',
                'created_at' => '2017-10-12 21:55:11',
                'updated_at' => '2017-10-12 21:55:11',
            ),
            180 => 
            array (
                'id' => 187,
                'marca' => 'TOYOTA',
                'modelo' => 'AVALON',
                'created_at' => '2017-10-12 21:55:11',
                'updated_at' => '2017-10-12 21:55:11',
            ),
            181 => 
            array (
                'id' => 188,
                'marca' => 'TOYOTA',
                'modelo' => '4RUNNER',
                'created_at' => '2017-10-12 21:55:11',
                'updated_at' => '2017-10-12 21:55:11',
            ),
            182 => 
            array (
                'id' => 189,
                'marca' => 'VOLKSWAGEN',
                'modelo' => 'ATLAS',
                'created_at' => '2017-10-12 21:55:44',
                'updated_at' => '2017-10-12 21:55:44',
            ),
            183 => 
            array (
                'id' => 190,
                'marca' => 'VOLKSWAGEN',
                'modelo' => 'TIGUAN',
                'created_at' => '2017-10-12 21:55:44',
                'updated_at' => '2017-10-12 21:55:44',
            ),
            184 => 
            array (
                'id' => 191,
                'marca' => 'VOLKSWAGEN',
                'modelo' => 'PASSAT',
                'created_at' => '2017-10-12 21:55:44',
                'updated_at' => '2017-10-12 21:55:44',
            ),
            185 => 
            array (
                'id' => 192,
                'marca' => 'VOLKSWAGEN',
                'modelo' => 'JETTA GLI',
                'created_at' => '2017-10-12 21:55:45',
                'updated_at' => '2017-10-12 21:55:45',
            ),
            186 => 
            array (
                'id' => 193,
                'marca' => 'VOLKSWAGEN',
                'modelo' => 'JETTA',
                'created_at' => '2017-10-12 21:55:45',
                'updated_at' => '2017-10-12 21:55:45',
            ),
            187 => 
            array (
                'id' => 194,
                'marca' => 'VOLKSWAGEN',
                'modelo' => 'GOLF GTI',
                'created_at' => '2017-10-12 21:55:45',
                'updated_at' => '2017-10-12 21:55:45',
            ),
            188 => 
            array (
                'id' => 195,
                'marca' => 'VOLKSWAGEN',
                'modelo' => 'GOLF',
                'created_at' => '2017-10-12 21:55:45',
                'updated_at' => '2017-10-12 21:55:45',
            ),
            189 => 
            array (
                'id' => 196,
                'marca' => 'VOLKSWAGEN',
                'modelo' => 'BEETLE BUG',
                'created_at' => '2017-10-12 21:55:45',
                'updated_at' => '2017-10-12 21:55:45',
            ),
            190 => 
            array (
                'id' => 197,
                'marca' => 'VOLVO',
                'modelo' => 'XC90',
                'created_at' => '2017-10-12 21:55:57',
                'updated_at' => '2017-10-12 21:55:57',
            ),
        ));
        
        
    }
}
