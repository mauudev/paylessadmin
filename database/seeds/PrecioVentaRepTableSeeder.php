<?php

use Illuminate\Database\Seeder;

class PrecioVentaRepTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('precio_venta_rep')->delete();
        
        \DB::table('precio_venta_rep')->insert(array (
            0 => 
            array (
                'id' => 1,
                'transporte' => 20.0,
                'adicional' => 20.0,
                'precio_total' => 965.0,
                'cotizacion_repuestos_id' => 1,
                'proveedores_cotizacion_rep_id' => 1,
                'proveedores_id' => 2,
                'created_at' => '2017-12-01 01:59:22',
                'updated_at' => '2017-12-01 01:59:22',
            ),
        ));
        
        
    }
}
