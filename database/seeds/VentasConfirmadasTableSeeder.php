<?php

use Illuminate\Database\Seeder;

class VentasConfirmadasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('ventas_confirmadas')->delete();
    }
}
