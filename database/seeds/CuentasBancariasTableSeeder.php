<?php

use Illuminate\Database\Seeder;

class CuentasBancariasTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cuentas_bancarias')->delete();
        
        \DB::table('cuentas_bancarias')->insert(array (
            0 => 
            array (
                'id' => 1,
                'banco' => 'BANCO UNION',
                'cuenta_bancaria' => '2-48874378',
                'moneda' => 'Bolivianos',
                'created_at' => '2017-09-11 02:00:17',
                'updated_at' => '2017-09-11 02:00:17',
            ),
            1 => 
            array (
                'id' => 2,
                'banco' => 'BANCO UNION',
                'cuenta_bancaria' => '2-48874333',
                'moneda' => 'Dolares',
                'created_at' => '2017-09-11 02:00:29',
                'updated_at' => '2017-09-11 02:00:29',
            ),
            2 => 
            array (
                'id' => 3,
                'banco' => 'BANCO ECONOMICO',
                'cuenta_bancaria' => '3-873783788',
                'moneda' => 'Bolivianos',
                'created_at' => '2017-09-11 02:00:47',
                'updated_at' => '2017-09-11 02:00:47',
            ),
        ));
        
        
    }
}
