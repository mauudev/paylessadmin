<?php

use Illuminate\Database\Seeder;

class ProveedoresCotizacionRepTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('proveedores_cotizacion_rep')->delete();
        
        \DB::table('proveedores_cotizacion_rep')->insert(array (
            0 => 
            array (
                'id' => 1,
                'precio_cot_rep' => 500.0,
                'ventas_id' => 1,
                'repuestos_id' => 1,
                'proveedores_id' => 2,
                'created_at' => '2017-12-01 01:58:55',
                'updated_at' => '2017-12-01 01:58:55',
            ),
        ));
        
        
    }
}
