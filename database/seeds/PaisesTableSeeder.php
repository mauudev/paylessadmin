<?php

use Illuminate\Database\Seeder;

class PaisesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \Schema::disableForeignKeyConstraints();

        \DB::table('paises')->delete();
        
        \DB::table('paises')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre_largo' => 'Cochabamba',
                'nombre_corto' => 'CB',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'nombre_largo' => 'Santa Cruz',
                'nombre_corto' => 'SCZ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'nombre_largo' => 'La Paz',
                'nombre_corto' => 'LP',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'nombre_largo' => 'Tarija',
                'nombre_corto' => 'TJ',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'nombre_largo' => 'Potosi',
                'nombre_corto' => 'PT',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'nombre_largo' => 'Oruro',
                'nombre_corto' => 'OR',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'nombre_largo' => 'Sucre',
                'nombre_corto' => 'SU',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'nombre_largo' => 'Pando',
                'nombre_corto' => 'PN',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'nombre_largo' => 'Beni',
                'nombre_corto' => 'BN',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'nombre_largo' => 'Yacuiba',
                'nombre_corto' => 'YA',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'nombre_largo' => 'El Alto',
                'nombre_corto' => 'EA',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'nombre_largo' => 'Bermejo',
                'nombre_corto' => 'BR',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'nombre_largo' => 'Camiri',
                'nombre_corto' => 'CA',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'nombre_largo' => 'Villamontes',
                'nombre_corto' => 'VI',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'nombre_largo' => 'Uyuni',
                'nombre_corto' => 'UY',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'nombre_largo' => 'Puerto Suarez',
                'nombre_corto' => 'PS',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
    }
}