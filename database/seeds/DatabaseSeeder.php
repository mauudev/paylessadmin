<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(TipoUsuariosTableSeeder::class);
        $this->call(TipoProveedoresTableSeeder::class);
        $this->call(ClientesTableSeeder::class);
        $this->call(UsuariosTableSeeder::class);
        $this->call(PaisesTableSeeder::class);
        $this->call(ProveedoresTableSeeder::class);
        $this->call(MercaderiasTableSeeder::class);
        $this->call(RepuestosTableSeeder::class);
        $this->call(CotizacionMercaderiasTableSeeder::class);
        $this->call(CotizacionRepuestosTableSeeder::class);
        $this->call(ProveedoresCotizacionRepTableSeeder::class);
        $this->call(VentasTableSeeder::class);

        /*$this->call(TipoProveedoresTableSeeder::class);
        $this->call(UsuariosTableSeeder::class);
        $this->call(ProveedoresTableSeeder::class);
        $this->call(MarcasProveedoresTableSeeder::class);
        $this->call(PaisesTableSeeder::class);*/
        $this->call(PrecioVentaRepTableSeeder::class);
        $this->call(PrecioVentaMerTableSeeder::class);
        $this->call(ProveedoresCotizacionMerTableSeeder::class);
        $this->call(CuentasBancariasTableSeeder::class);
        $this->call(PrecioTransporteRepuestosTableSeeder::class);
        $this->call(PrecioTransporteMercaderiasTableSeeder::class);
        $this->call('TipoUsuariosTableSeeder');
        $this->call('ProveedoresTableSeeder');
        $this->call('TipoProveedoresTableSeeder');
        $this->call('MarcasProveedoresTableSeeder');
        $this->call('UsuariosTableSeeder');
        $this->call('CuentasBancariasTableSeeder');
        $this->call('VentasConfirmadasTableSeeder');
        $this->call('MarcasModelosTableSeeder');
        $this->call('ClientesTableSeeder');
        $this->call('CotizacionMercaderiasTableSeeder');
        $this->call('CotizacionRepuestosTableSeeder');
        $this->call('PrecioVentaMerTableSeeder');
        $this->call('PrecioVentaRepTableSeeder');
        $this->call('ProveedoresCotizacionMerTableSeeder');
        $this->call('ProveedoresCotizacionRepTableSeeder');
        $this->call('RepuestosTableSeeder');
        $this->call('MercaderiasTableSeeder');
        $this->call('VentasTableSeeder');
        $this->call('DetallePagosEfectivoSeeder');
        $this->call('DetallePagosDepositoSeeder');
    }
}
