<?php

use Illuminate\Database\Seeder;

class UsuariosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('usuarios')->delete();
        
        \DB::table('usuarios')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre_u' => 'Sergio',
                'apellidos_u' => 'Matamoros Zegarra',
                'telefono_u' => '4467367',
                'celular_u' => '76400111',
                'email_u' => 'sergio@mail.com',
                'user_name' => 'sergio123',
                'password' => '$2y$10$rYg1N8QIK8DPrVTxFZrt2.vefoCcEB9DGMAXLVCIRaIBmtqB/3qrS',
                'tipo_usuarios_id' => 1,
                'type' => 'admin',
                'remember_token' => NULL,
                'deleted_at' => NULL,
                'created_at' => '2017-02-04 20:08:07',
                'updated_at' => '2017-02-04 20:10:50',
            ),
            1 => 
            array (
                'id' => 2,
                'nombre_u' => 'Mauricio',
                'apellidos_u' => 'Trigo Uriona',
                'telefono_u' => '428-6905',
                'celular_u' => '793-31785',
                'email_u' => 'mtrigo143@mail.com',
                'user_name' => 'maudev143',
                'password' => '$2y$10$Fn2Ip5o/Bhb9BL6hJP64XuLFwAFAERGNQQzU8KT7dPb03fPCVPmKO',
                'tipo_usuarios_id' => 1,
                'type' => 'admin',
                'remember_token' => NULL,
                'deleted_at' => NULL,
                'created_at' => '2017-02-04 20:08:07',
                'updated_at' => '2017-08-24 02:21:52',
            ),
        ));
        
        
    }
}
