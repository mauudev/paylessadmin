<?php

use Illuminate\Database\Seeder;

class PrecioVentaMerTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('precio_venta_mer')->delete();
        
        \DB::table('precio_venta_mer')->insert(array (
            0 => 
            array (
                'id' => 1,
                'transporte' => 25.0,
                'adicional' => 20.0,
                'precio_total' => 692.5,
                'cotizacion_mercaderias_id' => 1,
                'proveedores_cotizacion_mer_id' => 1,
                'proveedores_id' => 2,
                'created_at' => '2017-12-01 01:59:46',
                'updated_at' => '2017-12-01 01:59:46',
            ),
        ));
        
        
    }
}
