<?php

use Illuminate\Database\Seeder;

class DetallePagosDepositoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('detalle_pagos_deposito')->delete();
    }
}
