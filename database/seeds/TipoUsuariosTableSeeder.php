<?php

use Illuminate\Database\Seeder;

class TipoUsuariosTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \Schema::disableForeignKeyConstraints();


        \DB::table('tipo_usuarios')->delete();
        
        \DB::table('tipo_usuarios')->insert(array (
            0 => 
            array (
                'id' => 1,
                'tipo' => 'Administrador',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'tipo' => 'Usuario',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}
