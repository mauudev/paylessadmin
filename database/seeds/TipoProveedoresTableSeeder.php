<?php

use Illuminate\Database\Seeder;

class TipoProveedoresTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tipo_proveedores')->delete();
        
        \DB::table('tipo_proveedores')->insert(array (
            0 => 
            array (
                'id' => 1,
                'tipo' => 'Repuestos AFT',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'tipo' => 'Repuestos OEM',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'tipo' => 'Repuestos usados',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'tipo' => 'Electronica',
                'created_at' => '2017-01-18 00:00:00',
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'tipo' => 'Ropa',
                'created_at' => '2017-01-18 00:00:00',
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}
