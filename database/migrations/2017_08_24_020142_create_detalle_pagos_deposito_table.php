<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetallePagosDepositoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_pagos_deposito', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ventas_confirmadas_id')->unsigned();
            $table->foreign('ventas_confirmadas_id')->references('id')->on('ventas_confirmadas');
            $table->integer('cuentas_bancarias_id')->unsigned();
            $table->foreign('cuentas_bancarias_id')->references('id')->on('cuentas_bancarias');
            $table->string('detalle');
            $table->double('monto',15,2);
            $table->string('codigo_recibo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detalle_pagos_deposito');
    }
}
