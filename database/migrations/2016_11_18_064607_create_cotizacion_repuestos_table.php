<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCotizacionRepuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cotizacion_repuestos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('estado');
            $table->integer('ventas_id')->unsigned();
            $table->foreign('ventas_id')->references('id')->on('ventas')->onDelete('cascade'); 
            $table->integer('repuestos_id')->unsigned();
            $table->foreign('repuestos_id')->references('id')->on('repuestos')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('cotizacion_repuestos');
    }
}
