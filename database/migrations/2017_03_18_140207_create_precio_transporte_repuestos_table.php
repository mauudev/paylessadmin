<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrecioTransporteRepuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('precio_transporte_repuestos', function (Blueprint $table) {
            $table->increments('id');
            $table->double('transporte',15,2);
            $table->integer('repuestos_id')->unsigned();
            $table->foreign('repuestos_id')->references('id')->on('repuestos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('precio_transporte_repuestos');
    }
}
