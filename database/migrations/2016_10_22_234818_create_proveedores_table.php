<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProveedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proveedores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_compania_p');
            $table->string('direccion_p');
            $table->string('telefono_p');
            $table->string('celular_p');
            $table->string('email_p');
            $table->string('persona_contacto_p');
            $table->integer('tipo_proveedores_id')->unsigned();
            $table->foreign('tipo_proveedores_id')->references('id')->on('tipo_proveedores');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proveedores');
    }
}
