<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMontoBsToDetallePagosEfectivo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detalle_pagos_efectivo', function (Blueprint $table) {
            $table->double('monto_bs',15,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalle_pagos_efectivo', function (Blueprint $table) {
            $table->double('monto_bs',15,2);
        });
    }
}
