<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProveedoresCotizacionMerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proveedores_cotizacion_mer', function (Blueprint $table) {
            $table->increments('id');
            $table->double('precio_cot_mer', 15, 2);
            $table->integer('ventas_id')->unsigned();
            $table->foreign('ventas_id')->references('id')->on('ventas')->onDelete('cascade');
            $table->integer('mercaderias_id')->unsigned();
            $table->foreign('mercaderias_id')->references('id')->on('mercaderias')->onDelete('cascade');
            $table->integer('proveedores_id')->unsigned();
            $table->foreign('proveedores_id')->references('id')->on('proveedores')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('proveedores_cotizacion_mer');
    }
}
