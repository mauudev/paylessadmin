<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repuestos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('marca_r');
            $table->string('modelo_r');
            $table->string('anio_r');
            $table->string('vin_r');
            $table->string('detalle_r');
            $table->double('precio_venta_r',15,2)->nullable();
            $table->string('codigo_repuesto')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('repuestos');
    }
}
