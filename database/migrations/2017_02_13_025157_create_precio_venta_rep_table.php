<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrecioVentaRepTable extends Migration
{   
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('precio_venta_rep', function (Blueprint $table) {
            $table->increments('id');
            $table->double('transporte',15,2);
            $table->double('adicional',15,2);
            $table->double('precio_total',15,2);
            $table->integer('cotizacion_repuestos_id')->unsigned();
            $table->foreign('cotizacion_repuestos_id')->references('id')->on('cotizacion_repuestos')->onDelete('cascade');
            $table->integer('proveedores_cotizacion_rep_id')->unsigned();
            $table->foreign('proveedores_cotizacion_rep_id')->references('id')->on('proveedores_cotizacion_rep')->onDelete('cascade');
            $table->integer('proveedores_id')->unsigned();
            $table->foreign('proveedores_id')->references('id')->on('proveedores')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('precio_venta_rep');
    }
}
