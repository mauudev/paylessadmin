<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrecioTransporteMercaderiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('precio_transporte_mercaderias', function (Blueprint $table) {
            $table->increments('id');
            $table->double('transporte',15,2);
            $table->integer('mercaderias_id')->unsigned();
            $table->foreign('mercaderias_id')->references('id')->on('mercaderias');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('precio_transporte_mercaderias');
    }
}
