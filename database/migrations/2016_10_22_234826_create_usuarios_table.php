c<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_u');
            $table->string('apellidos_u');
            $table->string('telefono_u');
            $table->string('celular_u');
            $table->string('email_u');
            $table->string('user_name')->unique();
            $table->string('password');
            $table->integer('tipo_usuarios_id')->unsigned();
            $table->foreign('tipo_usuarios_id')->references('id')->on('tipo_usuarios');
            $table->enum('type',['member','admin']);
            $table->rememberToken()->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usuarios');
    }
}
