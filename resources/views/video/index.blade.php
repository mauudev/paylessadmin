<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Payless Import - Invoice</title>
        {!! Html::style('css/invoice-style.css') !!}
        <style type="text/css">
        #watermark {
        opacity: .9;
        position: absolute;
        top: 90%;
        }
        table hr{
        position: absolute;
        left: 0;
        bottom: 0;
        width: 100%;
        margin: 0;
        top:89%;
        }
        table td{
        padding-top: 0;
        }
        #totalMark1
        {
         padding-left: 93%;
         padding-right: 0%;
         padding-bottom: 0%;
         padding-top: 0%;
         color:black;
        }
        #h5fixed{
                display: block;
                font-size: 0.83em;
                
                font-weight: bold;
        }
        p {
            display: block;
            -webkit-margin-before: 0px;
            -webkit-margin-after: 0px;
            -webkit-margin-start: 0px;
            -webkit-margin-end: 0px;
        }
        </style>
    </head>
    <body>
        
        <div class="invoice-box">
            <table class="qrtable" cellpadding="0" cellspacing="0" border="0" style="empty-cells: hide;border-collapse: separate; ">
                <tr class="information">
                    <td align="left" class="heading"><br>
                        <img src="{{ asset('/images/payless.jpg') }}" style="width:120px; max-width:300px;">
                    </td>
                    <td align="right">
                        <h3><b>COTIZACION # PG123</b><br>
                        FECHA: @if(isset($venta)){{date('d-m-Y')  }}@endif</h3>
                    </td>
                    <td class="qrcode">
                        <img src="{{ asset('/qrcodes/qrcode.png') }}" style="width:140px; max-width:300px;">
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" style="empty-cells: hide;border-collapse: separate; " >
                <tr class="heading header-text">
                    <td>CLIENTE</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>ASESOR DE VENTA</td>
                </tr>
                @if(isset($cliente) && isset($usuario))
                <tr class="datos-cliente-usuario">
                    <td>
                        <b>NOMBRE: </b><br>
                        <b>DIRECCION: </b><br>
                        <b>TELEFONO: </b><br>
                        <b>CELULAR: </b><br>
                        <b>EMAIL: </b>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <b>NOMBRE: </b><br>
                        <b>TELEFONO: </b><br>
                        <b>CELULAR: </b><br>
                        <b>EMAIL: </b>
                    </td>
                </tr>
                @endif
            </table>
            <table class="elementos-invoice" style="width:100%; border-collapse: collapse;border-spacing: 0;margin: 0;padding: 0;" border="1">
                <tr class="heading header-text">
                    <td style="border:none;" class="col-details-headers-text"><small>ITEM</small></td>
                    <td style="border:none;" class="col-details-headers-text"><small>DESCRIPCION</small></td>
                    <td style="border:none;" class="col-details-headers-text"><small>CANTIDAD</small></td>
                    <td style="border:none;" class="col-details-headers-text"><small>PRECIO UNITARIO</small></td>
                    <td style="border:none;" class="col-details-headers-text"><small>PRECIO TOTAL</small></td>
                </tr>
                
            </table><p id="brFixed">&nbsp;</p>
            
            <table style="width:100%; border-collapse: collapse;border-spacing: 0;margin: 0;" border="1" class="observaciones">
                <tr>
                    <td style="border:none;"><b>OBSERVACIONES</b><small id="h5fixed">El precio cotizado es antes de IVA</small><b id="totalMark1">TOTAL: 9999$</b></td>
                </tr>

            </table>
            <h5><b><u>TERMINOS Y CONDICIONES DE LA OFERTA</u></b><br>
            <b><u>M&Eacute;TODO DE PAGO:</u></b><small> 50% del total por adelantado, 50% del total contra-entrega. Los pagos deben realizarse en la cuenta corriente de la empresa.</small><br>
            <b><u>PLAZO DE ENTREGA:</u></b><small> 15-20 d&iacute;as a partir del adelanto.</small><br>
            <b><u>DATOS BANCARIOS:</u></b><br>
            NOMBRE: Payless Import<br> 
            BANCO UNIÓN<br>
            CUENTA CORRIENTE BOLIVIANOS No. 1-23026368<br>
            CUENTA CORRIENTE DOL&Aacute;RES No. 2-23026384<br>
            </h5>
            <table id="watermark" class="contact" cellpadding="0" cellspacing="0" border="0" style="empty-cells: hide;border-collapse: separate; " ><hr>
                <tr>
                    <td class="contact-td-padding"><b>COCHABAMBA - BOLIVIA</b></td>
                    <td class="contact-miami contact-td-padding"><b>MIAMI - USA</b></td>
                </tr>
                <tr>
                    <td class="contact-td-padding" style="border:none;">
                        Av. Blanco galindo casi Av. per&uacute;<br>
                        EDIFICIO SUPERMALL<br>
                        MEZANINE, OF 33<br>
                        Telf:(591)4-4067755<br>
                        Cel:(591)76400111<br>
                    </td>
                    <td class="contact-miami contact-td-padding" style="border:none;">
                        8285 NW 65th Street<br>
                        Suite #2<br>
                        MIAMI, FL 33166<br>
                        Telf:(305)848-0095<br>
                        Cel:(786)856-3907<br>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>