<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Payless Import - Invoice</title>
        {!! Html::style('css/invoice-style.css') !!}
    </head>
    <body>
        <div class="invoice-box">
            <table cellpadding="0" cellspacing="0" border="0" style="empty-cells: hide;border-collapse: separate; ">
                <tr class="information">
                    <td class="heading"><br><br><br>
                        <img src="{{ asset('/images/payless.jpg') }}" style="width:150px; max-width:300px;">
                    </td>
                    
                    <td>
                        <h3><b>COTIZACION # @if(isset($venta)){{ $venta->id }}@endif</b><br>
                       FECHA: @if(isset($venta)){{date_format($venta->created_at, 'Y-m-d')  }}@endif<br></h3>
                       <img src="{{ asset('/qrcodes/qrcode.png') }}" style="width:150px; max-width:300px;">
                    </td>
                </tr>
            </table>
            <table cellpadding="0" cellspacing="0" border="0" style="empty-cells: hide;border-collapse: separate; " >
                <tr class="heading header-text">
                    <td>CLIENTE</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>ASESOR DE VENTA</td>
                </tr>
                <tr class="datos-cliente-usuario">
                    <td>
                        <b>NOMBRE: </b>@if(isset($cliente)){{ $cliente->nombre_c.' '.$cliente->apellidos_c }}@endif<br>
                        <b>DIRECCION: </b>@if(isset($cliente)){{ $cliente->direccion_c }}@endif<br>
                        <b>TELEFONO: </b> @if(isset($cliente)){{ $cliente->celular_c }}@endif<br>            
                        <b>EMAIL: </b> @if(isset($cliente)){{ $cliente->email_c }}@endif              
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <b>NOMBRE: </b> @if(isset($usuario)){{ $usuario->nombre_u.' '.$usuario->apellidos_u }}@endif<br>
                        <b>TELEFONO: </b> @if(isset($usuario)){{ $usuario->telefono_c.' '.$usuario->celular_u }}@endif<br>
                        <b>EMAIL: </b> @if(isset($usuario)){{ $usuario->email_u }}@endif
                    </td>
                </tr>
            </table><br>
            <table class="elementos-invoice" style="width:100%; border-collapse: collapse;border-spacing: 0;margin: 0;padding: 0;" border="1">
                <tr class="heading header-text">
                    <td style="border:none;" class="col-details-headers-text">ITEM</td>
                    <td style="border:none;" class="col-details-headers-text">DESCRIPCION</td>
                    <td style="border:none;" class="col-details-headers-text">CANTIDAD</td>
                    <td style="border:none;" class="col-details-headers-text">PRECIO UNITARIO</td>
                    <td style="border:none;" class="col-details-headers-text">PRECIO TOTAL</td>
                </tr>
                @if(isset($repuestos))
                    @for($i = 0; $i < count($repuestos); $i++)
                        <tr class="item">
                            <td class="td-details-item">{{ $i+1 }}</td>
                            <td class="td-details-desc">{!! $repuestos[$i]->marca_r." ".$repuestos[$i]->modelo_r." ".$repuestos[$i]->anio_r." ".$repuestos[$i]->detalle_r !!} </td>
                            <td class="td-details-prices">{!! $repuestos[$i]->cantidad !!}</td>
                            <td class="td-details-prices">{{ round($repuestos[$i]->precio_total / $repuestos[$i]->cantidad,2) }} $</td>
                            <td class="td-details-prices">{{ $repuestos[$i]->precio_total}} $</td>
                        </tr>
                    @endfor
                @endif
                @if(isset($mercaderias))
                    @for($i = 0; $i < count($mercaderias); $i++)
                        <tr class="item">
                            <td class="td-details-item">{{ $i+1 }}</td>
                            <td class="td-details-desc">{!! $mercaderias[$i]->nombre_m !!}</td>
                            <td class="td-details-prices">{!! $mercaderias[$i]->cantidad !!}</td>
                            <td class="td-details-prices">{{ round($mercaderias[$i]->precio_total / $mercaderias[$i]->cantidad, 2) }} $</td>
                            <td class="td-details-prices">{{ $mercaderias[$i]->precio_total}} $</td>
                        </tr>
                    @endfor
                @endif
            </table><br>
            
            <table style="width:100%; border-collapse: collapse;border-spacing: 0;margin: 0;" border="1" class="observaciones">
                <tr>
                    <td style="border:none;"><b>OBSERVACIONES</b></td>
                    <td class="adicionales header-text" style="border:none;">SUB-TOTAL</td>
                    <td class="adicionales-values" style="border:none;">@if(isset($venta)){{ $venta->pago_total }}@endif $</td>
                </tr>
                <tr>
                    <td style="border:none;" rowspan="3"></td>
                    <td class="adicionales header-text" style="border:none;">ENVIO</td>
                    <td class="adicionales-values" style="border:none;">valor-envio</td>
                </tr>
                <tr>
                    <td class="adicionales header-text" style="border:none;">IVA</td>
                    <td class="adicionales-values" style="border:none;">valor-iva</td>
                </tr>
                <tr>
                    <td class="adicionales header-text" style="border:none;">TOTAL</td>
                    <td class="adicionales-values-total" style="border:none;">valor-total</td>
                </tr>
            </table>
            <h5>
            <b>TERMINOS Y CONDICIONES DE LA OFERTA</b><br>
            <b>TIEMPO DE VALIDEZ:</b> La oferta es v&aacute;lida por 10 d&iacute;as.<br>
            <b>TIEMPO DE ENTREGA:</b> 15-20 d&iacute;s a partir de la confirmaci&oacute;n del pedido.<br>
            <b>FORMA DE PAGO:</b> 50% del total por adelantado, 50% del total contra-entrega.<br>
            </h5><br><br><br><br><br><br><hr>
            <table class="contact" cellpadding="0" cellspacing="0" border="0" style="empty-cells: hide;border-collapse: separate; " >
                <tr>
                    <td class="contact-td-padding"><b>COCHABAMBA - BOLIVIA</b></td>
                    <td class="contact-miami contact-td-padding"><b>MIAMI - USA</b></td>
                </tr>
                <tr>
                    <td class="contact-td-padding" style="border:none;">
                        Av. Blanco galindo casi Av. per&uacute;<br>
                        EDIFICIO SUPERMALL<br>
                        MEZANINE, OF 33<br>
                        Telf:(591)4-4067755<br>
                        Cel:(591)76400111<br>
                    </td>
                    <td class="contact-miami contact-td-padding" style="border:none;">
                        8285 NW 65th Street<br>
                        Suite #2<br>
                        MIAMI, FL 33166<br>
                        Telf:(305)848-0095<br>
                        Cel:(786)856-3907<br>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>