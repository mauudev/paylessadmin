@if(Session::has('message-error'))
    <div class="alert alert-danger" role="alert" id="msg_confirmation1">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong><i class="fa fa-ban"></i> Error!</strong> {!! session('message-error') !!}
    </div>
@endif
