
  @if(Session::has('store-success'))
    <div class="alert alert-success" role="alert" id="msg_confirmation1">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong><i class="fa fa-check"></i> Exito!</strong> {!! session('store-success') !!}
    </div>
  @elseif(Session::has('update-success'))
    <div class="alert alert-success" role="alert" id="msg_confirmation1">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong><i class="fa fa-check"></i> Exito!</strong> {!! session('update-success') !!}
    </div>
  @elseif(Session::has('edit-warning'))
    <div class="alert alert-warning" role="alert" id="msg_confirmation1">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong><i class="fa fa-warning"></i> NOTA:</strong> {!! session('edit-warning') !!}
    </div>
  @elseif(Session::has('delete-success'))
    <div class="alert alert-success" role="alert" id="msg_confirmation1">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong><i class="fa fa-check"></i> Exito!</strong> {!! session('delete-success') !!}
    </div>
  @elseif(Session::has('toast-actualizado'))
  <script type="text/javascript">
    toastr.options = {
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-center",
    "preventDuplicates": true,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
    };
    toastr.success('<b>Exito!</b> {!! session("toast-actualizado") !!} ');
  </script>
  @endif
  <script type="text/javascript">
    $(document).ready(function(){
        $("#msg_confirmation1").fadeOut(6000)
    });
  </script>