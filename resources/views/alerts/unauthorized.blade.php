@if(Session::has('unauthorized'))
<br/>
<div class="alert alert-danger" role="alert">
    <button type="button" class="close" ></button>
    <strong><i class="fa fa-ban"></i> Error!</strong> {!! session('unauthorized') !!}
</div>
@elseif(Session::has('unauthorized-login'))
 <div class="alert alert-danger" role="alert">
    <button type="button" class="close" ></button>
    <strong><i class="fa fa-ban"></i> Error!</strong> {!! session('unauthorized-login') !!}
</div>
 @endif
