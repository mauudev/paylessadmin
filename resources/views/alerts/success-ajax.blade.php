<div id="update-success-ajax" class="alert alert-success" role="alert" style="display:none">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<strong>Success!</strong> Data updated correctly !
</div>
<div id="delete-success-ajax" class="alert alert-success" role="alert" style="display:none">
<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
<strong>Success!</strong> Data deleted correctly !
</div>