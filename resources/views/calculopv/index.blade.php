@extends('layouts.principal')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-filled">
			<div class="panel-heading">
				<div class="panel-tools">
					<div class="form-group">
					</div>
				</div>
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-calculator"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">C&aacute;lculo del precio de venta</h3>
						<small>
						Gesti&oacute;n de cotizaciones
						</small>
					</div>
				</div>
			</div>
			{!! Form::open(['route'=>'calcular-precio-venta.store','method'=>'POST','class'=>'form-group',"onsubmit"=>"return checkSubmit();",'id'=>'myForm']) !!}
			{!! Form::hidden('ventas_id',$venta->id) !!}
			{!! Form::hidden('repuestos_id',$repuesto->id) !!}
			{!! Form::hidden('clientes_id',$cliente->id) !!}
			<div class="panel-body">
				<div class="row">
					<div class="col-md-4">
						<div class="panel panel-filled panel-c-success">
							<div class="panel-heading">
								Detalles del repuesto
							</div>
							<div class="panel-body">
								@if(isset($repuesto))
								<div class="col-md-3">
									<strong class="c-white">Marca: </strong>
								</div>
								<div class="col-md-9">
									{{ $repuesto->marca_r }}
								</div><br/>
								<div class="col-md-3">
									<strong class="c-white">Modelo: </strong>
								</div>
								<div class="col-md-9">
									{{ $repuesto->modelo_r }}
								</div><br/>
								<div class="col-md-3">
									<strong class="c-white">Año: </strong>
								</div>
								<div class="col-md-9">
									{{ $repuesto->anio_r }}
								</div><br/>
								<div class="col-md-3">
									<strong class="c-white">VIN: </strong>
								</div>
								<div class="col-md-9">
									{{ $repuesto->vin_r }}
								</div><br/>
								<div class="col-md-3">
									<strong class="c-white">Detalle: </strong>
								</div>
								<div class="col-md-9">
									{{ $repuesto->detalle_r }}
								</div><br/>
								<div class="col-md-3">
									<strong class="c-white">C&oacute;digo: </strong>
								</div>
								<div class="col-md-9">
									{{ $repuesto->codigo_repuesto }}
								</div><br/>
								<div class="col-md-3">
									<strong class="c-white">Cantidad: </strong>
								</div>
								<div class="col-md-4">
									{{ $cotizacion_repuestos_id->cantidad }}
									{!! Form::hidden('cantidad_rep',$cotizacion_repuestos_id->cantidad) !!}
								</div><br/>
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="panel panel-filled panel-c-primary">
							<div class="panel-heading">
								Proveedores asignados al repuesto
							</div>
							<div class="panel-body">
								<div class="col-md-12">
									<div class="col-md-7">
										<strong class="c-white">Proveedor </strong>
									</div>
									<div class="col-md-2">
										<strong class="c-white">Precio </strong>
									</div>
								</div>
								
								@if(isset($proveedores_cotizacion_rep))
								@for($i = 0; $i < count($proveedores_cotizacion_rep); $i++)
								<div class="col-md-12">
									<div class="col-md-7">
										{{ $proveedores_cotizacion_rep[$i]->nombre_compania_p }}
									</div>
									<div class="col-md-2">
										{{ $proveedores_cotizacion_rep[$i]->precio_cot_rep }}
										{{ Form::hidden('precios_cot_rep[]',$proveedores_cotizacion_rep[$i]->precio_cot_rep) }}
										{!! Form::hidden('proveedores_all[]',$proveedores_cotizacion_rep[$i]->proveedores_id) !!}
									</div>
								</div>
								@endfor
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-12"><hr></div>
					<div class="col-md-12">
						<div class="panel panel-filled panel-c-danger">
							<div class="panel-heading">
								<div class="col-sm-6 center-block text-center">
									<strong class="c-white">Transporte</strong>
								</div>
								<div class="col-sm-3 center-block text-left">
									<strong class="c-white">Adicional</strong>
								</div>
								<div class="col-sm-3 center-block text-left">
									<strong class="c-white">Tipo cambio</strong>
								</div>
							</div>
							<div class="panel-body">
								<div class="row">
									<div class="col-md-12">
										<div class="col-sm-6 transporte-container">
											<div class="col-sm-6 center-block text-center"><br>
												{!! Form::select('precio_transporte', ['lista' => 'De la lista', 'manual' => 'Ingreso manual'], null, ['placeholder' => 'Elegir opci&oacute;n..','class'=>'form-control rep-t-select']); !!}
											</div>
											<div class="col-sm-6"><br>
												{!! Form::text("transporte_rep",null,["class"=>"form-control","placeholder"=>"Ingrese el costo..","required"]) !!}
											</div>	
										</div>
											<div class="col-sm-3 center-block text-center"><br>
												{{ Form::text("adicional_rep",0,["class"=>"form-control","placeholder"=>"Ingrese el costo.."]) }}
											</div>
											<div class="col-sm-3 center-block text-center"><br>
												{{ Form::text("tipo_cambio",6.96,["class"=>"form-control","placeholder"=>"Ingrese el tipo de cambio.."]) }}
											</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					@if(isset($proveedores_cotizacion_rep))
					<script type="text/javascript">
					function roundNumber(num, scale) {
					var number = Math.round(num * Math.pow(10, scale)) / Math.pow(10, scale);
					if(num - number > 0) {
					return (number + Math.floor(2 * Math.round((num - number) * Math.pow(10, (scale + 1))) / 10) / Math.pow(10, scale));
					} else {
					return number;
					}
					}
					$(document).on('change','.rep-t-select',function(){
						var precios_transporte = {!! json_encode($precios_transporte) !!}
						if(precios_transporte.length == 0){
							toastr.options = {
								"closeButton": true,
								"debug": false,
								"newestOnTop": false,
								"progressBar": false,
								"positionClass": "toast-top-center",
								"preventDuplicates": true,
								"onclick": null,
								"showDuration": "300",
								"hideDuration": "1000",
								"timeOut": "5000",
								"extendedTimeOut": "1000",
								"showEasing": "swing",
								"hideEasing": "linear",
								"showMethod": "fadeIn",
								"hideMethod": "fadeOut"
								};
								toastr.error('<b>No existen registros !</b> no se han guardado precios de transporte para usar esta opci&oacute;n. ');
						}else{
							var div_target = $(this).parents('.transporte-container')
							if(this.value == 'manual'){
								div_target.empty()
								template = '<div class="col-sm-4 center-block text-center"><br>{!! Form::select("precio_transporte", ["lista" => "De la lista", "manual" => "Ingreso manual"], null, ["placeholder" => "Elegir opci&oacute;n..","class"=>"form-control rep-t-select"]); !!}</div><div class="col-sm-8"><br>{!! Form::text("transporte_rep",null,["class"=>"form-control","placeholder"=>"Ingrese el costo..","required"]) !!}</div>'
								div_target.append(template)
							}
							if(this.value == 'lista'){
								div_target.empty()
								var template = '<div class="col-sm-4 center-block text-center"><br>{!! Form::select("precio_transporte", ["lista" => "De la lista", "manual" => "Ingreso manual"], null, ["placeholder" => "Elegir opci&oacute;n..","class"=>"form-control rep-t-select"]); !!}</div><div class="col-sm-8"><br><select class="form-control"id="select1" name="transporte_rep" size="10" required>'
								var options = ''
								for(i = 0; i < precios_transporte.length; i ++){
									options += '<option value="'+precios_transporte[i].precio+'"><p class="text-accent">'+precios_transporte[i].descripcion+"</p> - "+precios_transporte[i].precio+' $</option>'
								}
					options += '</select></div>'
					template = template + options
					div_target.append(template)
				}
				}
				});
				
				
				</script>
				@endif
				<!-- DIVS OCULTOS -->
				<div class="row" id="calculosPrecio1">
					
				</div>
				<!-- DIVS OCULTOS -->
			</div>
		</div>
	</div>
</div>
</div>
@include('alerts.validation')
@include('alerts.success')
<div class="row">
<div class="form-group">
	<div class="col-md-7" >
		{!!Form::submit('Guardar',['class'=>'btn btn-w-md btn-accent','id'=>'submit1'])!!} <a class="btn btn-w-md btn-default" href="{{ url('/administracion/mostrar-cotizacion-principal/show/'.$venta->id ) }}">Cancelar</a>
	</div>
</div>
{!! Form::close() !!}
</div>
@endsection