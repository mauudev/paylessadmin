@extends('layouts.principal')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <div class="panel-tools">
                    <div class="form-group">
                    </div>
                </div>
                <div class="view-header">
                    <div class="header-icon">
                        <i class="pe page-header-icon pe-7s-calculator"></i>
                    </div>
                    <div class="header-title">
                        <h3 class="page-header">C&aacute;lculo del precio de venta</h3>
                        <small>
                            Gesti&oacute;n de cotizaciones
                        </small>
                    </div>
                </div>
            </div>
            {!! Form::open(['route'=>'calcular-precio-venta.store','method'=>'POST','class'=>'form-group',"onsubmit"=>"return checkSubmit();",'id'=>'myForm']) !!}
            {!! Form::hidden('ventas_id',$ventas_id) !!}
            {!! Form::hidden('repuestos_id',$repuestos_id) !!}
            {!! Form::hidden('cotizacion_repuestos_id',$cotizacion_repuestos_id->id) !!}
            @for($i = 0; $i < count($proveedores_elegidos); $i ++)
                {!! Form::hidden('proveedores_elegidos[]', $proveedores_elegidos[$i]) !!}
            @endfor
            <!-- Content -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-filled panel-c-danger">
                            <div class="panel-heading">
                                Cliente
                            </div>
                            <div class="panel-body" >
                                <div class="row">
                                    <div class="col-md-2"><strong class="c-white">Nombre: </strong></div><div class="col-md-10"> {!! $cliente->nombre_c !!}</div><br/>
                                    <div class="col-md-2"><strong class="c-white">Apellidos: </strong></div><div class="col-md-10"> {!! $cliente->apellidos_c !!}</div><br/>
                                    <div class="col-md-2"><strong class="c-white">Direcci&oacute;n: </strong></div><div class="col-md-10"> {!! $cliente->direccion_c !!}</div><br/>
                                    <div class="col-md-2"><strong class="c-white">Tel&eacute;fono: </strong></div><div class="col-md-10"> {!! $cliente->telefono_c !!}</div><br/>
                                    <div class="col-md-2"><strong class="c-white">E-mail: </strong></div><div class="col-md-10"> {!! $cliente->email_c !!}</div><br/>
                                    <div class="col-md-2"><strong class="c-white">Pa&iacute;s: </strong></div><div class="col-md-10"> {!! $cliente->pais !!}</div><br/><br>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-filled panel-c-success">
                            <div class="panel-heading">
                                Detalles del repuesto
                            </div>
                            <div class="panel-body">
                            @if(isset($repuesto))
                                <div class="col-md-2">
                                    <strong class="c-white">Marca: </strong>
                                </div>
                                <div class="col-md-10">
                                    {{ $repuesto->marca_r }}
                                </div><br/>
                                <div class="col-md-2">
                                    <strong class="c-white">Modelo: </strong>
                                </div>
                                <div class="col-md-10">
                                    {{ $repuesto->modelo_r }}
                                </div><br/>
                                <div class="col-md-2">
                                    <strong class="c-white">Año: </strong>
                                </div>
                                <div class="col-md-10">
                                    {{ $repuesto->anio_r }}
                                </div><br/>
                                <div class="col-md-2">
                                    <strong class="c-white">VIN: </strong>
                                </div>
                                <div class="col-md-10">
                                    {{ $repuesto->vin_r }}
                                </div><br/>
                                <div class="col-md-2">
                                    <strong class="c-white">Detalle: </strong>
                                </div>
                                <div class="col-md-10">
                                    {{ $repuesto->detalle_r }}
                                </div><br/>
                                <div class="col-md-2">
                                    <strong class="c-white">C&oacute;digo: </strong>
                                </div>
                                <div class="col-md-10">
                                    {{ $repuesto->codigo_repuesto }}
                                </div><br/>
                                <div class="col-md-2">
                                    <strong class="c-white">Cantidad: </strong>
                                </div>
                                <div class="col-md-10">
                                    {{ $cotizacion_repuestos_id->cantidad }}
                                </div><br/>
                            @endif
                            </div>
                        </div>
                    </div>
                </div><hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-c-info">
                            <div class="panel-heading">
                                <h4>Descripci&oacute;n del c&aacute;lculo</h4>
                            </div>
                            <div class="panel-body" >
                                <div class="table-responsive">
                                    <table class="table" id="table1">
                                        <thead>
                                            <tr align="center">
                                                <th align="center">Proveedor</th>
                                                <th align="center">Precio compra</th>
                                                <th align="center">Cantidad</th>
                                                @if(\Auth::user()->type == "admin")
                                                <th align="center">Warehouse</th>
                                                <th align="center">Aduana</th>
                                                <th align="center">Ganancia</th>
                                                @endif
                                                <th align="center">Transporte</th>
                                                <th align="center">Adicionales</th>
                                                <th align="center">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @for($i = 0; $i < count($precios_cot_rep); $i++)
                                            <?php 
                                            $total = (15/100)*$precios_cot_rep[$i] + (30/100)*$precios_cot_rep[$i] + (40/100)*$precios_cot_rep[$i] + $transportes + $adicionales + $precios_cot_rep[$i];
                                                $total *= $cotizacion_repuestos_id->cantidad;
                                                $datos_cotizacion[$i] = [
                                                    'proveedor_id'=>$proveedores_elegidos[$i],
                                                    'precios_compra'=>$precios_cot_rep[$i],
                                                    'transporte'=>$transportes,
                                                    'adicionales'=>$adicionales,
                                                    'total'=>$total
                                                ];
                                            ?>
                                            <tr class="rowData1">
                                                <td align="center">{!! $proveedores_nombre_compania[$i] !!}</td>
                                                <td align="center"><p class="text-accent">{!! $precios_compra[$i] !!}</p></td>
                                                <td align="center">{{ $cotizacion_repuestos_id->cantidad }}
                                                {{ Form::hidden('cantidad_rep',$cotizacion_repuestos_id->cantidad) }}</td>
                                                @if(\Auth::user()->type == "admin")
                                                <td align="center">{!! (15/100)*$precios_compra[$i] !!}</td>
                                                <td align="center">{!! (30/100)*$precios_compra[$i] !!}</td>
                                                <td align="center">{!! (40/100)*$precios_compra[$i] !!}</td>
                                                @endif
                                                <td align="center">{!! $transportes !!}{!! Form::hidden('transporte', $transportes ) !!}</td>
                                                <td align="center">{!! $adicionales !!}{!! Form::hidden('adicionales', $adicionales ) !!}</td>
                                                <td align="center"><p class="text-success">{!! $total !!}</p>
                                                {!! Form::hidden('totales[]', $total ) !!}
                                                {!! Form::hidden('precios_compra[]',$precios_compra[$i]) !!}
                                                </td>
                                            </tr>
                                        @endfor
                                        {{ Form::hidden("datos_cotizacion",serialize($datos_cotizacion)) }}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group">
        <div class="col-md-7" >
            {!!Form::submit('Guardar',['class'=>'btn btn-w-md btn-accent','id'=>'submit1'])!!} <a class="btn btn-w-md btn-default" href="{{ url('/calcular-precio-venta/repuesto/'.$repuestos_id.'/venta/'.$ventas_id ) }}">Atr&aacute;s</a>
        </div>
    </div>
    {!! Form::close() !!}
</div>
<script type="text/javascript">
    //ELIMINAR FILA ESPECIFICA DE LA TABLA
    $(document).ready(function() {
        $('.row1').click(DeleteRow);//BOTON
      });

    function DeleteRow(){
      var count = $('#table1 tr').length;
      if(count <= 2){
            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-center",
                                "preventDuplicates": true,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                              };
                            toastr.error('<b>No se puede eliminar el proveedor!</b> Debe existir un solo proveedor para asignar a esta cotizaci&oacute;n ');
            
        }else $(this).parents('tr').first().remove();//FILA
    }
</script>
@endsection