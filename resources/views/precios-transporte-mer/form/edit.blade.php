<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('mercaderia','Mercaderia: ',['class'=>'control-label']) !!}
			{!! Form::text('mercaderia',$precio_transporte_mer->nombre_m,['class'=>'form-control',"readonly"]) !!}
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('transporte','Precio de transporte: ',['class'=>'control-label']) !!}
			{!! Form::text('transporte',$precio_transporte_mer->transporte,['class'=>'form-control','placeholder'=>'Ingrese el precio de transporte del mercaderia',"required"]) !!}<br/>
		</div>
	</div>
</div>