<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('mercaderia','Mercaderia: ',['class'=>'control-label']) !!}
			<select class="js-example-theme-multiple form-control" name="mercaderias_id[]" required class="js-example-basic-multiple" multiple="multiple">
				@if(count($mercaderias) > 0)
					@foreach($mercaderias as $mercaderia)
						<option value="{{ $mercaderia->id }}">{{ $mercaderia->nombre_m }}
						</option>
					@endforeach
				@endif
			</select><br>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('transporte','Precio de transporte: ',['class'=>'control-label']) !!}
			{!! Form::text('transporte',null,['class'=>'form-control','placeholder'=>'Ingrese el precio de transporte del mercaderia',"required"]) !!}<br/>
		</div>
	</div>
</div>