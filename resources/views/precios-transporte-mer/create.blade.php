@extends('layouts.principal')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-filled">
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-car"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Registro de precio de transporte</h3>
						<small>
						Gesti&oacute;n de precios de transporte para mercaderias
						</small>
					</div>
				</div>
				<div class="panel-body">
					{!! Form::open(['route'=>'precios-transporte-mercaderias.store','method'=>'POST','class'=>'form-group',"onsubmit"=>"return checkSubmit();"]) !!}
					@include('precios-transporte-mer.form.form')
					@include('alerts.validation')
				</div>
			</div>
		</div>
		<div class="col-md-7" >
			{!!Form::submit('Guardar',['class'=>'btn btn-w-md btn-accent'])!!} 
			<a class="btn btn-w-md btn-default" href="{!! URL::to('precios-transporte-mercaderias') !!}">Cancelar</a>
		</div>
	</div>
</div>
@endsection
