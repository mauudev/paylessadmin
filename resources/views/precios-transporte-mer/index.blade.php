@extends('layouts.principal')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-filled">
			<div class="panel-heading">
				<div class="panel-tools">
					<div class="col-md-12">
						{!! Form::open(['route'=>'precios-transporte-mercaderias.index','method'=>'GET','class'=>'navbar-form navbar-left pull-right','role'=>'search']) !!}
						<div class="form-group">
							{!! Form::text('search',null,['class'=>'form-control','placeholder'=>'Buscar..']) !!}
						</div>
						<button type="submit" class="btn btn-accent">Buscar</button>
						<a href="{!! URL::to('precios-transporte-mercaderias/create') !!}" class="btn btn-w-md btn-accent"><i class="fa fa-plus"></i> Nuevo registro</a>
						<a href="{!! URL::to('precios-transporte-mercaderias') !!}" class="btn btn-info"><i class="fa fa-refresh"></i> Actualizar</a>
						{!! Form::close() !!}
					</div>
				</div>
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-car"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Precios registrados</h3>
						<small>
						Gesti&oacute;n de precios de transporte para mercaderias
						</small>
					</div>
				</div>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th class="left-align">Mercader&iacute;a</th>
								<th class="left-align">Precio de transporte</th>
								<th class="left-align">Acci&oacute;n</th>
							</tr>
						</thead>
						<tbody>
							@foreach($precios as $precio)
							<tr>
								<td>{{ $precio->nombre_m }}</td>
								<td>{{ $precio->transporte }} $</td>
								<td>
									<a href="{!! route('precios-transporte-mercaderias.edit',$parameters = $precio->id) !!}"><i class="fa fa-pencil"></i></a>&nbsp;
									<a href="{!! route('precios-transporte-mercaderias.destroy',$parameters = $precio->id) !!}" onclick='return confirm("¿Estas seguro de dar de baja este elemento?")'><i class="fa fa-times"></i></a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					{{ $precios->render() }}
					@include('alerts.success')
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection