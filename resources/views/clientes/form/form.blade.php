<div class="container-fluid" id="clienteForm1">
	<div class="row">
		<div class="col-lg-5">
		@if(isset($accion))
			@if($accion == "editar")
				<div class="form-group">
						{!! Form::label('nombre_c','Nombres: ',['class'=>'control-label']) !!}
						{!! Form::text('nombre_c',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del cliente','required','min'=>5,'readonly']) !!}
						{!! Form::hidden('usuarios_id',\Auth::user()->id) !!}<br/>
					</div>
				</div>
				<div class="col-lg-5">
					<div class="form-group">
						{!! Form::label('apellidos_c','Apellidos: ',['class'=>'control-label']) !!}
						{!! Form::text('apellidos_c',null,['class'=>'form-control','placeholder'=>'Ingrese el apellido del cliente','required','min'=>5,'readonly']) !!}<br/>
					</div>
				</div>
			@else
				<div class="form-group">
						{!! Form::label('nombre_c','Nombres: ',['class'=>'control-label']) !!}
						{!! Form::text('nombre_c',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del cliente','required','min'=>5]) !!}
						{!! Form::hidden('usuarios_id',\Auth::user()->id) !!}<br/>
					</div>
				</div>
				<div class="col-lg-5">
					<div class="form-group">
						{!! Form::label('apellidos_c','Apellidos: ',['class'=>'control-label']) !!}
						{!! Form::text('apellidos_c',null,['class'=>'form-control','placeholder'=>'Ingrese el apellido del cliente','required','min'=>5]) !!}<br/>
					</div>
				</div>
			@endif
		@else
		<div class="form-group">
				{!! Form::label('nombre_c','Nombres: ',['class'=>'control-label']) !!}
				{!! Form::text('nombre_c',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del cliente','required','min'=>5]) !!}
				{!! Form::hidden('usuarios_id',\Auth::user()->id) !!}<br/>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="form-group">
				{!! Form::label('apellidos_c','Apellidos: ',['class'=>'control-label']) !!}
				{!! Form::text('apellidos_c',null,['class'=>'form-control','placeholder'=>'Ingrese el apellido del cliente','required','min'=>5]) !!}<br/>
			</div>
		</div>
		@endif
	</div>
	<div class="row">
		<div class="col-lg-5">
			<div class="form-group">
				{!! Form::label('direccion_c','Direcci&oacute;n: ',['class'=>'control-label']) !!}
				{!! Form::text('direccion_c',null,['class'=>'form-control','placeholder'=>'Ingrese la direccion del cliente','min'=>5]) !!}<br/>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="form-group" {{-- id="alertTelf1" ,'id'=>'telefono_c1'--}}>
				{!! Form::label('telefono_c','Tel&eacute;fono: ',['class'=>'control-label']) !!}
				{!! Form::text('telefono_c',null,['class'=>'form-control','placeholder'=>'Ingrese el telefono del cliente','min'=>5,'id'=>"telefono_c_1"]) !!}<br/>
			</div>
		</div>
		<div class="col-lg-2">
			&nbsp;
		</div>
		<div class="col-lg-2" id="checkTelf1"></div>
	</div>
	<div class="row">
		<div class="col-lg-5">
			<div class="form-group">
				{!! Form::label('email_c','E-mail: ',['class'=>'control-label']) !!}
				{!! Form::text('email_c',null,['class'=>'form-control','placeholder'=>'Ingrese el e-mail del cliente','min'=>5]) !!}<br/>
			</div>
		</div>
		@if($accion == "editar")
		<div class="col-lg-5">
			<div class="form-group" id="alertCel1_edit">
				{!! Form::label('celular_c','Celular: ',['class'=>'control-label']) !!}
				{!! Form::text('celular_c',null,['class'=>'form-control','placeholder'=>'Ingrese el celular del cliente','id'=>'celular_c1_edit','required','min'=>5]) !!}<br/>
			</div>
		</div>
		<div class="col-lg-2">
			&nbsp;
		</div>
		<div class="col-lg-2" id="checkCel1"></div>
		@else
		<div class="col-lg-5">
			<div class="form-group" id="alertCel1">
				{!! Form::label('celular_c','Celular: ',['class'=>'control-label']) !!}
				{!! Form::text('celular_c',null,['class'=>'form-control','placeholder'=>'Ingrese el celular del cliente','id'=>'celular_c1','required','min'=>5]) !!}<br/>
			</div>
		</div>
		<div class="col-lg-2">
			&nbsp;
		</div>
		<div class="col-lg-2" id="checkCel1"></div>
		@endif 
		
	</div>
	<div class="row">
		<div class="col-lg-5">
			<div class="form-group">
				{!! Form::label('origen_contacto','Origen contacto: ',['class'=>'control-label']) !!}<br/>
				<select name="origen_contacto" class="form-control">
				  @foreach($origen_contacto as $key => $value)
					  <option value="{{ $key }}">{{ $value }}</option>
				  @endforeach
				</select>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="form-group">
				{!! Form::label('pais','Localidad: ',['class'=>'control-label']) !!}
				{!!Form::select('pais', $paises,null,['class'=>'form-control','placeholder'=>'Seleccione una localidad..','required'])!!}<br>
			</div>
		</div>
	</div>
</div>