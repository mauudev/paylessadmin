@extends('layouts.principal')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-filled">
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-car"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Actualizar datos del precio de transporte</h3>
						<small>
						Gesti&oacute;n de repuestos
						</small>
					</div>
				</div>
				<div class="panel-body">
					{!!Form::model($precio_transporte_rep,['route'=>['precios-transporte-repuestos.update',$precio_transporte_rep->id],'method'=>'PUT'])!!}
					@include('precios-transporte-rep.form.edit')
					@include('alerts.validation')
				</div>
			</div>
		</div>
		<div class="col-md-7" >
			{!!Form::submit('Actualizar',['class'=>'btn btn-w-md btn-accent'])!!} <a class="btn btn-w-md btn-default" href="{!! URL::to('precios-transporte-repuestos.update') !!}">Cancelar</a>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection