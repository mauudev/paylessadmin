<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('repuesto','Repuesto: ',['class'=>'control-label']) !!}
			{!! Form::text('repuesto',$precio_transporte_rep->marca_r.' '.$precio_transporte_rep->modelo_r.' '.$precio_transporte_rep->anio_r.' '.$precio_transporte_rep->detalle_r,['class'=>'form-control',"readonly"]) !!}
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('transporte','Precio de transporte: ',['class'=>'control-label']) !!}
			{!! Form::text('transporte',$precio_transporte_rep->transporte,['class'=>'form-control','placeholder'=>'Ingrese el precio de transporte del repuesto',"required"]) !!}<br/>
		</div>
	</div>
</div>