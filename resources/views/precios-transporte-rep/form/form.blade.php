<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('repuesto','Repuesto: ',['class'=>'control-label']) !!}
			<select class="js-example-theme-multiple form-control" name="repuestos_id[]" required class="js-example-basic-multiple" multiple="multiple">
				@if(count($repuestos) > 0)
					@foreach($repuestos as $repuesto)
						<option value="{{ $repuesto->id }}">{{ $repuesto->marca_r.' '.$repuesto->modelo_r.' '.$repuesto->anio_r.' '.$repuesto->detalle_r }}
						</option>
					@endforeach
				@endif
			</select><br>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('transporte','Precio de transporte: ',['class'=>'control-label']) !!}
			{!! Form::text('transporte',null,['class'=>'form-control','placeholder'=>'Ingrese el precio de transporte del repuesto',"required"]) !!}<br/>
		</div>
	</div>
</div>