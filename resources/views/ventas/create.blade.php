@extends('layouts.principal')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-filled">
			<div class="view-header">
				<div class="header-icon">
					<i class="pe page-header-icon pe-7s-note2"></i>
				</div>
				<div class="header-title">
					<h3 class="page-header">Pago parcial de la cotizaci&oacute;n</h3>
					<small>
					Cotizaci&oacute;n de repuestos/mercader&iacute;as
					</small>
				</div>
			</div>
			<div class="panel-body">
                {!!Form::open(['route'=>'ventas.store','method'=>'POST','class'=>'form-group',"onsubmit"=>"return checkSubmit();"])!!}
                {!! Form::hidden("ventas_id",$venta->id) !!}
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-md-5">
                        <div class="panel panel-filled">
                            <div class="panel-heading">
                                Total y saldos
                            </div>
                            <div class="panel-body">
                                <div class="bs-example">
                                    <strong class="c-white">Total USD:</strong> <b class="text-info">{{ $venta->pago_total }}</b>&nbsp;&nbsp;<strong class="c-white">Total Bs:</strong> <b class="text-info pago_total_bs_value">{{ $venta->pago_total_bs }}</b>
                                    @if($venta->descuento != null)
                                    @endif
                                    {{ Form::hidden('pago_total_usd',$venta->pago_total,["id"=>"pago_total_usd_1"]) }}
                                    {{ Form::hidden('pago_total_bs',$venta->pago_total_bs,["id"=>"pago_total_bs_1"]) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        {!! Form::label('tipo_pago','Tipo de pago: ') !!}
                        {!! Form::select('tipo_pago',['efectivo'=>'Pago en efectivo','deposito'=>'Dep&oacute;sito a cuenta bancaria'],'efectivo' ,['class'=>'form-control','placeholder'=>'Seleccione el tipo de pago..','id'=>'tipo_pago1','required']) !!}
                    </div>
                </div><br>
                {!! Form::hidden("monto",$venta->pago_total,["id"=>"montoTotalDls"]) !!}
                <input type='hidden' name='montoBs' value='{!! $venta->pago_total !!}'>
                <div class="row" id="datosPago1">
                    <div class="col-md-12">
                        <div class="panel panel-filled panel-c-warning">
                            <div class="panel-heading">
                                Pago en efectivo
                            </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-4">
                                    {!! Form::hidden("cuenta_bancaria",null) !!}
                                    {!! Form::label("cod_deposito_recibo","C&oacute;digo dep&oacute;sito/recibo: ",["class"=>"control-label"]) !!}
                                    {!! Form::text("cod_deposito_recibo",null,["class"=>"form-control","placeholder"=>"Ingrese el c&oacute;digo","required"]) !!}
                                </div>
                                <div class="col-md-3">
                                    {!! Form::label("pago_parcial","Monto: ",["class"=>"control-label"]) !!}
                                    {!! Form::text("pago_parcial",null,["class"=>"form-control","placeholder"=>"Ingrese el monto","required","id"=>"montoParcial"]) !!}
                                </div>
                                <div class="col-md-2 monedaDiv">
                                    {!! Form::label("moneda","Moneda: ",["class"=>"control-label"]) !!}
                                    {!!Form::select("moneda",["Dolares"=>"D&oacute;lares","Bolivianos"=>"Bolivianos"],null,['class'=>'form-control monedaSelect1','required'])!!}
                                </div>
                                <div class="col-md-2 tipo-cambio-div">
                                    {!! Form::label("tipo_cambio","Tipo de cambio: ") !!}
                                    {!! Form::text("tipo_cambio",6.96,["class"=>"form-control tipo_cambio_input"]) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div><br>
                @include('alerts.validation')
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="form-group">
        <div class="col-md-7" >
            {!!Form::submit('Guardar',['class'=>'btn btn-w-md btn-accent','id'=>'guardarBtn1'])!!} <a class="btn btn-w-md btn-default" href="{!! url('confirmar-venta/'.$venta->id.'/detalle') !!}">Cancelar</a>
        </div>
    </div>
    {!! Form::close() !!}
</div>
<script type="text/javascript">
function roundNumber(num, scale) {
  var number = Math.round(num * Math.pow(10, scale)) / Math.pow(10, scale);
  if(num - number > 0) {
    return (number + Math.floor(2 * Math.round((num - number) * Math.pow(10, (scale + 1))) / 10) / Math.pow(10, scale));
  } else {
    return number;
  }
}
$(document).ready(function(){
    $('#tipo_pago1').on('change', function(){
        var template =''
        var value = $("#tipo_pago1").val()
        if(value == 'efectivo'){
            template = '<div class="col-md-12"><div class="panel panel-filled panel-c-warning"><div class="panel-heading">Pago en efectivo</div><div class="panel-body"><div class="row"><div class="col-md-4">{!! Form::hidden("cuenta_bancaria",null) !!}{!! Form::label("cod_deposito_recibo","C&oacute;digo dep&oacute;sito/recibo: ",["class"=>"control-label"]) !!}{!! Form::text("cod_deposito_recibo",null,["class"=>"form-control","placeholder"=>"Ingrese el c&oacute;digo","required"]) !!}</div><div class="col-md-3">{!! Form::label("pago_parcial","Monto: ",["class"=>"control-label"]) !!}{!! Form::text("pago_parcial",null,["class"=>"form-control","placeholder"=>"Ingrese el monto","required","id"=>"montoParcial"]) !!}</div><div class="col-md-2 monedaDiv">{!! Form::label("moneda","Moneda: ",["class"=>"control-label"]) !!}{!!Form::select("moneda",["Dolares"=>"D&oacute;lares","Bolivianos"=>"Bolivianos"],null,['class'=>'form-control monedaSelect1','required'])!!}</div><div class="col-md-2 tipo-cambio-div">{!! Form::label("tipo_cambio","Tipo de cambio: ") !!}{!! Form::text("tipo_cambio",6.96,["class"=>"form-control tipo_cambio_input"]) !!}</div></div></div></div></div>'
            $("#datosPago1").html(template)
            toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-center",
                            "preventDuplicates": true,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                          };
            toastr.info('El tipo de cambio por defecto es <b>1 USD = 6.96 BOB</b>.');
        }
        if(value == 'deposito'){
            template = '<div class="col-md-12"><div class="panel panel-filled panel-c-warning"><div class="panel-heading">Dep&oacute;sito a cuenta</div><div class="panel-body"><div class="row"><div class="col-md-4 cuentaBancariaDiv">{!! Form::label("cuenta_bancaria","Cuenta bancaria: ",["class"=>"control-label"]) !!}{!! Form::select("cuenta_bancaria",$cuentas,null,["class"=>"form-control cuentaBancaria1","placeholder"=>"Seleccione una cuenta..","required"]) !!}</div><div class="col-md-2">{!! Form::label("cod_deposito_recibo","Cod. dep&oacute;sito/recibo: ",["class"=>"control-label"]) !!}{!! Form::text("cod_deposito_recibo",null,["class"=>"form-control","placeholder"=>"Ingrese el c&oacute;digo","required"]) !!}</div><div class="col-md-2">{!! Form::label("pago_parcial","Monto: ",["class"=>"control-label"]) !!}{!! Form::text("pago_parcial",null,["class"=>"form-control","placeholder"=>"Ingrese el monto","required","id"=>"montoParcial"]) !!}</div><div class="col-md-2 monedaDiv">{!! Form::label("moneda","Moneda: ",["class"=>"control-label"]) !!}{!!Form::select("moneda",["Dolares"=>"D&oacute;lares","Bolivianos"=>"Bolivianos"],null ,['class'=>'form-control monedaSelect1','required'])!!}</div><div class="col-md-2 tipo-cambio-div">{!! Form::label("tipo_cambio","Tipo de cambio: ") !!}{!! Form::text("tipo_cambio",6.96,["class"=>"form-control tipo_cambio_input"]) !!}</div></div></div></div></div>'
            $("#datosPago1").html(template)
        }
    }); 
});

res = 1; //variable para verificar si existe incoherencia en la cuenta bancaria y el tipo de moneda
$(document).on({//SIRVE PARA INPUTS CREADOS DINAMICAMENTE PUTO PROBLEMA -.- !
      'focus': function () {
          //hacer algo aca
      },
      'blur': function (e) {

      },
      'change':function(){
        str = $(".cuentaBancaria1 option:selected").text()
        moneda = $(".monedaSelect1 option:selected").val()
        if(str != ''){
            index = str.indexOf(moneda)
            if(index == -1){
                $(".cuentaBancariaDiv").attr("class","col-md-4 cuentaBancariaDiv has-error")
                $(".monedaDiv").attr("class","col-md-2 monedaDiv has-error")
                res = -1;
            }else{
                $(".cuentaBancariaDiv").attr("class","col-md-4 cuentaBancariaDiv")
                $(".monedaDiv").attr("class","col-md-2 monedaDiv")
                res = 1;    
            }
        }
      },
    }, '.monedaSelect1');

$(document).on({//SIRVE PARA INPUTS CREADOS DINAMICAMENTE PUTO PROBLEMA -.- !
      'focus': function () {
          //hacer algo aca
      },
      'blur': function (e) {

      },
      'change':function(){
        str = $(".cuentaBancaria1 option:selected").text()
        moneda = $(".monedaSelect1 option:selected").val()
        index = str.indexOf(moneda)
        if(index == -1){
            $(".cuentaBancariaDiv").attr("class","col-md-4 cuentaBancariaDiv has-error")
            $(".monedaDiv").attr("class","col-md-2 monedaDiv has-error")
            res = -1;
        }else{
            $(".cuentaBancariaDiv").attr("class","col-md-4 cuentaBancariaDiv")
            $(".monedaDiv").attr("class","col-md-2 monedaDiv")
            res = 1;
        }
      },
    }, '.cuentaBancaria1');

$(document).ready(function(){
    tipo_cambio = $(".tipo_cambio_input").val()
    tipo_cambio = parseFloat(tipo_cambio)
    pago_total_bs = $("#montoTotalDls").val()
    pago_total_bs = pago_total_bs * tipo_cambio
    $(".pago_total_bs_value").html(pago_total_bs+" Bs")
    $("#pago_total_bs_1").val(pago_total_bs)
    toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-center",
                            "preventDuplicates": true,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                          };
            toastr.info('El tipo de cambio por defecto es <b>1 USD = '+tipo_cambio+' BOB</b>.');
    $("#guardarBtn1").on('click',function(){
        if(res < 0){
            toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-center",
                            "preventDuplicates": true,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                          };
            toastr.error('<b>Error!</b> El tipo de moneda de la cuenta bancaria debe ser igual al ingresado.');
            return false; //error de tipo de moneda
        }
        moneda = $(".monedaSelect1").val()
        montoTotal = 0
        montoParcial = $("#montoParcial").val()
        if(moneda == 'Bolivianos'){
            montoTotal = $("#pago_total_bs_1").val()
        }
        if(moneda == 'Dolares'){
            montoTotal = $("#pago_total_usd_1").val()
        }
        montoTotal = parseFloat(montoTotal)
        montoParcial = parseFloat(montoParcial)
        if(montoParcial > montoTotal){
            toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-center",
                            "preventDuplicates": true,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                          };
            toastr.error('<b>Error!</b> el pago parcial debe ser menor al total !');
            return false
        }else return true
        return false;
    });
});
</script>
@endsection
@section('scripts')
@endsection

