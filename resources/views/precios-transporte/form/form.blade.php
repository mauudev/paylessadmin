<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('descripcion','Descripci&oacute;n: ',['class'=>'control-label']) !!}
			{!! Form::text('descripcion',null,['class'=>'form-control','placeholder'=>'Marca-modelo-anio-descripcion',"required"]) !!}
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('precio','Precio de transporte: ',['class'=>'control-label']) !!}
			{!! Form::text('precio',null,['class'=>'form-control','placeholder'=>'Ingrese el precio de transporte del item',"required"]) !!}<br/>
		</div>
	</div>
</div>