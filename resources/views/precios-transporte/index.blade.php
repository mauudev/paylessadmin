@extends('layouts.principal')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-filled">
			<div class="panel-heading">
				<div class="panel-tools">
					<div class="col-md-12">
						{!! Form::open(['route'=>'precios-transporte.index','method'=>'GET','class'=>'navbar-form navbar-left pull-right','role'=>'search']) !!}
						<div class="form-group">
							{!! Form::text('search',null,['class'=>'form-control','placeholder'=>'Buscar..']) !!}
						</div>
						<button type="submit" class="btn btn-accent">Buscar</button>
						<a href="{!! URL::to('precios-transporte/create') !!}" class="btn btn-w-md btn-accent"><i class="fa fa-plus"></i> Nuevo registro</a>
						{!! Form::close() !!}
					</div>
				</div>
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-car"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Precios registrados</h3>
						<small>
						Gesti&oacute;n de precios de transporte para repuestos/mercader&iacute;as
						</small>
					</div>
				</div>
			</div>
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-hover table-striped">
						<thead>
							<tr>
								<th class="left-align">Descripci&oacute;n</th>
								<th >Precio de transporte</th>
								<th>Acci&oacute;n</th>
							</tr>
						</thead>
						<tbody>
							@foreach($precios as $precio)
							<tr>
								<td>{{ $precio->descripcion }}</td>
								<td align="center">{{ $precio->precio }} $</td>
								<td align="center">
									<a class="btn btn-accent" href="{!! route('precios-transporte.edit',$parameters = $precio->id) !!}" data-tooltip="Editar precio"><i class="fa fa-pencil"></i> Editar</a>&nbsp;
									<a class="btn btn-danger" href="{!! route('precios-transporte.destroy',$parameters = $precio->id) !!}" data-tooltip="Eliminar precio"><i class="fa fa-trash" onclick='return confirm("¿Estas seguro de dar de baja este elemento?")'></i> Eliminar</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					{{ $precios->render() }}
					@include('alerts.success')
				</div>
			</div>
		</div>
	</div>
</div>
</div>
@endsection