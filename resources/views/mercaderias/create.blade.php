@extends('layouts.principal')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-filled">
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-phone"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Registro de nuevo productos varios</h3>
						<small>
						Gesti&oacute; de registros
						</small>
					</div>
				</div>
				<div class="panel-body">	
				{!! Form::open(['route'=>'mercaderias.store','method'=>'POST','class'=>'form-group',"onsubmit"=>"return checkSubmit();"]) !!}
					@include('mercaderias.form.form')
					@include('alerts.validation')
					<div class="form-group">
					<div class="col-md-7" >
						{!!Form::submit('Registrar',['class'=>'btn btn-w-md btn-accent'])!!} <a class="btn btn-w-md btn-default" href="{!! URL::to('mercaderias') !!}">Cancelar</a>
					</div>
				</div>
				{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection