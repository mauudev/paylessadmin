<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('nombre_m','Descripci&oacute;n: ',['class'=>'control-label']) !!}
	{!! Form::text('nombre_m',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre de la mercaderia','required','min'=>5]) !!}<br/>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('nro_item','N&uacute;mero de &iacute;tem: ',['class'=>'control-label']) !!}
			{!! Form::text('nro_item',null,['class'=>'form-control','placeholder'=>'Ingrese el numero de item','min'=>5]) !!}<br/>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('precio_venta_m','Precio de venta: ',['class'=>'control-label']) !!}
			{!! Form::text('precio_venta_m',null,['class'=>'form-control','placeholder'=>'Ingrese el precio de venta','min'=>5]) !!}<br/>
		</div>
	</div>
</div>