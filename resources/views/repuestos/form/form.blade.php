<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('marca_r','Marca: ',['class'=>'control-label']) !!}
	{!! Form::text('marca_r',null,['class'=>'form-control','placeholder'=>'Ingrese la marca del repuesto','required','min'=>5]) !!}<br/>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('modelo_r','Modelo: ',['class'=>'control-label']) !!}
	{!! Form::text('modelo_r',null,['class'=>'form-control','placeholder'=>'Ingrese el modelo de la marca','required','min'=>5]) !!}<br/>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('anio_r','Año: ',['class'=>'control-label']) !!}
	{!! Form::text('anio_r',null,['class'=>'form-control','placeholder'=>'Ingrese el año del modelo','required','min'=>5]) !!}<br/>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('vin_r','VIN: ',['class'=>'control-label']) !!}
	{!! Form::text('vin_r',null,['class'=>'form-control','placeholder'=>'Ingrese el VIN del repuesto','min'=>5]) !!}<br/>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('detalle_r','Detalle: ',['class'=>'control-label']) !!}
	{!!Form::textarea('detalle_r',null,['class'=>'form-control', 'placeholder'=>'Detalles','rows' => 4,'required','min'=>20]) !!}<br>
		</div>
	</div>
</div>
