@extends('layouts.principal')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-filled">
				<div class="col-md-11">
					<div class="view-header">
						<div class="header-icon">
							<i class="pe page-header-icon pe-7s-note2"></i>
						</div>
						@if($venta->estado == 1 && $venta->finalizado == 1)	
						<div class="header-title">
							<h3 class="page-header">Detalles de la venta</h3>
							<small>
								Cotizaci&oacute;n de repuestos/mercader&iacute;as
							</small>
						</div>
						@else
						<div class="header-title">
							<h3 class="page-header">Detalles de la cotizaci&oacute;n</h3>
							<small>
								Cotizaci&oacute;n de repuestos/mercader&iacute;as
							</small>
						</div>
						@endif
					</div>
				</div>
				@if($venta->estado == 1)
                <div class="col-md-1"><br><br><br>
                    <a class="btn btn-accent" href="{!! URL::to('cotizaciones/'.$venta->id.'/edit') !!}" data-tooltip="Editar cotizaci&oacute;n" onclick='return confirm("El estado de la venta cambiar&aacute; ¿Desea continuar?")'><i class="fa fa-edit"></i></a>
                </div>
                @else
                <div class="col-md-1"><br><br><br>
                    <a class="btn btn-accent" href="{!! URL::to('cotizaciones/'.$venta->id.'/edit') !!}" data-tooltip="Editar cotizaci&oacute;n"><i class="fa fa-edit"></i></a>
                </div>
                @endif
				<div class="panel-body">
					<div class="col-md-12">
		               	@include('alerts.success')
						@include('alerts.error')
					</div>
					<div class="col-md-12">
						<div class="col-md-6">
		                    <div class="panel panel-filled panel-c-primary">
		                        <div class="panel-heading">
		                            <div class="panel-tools">
		                                <a class="panel-toggle"><i class="fa fa-chevron-down"></i></a>
		                            </div>
		                            <b>Datos del cliente</b>
		                        </div>
		                        <div class="panel-body" style="display: none;">
		                            <dl class="dl-horizontal">
											<dt>Cliente:</dt>
											<dd>{{ $cliente->nombre_c }}</dd>
											<dt>Teléfono:</dt>
											<dd>{{ $cliente->telefono_c }}</dd>
											<dt>Celular:</dt>
											<dd>{{ $cliente->celular_c }}</dd>
											<dt>E-mail:</dt>
											<dd>{{ $cliente->email_c }}</dd>
											<dt>Localidad:</dt>
											<dd>{{ $cliente->pais }}</dd>
											<dt>Vía contacto:</dt>
											<dd>{{ $cliente->origen_contacto }}</dd>
										</dl>
		                        </div>
		                    </div>
	                	</div>
	                	<div class="col-md-6">
		                    <div class="panel panel-filled panel-c-info">
		                        <div class="panel-heading">
		                            <div class="panel-tools">
		                                <a class="panel-toggle"><i class="fa fa-chevron-down"></i></a>
		                            </div>
		                            <b>Datos del responsable de venta</b>
		                        </div>
		                        <div class="panel-body" style="display: none;">
		                            <dl class="dl-horizontal">
										<dl class="dl-horizontal">
											<dt>Usuario:</dt>
											<dd>{{ $usuario->nombre_u }}</dd>
											<dt>Teléfono:</dt>
											<dd>{{ $usuario->telefono_u }}</dd>
											<dt>Celular:</dt>
											<dd>{{ $usuario->celular_u }}</dd>
											<dt>E-mail:</dt>
											<dd>{{ $usuario->email_u }}</dd>
											<dt>Tipo usuario:</dt>
											@if($usuario->type == 'admin')
											<dd>Administrador</dd>
											@else
											<dd>Usuario</dd>
											@endif
										</dl>
		                        </dl></div>
		                    </div>
	                	</div>
	                </div>
					<div class="col-md-12">
						@if(count($cotizacion_rep) > 0)
						<div class="col-md-12">
							<h4>Repuestos de la cotizaci&oacute;n</h4>
							<hr><br>
						</div>
						@endif
					@for($i = 0; $i < count($cotizacion_rep); $i++)
					<div class="col-md-4" >
						<div class="panel panel-filled panel-c-success">
							<div class="panel-heading">
								<div class="panel-tools">
									<?php $count_prov_rep = PaylessAdmin\CotizacionRepuesto::getCantidad_proveedores_rep($cotizacion_rep[$i]->repuesto_id)[0]->count; ?>
									@if(0 == $cotizacion_rep[$i]->estado)
									<a class="btn btn-danger btn-xs btn-rounded" data-tooltip="No se calcul&oacute; el precio"><i class="fa fa-exclamation-circle"></i> S/P</a>
									@else <a class="btn btn-success btn-xs btn-rounded" data-tooltip="Se ha calculado el precio"><i class="fa fa-check-circle"></i> C/P</a>
									@endif
									@if( $count_prov_rep <= 0)
									<a class="btn btn-danger btn-xs btn-rounded" data-tooltip="Sin proveedores"><i class="fa fa-warning"></i> {{ $count_prov_rep }}</a>
									@else
									<a class="btn btn-primary btn-xs btn-rounded" data-tooltip="Tiene {{ $count_prov_rep }} proveedor/es"><i class="fa fa-truck"></i> {{ $count_prov_rep }}</a>
									@endif
									<a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
								</div>
								<b>Detalle del repuesto</b>
							</div>
							<div class="panel-body">
								<b>Repuesto: </b>{{ $cotizacion_rep[$i]->marca_r }}<br/>
								<b>Modelo: </b>{{ $cotizacion_rep[$i]->modelo_r }}<br/>
								<b>A&ntilde;o: </b>{{ $cotizacion_rep[$i]->anio_r }}<br/>
								<b>VIN: </b>{{ $cotizacion_rep[$i]->vin_r }}<br/>
								<b>Detalle: </b>{{ $cotizacion_rep[$i]->detalle_r }}<br/>
								<b>C&oacute;digo: </b>{{ $cotizacion_rep[$i]->codigo_repuesto }}<br/>
								<b>Cantidad: </b>{{ $cotizacion_rep[$i]->cantidad }}<br/>
							</div>
						@if($msg_venta_trashed != "desactivado")
							@if($count_prov_rep <= 0)
							<div class="panel-footer">
								<input class="form-group" name="id_cot" type="hidden" value="{{ $cotizacion_rep[$i]->id_cot }}">
								<a href="{{ url('/agregar-proveedores/' .$cotizacion_rep[$i]->repuesto_id .'/create-rep/'.$venta->id ) }}"  class="btn btn-w-md btn-success btn-block btn-add-proveedor-rep"><i class="fa fa-plus-square-o"></i> Agregar proveedores</a>
							</div>
							@elseif(0 == $cotizacion_rep[$i]->estado)
							<div class="panel-footer">
								<div class="row">
									
									<div class="btn-group col-md-12">
			                                <button data-toggle="dropdown" class="btn btn-w-md btn-success btn-block dropdown-toggle" aria-expanded="false">Opciones de cotizaci&oacute;n <span class="caret"></span></button>
			                                <ul class="dropdown-menu">
			                                    <li><input class="form-group" name="id_cot" type="hidden" value="{{ $cotizacion_rep[$i]->id_cot }}">
													<a href="{{ url('/agregar-proveedores/' .$cotizacion_rep[$i]->repuesto_id .'/create-rep/'.$venta->id ) }}">Agregar nuevo proveedor</a>
												</li>
			                                    <li class="divider"></li>
			                                    <li><input class="form-group" name="id_cot" type="hidden" value="{{ $cotizacion_rep[$i]->id_cot }}">
													<a href="{{ url('calcular-precio-venta/repuesto/'.$cotizacion_rep[$i]->repuesto_id.'/venta/' .$venta->id ) }}">Calcular el precio de venta</a>
												</li>
			                                </ul>
			                        </div>
								</div>
							</div>

							@elseif(1 == $cotizacion_rep[$i]->estado && $count_prov_rep > 1)
							
							<div class="panel-footer">
								<div class="row">
									<div class="col-md-12">
										<div class="btn-group col-md-12">
			                                <button data-toggle="dropdown" class="btn btn-w-md btn-success btn-block dropdown-toggle" aria-expanded="false">Opciones de cotizaci&oacute;n <span class="caret"></span></button>
			                                <ul class="dropdown-menu">
			                                    <li><input class="form-group" name="id_cot" type="hidden" value="{{ $cotizacion_rep[$i]->id_cot }}"><a href="{{ url('/ver-precio-venta-rep/cotizacion-rep/'.$cotizacion_rep[$i]->id_cot.'/venta/'.$venta->id ) }}">Ver detalle del precio de venta</a></li>
			                                    <li class="divider"></li>
			                                    <li><input class="form-group" name="id_cot" type="hidden" value="{{ $cotizacion_rep[$i]->id_cot }}"><a href="{{ url('calcular-precio-venta/'.$cotizacion_rep[$i]->id_cot.'/edit') }}">Editar precio de venta</a></li>
			                                </ul>
			                            </div>
									</div>
								</div>
							</div>
							@elseif($cotizacion_rep[$i]->estado == 1 && $count_prov_rep == 1)
							<div class="panel-footer">
								<div class="row">
									<div class="col-md-12">
										<div class="btn-group col-md-12">
			                                <button data-toggle="dropdown" class="btn btn-w-md btn-success btn-block dropdown-toggle" aria-expanded="false">Opciones de cotizaci&oacute;n <span class="caret"></span></button>
			                                <ul class="dropdown-menu">
			                                    <li><input class="form-group" name="id_cot" type="hidden" value="{{ $cotizacion_rep[$i]->id_cot }}"><a href="{{ url('calcular-precio-venta/'.$cotizacion_rep[$i]->id_cot.'/precioventa') }}">Ver detalle del precio de venta</a></li>
			                                    <li class="divider"></li>
			                                    <li><input class="form-group" name="id_cot" type="hidden" value="{{ $cotizacion_rep[$i]->id_cot }}"><a href="{{ url('calcular-precio-venta/'.$cotizacion_rep[$i]->id_cot.'/edit') }}">Editar precio de venta</a></li>
			                                </ul>
			                            </div>
									</div>
								</div>
							</div>
							@elseif($venta->estado == 1 || $count_prov_rep == 1)
							<div class="panel-footer">
								<div class="row">
									<div class="col-md-6">
										<input class="form-group" name="id_cot" type="hidden" value="{{ $cotizacion_rep[$i]->id_cot }}">
										<a href="{{ url('calcular-precio-venta-mer/'.$cotizacion_rep[$i]->id_cot.'/precioventa') }}"  class="btn btn-w-md btn-info btn-block btn-add-proveedor-mer">Ver detalle del precio de venta</a>
									</div>
									<div class="col-md-6">
										<input class="form-group" name="id_cot" type="hidden" value="{{ $cotizacion_rep[$i]->id_cot }}">
										<a class="btn btn-w-md btn-primary btn-block editarPrecio1">Editar precio de venta</a>
									</div>
								</div>
							</div>
							<script type="text/javascript">
								$(document).on('click','.editarPrecio1',function(){
									toastr.options = {
							                        "closeButton": true,
							                        "debug": false,
							                        "newestOnTop": false,
							                        "progressBar": false,
							                        "positionClass": "toast-top-center",
							                        "preventDuplicates": true,
							                        "onclick": null,
							                        "showDuration": "300",
							                        "hideDuration": "1000",
							                        "timeOut": "5000",
							                        "extendedTimeOut": "1000",
							                        "showEasing": "swing",
							                        "hideEasing": "linear",
							                        "showMethod": "fadeIn",
							                        "hideMethod": "fadeOut"
							                      };
		        					toastr.error('<b>Error!</b> Debe deshabilitar la venta para poder editar el precio, haga click en "Ver detalle venta" al final de la p&aacute;gina y deshabilite la venta');
								});	
							</script>
							@endif
						@endif
						</div>
					</div>
					@endfor
					</div>
					<div class="col-md-12">
						@if(count($cotizacion_mer) > 0)
							<div class="col-md-12">
								<h4>Productos varios de la cotizaci&oacute;n</h4>
								<hr><br>
							</div>
						@endif
					@for($i = 0; $i < count($cotizacion_mer); $i++)
					<div class="col-md-4">
						<div class="panel panel-filled panel-c-warning">
							<div class="panel-heading">
								<div class="panel-tools">
									<?php $count_prov_mer = PaylessAdmin\CotizacionMercaderia::getCantidad_proveedores_mer($cotizacion_mer[$i]->mercaderia_id)[0]->count; ?>
									@if(0 == $cotizacion_mer[$i]->estado)
									<a class="btn btn-danger btn-xs btn-rounded" data-tooltip="No se calcul&oacute; el precio"><i class="fa fa-exclamation-circle"></i> S/P</a>
									@else <a class="btn btn-success btn-xs btn-rounded" data-tooltip="Se ha calculado el precio"><i class="fa fa-check-circle"></i> C/P</a>
									@endif
									@if( $count_prov_mer <= 0)
									<a class="btn btn-danger btn-xs btn-rounded" data-tooltip="Sin proveedores"><i class="fa fa-warning"></i> {{ $count_prov_mer }}</a>
									@else
									<a class="btn btn-primary btn-xs btn-rounded" data-tooltip="Tiene {{ $count_prov_mer }} proveedor/es"><i class="fa fa-truck"></i> {{ $count_prov_mer }}</a>
									@endif
									<a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
								</div>
								<b>Detalle del producto</b>
							</div>
							<div class="panel-body"><br/><br/>
								<b>Detalle: </b>{{ $cotizacion_mer[$i]->nombre_m }}
								<p><b>Nro. &iacute;tem/URL: </b>
								@if(filter_var($cotizacion_mer[$i]->nro_item, FILTER_VALIDATE_URL) == true)	
									<a href="{{ $cotizacion_mer[$i]->nro_item }}" target="_blank">{{ str_limit($cotizacion_mer[$i]->nro_item,$limit = 30,$end='..') }}</a></p>
									<p><b>Cantidad: </b>{{ $cotizacion_mer[$i]->cantidad }}</p>
								@else
									{{ str_limit($cotizacion_mer[$i]->nro_item,$limit = 30,$end='..')  }}</p>
									<p><b>Cantidad: </b>{{ $cotizacion_mer[$i]->cantidad }}</p><br>
								@endif
							</div>
						@if($msg_venta_trashed != "desactivado")
							@if($count_prov_mer <= 0)
							<div class="panel-footer">
								<input class="form-group" name="id_cot" type="hidden" value="{{ $cotizacion_mer[$i]->id_cot }}">
								<a href="{{ url('/agregar-proveedores/' .$cotizacion_mer[$i]->mercaderia_id .'/create-mer/'.$venta->id ) }}"  class="btn btn-w-md btn-accent btn-block btn-add-proveedor-mer"><i class="fa fa-plus-square-o"></i> Agregar proveedores</a>
							</div>
							@elseif(0 == $cotizacion_mer[$i]->estado)
							<div class="panel-footer">
								<div class="row">
									<div class="btn-group col-md-12">
			                                <button data-toggle="dropdown" class="btn btn-w-md btn-accent btn-block dropdown-toggle" aria-expanded="false">Opciones de cotizaci&oacute;n <span class="caret"></span></button>
			                                <ul class="dropdown-menu">
			                                    <li><input class="form-group" name="id_cot" type="hidden" value="{{ $cotizacion_mer[$i]->id_cot }}">
													<a href="{{ url('/agregar-proveedores/' .$cotizacion_mer[$i]->mercaderia_id .'/create-mer/'.$venta->id ) }}">Agregar nuevo proveedor</a>
												</li>
			                                    <li class="divider"></li>
			                                    <li><input class="form-group" name="id_cot" type="hidden" value="{{ $cotizacion_mer[$i]->id_cot }}">
													<a href="{{ url('calcular-precio-venta-mer/mercaderia/'.$cotizacion_mer[$i]->mercaderia_id.'/venta/' .$venta->id ) }}">Calcular el precio de venta</a>
												</li>
			                                </ul>
			                        </div>
								</div>
							</div>
							@elseif(1 == $cotizacion_mer[$i]->estado && $count_prov_mer > 1)
							
							<div class="panel-footer">
								<div class="row">
									<div class="col-md-12">
										<div class="btn-group col-md-12">
			                                <button data-toggle="dropdown" class="btn btn-w-md btn-accent btn-block dropdown-toggle" aria-expanded="false">Opciones de cotizaci&oacute;n <span class="caret"></span></button>
			                                <ul class="dropdown-menu">
			                                    <li><input class="form-group" name="id_cot" type="hidden" value="{{ $cotizacion_mer[$i]->id_cot }}"><a href="{{ url('/ver-precio-venta-mer/cotizacion-mer/'.$cotizacion_mer[$i]->id_cot.'/venta/'.$venta->id ) }}">Elegir proveedor / Ver detalle del precio de venta</a></li>
			                                    <li class="divider"></li>
			                                    <li><input class="form-group" name="id_cot" type="hidden" value="{{ $cotizacion_mer[$i]->id_cot }}"><a href="{{ url('calcular-precio-venta-mer/'.$cotizacion_mer[$i]->id_cot.'/edit') }}">Editar precio de venta</a></li>
			                                </ul>
			                            </div>
									</div>
								</div>
							</div>
							@elseif($cotizacion_mer[$i]->estado == 1 && $count_prov_mer == 1)
							<div class="panel-footer">
								<div class="row">
									<div class="col-md-12">
										<div class="btn-group col-md-12">
			                                <button data-toggle="dropdown" class="btn btn-w-md btn-accent btn-block dropdown-toggle" aria-expanded="false">Opciones de cotizaci&oacute;n <span class="caret"></span></button>
			                                <ul class="dropdown-menu">
			                                    <li><input class="form-group" name="id_cot" type="hidden" value="{{ $cotizacion_mer[$i]->id_cot }}"><a href="{{ url('calcular-precio-venta-mer/'.$cotizacion_mer[$i]->id_cot.'/precioventa') }}">Ver detalle del precio de venta</a></li>
			                                    <li class="divider"></li>
			                                    <li><input class="form-group" name="id_cot" type="hidden" value="{{ $cotizacion_mer[$i]->id_cot }}"><a href="{{ url('calcular-precio-venta-mer/'.$cotizacion_mer[$i]->id_cot.'/edit') }}">Editar precio de venta</a></li>
			                                </ul>
			                            </div>
									</div>
								</div>
							</div>
							@elseif($venta->estado == 1 || $count_prov_mer == 1)
							<div class="panel-footer">
								<div class="row">
									<div class="col-md-6">
										<input class="form-group" name="id_cot" type="hidden" value="{{ $cotizacion_mer[$i]->id_cot }}">
										<a href="{{ url('calcular-precio-venta-mer/'.$cotizacion_mer[$i]->id_cot.'/precioventa') }}"  class="btn btn-w-md btn-info btn-block btn-add-proveedor-mer">Ver detalle del precio de venta</a>
									</div>
									<div class="col-md-6">
										<input class="form-group" name="id_cot" type="hidden" value="{{ $cotizacion_mer[$i]->id_cot }}">
										<a class="btn btn-w-md btn-primary btn-block editarPrecio1">Editar precio de venta</a>
									</div>
								</div>
							</div>
							<script type="text/javascript">
								$(document).on('click','.editarPrecio1',function(){
									toastr.options = {
							                        "closeButton": true,
							                        "debug": false,
							                        "newestOnTop": false,
							                        "progressBar": false,
							                        "positionClass": "toast-top-center",
							                        "preventDuplicates": true,
							                        "onclick": null,
							                        "showDuration": "300",
							                        "hideDuration": "1000",
							                        "timeOut": "5000",
							                        "extendedTimeOut": "1000",
							                        "showEasing": "swing",
							                        "hideEasing": "linear",
							                        "showMethod": "fadeIn",
							                        "hideMethod": "fadeOut"
							                      };
		        					toastr.error('<b>Error!</b> Debe deshabilitar la venta para poder editar el precio, haga click en "Ver detalle venta" al final de la p&aacute;gina y deshabilite la venta');
								});	
							</script>
							@endif
						@endif
						</div>
					</div>
					@endfor
				</div>
				</div>
			</div>

			<div class="row">
				<div class="form-group">
					@if($venta->estado == 1 && $venta->estado_ventas == 1)
						<div class="col-md-2" >
							<a class="btn btn-w-md btn-accent" href="{!! url('ventas') !!}">Regresar a ventas</a>
						</div>
					@else
						<div class="col-md-2" >
							<a class="btn btn-w-md btn-accent" href="{!! url('/cotizaciones/listar/'.$venta->estado) !!}">Regresar a cotizaciones</a>
						</div>
					@endif
					@if($msg_venta_trashed != "desactivado")
						@if("finalizado" == $msg_confirmation)
							<div class="col-md-2">
								<a class="btn btn-w-md btn-success" href="{!! URL::to('/confirmar-venta/'.$venta->id.'/detalle') !!}">Ver detalle de cotizaci&oacute;n</a>
							</div>
							<script type="text/javascript">
								toastr.options = {
			                        "closeButton": true,
			                        "debug": false,
			                        "newestOnTop": false,
			                        "progressBar": false,
			                        "positionClass": "toast-top-center",
			                        "preventDuplicates": true,
			                        "onclick": null,
			                        "showDuration": "300",
			                        "hideDuration": "1000",
			                        "timeOut": "5000",
			                        "extendedTimeOut": "1000",
			                        "showEasing": "swing",
			                        "hideEasing": "linear",
			                        "showMethod": "fadeIn",
			                        "hideMethod": "fadeOut"
			                      };
			        			toastr.info('Todos los elementos de la cotizaci&oacute;n se han completado, ahora puede finalizar la venta en <b>Ver detalle de cotizaci&oacute;n</b>.');
							</script>
						@elseif("pendiente" == $msg_confirmation)
							<div class="col-md-2">
								<a class="btn btn-w-md btn-danger" id="popMsg1">Ver detalle de cotizaci&oacute;n</a>
							</div>
						<script type="text/javascript">
						$('#popMsg1').on('click',function(){
							toastr.options = {
			                        "closeButton": true,
			                        "debug": false,
			                        "newestOnTop": false,
			                        "progressBar": false,
			                        "positionClass": "toast-top-center",
			                        "preventDuplicates": true,
			                        "onclick": null,
			                        "showDuration": "300",
			                        "hideDuration": "1000",
			                        "timeOut": "5000",
			                        "extendedTimeOut": "1000",
			                        "showEasing": "swing",
			                        "hideEasing": "linear",
			                        "showMethod": "fadeIn",
			                        "hideMethod": "fadeOut"
			                      };
			        			toastr.error('<b>Error!</b> Esta cotizaci&oacute;n a&uacute;n tiene elementos pendientes, aseg&uacute;rese de que todos los repuestos y/o mercader&iacute;as tengan un solo proveedor asignado y un precio de venta calculado.');
						});
						</script>
						@elseif("confirmado" == $msg_confirmation)
						<div class="col-md-2">
							<a class="btn btn-w-md btn-success" href="{!! URL::to('/ventas/'.$venta->id.'/detalle') !!}">Ver detalle de cotizaci&oacute;n</a>
						</div>
						@endif
					@endif
				</div>
			</div>
		</div>
	</div>
	@if($msg_venta_trashed == "desactivado")
		<script type="text/javascript">
				toastr.options = {
		                        "closeButton": true,
		                        "debug": false,
		                        "newestOnTop": false,
		                        "progressBar": false,
		                        "positionClass": "toast-top-center",
		                        "preventDuplicates": true,
		                        "onclick": null,
		                        "showDuration": "300",
		                        "hideDuration": "1000",
		                        "timeOut": "5000",
		                        "extendedTimeOut": "1000",
		                        "showEasing": "swing",
		                        "hideEasing": "linear",
		                        "showMethod": "fadeIn",
		                        "hideMethod": "fadeOut"
		                      };
		        	toastr.warning('<b>ATENCION:</b> Esta cotizaci&oacute;n se encuentra desactivada, s&oacute;lo podr&aacute; actualizar los datos.');
		</script>
	@endif
	@if($msg_cliente == "desactivado")
		<script type="text/javascript">
				toastr.options = {
		                        "closeButton": true,
		                        "debug": false,
		                        "newestOnTop": false,
		                        "progressBar": false,
		                        "positionClass": "toast-top-center",
		                        "preventDuplicates": true,
		                        "onclick": null,
		                        "showDuration": "300",
		                        "hideDuration": "1000",
		                        "timeOut": "5000",
		                        "extendedTimeOut": "1000",
		                        "showEasing": "swing",
		                        "hideEasing": "linear",
		                        "showMethod": "fadeIn",
		                        "hideMethod": "fadeOut"
		                      };
		        	toastr.warning('<b>ATENCION:</b> Este cliente se encuentra desactivado.');
		</script>
	@endif
	@if($msg_usuario == "desactivado")
		<script type="text/javascript">
				toastr.options = {
		                        "closeButton": true,
		                        "debug": false,
		                        "newestOnTop": false,
		                        "progressBar": false,
		                        "positionClass": "toast-top-center",
		                        "preventDuplicates": true,
		                        "onclick": null,
		                        "showDuration": "300",
		                        "hideDuration": "1000",
		                        "timeOut": "5000",
		                        "extendedTimeOut": "1000",
		                        "showEasing": "swing",
		                        "hideEasing": "linear",
		                        "showMethod": "fadeIn",
		                        "hideMethod": "fadeOut"
		                      };
		        	toastr.warning('<b>ATENCION:</b> El usuario que realiz&oacute; esta cotizaci&oacute;n se encuentra desactivado.');
		</script>
	@endif
	@if(isset($mensaje_pagos))
		@if($mensaje_pagos == "actualizar-pagos")
		<script type="text/javascript">
				toastr.options = {
		                        "closeButton": true,
		                        "debug": false,
		                        "newestOnTop": false,
		                        "progressBar": false,
		                        "positionClass": "toast-top-center",
		                        "preventDuplicates": true,
		                        "onclick": null,
		                        "showDuration": "1000",
		                        "hideDuration": "1000",
		                        "timeOut": "50000",
		                        "extendedTimeOut": "1000",
		                        "showEasing": "swing",
		                        "hideEasing": "linear",
		                        "showMethod": "fadeIn",
		                        "hideMethod": "fadeOut"
		                      };
		        	toastr.warning('<b>ATENCION:</b> Ha agregado un nuevo &iacute;tem a esta cotizaci&oacute;n, debe actualizar los pagos realizados !');
		</script>
		@endif
	@endif
	@endsection
	@section('scripts')
	@endsection
	<script type="text/javascript">
		function isUrlValid(url) {
    	return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
		}
	</script>