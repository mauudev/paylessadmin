<div class="row">
<div class="panel-heading">
	<h4 class="page-header">sDatos del cliente</h4>
</div>
	<div class="col-lg-6">
		<div class="form-group">
		{!! Form::label('nombre_c','Nombres: ',['class'=>'control-label']) !!}
		{!! Form::text('nombre_c',null,['class'=>'form-control','placeholder'=>'Ingrese el nombre del cliente','required','min'=>5]) !!}
		{!! Form::hidden('usuarios_id',\Auth::user()->id) !!}<br/>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('apellidos_c','Apellidos: ',['class'=>'control-label']) !!}
			{!! Form::text('apellidos_c',null,['class'=>'form-control','placeholder'=>'Ingrese el apellido del cliente','required','min'=>5]) !!}<br/>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('direccion_c','Direcci&oacute;n: ',['class'=>'control-label']) !!}
			{!! Form::text('direccion_c',null,['class'=>'form-control','placeholder'=>'Ingrese la direccion del cliente','min'=>5]) !!}<br/>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('telefono_c','Tel&eacute;fono: ',['class'=>'control-label']) !!}
			{!! Form::text('telefono_c',null,['class'=>'form-control','placeholder'=>'Ingrese el telefono del cliente','required','min'=>5,'id'=>'telefonoField1']) !!}<br/>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
		{!! Form::label('celular_c','Celular: ',['class'=>'control-label']) !!}
		{!! Form::text('celular_c',null,['class'=>'form-control','placeholder'=>'Ingrese el celular del cliente','required','min'=>5]) !!}<br/>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('email_c','E-mail: ',['class'=>'control-label']) !!}
			{!! Form::text('email_c',null,['class'=>'form-control','placeholder'=>'Ingrese el e-mail del cliente','required','min'=>5]) !!}<br/>
		</div>
	</div>
</div>
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('origen_contacto','Origen contacto: ',['class'=>'control-label']) !!}<br/>
			{!! Form::label('origen_contacto','Anuncio ',['class'=>'control-label form-group']) !!}&nbsp;
			{!! Form::radio('origen_contacto','Anuncio') !!}&nbsp;&nbsp;
		    {!! Form::label('origen_contacto','Pagina Facebook ',['class'=>'control-label']) !!}&nbsp;
			{!! Form::radio('origen_contacto','Pagina Facebook') !!}&nbsp;&nbsp;
			{!! Form::label('origen_contacto','Whatsapp ',['class'=>'control-label']) !!}&nbsp;
			{!! Form::radio('origen_contacto','Whatsapp') !!}		
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('pais','Pa&iacute;s: ',['class'=>'control-label']) !!}
			{!!Form::select('pais', $paises,null,['class'=>'form-control','placeholder'=>'Seleccione un pais..','required'])!!}<br>
		</div>
	</div>
</div>