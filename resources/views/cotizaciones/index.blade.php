@extends('layouts.principal')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-filled">
			<div class="panel-heading">
				<div class="panel-tools">
					<div class="col-md-12">
						<?php $estados = [1=>'Activos',0=>'Inactivos']?>
						{!! Form::open(['route'=>'cotizaciones.index','method'=>'GET','class'=>'navbar-form navbar-left pull-right','role'=>'search']) !!}
						<div class="form-group">
							{!!Form::select('estado', $estados,null,['class'=>'form-control','placeholder'=>'Filtrar registros..','id'=>'estado1'])!!}
							{!! Form::text('search',null,['id'=>'searchField1','class'=>'form-control','placeholder'=>'Buscar..']) !!}
							{!! Form::hidden('pointer',$pointer) !!}
							<br/>
						</div>
						<button id="btnBuscar1" type="submit" class="btn btn-accent">Buscar</button>
						<a href="{!! URL::to('cotizaciones/create') !!}" class="btn btn-w-md btn-accent"><i class="fa fa-plus"></i> Nueva cotizaci&oacute;n</a>
						{!! Form::close() !!}
					</div>
				</div>
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-note2"></i>
					</div>
					<div class="header-title">
						@if(isset($flag))
						@if($flag == "finalizado")
						<h3 class="page-header">Cotizaciones finalizadas</h3>
						@else
						<h3 class="page-header">Cotizaciones pendientes</h3>
						@endif
						@endif
						<small>
						Cotizaci&oacute;n de repuestos/mercader&iacute;as
						</small>
					</div>
				</div>
			</div>
			<div class="panel-body">
				<div class="table-responsive" id="tabla1">
					<table class="table table-hover table-striped">
						<thead>
							<tr role="row">
								<th class="sorting_asc left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
									aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" style="width: 45px;">ID
								</th>
								<th class="sorting_asc left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
									aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" style="width: 40px;">Fecha
								</th>
								<th class="sorting_asc left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
									aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" style="width: 100px;">Nombre
								</th>
								<th class="sorting_asc left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
									aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" style="width: 100px;">Apellidos
								</th>
								<th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
									aria-label="Engine version: activate to sort column ascending" style="width: 85px;">Responsable
								</th>
								<th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
									aria-label="CSS grade: activate to sort column ascending" style="width: 80px;">Acci&oacute;n
								</th>
							</tr>
						</thead>
						<tbody id="resultados1">
							@if(isset($ventas))
							@if(count($ventas) > 0)
							@foreach($ventas as $venta)
							<tr>
								<td data-toggle="modal" data-target="#myModal" data-id="{{ $venta->id }}">{{ $venta->id }}
									{{ Form::hidden('apellidos_c',$venta->apellidos_c,['class'=>'dataVenta']) }}
									{{ Form::hidden('nombre_c',$venta->nombre_c,['class'=>'dataVenta']) }}
									{{ Form::hidden('nombre_u',$venta->nombre_u,['class'=>'dataVenta']) }}{{ Form::hidden('apellidos_u',$venta->apellidos_u,['class'=>'dataVenta']) }}
									{{ Form::hidden('marca_r',$venta->marca_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('modelo_r',$venta->modelo_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('anio_r',$venta->anio_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('detalle_r',$venta->detalle_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('nombre_m',$venta->nombre_m,['class'=>'dataVenta']) }}
									{{ Form::hidden('created_at',$venta->created_at,['class'=>'dataVenta']) }}</td>
								<td data-toggle="modal" data-target="#myModal" data-id="{{ $venta->id }}">{{ str_limit($venta->created_at,$limit = 10,$end='') }}
									{{ Form::hidden('apellidos_c',$venta->apellidos_c,['class'=>'dataVenta']) }}
									{{ Form::hidden('nombre_c',$venta->nombre_c,['class'=>'dataVenta']) }}
									{{ Form::hidden('nombre_u',$venta->nombre_u,['class'=>'dataVenta']) }}{{ Form::hidden('apellidos_u',$venta->apellidos_u,['class'=>'dataVenta']) }}
									{{ Form::hidden('marca_r',$venta->marca_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('modelo_r',$venta->modelo_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('anio_r',$venta->anio_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('detalle_r',$venta->detalle_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('nombre_m',$venta->nombre_m,['class'=>'dataVenta']) }}
									{{ Form::hidden('created_at',$venta->created_at,['class'=>'dataVenta']) }}
								</td>
								<td data-toggle="modal" data-target="#myModal" data-id="{{ $venta->id }}">{{ str_limit($venta->nombre_c,$limit = 25,$end='...') }}
									{{ Form::hidden('apellidos_c',$venta->apellidos_c,['class'=>'dataVenta']) }}
									{{ Form::hidden('nombre_c',$venta->nombre_c,['class'=>'dataVenta']) }}
									{{ Form::hidden('nombre_u',$venta->nombre_u,['class'=>'dataVenta']) }}{{ Form::hidden('apellidos_u',$venta->apellidos_u,['class'=>'dataVenta']) }}
									{{ Form::hidden('marca_r',$venta->marca_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('modelo_r',$venta->modelo_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('anio_r',$venta->anio_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('detalle_r',$venta->detalle_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('nombre_m',$venta->nombre_m,['class'=>'dataVenta']) }}
									{{ Form::hidden('created_at',$venta->created_at,['class'=>'dataVenta']) }}
								</td>
								<td data-toggle="modal" data-target="#myModal" data-id="{{ $venta->id }}">{{ str_limit($venta->apellidos_c,$limit = 25,$end='...') }}
									{{ Form::hidden('apellidos_c',$venta->apellidos_c,['class'=>'dataVenta']) }}
									{{ Form::hidden('nombre_c',$venta->nombre_c,['class'=>'dataVenta']) }}
									{{ Form::hidden('nombre_u',$venta->nombre_u,['class'=>'dataVenta']) }}{{ Form::hidden('apellidos_u',$venta->apellidos_u,['class'=>'dataVenta']) }}
									{{ Form::hidden('marca_r',$venta->marca_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('modelo_r',$venta->modelo_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('anio_r',$venta->anio_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('detalle_r',$venta->detalle_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('nombre_m',$venta->nombre_m,['class'=>'dataVenta']) }}
									{{ Form::hidden('created_at',$venta->created_at,['class'=>'dataVenta']) }}
								</td>
								<td data-toggle="modal" data-target="#myModal" data-id="{{ $venta->id }}">
									{{ str_limit($venta->nombre_u.' '.$venta->apellidos_u,$limit = 40,$end='...') }}
									{{ Form::hidden('apellidos_c',$venta->apellidos_c,['class'=>'dataVenta']) }}
									{{ Form::hidden('nombre_c',$venta->nombre_c,['class'=>'dataVenta']) }}
									{{ Form::hidden('nombre_u',$venta->nombre_u,['class'=>'dataVenta']) }}{{ Form::hidden('apellidos_u',$venta->apellidos_u,['class'=>'dataVenta']) }}
									{{ Form::hidden('marca_r',$venta->marca_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('modelo_r',$venta->modelo_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('anio_r',$venta->anio_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('detalle_r',$venta->detalle_r,['class'=>'dataVenta']) }}
									{{ Form::hidden('nombre_m',$venta->nombre_m,['class'=>'dataVenta']) }}
									{{ Form::hidden('created_at',$venta->created_at,['class'=>'dataVenta']) }}
								</td>
								<td align="center">
									@if($venta->estado == 1 || $venta->pago_parcial != null || $venta->segundo_pago != null)
									<a class="btn btn-info" href="{{ URL::to('/ventas/'. $venta->id.'/detalle' ) }}"><i class="fa fa-eye"></i> Detalle</a>
									<a class="btn btn-accent" href="{!! URL::to('cotizaciones/'.$venta->id.'/edit') !!}" onclick='return confirm("El estado de la venta cambiar&aacute; ¿Desea continuar?")'><i class="fa fa-edit"></i> Editar</a>
									@else
									<a class="btn btn-info" href="{{ URL::to('/administracion/mostrar-cotizacion-principal/show/'. $venta->id ) }}"><i class="fa fa-eye"></i> Detalle</a>
									<a class="btn btn-accent" href="{!! URL::to('cotizaciones/'.$venta->id.'/edit') !!}"><i class="fa fa-edit"></i> Editar</a>
									@endif
									@if($venta->deleted_at == null)
									@if($flag == 'pendiente')
									<a class="btn btn-danger" href="{{ URL::to('/administracion/cotizaciones/'.$venta->id.'/destroy/0/vista' ) }}"onclick='return confirm("Estas seguro de dar de baja este elemento?")'><i class="fa fa-times"></i>Desactivar</a>
									@else
									<a class="btn btn-danger" href="{{ URL::to('/administracion/cotizaciones/'.$venta->id.'/destroy/1/vista' ) }}"onclick='return confirm("Estas seguro de dar de baja este elemento?")'><i class="fa fa-times"></i>Desactivar</a>
									@endif
									@else
									@if($flag == 'pendiente')
									<a class="btn btn-success" href="{{ URL::to('/administracion/cotizaciones/'.$venta->id.'/activate/0/vista' ) }}"><i class="fa fa-check"></i> Activar</a>
									@else
									<a class="btn btn-success" href="{{ URL::to('/administracion/cotizaciones/'.$venta->id.'/activate/1/vista' ) }}"><i class="fa fa-check"></i> Activar</a>
									@endif
									@endif
								</td>
							</tr>
							@endforeach
							@endif
							@else
							@endif
						</tbody>
					</table>
					@if(isset($ventas))
					{{ $ventas->render() }}
					@endif
					@include('alerts.success')
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div id="myModal" class="modal fade" role="dialog">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header center-block">
			
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title text-center">Informaci&oacute;n r&aacute;pida de la cotizaci&oacute;n</h4>
		</div>
		<div class="modal-body" id="infoVenta">
			<!-- CONTENIDO DEL MODAL -->
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		</div>
	</div>
</div>
</div>
@if(!isset($ventas))
<script type="text/javascript">
	template = "<div class='alert alert-danger alert-dismissable' id='alert-msg1'><strong>No se encontraron resultados</strong></div>"
	$("#tabla1").after(template)
</script>
@endif
<script type="text/javascript">
	$("#btnBuscar1").on('click',function(){
	if($("#searchField1").val() == ""){
		toastr.options = {
						"closeButton": true,
						"debug": false,
						"newestOnTop": false,
						"progressBar": false,
						"positionClass": "toast-top-center",
						"preventDuplicates": true,
						"onclick": null,
						"showDuration": "300",
						"hideDuration": "1000",
						"timeOut": "5000",
						"extendedTimeOut": "1000",
						"showEasing": "swing",
						"hideEasing": "linear",
						"showMethod": "fadeIn",
						"hideMethod": "fadeOut"
						};
							toastr.info('Ingrese alg&uacute;n dato para realizar la b&uacute;squeda !');
						return false;
							}
							return true;
						});

	$('#estado1').on('change',function(){
		var estado = $('#estado1').val()
		var filtro = {!! json_encode($flag) !!}
		var filtro_value = -1;
		if(filtro == 'pendiente') filtro_value = 0
		if(filtro == 'finalizado') filtro_value = 1
		$("#successMsg1").html(" ");
		$("#deleteMsg1").html(" ");
		$("#alert-msg1").remove();
		var target = document.getElementById('resultados1')
		$.get('/administracion/cotizaciones/estado/'+estado+'/filtro/'+filtro_value+'',function(response){
			var ventas = response
			var template = ''
			target.innerHTML = ''
			var url_edit = '/administracion/cotizaciones/'
			var url_destroy = '/administracion/cotizaciones/'
			var url_activate = '/administracion/cotizaciones/'
			for(i = 0; i < ventas.length; i++){
				var url_detalle = '/administracion/mostrar-cotizacion-principal/show/'+ventas[i].id
				template += '<tr><td data-toggle="modal" data-target="#myModal" data-id="'+ventas[i].id+'"><input class="dataVenta" name="nombre_c" type="hidden" value="'+ventas[i].nombre_c+'"><input class="dataVenta" name="apellidos_c" type="hidden" value="'+ventas[i].apellidos_c+'"><input class="dataVenta" name="nombre_u" type="hidden" value="'+ventas[i].nombre_u+'"><input class="dataVenta" name="apellidos_u" type="hidden" value="'+ventas[i].apellidos_u+'"><input class="dataVenta" name="marca_r" type="hidden" value="'+ventas[i].marca_r+'"><input class="dataVenta" name="modelo_r" type="hidden" value="'+ventas[i].modelo_r+'"><input class="dataVenta" name="anio_r" type="hidden" value="'+ventas[i].anio_r+'"><input class="dataVenta" name="detalle_r" type="hidden" value="'+ventas[i].detalle_r+'"><input class="dataVenta" name="nombre_m" type="hidden" value="'+ventas[i].nombre_m+'"><input class="dataVenta" name="created_at" type="hidden" value="'+ventas[i].created_at+'">'+ventas[i].id+'</td><td data-toggle="modal" data-target="#myModal" data-id="'+ventas[i].id+'"><input class="dataVenta" name="nombre_c" type="hidden" value="'+ventas[i].nombre_c+'"><input class="dataVenta" name="apellidos_c" type="hidden" value="'+ventas[i].apellidos_c+'"><input class="dataVenta" name="nombre_u" type="hidden" value="'+ventas[i].nombre_u+'"><input class="dataVenta" name="apellidos_u" type="hidden" value="'+ventas[i].apellidos_u+'"><input class="dataVenta" name="marca_r" type="hidden" value="'+ventas[i].marca_r+'"><input class="dataVenta" name="modelo_r" type="hidden" value="'+ventas[i].modelo_r+'"><input class="dataVenta" name="anio_r" type="hidden" value="'+ventas[i].anio_r+'"><input class="dataVenta" name="detalle_r" type="hidden" value="'+ventas[i].detalle_r+'"><input class="dataVenta" name="nombre_m" type="hidden" value="'+ventas[i].nombre_m+'"><input class="dataVenta" name="created_at" type="hidden" value="'+ventas[i].created_at+'">'+ventas[i].created_at.substring(0, 10)+'</td><td data-toggle="modal" data-target="#myModal" data-id="'+ventas[i].id+'"><input class="dataVenta" name="nombre_c" type="hidden" value="'+ventas[i].nombre_c+'"><input class="dataVenta" name="apellidos_c" type="hidden" value="'+ventas[i].apellidos_c+'"><input class="dataVenta" name="nombre_u" type="hidden" value="'+ventas[i].nombre_u+'"><input class="dataVenta" name="apellidos_u" type="hidden" value="'+ventas[i].apellidos_u+'"><input class="dataVenta" name="marca_r" type="hidden" value="'+ventas[i].marca_r+'"><input class="dataVenta" name="modelo_r" type="hidden" value="'+ventas[i].modelo_r+'"><input class="dataVenta" name="anio_r" type="hidden" value="'+ventas[i].anio_r+'"><input class="dataVenta" name="detalle_r" type="hidden" value="'+ventas[i].detalle_r+'"><input class="dataVenta" name="nombre_m" type="hidden" value="'+ventas[i].nombre_m+'"><input class="dataVenta" name="created_at" type="hidden" value="'+ventas[i].created_at+'">'+ventas[i].nombre_c+'</td><td data-toggle="modal" data-target="#myModal" data-id="'+ventas[i].id+'"><input class="dataVenta" name="nombre_c" type="hidden" value="'+ventas[i].nombre_c+'"><input class="dataVenta" name="apellidos_c" type="hidden" value="'+ventas[i].apellidos_c+'"><input class="dataVenta" name="nombre_u" type="hidden" value="'+ventas[i].nombre_u+'"><input class="dataVenta" name="apellidos_u" type="hidden" value="'+ventas[i].apellidos_u+'"><input class="dataVenta" name="marca_r" type="hidden" value="'+ventas[i].marca_r+'"><input class="dataVenta" name="modelo_r" type="hidden" value="'+ventas[i].modelo_r+'"><input class="dataVenta" name="anio_r" type="hidden" value="'+ventas[i].anio_r+'"><input class="dataVenta" name="detalle_r" type="hidden" value="'+ventas[i].detalle_r+'"><input class="dataVenta" name="nombre_m" type="hidden" value="'+ventas[i].nombre_m+'"><input class="dataVenta" name="created_at" type="hidden" value="'+ventas[i].created_at+'">'+ventas[i].apellidos_c+'</td><td data-toggle="modal" data-target="#myModal" data-id="'+ventas[i].id+'"><input class="dataVenta" name="nombre_c" type="hidden" value="'+ventas[i].nombre_c+'"><input class="dataVenta" name="apellidos_c" type="hidden" value="'+ventas[i].apellidos_c+'"><input class="dataVenta" name="nombre_u" type="hidden" value="'+ventas[i].nombre_u+'"><input class="dataVenta" name="apellidos_u" type="hidden" value="'+ventas[i].apellidos_u+'"><input class="dataVenta" name="marca_r" type="hidden" value="'+ventas[i].marca_r+'"><input class="dataVenta" name="modelo_r" type="hidden" value="'+ventas[i].modelo_r+'"><input class="dataVenta" name="anio_r" type="hidden" value="'+ventas[i].anio_r+'"><input class="dataVenta" name="detalle_r" type="hidden" value="'+ventas[i].detalle_r+'"><input class="dataVenta" name="nombre_m" type="hidden" value="'+ventas[i].nombre_m+'"><input class="dataVenta" name="created_at" type="hidden" value="'+ventas[i].created_at+'">'+ventas[i].nombre_u+' '+ventas[i].apellidos_u+'</td><td align="center"><div><a class="btn btn-info" href="'+url_detalle+'" data-tooltip="Ver detalle"><i class="fa fa-eye"></i> Detalle</a>&nbsp;<a class="btn btn-accent" href="'+url_edit+ventas[i].id+'/edit"><i class="fa fa-pencil"></i> Editar</a>'
				if(estado == 1){
					var msg = "Estas seguro de eliminar?"
					template += '&nbsp;<a class="btn btn-danger" href="'+url_destroy+ventas[i].id+'/destroy/'+filtro_value+'/vista" onclick="return confirm('+msg+')"><i class="fa fa-times"></i>Desactivar</a></div></td></tr>'
				}else{
					template += '&nbsp;<a class="btn btn-success" href="'+url_activate+ventas[i].id+'/activate/'+filtro_value+'/vista"><i class="fa fa-check"></i> Activar</a></div></td></tr>'
				}
			}
			target.innerHTML = template;
		});
	});
	$('#myModal').on('show.bs.modal', function (event) {
		    //var getIdFromRow = $(event.relatedTarget).attr('data-id');
		    // if you wnat to take the text of the first cell  
		    //var compania = $(event.relatedTarget).find('td:nth-child(1)').html();
		    //var contacto = $(event.relatedTarget).find('td:nth-child(2)').text();
		    var data = Array()
		    $(event.relatedTarget).find(".dataVenta").each(function(i) {
		        //console.log(String(this.value))
		        data[i] += String(this.value)
	    	}); 
		marca = (data[4].replace('undefined','') == "" || data[4].replace('undefined','') == "null") ? "Sin repuestos" : data[4].replace('undefined','')
		modelo = (data[5].replace('undefined','') == "" || data[5].replace('undefined','') == "null") ? "Sin repuestos" : data[5].replace('undefined','')
		anio = (data[6].replace('undefined','') == "" || data[6].replace('undefined','') == "null") ? "Sin repuestos" : data[6].replace('undefined','')
		detalles = (data[7].replace('undefined','') == "" || data[7].replace('undefined','') == "null") ? "Sin repuestos" : data[7].replace('undefined','')
		nombre_m = (data[8].replace('undefined','') == "" || data[8].replace('undefined','') == "null") ? "Sin mercaderias" : data[8].replace('undefined','') 
	    var template2 = '<div class="row"><div class="col-md-5"><strong class="c-white">Nombre cliente: </strong></div><div class="col-sm-7">'+data[0].replace('undefined','')+' '+data[1].replace('undefined','')+'</div></div><div class="row"><div class="col-md-5"><strong class="c-white">Nombre responsable: </strong></div><div class="col-md-7">'+data[2].replace('undefined','')+' '+data[3].replace('undefined','')+'</div></div><div class="row"><div class="col-md-5"><strong class="c-white">Marcas de los repuestos: </strong></div><div class="col-md-7">'+marca+'</div></div><div class="row"><div class="col-md-5"><strong class="c-white">Modelos de los repuestos: </strong></div><div class="col-md-7">'+modelo+'</div></div><div class="row"><div class="col-md-5"><strong class="c-white">A&ntilde;o de los repuestos: </strong></div><div class="col-md-7">'+anio+'</div></div><div class="row"><div class="col-md-5"><strong class="c-white">Detalles de los repuestos: </strong></div><div class="col-md-7">'+detalles+'</div></div><div class="row"><div class="col-md-5"><strong class="c-white">Productos varios: </strong></div><div class="col-md-7">'+nombre_m+'</div></div><div class="row"><div class="col-md-5"><strong class="c-white">Fecha de la cotizaci&oacute;n: </strong></div><div class="col-md-7">'+data[9].replace('undefined','')+'</div></div>'

	    $(this).find('#infoVenta').html($(template2))
	})
</script>
@endsection