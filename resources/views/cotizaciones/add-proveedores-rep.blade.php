@extends('layouts.principal')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-filled" >
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-plane"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Agregar proveedores</h3>
						<small>
						Cotizaci&oacute;n de repuesto
						</small>
					</div>
				</div>
				<div class="panel-body">
					{!! Form::open(['route'=>'agregar-proveedores-rep.store','method'=>'POST','class'=>'form-group',"onsubmit"=>"return checkSubmit();"]) !!}
					{!!Form::hidden('usuarios_id', Auth::user()->id)!!}
					{!!Form::hidden('venta_id', $venta_id)!!}
					{!!Form::hidden('repuestos_id', $cotizacion_repuesto->repuesto_id)!!}
					{!! Form::hidden('cotizacion_repuestos_id',$cotizacion_repuesto->id_cot) !!}
					<div class="container-fluid">
						<div class="row">
							<div class="col-md-4">
								<h4 class="page-header">Datos del repuesto<a href="{!! route('repuestos.edit',$parameters = $cotizacion_repuesto->repuesto_id) !!}"><i class="fa fa-edit pull-right"></i></a></h4>
								<div class="panel">
										<p><strong class="c-white">Marca: </strong>
										{{ $cotizacion_repuesto->marca_r }}</p>
										<p><strong class="c-white">Modelo: </strong>
										{{ $cotizacion_repuesto->modelo_r }}</p>
										<p><strong class="c-white">Año: </strong>
										{{ $cotizacion_repuesto->anio_r }}</p>
										<p><strong class="c-white">VIN: </strong>
										{{ $cotizacion_repuesto->vin_r }}</p>
										<p><strong class="c-white">Detalle: </strong>
										{{ $cotizacion_repuesto->detalle_r }}</p>
										<p><strong class="c-white">C&oacute;digo: </strong>
										{{ $cotizacion_repuesto->codigo_repuesto }}</p>
										<p><strong class="c-white">Cantidad: </strong>
										{{ $cotizacion_repuesto->cantidad }}</p>
								</div>
							</div>
							<div class="col-lg-8">
								<h4 class="page-header">Opciones de agregado</h4>
								<div class="tabs-container">
									<ul class="nav nav-tabs">
										<li class="active"><a data-toggle="tab" href="#tab-3" aria-expanded="true"> <i class="fa fa-list"></i></a></li>
										<li class=""><a data-toggle="tab" href="#tab-4" aria-expanded="false"><i class="fa fa-search"></i></a></li>
									</ul>
									<div class="tab-content">
										<div id="tab-3" class="tab-pane active">
											<div class="panel-body">
												<strong class="c-white">Elija un proveedor de la lista din&aacute;mica y presione Agregar </strong><br/><br/>
												<div class="container-fluid"><div class="col-md-10">{!!Form::select("tipo_proveedores_id", $tipo_proveedores,null,["id"=>"tipo_proveedores_id","class"=>"form-control","placeholder"=>"Seleccione una opcion.."])!!}</div><div class="col-md-10">{!!Form::select("proveedores_id",["placeholder"=>"Seleccione una opcion.."],null,["id"=>"proveedores_id","class"=>"form-control"])!!}</div><button type="button" class="btn btn-success addFromList1">Agregar</button></div>
											</div>
										</div>
										<div id="tab-4" class="tab-pane">
											<div class="panel-body">
												<strong class="c-white">Buscar un proveedor</strong>
												<p>Ingrese los datos del proveedor y presione <b>Buscar</b></p>
												<div class="row"><div class="col-md-12"></div></div><div class="row"><div class="col-md-10">{!! Form::text("search",null,["class"=>"form-control","placeholder"=>"Buscar..","id"=>"searchField1"]) !!}</div><div class="col-md-2"><button type="button" class="btn btn-accent doSearch1">Buscar</button>&nbsp;</div></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 searchContent1">
						<div class="panel panel-filled panel-c-info">
							<div class="panel-heading">
								<div class="panel-tools">
									<a class="panel-toggle"><i class="fa fa-chevron-up"></i></a>
								</div>
								<strong class="c-white">Resultados de la b&uacute;squeda</strong>
							</div>
							<div class="panel-body" id="searchData1">
								<!-- SEARCH RESULTS -->
							</div>
						</div>
					</div>
					<div class="option-container" id="proveedoresDiv"></div>
					<!-- FILLING -->
					@if(count($proveedores_cotizacion) > 0)
					@for($i = 0; $i < count($proveedores_cotizacion); $i++)
					<div class="option-container">
						<div class="panel-heading">
							<input type="hidden" name="id_cot_p_rep[]" value="{{ $proveedores_cotizacion[$i]->proveedores_cot_id }}">
							<input type="hidden" name="target1" value="repuesto">
							<h4 class="page-header">Datos del proveedor </h4>
							<span opcion-id-cot-p-r="{{$proveedores_cotizacion[$i]->proveedores_cot_id}}" class="input-group-btn">
								<button type="button" class="btn btn-danger btn-xs pull-right btn-remove-proveedor-edit" type="button" data-tooltip="Quitar elemento"><i class="fa fa-trash-o"></i>
								<span class="bold"> Quitar</span>
							</button></span>
						</div><br/>
						<div class="row">
							<div class="col-md-2">
								<b>Tipo de proveedor:</b>
								<input class="form-control m-t-sm"type="text" name="tipo_proveedor_old" value="{{ $proveedores_cotizacion[$i]->tipo }}"readonly>
								<input type="hidden" name="tipos_p_id_old[]" value="{{ $proveedores_cotizacion[$i]->tipos_p_id }}">
							</div>
							<div class="col-md-2">
								<b>Proveedor:</b>
								<input class="form-control m-t-sm" type="text" name="nombre_compania_p_old" value="{{ $proveedores_cotizacion[$i]->nombre_compania_p }}"  readonly >
								<input type="hidden" name="proveedores_old[]" value="{{ $proveedores_cotizacion[$i]->proveedores_id }}">
							</div>
							<div class="col-md-2">
								<b>Persona contacto:</b>
								<input class="form-control m-t-sm" type="text" name="persona_contacto_p_old" value="{{ $proveedores_cotizacion[$i]->persona_contacto_p }}"readonly >
							</div>
							<div class="col-md-2">
								<b>Tel&eacute;fono:</b>
								<input class="form-control m-t-sm" type="text" name="telefono_p_old" value="{{ $proveedores_cotizacion[$i]->telefono_p }}"readonly >
							</div>
							<div class="col-md-2">
								<b>Celular:</b>
								<input class="form-control m-t-sm" type="text" name="celular_p_old" value="{{ $proveedores_cotizacion[$i]->celular_p }}"readonly >
							</div>
							<div class="col-md-2">
								<b>Precio compra:</b>
								<input class="form-control m-t-sm" type="text" name="precio_cot_old[]" value="{{ $proveedores_cotizacion[$i]->precio_cot_rep }}" >
							</div>
						</div>
					</div>
					@endfor
					@endif
					<!-- FILLING -->
				</div>
			</div>
			@include('alerts.success')
			@include('alerts.validation')
		</div>
		
		<script type="text/javascript">
		var proveedores_selecionados = Array()
		$(document).on('click','.addFromList1',function(){
			var tipo_proveedor_id = $("[name=tipo_proveedores_id]").val();
			var proveedor_id = $("[name=proveedores_id]").val();
			if("placeholder" == proveedor_id){
				toastr.options = {
		                        "closeButton": true,
		                        "debug": false,
		                        "newestOnTop": false,
		                        "progressBar": false,
		                        "positionClass": "toast-top-center",
		                        "preventDuplicates": true,
		                        "onclick": null,
		                        "showDuration": "300",
		                        "hideDuration": "1000",
		                        "timeOut": "5000",
		                        "extendedTimeOut": "1000",
		                        "showEasing": "swing",
		                        "hideEasing": "linear",
		                        "showMethod": "fadeIn",
		                        "hideMethod": "fadeOut"
		                      };
		        			toastr.warning('Por favor seleccione un elemento de la lista');
			}
			if("#" == proveedor_id){
				toastr.options = {
		                        "closeButton": true,
		                        "debug": false,
		                        "newestOnTop": false,
		                        "progressBar": false,
		                        "positionClass": "toast-top-center",
		                        "preventDuplicates": true,
		                        "onclick": null,
		                        "showDuration": "300",
		                        "hideDuration": "1000",
		                        "timeOut": "5000",
		                        "extendedTimeOut": "1000",
		                        "showEasing": "swing",
		                        "hideEasing": "linear",
		                        "showMethod": "fadeIn",
		                        "hideMethod": "fadeOut"
		                      };
		        			toastr.error('<b>No existen proveedores para agregar !</b> Por favor intente con otro tipo de proveedor');
			}else{
				if($.inArray(proveedor_id, proveedores_selecionados) >= 0){
					toastr.options = {
		                        "closeButton": true,
		                        "debug": false,
		                        "newestOnTop": false,
		                        "progressBar": false,
		                        "positionClass": "toast-top-center",
		                        "preventDuplicates": true,
		                        "onclick": null,
		                        "showDuration": "300",
		                        "hideDuration": "1000",
		                        "timeOut": "5000",
		                        "extendedTimeOut": "1000",
		                        "showEasing": "swing",
		                        "hideEasing": "linear",
		                        "showMethod": "fadeIn",
		                        "hideMethod": "fadeOut"
		                      };
		        	toastr.error('<b>Este proveedor ya esta seleccionado !');
				}else{
					if(tipo_proveedor_id.length && proveedor_id.length ){
						var target = document.getElementById("proveedoresDiv");
						var template1 = '';
						var template2 = '';
						$.get('/administracion/tipo-proveedor/'+tipo_proveedor_id+'', function(response){
							var data1 = response.response;
							
						$.get('/administracion/proveedor/'+proveedor_id+'', function(response){
							
							var data2 = response.response;
							//alert(data[0].nombre_compania_p)
							template1 += '<br/><div class="option-container"><div class="panel-heading"><h4 class="page-header">Datos del proveedor </h4><span proveedor-id="'+data2[0].id+'" class="input-group-btn"><button type="button" class="btn btn-danger btn-xs pull-right btn-remove-proveedor" type="button" data-tooltip="Quitar elemento"><i class="fa fa-trash-o"></i><span class="bold"> Quitar</span></button></span></div><br/><div class="row"><div class="col-md-2"><b>Tipo de proveedor:</b><input class="form-control m-t-sm"type="text" name="tipo_proveedor" value="'+data1[0].tipo+'"readonly><input type="hidden" name="tipos_p_id[]" value="'+data1[0].id+'"></div>';

							template2 = template1 + '<div class="col-md-2"><b>Proveedor:</b><input type="hidden" name="proveedores[]" value="'+data2[0].id+'"><input class="form-control m-t-sm" type="text" name="nombre_compania_p" value="'+data2[0].nombre_compania_p+'"  readonly ></div><div class="col-md-2"><b>Persona contacto:</b><input class="form-control m-t-sm" type="text" name="persona_contacto_p" value="'+data2[0].persona_contacto_p+'"readonly ></div><div class="col-md-2"><b>Tel&eacute;fono:</b><input class="form-control m-t-sm" type="text" name="telefono_p" value="'+data2[0].telefono_p+'"readonly ></div><div class="col-md-2"><b>Celular:</b><input class="form-control m-t-sm" type="text" name="celular_p" value="'+data2[0].celular_p+'"readonly ></div><div class="col-md-2"><b>Precio compra ($):</b><input class="form-control m-t-sm" type="text" name="precio_cot[]"required ></div></div></div>'
							//target.innerHTML = target.innerHTML + template1;
							$("#proveedoresDiv").after(template2);
							proveedores_selecionados.push(proveedor_id)
						});
						});
					}
				}
			}
		});

		$(document).on('click','.btn-remove-proveedor-edit',function(e){
			e.preventDefault();
			if(confirm('Está seguro de eliminar? Esta opción NO se puede deshacer !')){
				var id = $(this).parent('span').attr('opcion-id-cot-p-r');
				var form = $('#form-delete-cot-p-r');
				var url = form.attr('action').replace(':OPTION_ID_P_R',id);
				var data = form.serialize();
				$.post(url, data, function(result){
				//alert(result);
				});
				$(this).parents('.option-container').remove();
			}
		});
		$(document).on('click','.btn-remove-proveedor',function(e){
			e.preventDefault();
			var proveedor_id = $(this).parent('span').attr('proveedor-id');
			var index = $.inArray(proveedor_id, proveedores_selecionados)
			if (index > -1) {
	   			proveedores_selecionados.splice(index, 1);
	   			console.log(proveedores_selecionados)
			}
			$(this).parents('.option-container').remove();
		});
		$(document).ready(function(){
			$('.searchContent1').hide();
			$(document).on('click','.doSearch1',function(){
			var search_value = $('#searchField1').val()
			if("" != search_value){
				$.get('/administracion/proveedores/search/'+search_value+'', function(response){
						if(response.response.length > 0){
							console.log(response)
							$('.searchContent1').show();
							var search_content = document.getElementById('searchData1')
							search_content.innerHTML = ''
							var table = '<div class="table-responsive"><table class="table table-hover table-striped" id="tableSearch1"><thead><tr role="row"><th class="sorting_asc left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" style="width: 15px;">ID</th><th class="sorting_asc left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" style="width: 172px;">Compañia</th><th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"aria-label="Browser: activate to sort column ascending" style="width: 100px;">Contacto</th><th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"aria-label="Browser: activate to sort column ascending" style="width: 80px;">Tipo</th><th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"aria-label="Browser: activate to sort column ascending" style="width: 80px;">Marca</th><th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"aria-label="Platform(s): activate to sort column ascending" style="width: 85px;">Tel&eacute;fono</th><th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"aria-label="Engine version: activate to sort column ascending" style="width: 85px;">Celular</th><th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"aria-label="CSS grade: activate to sort column ascending" style="width: 100px;">Selecci&oacute;n</th></tr></thead><tbody>'
							var data = ''
							for(i = 0; i < response.response.length; i++){
								data += '<tr>'
											data += '<td class="id_prov">'+response.response[i].id+'</td>'
											data += '<td class="nombre_compania_p">'+response.response[i].nombre_compania_p+'</td>'
											data += '<td class="persona_contacto_p">'+response.response[i].persona_contacto_p+'</td>'
											data += '<td class="tipo">'+response.response[i].tipo.substring(0,25)+'</td>'
											data += '<td class="marca_prov">'+response.response[i].marca_prov.substring(0,25)+'</td>'
											data += '<td class="telefono_p">'+response.response[i].telefono_p+'</td>'
											data += '<td class="celular_p">'+response.response[i].celular_p+'</td>'
											data += '<td class="center-block"><button type="button" class="btn btn-success addFromSearch1">Agregar</button></td></tr>'
							}
							data += '</table></div>'
							var busqueda_temp = table + data
							search_content.innerHTML = ''
							search_content.innerHTML = busqueda_temp
							toastr.options = {
		                        "closeButton": true,
		                        "debug": false,
		                        "newestOnTop": false,
		                        "progressBar": false,
		                        "positionClass": "toast-top-center",
		                        "preventDuplicates": true,
		                        "onclick": null,
		                        "showDuration": "300",
		                        "hideDuration": "1000",
		                        "timeOut": "5000",
		                        "extendedTimeOut": "1000",
		                        "showEasing": "swing",
		                        "hideEasing": "linear",
		                        "showMethod": "fadeIn",
		                        "hideMethod": "fadeOut"
		                      };
		        			toastr.success('<b>Se han encontrado '+response.response.length+' resultados !</b> Presione "Agregar" para seleccionar el proveedor');
						}else{
							toastr.options = {
		                        "closeButton": true,
		                        "debug": false,
		                        "newestOnTop": false,
		                        "progressBar": false,
		                        "positionClass": "toast-top-center",
		                        "preventDuplicates": true,
		                        "onclick": null,
		                        "showDuration": "300",
		                        "hideDuration": "1000",
		                        "timeOut": "5000",
		                        "extendedTimeOut": "1000",
		                        "showEasing": "swing",
		                        "hideEasing": "linear",
		                        "showMethod": "fadeIn",
		                        "hideMethod": "fadeOut"
		                      };
		        			toastr.error('<b>No se han encontrado resultados !</b> Verifique los datos ingresados e intentelo nuevamente');
						}
				});
			}
		});
		});
		
		$(document).on('click','.addFromSearch1',function() {
    		var $row = $(this).closest("tr");    // Busca el tr
    		var id_prov = $row.find(".id_prov").text();
    		var nombre_compania_p = $row.find(".nombre_compania_p").text(); // Busca el texto
    		var persona_contacto_p = $row.find(".persona_contacto_p").text();
    		var tipo = $row.find(".tipo").text();
    		var telefono_p = $row.find(".telefono_p").text();
    		var celular_p = $row.find(".celular_p").text();
    		var template_search_prov = '<br/><div class="option-container"><div class="panel-heading"><h4 class="page-header">Datos del proveedor </h4><span proveedor-id="'+id_prov+'" class="input-group-btn"><button type="button" class="btn btn-danger btn-xs pull-right btn-remove-proveedor" type="button" data-tooltip="Quitar elemento"><i class="fa fa-trash-o"></i><span class="bold"> Quitar</span></button></span></div><br/><div class="row"><div class="col-md-2"><b>Tipo de proveedor:</b><input class="form-control m-t-sm"type="text" name="tipo_proveedor" value="'+tipo+'"readonly><input type="hidden" name="tipos_p_id[]" value="'+id_prov+'"></div><div class="col-md-2"><b>Proveedor:</b><input class="form-control m-t-sm" type="text" name="nombre_compania_p" value="'+nombre_compania_p+'"readonly ><input type="hidden" name="proveedores[]" value="'+id_prov+'"></div><div class="col-md-2"><b>Persona contacto:</b><input class="form-control m-t-sm" type="text" name="persona_contacto_p" value="'+persona_contacto_p+'"readonly ></div><div class="col-md-2"><b>Tel&eacute;fono:</b><input class="form-control m-t-sm" type="text" name="telefono_p" value="'+telefono_p+'"readonly ></div><div class="col-md-2"><b>Celular:</b><input class="form-control m-t-sm" type="text" name="celular_p" value="'+celular_p+'"readonly ></div><div class="col-md-2"><b>Precio compra:</b><input class="form-control m-t-sm" type="text" name="precio_cot[]"required ></div></div></div>';
    		if($.inArray(id_prov, proveedores_selecionados) >= 0){
					toastr.options = {
		                        "closeButton": true,
		                        "debug": false,
		                        "newestOnTop": false,
		                        "progressBar": false,
		                        "positionClass": "toast-top-center",
		                        "preventDuplicates": true,
		                        "onclick": null,
		                        "showDuration": "300",
		                        "hideDuration": "1000",
		                        "timeOut": "5000",
		                        "extendedTimeOut": "1000",
		                        "showEasing": "swing",
		                        "hideEasing": "linear",
		                        "showMethod": "fadeIn",
		                        "hideMethod": "fadeOut"
		                      };
		        	toastr.error('<b>Este proveedor ya esta seleccionado !');
				}else{
    				$("#proveedoresDiv").after(template_search_prov)
    				proveedores_selecionados.push(id_prov)
    			}
    		
		});
		// $.ajax({
		//        type: 'GET',
		//        url: '/proveedor/'+proveedor_id+'',
		//        dataType: "json", //add this line
		//        success: function(data){ //only the data object passed to success handler with JSON dataType
													//        	var resp = data.data;
		//            console.log(resp);
		//            //console.log(data.startDate);
		//            //console.log(data[startDate]);
		//            //console.log(data['startDate']);
		//        },
		//        error: function(){
		//            alert('There has been an error, please consult the application developer.');
		//        }
		//    });
		</script>
		<div class="row">
			<div class="form-group">
				<div class="col-md-7" >
					{!!Form::submit('Guardar',['class'=>'btn btn-w-md btn-accent'])!!} <a class="btn btn-w-md btn-default" href="{{ url('/administracion/mostrar-cotizacion-principal/show/'.$venta_id ) }}">Cancelar</a>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
{!! Html::script('js/proveedores-dropdown.js') !!}
{!! Html::script('js/proveedores-add-fields.js') !!}
{!!Form::open(['route'=>['agregar-proveedores-rep.destroy',':OPTION_ID_P_R'],'method'=>'DELETE','id'=>'form-delete-cot-p-r'])!!}
{!!Form::close()!!}
@endsection
@section('scripts')
@endsection