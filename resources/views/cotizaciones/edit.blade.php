@extends('layouts.principal')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-filled">
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-note2"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Actualizar datos de la cotizaci&oacute;n</h3>
						<small>
						Cotizaci&oacute;n de repuestos/mercader&iacute;as
						</small>
					</div>
				</div>
				<div class="panel-body">
					{!!Form::model($venta,['route'=>['cotizaciones.update',$venta->id],'method'=>'PUT',"onsubmit"=>"return validateForm();"])!!}
					{!!Form::hidden('usuarios_id', Auth::user()->id)!!}
					{!!Form::hidden('cliente_id', $cliente->id,['id'=>'clientes_id'])!!}
					{!! Form::hidden('ventas_id',$venta->id) !!}
					<div class="row">
						<div class="panel-heading">
							<h4 class="page-header">Datos del cliente</h4>
						</div>
						<div class="col-lg-5">
							<div class="form-group">
								{!! Form::label('nombre_c','Nombres: ',['class'=>'control-label']) !!}
								{!! Form::text('nombre_c',$cliente->nombre_c,['class'=>'form-control','placeholder'=>'Ingrese el nombre del cliente','required','min'=>5,'readonly']) !!}<br/>
							</div>
						</div>
						<div class="col-lg-5">
							<div class="form-group">
								{!! Form::label('apellidos_c','Apellidos: ',['class'=>'control-label']) !!}
								{!! Form::text('apellidos_c',$cliente->apellidos_c,['class'=>'form-control','placeholder'=>'Ingrese el apellido del cliente','required','min'=>5,'readonly']) !!}<br/>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-5">
							<div class="form-group">
								{!! Form::label('direccion_c','Direcci&oacute;n: ',['class'=>'control-label']) !!}
								{!! Form::text('direccion_c',$cliente->direccion_c,['class'=>'form-control','placeholder'=>'Ingrese la direccion del cliente','min'=>5]) !!}<br/>
							</div>
						</div>
						<div class="col-lg-5">
							<div class="form-group" id="alertTelf1_edit">
								{!! Form::label('telefono_c','Tel&eacute;fono: ',['class'=>'control-label']) !!}
								{!! Form::text('telefono_c',$cliente->telefono_c,['class'=>'form-control','placeholder'=>'Ingrese el telefono del cliente','min'=>5]) !!}<br/>
							</div>
						</div>
						<div class="col-lg-2">
							&nbsp;
						</div>
					</div>
					<div class="row">
						<div class="col-lg-5">
							<div class="form-group">
								{!! Form::label('email_c','E-mail: ',['class'=>'control-label']) !!}
								{!! Form::text('email_c',$cliente->email_c,['class'=>'form-control','placeholder'=>'Ingrese el e-mail del cliente','min'=>5]) !!}<br/>
							</div>
						</div>
						<div class="col-lg-5">
							<div class="form-group" id="alertCel1_edit">
								{!! Form::label('celular_c','Celular: ',['class'=>'control-label']) !!}
								{!! Form::text('celular_c',$cliente->celular_c,['class'=>'form-control','placeholder'=>'Ingrese el celular del cliente','required','min'=>5,'id'=>'celular_c1_edit']) !!}<br/>
							</div>
						</div>
						<div class="col-lg-2">
							&nbsp;
						</div>
						<div class="col-lg-2" id="checkCel1_edit"></div>
						
					</div>
					<div class="row">
						<div class="col-lg-5">
							<div class="form-group">
								{!! Form::label('origen_contacto','Origen contacto: ',['class'=>'control-label']) !!}<br/>
								<select name="origen_contacto" class="form-control">
									@foreach($origen_contacto as $key => $value)
									<option value="{{ $key }}">{{ $value }}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-lg-5">
							<div class="form-group">
								{!! Form::label('pais','Localidad: ',['class'=>'control-label']) !!}
								{!! Form::select('pais',$paises,null,['class'=>'form-control']) !!}<br>
							</div>
						</div>
					</div>
					@include('alerts.validation')
					<div class="row" id="begin1">
						<div class="col-lg-2" >
							<div class="form-group" >
								<button class="btn btn-success btn-add-repuesto"><i class="fa fa-plus-square-o"></i> Agregar repuesto</button>
							</div>
						</div>
						<div class="col-lg-2" >
							<div class="form-group" >
								<button class="btn btn-primary btn-add-mercaderia"><i class="fa fa-plus-square-o"></i> Agregar productos varios</button>
							</div>
						</div>
					</div>
					@if(isset($repuestos_cot))
					@for($i = 0; $i < count($repuestos_cot); $i++)
					<div class="form-group option-container">
						<div class="row">
							<div class="panel-heading">
								<h4 class="page-header">Datos del repuesto </h4>
								@if($venta->pago_parcial != null && $venta->segundo_pago != null)
								<span opcion-id-cot-r="{{$repuestos_cot[$i]->id}}" class="input-group-btn">
									<button class="btn btn-danger btn-remove-repuesto-blocked pull-right" type="button" data-tooltip="Quitar elemento"><i class="fa fa-times"></i> Quitar</button>
								</span>
								@else
								<span opcion-id-cot-r="{{$repuestos_cot[$i]->id}}" class="input-group-btn">
									<button class="btn btn-danger btn-remove-repuesto pull-right" type="button" data-tooltip="Quitar elemento"><i class="fa fa-times"></i> Quitar</button>
								</span>
								@endif
							</div>
						</div>
							{!! Form::hidden('repuestos_id[]',$repuestos_cot[$i]->repuesto_id) !!}
							<div class="row">
								<div class="col-lg-4">
									<div class="form-group">
										{!! Form::label("marca_r","Marca: ",["class"=>"control-label"]) !!}
										<?php 
											$marcas = PaylessAdmin\MarcaModelo::getMarcas_except($repuestos_cot[$i]->marca_r);
											$modelos_marca = PaylessAdmin\MarcaModelo::getModelos_except($repuestos_cot[$i]->marca_r,$repuestos_cot[$i]->modelo_r);
										?>
										<select name="marca_r[]" class="form-control marca_r_1" required>
										  <option value="{{ $repuestos_cot[$i]->marca_r }}">{{ $repuestos_cot[$i]->marca_r }}</option>
										  <?php for($j = 0; $j < count($marcas); $j++){
										  			echo '<option value="'.$marcas[$j]->marca.'">'.$marcas[$j]->marca."</option>";
										  		}
										  ?>
										</select>
										<br/>
									</div>
								</div>

								<div class="col-lg-4">
									<div class="form-group">
										{!! Form::label("modelo_r","Modelo: ",["class"=>"control-label"]) !!}
										<select name="modelo_r[]" class="form-control modelo_r_1" required>
										  <option value="{{ $repuestos_cot[$i]->modelo_r }}">{{ $repuestos_cot[$i]->modelo_r }}</option>
										  <?php for($j = 0; $j < count($modelos_marca); $j++){
										  			echo '<option value="'.$modelos_marca[$j]->modelo.'">'.$modelos_marca[$j]->modelo."</option>";
										  		}
										  ?>
										</select>
										<br/>
									</div>
								</div>
								<div class="col-lg-2">
									<div class="form-group">
										{!! Form::label("anio_r","Año: ",["class"=>"control-label"]) !!}
										{!! Form::text("anio_r[]",$repuestos_cot[$i]->anio_r,["class"=>"form-control","placeholder"=>"Ingrese el año del modelo","required","min"=>5]) !!}<br/>
									</div>
								</div>
								<div class="col-lg-2">
									<div class="form-group">
										{!! Form::label("cantidad_rep","Cantidad: ",["class"=>"control-label"]) !!}
										@if($repuestos_cot[$i]->estado == 0)
										{!! Form::number("cantidad_rep[]",$repuestos_cot[$i]->cantidad,["class"=>"form-control","placeholder"=>"Ingrese la cantidad","required","min"=>1]) !!}<br/>
										@else
										{!! Form::number("cantidad_nan[]",$repuestos_cot[$i]->cantidad,["class"=>"form-control","readonly"]) !!}
										{!! Form::hidden("cantidad_rep[]",$repuestos_cot[$i]->cantidad) !!}<br/>	
										@endif
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-4">
									<div class="form-group">
										{!! Form::label("vin_r","VIN: ",["class"=>"control-label"]) !!}
										{!! Form::text("vin_r[]",$repuestos_cot[$i]->vin_r,["class"=>"form-control","placeholder"=>"Ingrese el VIN del repuesto","min"=>5]) !!}<br/>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										{!! Form::label("detalle_r","Detalle: ",["class"=>"control-label"]) !!}
										{!!Form::textarea("detalle_r[]",$repuestos_cot[$i]->detalle_r,["class"=>"form-control", "placeholder"=>"Detalles","rows" => 3,"required","min"=>20]) !!}<br>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										{!! Form::label("codigo_repuesto","C&oacute;digo de repuesto: ",["class"=>"control-label"]) !!}
										{!!Form::text("codigo_repuesto[]",$repuestos_cot[$i]->codigo_repuesto,["class"=>"form-control", "placeholder"=>"Codigo de repuesto","min"=>20]) !!}<br>
									</div>
								</div>
							</div>
						</div>
						@endfor
						@endif
						@if(isset($mercaderias_cot))
						@for($i = 0; $i < count($mercaderias_cot); $i++)
						{!!Form::model($mercaderias_cot[$i],['route'=>['mercaderias.update',$mercaderias_cot[$i]->id_cot],'method'=>'PUT'])!!}
						<div class="form-group option-container">
							<div class="row">
								<div class="panel-heading">
									<h4 class="page-header">Datos del producto </h4>
									@if($venta->pago_parcial != null && $venta->segundo_pago != null)
									<span opcion-id-cot-m="{{$mercaderias_cot[$i]->id_cot}}" class="input-group-btn">
										<button class="btn btn-danger btn-remove-mercaderia-blocked pull-right" type="button" data-tooltip="Quitar elemento"><i class="fa fa-times"></i> Quitar</button>
									</div>
									@else
									<span opcion-id-cot-m="{{$mercaderias_cot[$i]->id_cot}}" class="input-group-btn">
										<button class="btn btn-danger btn-remove-mercaderia pull-right" type="button" data-tooltip="Quitar elemento"><i class="fa fa-times"></i> Quitar</button>
									</div>
									@endif
								</div>
								<div class="row">
									<div class="col-lg-4">
										<div class="form-group">
											{!! Form::label("nombre_m","Detalle: ",["class"=>"control-label"]) !!}
											{!! Form::text("nombre_m[]",$mercaderias_cot[$i]->nombre_m,["class"=>"form-control","placeholder"=>"Ingrese el nombre de la mercaderia","required","min"=>5]) !!}<br/>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											{!! Form::label("nro_item","N&uacute;mero de &iacute;tem/URL: ",["class"=>"control-label"]) !!}
											{!! Form::text("nro_item[]",$mercaderias_cot[$i]->nro_item,["class"=>"form-control","placeholder"=>"Numero de item","min"=>5]) !!}<br/>
										</div>
									</div>
									<div class="col-lg-2">
										<div class="form-group">
											{!! Form::label("cantidad_mer","Cantidad: ",["class"=>"control-label"]) !!}
											@if($mercaderias_cot[$i]->estado == 0)
											{!! Form::number("cantidad_mer[]",$mercaderias_cot[$i]->cantidad,["class"=>"form-control","placeholder"=>"Ingrese la cantidad","required","min"=>1]) !!}
											@else
											{!! Form::number("cantidad_nan[]",$mercaderias_cot[$i]->cantidad,["class"=>"form-control","readonly"]) !!}
											{!! Form::hidden("cantidad_mer[]",$mercaderias_cot[$i]->cantidad) !!}
											@endif
										</div>
									</div>
								</div>
							</div>
							@endfor
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript">
			var flag_mercaderias = {!! json_encode($flag_mercaderias) !!}
			var flag_repuestos = {!! json_encode($flag_repuestos) !!}
			if(flag_repuestos == 'fail' || flag_mercaderias == 'fail'){
				toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-center",
                                "preventDuplicates": true,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                              };
                            toastr.warning('<b>NOTA:</b> Se ha deshabilitado editar cantidad, porque el precio de venta de algunas mercader&iacute;as y/o repuestos de esta cotizaci&oacute;n, ya se ha calculado.');
			}
		</script>
		@if($msg_trashed == "desactivado")
		<script type="text/javascript">
			toastr.options = {
                            "closeButton": true,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toast-top-center",
                            "preventDuplicates": true,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                          };
                        toastr.warning('<b>ATENCION:</b> Este cliente se encuentra desactivado.');
		</script>
		@endif
		@if($msg_venta_trashed == "desactivado")
		<script type="text/javascript">
				toastr.options = {
		                        "closeButton": true,
		                        "debug": false,
		                        "newestOnTop": false,
		                        "progressBar": false,
		                        "positionClass": "toast-top-center",
		                        "preventDuplicates": true,
		                        "onclick": null,
		                        "showDuration": "300",
		                        "hideDuration": "1000",
		                        "timeOut": "5000",
		                        "extendedTimeOut": "1000",
		                        "showEasing": "swing",
		                        "hideEasing": "linear",
		                        "showMethod": "fadeIn",
		                        "hideMethod": "fadeOut"
		                      };
		        	toastr.warning('<b>ATENCION:</b> Esta cotizaci&oacute;n se encuentra desactivada.');
		</script>
		@endif
		<div class="form-group">
			<div class="col-md-7" >
				{!!Form::submit('Actualizar',['class'=>'btn btn-w-md btn-accent'])!!} 
				@if($venta->estado == 0 && $venta->finalizado == 0)
				<a class="btn btn-w-md btn-default" href="{!! URL::to('/administracion/mostrar-cotizacion-principal/show/'. $venta->id ) !!}">Cancelar</a>
				@else
				<a class="btn btn-w-md btn-default" href="{!! URL::to('/ventas/'. $venta->id.'/detalle' ) !!}">Cancelar</a>
				@endif
			</div>
		</div>
		{!! Form::close() !!}
		{!!Form::open(['route'=>['cotizacion-repuestos.destroy',':OPTION_ID_R'],'method'=>'DELETE','id'=>'form-delete-cot-r'])!!}
		{!!Form::close()!!}
		{!!Form::open(['route'=>['cotizacion-mercaderias.destroy',':OPTION_ID_M'],'method'=>'DELETE','id'=>'form-delete-cot-m'])!!}
		{!!Form::close()!!}
		@endsection
		@section('scripts')
		@include('cotizaciones.dinamic-form-edit')
<script type="text/javascript">
	jQuery(function($){
	   $("#date").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
	   $("#telefono_c_1").mask("999-9999");
	   $("#celular_c1_edit").mask("999-99999");
	   $("#phone").mask("99-9999999");
	   $("#ssn").mask("999-99-9999");
	});
	$(document).ready(function(){
		$(document).on("click",".btn-remove-mercaderia-blocked",function(){
			toastr.options = {
		                        "closeButton": true,
		                        "debug": false,
		                        "newestOnTop": false,
		                        "progressBar": false,
		                        "positionClass": "toast-top-center",
		                        "preventDuplicates": true,
		                        "onclick": null,
		                        "showDuration": "300",
		                        "hideDuration": "1000",
		                        "timeOut": "5000",
		                        "extendedTimeOut": "1000",
		                        "showEasing": "swing",
		                        "hideEasing": "linear",
		                        "showMethod": "fadeIn",
		                        "hideMethod": "fadeOut"
		                      };
		        	toastr.error('<b>Denegado!</b> No puede eliminar mercader&iacute;as pertenecientes a una venta completada !');
		});
		$(document).on("click",".btn-remove-repuesto-blocked",function(){
			toastr.options = {
		                        "closeButton": true,
		                        "debug": false,
		                        "newestOnTop": false,
		                        "progressBar": false,
		                        "positionClass": "toast-top-center",
		                        "preventDuplicates": true,
		                        "onclick": null,
		                        "showDuration": "300",
		                        "hideDuration": "1000",
		                        "timeOut": "5000",
		                        "extendedTimeOut": "1000",
		                        "showEasing": "swing",
		                        "hideEasing": "linear",
		                        "showMethod": "fadeIn",
		                        "hideMethod": "fadeOut"
		                      };
		        	toastr.error('<b>Denegado!</b> No puede eliminar repuestos pertenecientes a una venta completada !');
		});
	});
	 $(document).on({//SIRVE PARA INPUTS CREADOS DINAMICAMENTE PUTO PROBLEMA -.- !
      'focus': function () {
          //hacer algo aca
      },
      'change': function (e) {
          marca_selected = $(this).parents('.option-container').find('.modelo_r_n_1')
          if(e.target.value != null){
		    $.get("/administracion/marcas-modelos/get/"+e.target.value+"",function(response){
		      if(response.length > 0){
		        marca_selected.empty();
		        for(i=0; i<response.length; i++){
		          marca_selected.append("<option value='"+response[i].modelo+"'>"+response[i].modelo+"</option>");
		        }
		      } else {
		        marca_selected.empty();
		        marca_selected.append("<option value='#'>No se encontraron resultados..</option>");
		      }  
		    });
	  	  }
	  	}
    }, '.marca_r_n_1');
	$(document).on({//SIRVE PARA INPUTS CREADOS DINAMICAMENTE PUTO PROBLEMA -.- !
      'focus': function () {
          //hacer algo aca
      },
      'change': function (e) {
          marca_selected = $(this).parents('.option-container').find('.modelo_r_1')	
          if(e.target.value != null){
		    $.get("/administracion/marcas-modelos/get/"+e.target.value+"",function(response){
		      if(response.length > 0){
		        marca_selected.empty();
		        for(i=0; i<response.length; i++){
		          marca_selected.append("<option value='"+response[i].modelo+"'>"+response[i].modelo+"</option>");
		        }
		      } else {
		        marca_selected.empty();
		        marca_selected.append("<option value='#'>No se encontraron resultados..</option>");
		      }  
		    });
	  	  }
	  	}
    }, '.marca_r_1');
</script>
@endsection