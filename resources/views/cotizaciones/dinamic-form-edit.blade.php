<script type="text/javascript">
var vista = {!! json_encode($vista) !!}
var contador_r = {!! json_encode($contador_r) !!}
var contador_m = {!! json_encode($contador_m) !!}
var lim_edit = contador_m + contador_r;

if("edit" == vista){
  $(document).on('click','.btn-add-repuesto',function(e){
    e.preventDefault();
    anio_select = '<select name="anio_r_n[]" class="form-control anio_r_n_1">';
    for(i = 2018; i >= 1990; i--)
      anio_select += '<option value="'+i+'">'+i+'</option>';
    anio_select += '</select>';
    var template = '<div class="form-group option-container"><div class="row"><div class="panel-heading"><h4 class="page-header">Datos del repuesto </h4><button class="btn btn-danger btn-remove-repuesto pull-right" type="button" data-tooltip="Quitar elemento"><i class="fa fa-times"></i> Quitar</button></div></div><div class="row"><div class="col-lg-4"><div class="form-group">{!! Form::label("marca_r_n","Marca: ",["class"=>"control-label"]) !!}{!! Form::select("marca_r_n[]",$marcas_edit,null,["class"=>"form-control marca_r_n_1","placeholder"=>"Seleccione una marca..","required"]) !!}<br/></div></div><div class="col-lg-3"><div class="form-group">{!! Form::label("modelo_r_n","Modelo: ",["class"=>"control-label"]) !!}{!!Form::select("modelo_r_n[]",["placeholder"=>"Seleccione un modelo.."],null,["class"=>"form-control modelo_r_n_1"])!!}<br/></div></div><div class="col-lg-3"><div class="form-group">{!! Form::label("vin_r_n","VIN: ",["class"=>"control-label"]) !!}{!! Form::text("vin_r_n[]",null,["class"=>"form-control vin_r_n_1","placeholder"=>"Ingrese el VIN del repuesto","min"=>5]) !!}<br/></div></div><div class="col-lg-2"><div class="form-group">{!! Form::label("anio_r_n","Año: ",["class"=>"control-label"]) !!}'+anio_select+'<br/></div></div></div><div class="row divDetalles1"><div class="col-lg-4"><div class="form-group">{!! Form::label("detalle_r_n","Detalle: ",["class"=>"control-label"]) !!}{!!Form::text("detalle_r_n[]",null,["class"=>"form-control", "placeholder"=>"Detalles","required","min"=>10]) !!}<br></div></div><div class="col-lg-4"><div class="form-group">{!! Form::label("codigo_repuesto_n","C&oacute;digo del repuesto: ",["class"=>"control-label"]) !!}{!!Form::text("codigo_repuesto_n[]",null,["class"=>"form-control", "placeholder"=>"Ingrese el c&oacute;digo del repuesto","min"=>5]) !!}<br></div></div><div class="col-lg-2"><div class="form-group">{!! Form::label("cantidad_rep_n","Cantidad: ",["class"=>"control-label"]) !!}{!! Form::number("cantidad_rep_n[]",1,["class"=>"form-control","placeholder"=>"Ingrese la cantidad","required","min"=>1]) !!}<br/></div></div><div class="col-lg-2"><div class="form-group">{!! Form::label("boton","Opci&oacute;n: ",["class"=>"control-label"]) !!}<br><a class="btn btn-success btnAddDetalleR1"><i class="fa fa-plus"></i> Agregar</a></div></div></div></div>';
      $("#begin1").after(template);
      lim_edit++; 
  });
$(document).on('click','.btn-add-mercaderia',function(e){
    e.preventDefault();
    var template = '<div class="form-group option-container"><div class="row"><div class="panel-heading"><h4 class="page-header">Datos del producto </h4><button class="btn btn-danger btn-remove-mercaderia pull-right" type="button" data-tooltip="Quitar elemento"><i class="fa fa-times"></i> Quitar</button></div></div><div class="row divDetallesM1"><div class="col-lg-4"><div class="form-group">{!! Form::label("nombre_m_n","Detalle: ",["class"=>"control-label"]) !!}{!! Form::text("nombre_m_n[]",null,["class"=>"form-control nombre_m_n_1","placeholder"=>"Ingrese el detalle","required","min"=>5]) !!}<br/></div></div><div class="col-lg-4"><div class="form-group">{!! Form::label("nro_item_n","N&uacute;mero de &iacute;tem/URL: ",["class"=>"control-label"]) !!}{!! Form::text("nro_item_n[]",null,["class"=>"form-control","placeholder"=>"Ingrese el numero de item o URL ","min"=>5]) !!}<br/></div></div><div class="col-lg-2"><div class="form-group">{!! Form::label("cantidad_mer_n","Cantidad: ",["class"=>"control-label"]) !!}{!! Form::number("cantidad_mer_n[]",1,["class"=>"form-control","placeholder"=>"Ingrese la cantidad","required","min"=>1]) !!}<br/></div></div><div class="col-lg-2"><div class="form-group">{!! Form::label("boton","Opci&oacute;n: ",["class"=>"control-label"]) !!}<br><a class="btn btn-success btnAddDetalleM1"><i class="fa fa-plus"></i> Agregar</a></div></div></div></div></div>';
      $("#begin1").after(template);
      lim_edit++;
  });
$(document).on('click','.btn-remove-repuesto',function(e){
      if(lim_edit == 1){
        toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-bottom-right",
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      };
        toastr.error('<b>Error!</b> Debe existir al menos un repuesto o mercaderia para ser cotizado');
      }else{
        e.preventDefault();
        var id = $(this).parent('span').attr('opcion-id-cot-r');
        var form = $('#form-delete-cot-r');
        var url = form.attr('action').replace(':OPTION_ID_R',id);
        var data = form.serialize();  
        $.post(url, data, function(result){
            //alert(result);
          });
        $(this).parents('.option-container').fadeOut(500);
        lim_edit --;
      }
      });
  $(document).on('click','.btn-remove-mercaderia',function(e){
      if(lim_edit == 1){
        toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-bottom-right",
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      };
        toastr.warning('<b>Error!</b> Debe existir al menos un repuesto o mercaderia para ser cotizado');
      }else{
        e.preventDefault();
        var id = $(this).parent('span').attr('opcion-id-cot-m');
        var form = $('#form-delete-cot-m');
        var url = form.attr('action').replace(':OPTION_ID_M',id);
        var data = form.serialize();  
        $.post(url, data, function(result){
            //alert(result);
          });
        $(this).parents('.option-container').fadeOut(500);
        lim_edit --;
      }
      });
}
$(document).ready(function(){
  marca = ''
  modelo = ''
  anio = ''
  vin = ''
  $(document).on('click', '.btnAddDetalleR1', function(){
    marca = $(this).parents(".option-container").find('.marca_r_n_1').val()
    modelo = $(this).parents(".option-container").find('.modelo_r_n_1').val()
    anio = $(this).parents(".option-container").find('.anio_r_n_1').val()
    vin = $(this).parents(".option-container").find('.vin_r_n_1').val()
    if(marca == "" || modelo == "" || anio == ""){
      toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };
      toastr.error('<b>Error!</b> debe llenar los campos <b>Marca, Modelo y Año</b> para agregar mas detalles !');
    }else{
      template = '<div class="row detallesAdded1"><div class="col-lg-4"><div class="form-group"><input class="marca_r_n_2" type="hidden" name="marca_r_n[]" value="'+marca+'"><input class="modelo_r_n_2" type="hidden" name="modelo_r_n[]" value="'+modelo+'"><input class="vin_r_n_2"  type="hidden" name="vin_r_n[]" value="'+vin+'"><input class="anio_r_n_2" type="hidden" name="anio_r_n[]" value="'+anio+'">{!! Form::label("detalle_r_n","Detalle: ",["class"=>"control-label"]) !!}{!!Form::text("detalle_r_n[]",null,["class"=>"form-control", "placeholder"=>"Detalles","required","min"=>10]) !!}<br></div></div><div class="col-lg-4"><div class="form-group">{!! Form::label("codigo_repuesto_n","C&oacute;digo del repuesto: ",["class"=>"control-label"]) !!}{!!Form::text("codigo_repuesto_n[]",null,["class"=>"form-control", "placeholder"=>"Ingrese el c&oacute;digo del repuesto","min"=>5]) !!}<br></div></div><div class="col-lg-2"><div class="form-group">{!! Form::label("cantidad_rep_n","Cantidad: ",["class"=>"control-label"]) !!}{!! Form::number("cantidad_rep_n[]",1,["class"=>"form-control","placeholder"=>"Ingrese la cantidad","required","min"=>1]) !!}</div></div><div class="col-lg-2"><div class="form-group">{!! Form::label("boton","Opci&oacute;n: ",["class"=>"control-label"]) !!}<br><a class="btn btn-danger btnRemoveDetalle1"><i class="fa fa-times"></i> Quitar</a></div></div></div>'
      $(this).parents('.divDetalles1').after(template);
    }
  });
  $(document).on('click','.btnRemoveDetalle1',function(){
    $(this).parents('.detallesAdded1').remove();
  });

  $(document).on({
    'focus': function () {
        //hacer algo aca
    },
    'blur': function () {
        marca_nueva = $(this).val()
        marca_antigua = $(this).parents('.option-container').find(".marca_r_n_2").val()
        if(marca_antigua != marca_nueva){
          $(this).parents('.option-container').find('.marca_r_n_2').val(marca_nueva)
        }
    }
  }, 'input.marca_r_n_1');

  $(document).on({
      'focus': function () {
          //hacer algo aca
      },
      'blur': function () {
          modelo_nuevo = $(this).val()
          modelo_antiguo = $(this).parents('.option-container').find(".modelo_r_n_2").val()
          if(modelo_antiguo != modelo_nuevo){
            $(this).parents('.option-container').find('.modelo_r_n_2').val(modelo_nuevo)
          }
      }
    }, 'input.modelo_r_n_1');

  $(document).on({
      'focus': function () {
          //hacer algo aca
      },
      'blur': function () {
          vin_nuevo = $(this).val()
          vin_antiguo = $(this).parents('.option-container').find(".vin_r_n_2").val()
          if(vin_antiguo != vin_nuevo){
            $(this).parents('.option-container').find('.vin_r_n_2').val(vin_nuevo)
          }
      }
    }, 'input.vin_r_n_1');

  $(document).on({
      'focus': function () {
          //hacer algo aca
      },
      'blur': function () {
          anio_nuevo = $(this).val()
          anio_antiguo = $(this).parents('.option-container').find(".anio_r_n_2").val()
          if(anio_antiguo != anio_nuevo){
            $(this).parents('.option-container').find('.anio_r_n_2').val(anio_nuevo)
          }
      }
    }, 'input.anio_r_n_1');
    });
//REPUESTOS

//MERCADERIAS
$(document).ready(function(){
  nombre_m = ''
  $(document).on('click', '.btnAddDetalleM1', function(){
    nombre_m = $(this).parents(".option-container").find('.nombre_m_n_1').val()
    if(nombre_m == ""){
      toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-center",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
            };
      toastr.error('<b>Error!</b> debe llenar el campo <b>Detalle</b> para agregar mas campos !');
    }else{
      template = '<div class="row detallesAddedM1"><div class="col-lg-4"></div><div class="col-lg-4"><div class="form-group"><input class="nombre_m_2" type="hidden" name="nombre_m_n[]" value="'+nombre_m+'">{!! Form::label("nro_item_n","N&uacute;mero de &iacute;tem/URL: ",["class"=>"control-label"]) !!}{!!Form::text("nro_item_n[]",null,["class"=>"form-control", "placeholder"=>"Ingrese el numero de item o URL","required","min"=>10]) !!}<br></div></div><div class="col-lg-2"><div class="form-group">{!! Form::label("cantidad_mer_n","Cantidad: ",["class"=>"control-label"]) !!}{!! Form::number("cantidad_mer_n[]",1,["class"=>"form-control","placeholder"=>"Ingrese la cantidad","required","min"=>1]) !!}<br></div></div><div class="col-lg-2"><div class="form-group">{!! Form::label("boton","Opci&oacute;n: ",["class"=>"control-label"]) !!}<br><a class="btn btn-danger btnRemoveDetalleM1"><i class="fa fa-times"></i> Quitar</a></div></div></div>'
      $(this).parents('.divDetallesM1').after(template);
    }
  });
  $(document).on('click','.btnRemoveDetalleM1',function(){
    $(this).parents('.detallesAddedM1').remove();
  });

  $(document).on({
    'focus': function () {
        //hacer algo aca
    },
    'blur': function () {
        nombre_nuevo = $(this).val()
        nombre_antiguo = $(this).parents('.option-container').find(".nombre_m_2").val()
        if(nombre_antiguo != nombre_nuevo){
          $(this).parents('.option-container').find('.nombre_m_2').val(nombre_nuevo)
        }
    }
  }, 'input.nombre_m_n_1');
});
//MERCADERIAS
</script>