<script type="text/javascript">
var $vista = 'create'
if("create" == $vista){
  var lim = 1;
  var cont = 0;
  $(document).on('click','.btn-add-repuesto',function(e){
  	e.preventDefault();
    anio_select = '<select name="anio_r[]" class="form-control anio_r_1">';
    for(i = 2018; i >= 1990; i--)
      anio_select += '<option value="'+i+'">'+i+'</option>';
    anio_select += '</select>';
  	var template = '<div class="form-group option-container"><div class="row"><div class="panel-heading"><h4 class="page-header">Datos del repuesto </h4><button class="btn btn-danger btn-remove-repuesto pull-right" type="button" data-tooltip="Quitar elemento"><i class="fa fa-times"></i> Quitar</button></div></div><div class="row"><div class="col-lg-4"><div class="form-group">{!! Form::label("marca_r","Marca: ",["class"=>"control-label"]) !!}{!! Form::select("marca_r[]",$marcas,null,["class"=>"form-control marca_r_1","placeholder"=>"Seleccione una marca..","required"]) !!}<br/></div></div><div class="col-lg-3"><div class="form-group">{!! Form::label("modelo_r","Modelo: ",["class"=>"control-label"]) !!}{!!Form::select("modelo_r[]",["placeholder"=>"Seleccione un modelo.."],null,["class"=>"form-control modelo_r_1"])!!}<br/></div></div><div class="col-lg-3"><div class="form-group">{!! Form::label("vin_r","VIN: ",["class"=>"control-label"]) !!}{!! Form::text("vin_r[]",null,["class"=>"form-control vin_r_1","placeholder"=>"Ingrese el VIN del repuesto","min"=>5]) !!}<br/></div></div><div class="col-lg-2"><div class="form-group">{!! Form::label("anio_r","Año: ",["class"=>"control-label"]) !!}'+anio_select+'<br/></div></div></div><div class="row divDetalles1"><div class="col-lg-4"><div class="form-group">{!! Form::label("detalle_r","Detalle: ",["class"=>"control-label"]) !!}{!!Form::text("detalle_r[]",null,["class"=>"form-control", "placeholder"=>"Detalles","required","min"=>10]) !!}<br></div></div><div class="col-lg-4"><div class="form-group">{!! Form::label("codigo_repuesto","C&oacute;digo del repuesto: ",["class"=>"control-label"]) !!}{!!Form::text("codigo_repuesto[]",null,["class"=>"form-control", "placeholder"=>"Ingrese el c&oacute;digo del repuesto","min"=>5]) !!}<br></div></div><div class="col-lg-2"><div class="form-group">{!! Form::label("cantidad_rep","Cantidad: ",["class"=>"control-label"]) !!}{!! Form::number("cantidad_rep[]",1,["class"=>"form-control","placeholder"=>"Ingrese la cantidad","required","min"=>1]) !!}<br/></div></div><div class="col-lg-2"><div class="form-group">{!! Form::label("boton","Opci&oacute;n: ",["class"=>"control-label"]) !!}<br><a class="btn btn-success btnAddDetalleR1"><i class="fa fa-plus"></i> Agregar</a></div></div></div></div>';
      $("#begin1").after(template);
      lim++; 
  });
  $(document).on('click','.btn-add-mercaderia',function(e){
    e.preventDefault();
    var template = '<div class="form-group option-container"><div class="row"><div class="panel-heading"><h4 class="page-header">Datos del producto </h4><button class="btn btn-danger btn-remove-mercaderia pull-right" type="button" data-tooltip="Quitar elemento"><i class="fa fa-times"></i> Quitar</button></div></div><div class="row divDetallesM1"><div class="col-lg-4"><div class="form-group">{!! Form::label("nombre_m","Detalle: ",["class"=>"control-label"]) !!}{!! Form::text("nombre_m[]",null,["class"=>"form-control nombre_m_1","placeholder"=>"Ingrese el detalle","required","min"=>5]) !!}<br/></div></div><div class="col-lg-4"><div class="form-group">{!! Form::label("nro_item","N&uacute;mero de &iacute;tem/URL: ",["class"=>"control-label"]) !!}{!! Form::text("nro_item[]",null,["class"=>"form-control","placeholder"=>"Ingrese el numero de item o URL ","min"=>5]) !!}<br/></div></div><div class="col-lg-2"><div class="form-group">{!! Form::label("cantidad_mer","Cantidad: ",["class"=>"control-label"]) !!}{!! Form::number("cantidad_mer[]",1,["class"=>"form-control","placeholder"=>"Ingrese la cantidad","required","min"=>1]) !!}<br/></div></div><div class="col-lg-2"><div class="form-group">{!! Form::label("boton","Opci&oacute;n: ",["class"=>"control-label"]) !!}<br><a class="btn btn-success btnAddDetalleM1"><i class="fa fa-plus"></i> Agregar</a></div></div></div></div></div>';
      $("#begin1").after(template);
      lim++;
  });
  $(document).on('click','.btn-remove-repuesto',function(e){
    if(lim == 1){
      toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-bottom-right",
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      };
      toastr.warning('Debe existir al menos un repuesto o mercaderia para ser cotizado');
    }else{
      e.preventDefault();
      $(this).parents('.option-container').remove();
      lim--;
    }
  });
  $(document).on('click','.btn-remove-mercaderia',function(e){
    if(lim == 1){
      toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-bottom-right",
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      };
      toastr.warning('Debe existir al menos un repuesto o mercaderia para ser cotizado');
    }else{
      e.preventDefault();
      $(this).parents('.option-container').remove();
      lim--;
    }
  });
}

</script>

