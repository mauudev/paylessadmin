@extends('layouts.principal')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-filled">
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-plane"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Registrar nuevo proveedor</h3>
						<small>
						Gesti&oacute;n de proveedores
						</small>
					</div>
				</div>
				<div class="panel-body">
					{!! Form::open(['route'=>'proveedores.store','method'=>'POST','class'=>'form-group',"onsubmit"=>"return checkSubmit();"]) !!}
					@include('proveedores.form.form')
				</div>
			</div>
		</div>
	</div>
</div>
@include('alerts.validation')
<div class="form-group">
	<div class="col-md-7" >
		{!!Form::submit('Guardar',['class'=>'btn btn-w-md btn-accent'])!!} <a class="btn btn-w-md btn-default" href="{!! URL::to('proveedores') !!}">Cancelar</a>
	</div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
	$(document).on('click','.addMarca1',function(){
		var template = '<div class="row marca-container"><div class="col-lg-4" ><div class="form-group">{!! Form::label("marca_prov","Marca del proveedor: ",["class"=>"control-label"]) !!}{!! Form::text("marca_prov[]",null,["class"=>"form-control","placeholder"=>"Ingrese la marca que distribuye el proveedor","required","min"=>5]) !!}</div></div><div class="col-lg-2"><div class="form-group">{!! Form::label("opcion","Quitar:",["class"=>"control-label"]) !!}<br/><button type="button" class="btn btn-danger fa fa-trash block-center removeMarca1"></button></div></div></div>'
		$('#marcasProv1').after(template)
	});
	$(document).on('click','.removeMarca1',function(e){
			e.preventDefault();
			$(this).parents('.marca-container').remove();
	});

	jQuery(function($){
	   $("#date").mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
	   $("#telefono_p_1").mask("(999) 999-9999? x9999");
	   $("#celular_p_1").mask("(999) 999-9999");
	   $("#phone").mask("99-9999999");
	   $("#ssn").mask("999-99-9999");
	});

</script>
@endsection