@extends('layouts.principal')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <div class="panel-tools">
                    <div class="form-group">
                    </div>
                </div>
                <div class="view-header">
                    <div class="header-icon">
                        <i class="pe page-header-icon pe-7s-calculator"></i>
                    </div>
                    <div class="header-title">
                        <h3 class="page-header">C&aacute;lculo del precio de venta de mercader&iacute;a</h3>
                        <small>
                        Gesti&oacute;n de cotizaciones
                        </small>
                    </div>
                </div>
            </div>
            {!! Form::open(['route'=>'calcular-precio-venta-mer.store','method'=>'POST','class'=>'form-group',"onsubmit"=>"return checkSubmit();",'id'=>'myForm']) !!}
            {!! Form::hidden('ventas_id',$ventas_id) !!}
            {!! Form::hidden('mercaderias_id',$mercaderias_id) !!}
            {!! Form::hidden('cotizacion_mercaderias_id',$cotizacion_mercaderias_id->id) !!}
            @for($i =0; $i < count($proveedores_elegidos); $i ++)
            {!! Form::hidden('proveedores_elegidos[]',$proveedores_elegidos[$i]) !!}
            @endfor
            <!-- Content -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-filled panel-c-danger">
                            <div class="panel-heading">
                                Cliente
                            </div>
                            <div class="panel-body" >
                                <div class="row">
                                    <div class="col-md-3"><strong class="c-white">Nombre: </strong></div><div class="col-md-9"> {!! $cliente->nombre_c !!}</div><br/>
                                    <div class="col-md-3"><strong class="c-white">Apellidos: </strong></div><div class="col-md-9"> {!! $cliente->apellidos_c !!}</div><br/>
                                    <div class="col-md-3"><strong class="c-white">Direcci&oacute;n: </strong></div><div class="col-md-9"> {!! $cliente->direccion_c !!}</div><br/>
                                    <div class="col-md-3"><strong class="c-white">Tel&eacute;fono: </strong></div><div class="col-md-9"> {!! $cliente->telefono_c !!}</div><br/>
                                    <div class="col-md-3"><strong class="c-white">E-mail: </strong></div><div class="col-md-9"> {!! $cliente->email_c !!}</div><br/>
                                    <div class="col-md-3"><strong class="c-white">Pa&iacute;s: </strong></div><div class="col-md-9"> {!! $cliente->pais !!}</div><br/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-filled panel-c-accent">
                            <div class="panel-heading">
                                Detalles del mercaderia
                            </div>
                            <div class="panel-body">
                                <div class="col-md-4">
                                    <strong class="c-white">Mercader&iacute;a:</strong>
                                </div>
                                <div class="col-md-8">
                                    {{ $mercaderia->nombre_m }}
                                </div><br/>
                                
                                <div class="col-md-4">
                                    <strong class="c-white">Nro. &iacute;tem/URL:</strong>
                                </div>
                                <div class="col-md-8">
                                    @if(filter_var($mercaderia->nro_item, FILTER_VALIDATE_URL) == true) 
                                    <a href="{{ $mercaderia->nro_item }}" target="_blank">{{ str_limit($mercaderia->nro_item,$limit = 45,$end='..') }}</a>
                                @else
                                    {{ str_limit($mercaderia->nro_item,$limit = 45,$end='..')  }}
                                @endif
                                </div><br/>
                                <div class="col-md-4">
                                    <strong class="c-white">Cantidad:</strong>
                                </div>
                                <div class="col-md-8">
                                    {{ $cantidad_mer }}
                                    {{ Form::hidden('cantidad_mer',$cantidad_mer) }}
                                </div><br/>
                            </div>
                        </div>
                    </div>
                </div><hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-c-info">
                            <div class="panel-heading">
                                <h4>Descripci&oacute;n del c&aacute;lculo</h4>
                            </div>
                            <div class="panel-body" >
                                <div class="table-responsive">
                                    <table class="table" id="table1">
                                        <thead>
                                            <tr>
                                                <th>Proveedor</th>
                                                <th>Precio compra</th>
                                                <th>Cantidad</th>
                                                @if(\Auth::user()->type == "admin")
                                                <th>Warehouse</th>
                                                <th>Aduana</th>
                                                <th>Ganancia</th>
                                                @endif
                                                <th>Transporte</th>
                                                <th>Adicionales</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @for($i = 0; $i < count($precios_compra); $i++)
                                            <?php $total = $precios_compra[$i] + (15/100)*$precios_compra[$i] + (30/100)*$precios_compra[$i] + (40/100)*$precios_compra[$i] + $transportes + $adicionales; 
                                                $total *= $cantidad_mer;
                                                $datos_cotizacion[$i] = [
                                                    'proveedor_id'=>$proveedores_elegidos[$i],
                                                    'precios_compra'=>$precios_compra[$i],
                                                    'transporte'=>$transportes,
                                                    'adicionales'=>$adicionales,
                                                    'total'=>$total
                                                ];
                                                ?>
                                            <tr class="rowData1">
                                                <td align="center">{!! $proveedores_nombre_compania[$i] !!}</td>
                                                <td align="center"><p class="text-accent">{!! $precios_compra[$i] !!}</p></td>
                                                <td align="center">{!! $cantidad_mer !!}</td>
                                                @if(\Auth::user()->type == "admin")
                                                <td align="center">{!! (15/100)*$precios_compra[$i] !!}</td>
                                                <td align="center">{!! (30/100)*$precios_compra[$i] !!}</td>
                                                <td align="center">{!! (40/100)*$precios_compra[$i] !!}</td>
                                                @endif
                                                <td align="center">
                                                    {!! $transportes !!}
                                                    {!! Form::hidden('transporte', $transportes ) !!}
                                                </td>
                                                <td align="center">
                                                    {!! $adicionales !!}
                                                    {!! Form::hidden('adicionales', $adicionales) !!}
                                                </td>
                                                <td align="center">
                                                    <p class="text-success">{!! $total !!}</p>
                                                    {!! Form::hidden('totales[]', $total ) !!}
                                                    {!! Form::hidden('precios_compra[]',$precios_compra[$i]) !!}
                                                </td>
                                            </tr>
                                        @endfor
                                        {{ Form::hidden("datos_cotizacion",serialize($datos_cotizacion)) }}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('alerts.validation')
<div class="row">
    <div class="form-group">
        <div class="col-md-7" >
            {!!Form::submit('Guardar',['class'=>'btn btn-w-md btn-accent','id'=>'submit1'])!!} <a class="btn btn-w-md btn-default" href="{{ url('/calcular-precio-venta-mer/mercaderia/'.$mercaderias_id.'/venta/'.$ventas_id ) }}">Atr&aacute;s</a>
        </div>
    </div>
    {!! Form::close() !!}
</div>
<script type="text/javascript">
    // $("#submit1").on('click',function(){
    //     var names = Array()
    //     var inputs = document.getElementsByClassName('providersInputs1'),
    //     names  = [].map.call(inputs, function( input ) {
    //     return input.value;
    //     });
    //     if(names.length > 1) {
    //         toastr.options = {
    //                             "closeButton": true,
    //                             "debug": false,
    //                             "newestOnTop": false,
    //                             "progressBar": false,
    //                             "positionClass": "toast-top-center",
    //                             "preventDuplicates": true,
    //                             "onclick": null,
    //                             "showDuration": "300",
    //                             "hideDuration": "1000",
    //                             "timeOut": "5000",
    //                             "extendedTimeOut": "1000",
    //                             "showEasing": "swing",
    //                             "hideEasing": "linear",
    //                             "showMethod": "fadeIn",
    //                             "hideMethod": "fadeOut"
    //                           };
    //                         toastr.error('<b>No se puede enviar los datos!</b> Debe escoger un solo proveedor para asignar a esta cotizaci&oacute;n ');
    //         return false
    //     }else return true
    // });
    //ELIMINAR FILA ESPECIFICA DE LA TABLA
    $(document).ready(function() {
        $('.row1').click(DeleteRow);//BOTON
      });

    function DeleteRow(){
      var count = $('#table1 tr').length;
      if(count <= 2){
            toastr.options = {
                                "closeButton": true,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toast-top-center",
                                "preventDuplicates": true,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                              };
                            toastr.error('<b>No se puede eliminar el proveedor!</b> Debe existir un solo proveedor para asignar a esta cotizaci&oacute;n ');
            
        }else $(this).parents('tr').first().remove();//FILA
    }
</script>
@endsection