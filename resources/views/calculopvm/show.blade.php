@extends('layouts.principal')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-filled">
            <div class="panel-heading">
                <div class="panel-tools">
                    <div class="form-group">
                    </div>
                </div>
                <div class="view-header">
                    <div class="header-icon">
                        <i class="pe page-header-icon pe-7s-calculator"></i>
                    </div>
                    <div class="header-title">
                        <h3 class="page-header">Detalle del precio de venta</h3>
                        <small>
                        Gesti&oacute;n de cotizaciones
                        </small>
                    </div>
                </div>
            </div>
            <!-- Content -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="panel panel-c-danger">
                            <div class="panel-heading">
                                Cliente
                            </div>
                            <div class="panel-body" >
                                <div class="row">
                                    @if(isset($cliente))
                                    <div class="col-md-3"><strong class="c-white">Nombre: </strong></div><div class="col-md-9"> {!! $cliente->nombre_c !!}</div><br/>
                                    <div class="col-md-3"><strong class="c-white">Apellidos: </strong></div><div class="col-md-9"> {!! $cliente->apellidos_c !!}</div><br/>
                                    <div class="col-md-3"><strong class="c-white">Direcci&oacute;n: </strong></div><div class="col-md-9"> {!! $cliente->direccion_c !!}</div><br/>
                                    <div class="col-md-3"><strong class="c-white">Tel&eacute;fono: </strong></div><div class="col-md-9"> {!! $cliente->telefono_c !!}</div><br/>
                                    <div class="col-md-3"><strong class="c-white">E-mail: </strong></div><div class="col-md-9"> {!! $cliente->email_c !!}</div><br/>
                                    <div class="col-md-3"><strong class="c-white">Pa&iacute;s: </strong></div><div class="col-md-9"> {!! $cliente->pais !!}</div><br/>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-c-accent">
                            <div class="panel-heading">
                                Detalles de la mercader&iacute;a
                            </div>
                            <div class="panel-body">
                            @if(isset($mercaderia))
                                <div class="col-md-4">
                                    <strong class="c-white">Mercader&iacute;a: </strong>
                                </div>
                                <div class="col-md-8">
                                    {{ $mercaderia->nombre_m }}
                                </div><br/>
                                <div class="col-md-4">
                                    <strong class="c-white">Nro. &iacute;tem: </strong>
                                </div>
                                <div class="col-md-8">
                                    @if(filter_var($mercaderia->nro_item, FILTER_VALIDATE_URL) == true) 
                                    <a href="{{ $mercaderia->nro_item }}" target="_blank">{{ str_limit($mercaderia->nro_item,$limit = 25,$end='..') }}</a>
                                @else
                                    {{ str_limit($mercaderia->nro_item,$limit = 25,$end='..')  }}
                                @endif
                                </div><br/>
                                <div class="col-md-4">
                                    <strong class="c-white">Cantidad: </strong>
                                </div>
                                <div class="col-md-8">
                                    {{ $cotizacion_mercaderia->cantidad }}
                                </div><br/>
                            @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="panel panel-filled panel-c-primary">
                            <div class="panel-heading">
                                Proveedores seleccionados
                            </div>
                            <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <strong class="c-white">Proveedor </strong>
                                        </div>
                                    </div>
                                    @if(isset($proveedores))
                                    @for($i = 0; $i < count($proveedores); $i++)
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            {{ $proveedores[$i]->nombre_compania_p}}
                                        </div>
                                    </div>
                                    @endfor
                                    @endif
                                    
                            </div>
                        </div>
                    </div>
                </div><hr>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-c-info">
                            <div class="panel-heading">
                                <h4>Descripci&oacute;n del c&aacute;lculo</h4>
                            </div>
                            <div class="panel-body" >
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Proveedor</th>
                                                <th>Precio compra</th>
                                                <th>Cantidad</th>
                                                <th>Warehouse</th>
                                                <th>Aduana</th>
                                                <th>Ganancia</th>
                                                <th>Transporte</th>
                                                <th>Adicionales</th>
                                                <th>Totales</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if(isset($datos_venta))
                                        @for($i = 0; $i < count($datos_venta); $i++)
                                            <tr>
                                                <td>{!! $datos_venta[$i]->nombre_compania_p !!}</td>
                                                <td align="center"><p class="text-accent">{!! $datos_venta[$i]->precio_cot_mer !!}</p></td>
                                                <td align="center">{!! $cotizacion_mercaderia->cantidad !!}</td>
                                                <td align="center">{!! (15/100)*$datos_venta[$i]->precio_cot_mer !!}</td>
                                                <td align="center">{!! (30/100)*$datos_venta[$i]->precio_cot_mer !!}</td>
                                                <td align="center">{!! (40/100)*$datos_venta[$i]->precio_cot_mer !!}</td>
                                                <td align="center">{!! $datos_venta[$i]->transporte !!}</td>
                                                <td align="center">{!! $datos_venta[$i]->adicional !!}</td>
                                                <td align="center"><p class="text-success"> {{ $datos_venta[$i]->precio_total }} </p></td>
                                            </tr>
                                        @endfor
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="form-group">
        <div class="col-md-7" >
           <a class="btn btn-w-md btn-accent" href="{{ url('/administracion/mostrar-cotizacion-principal/show/'.$venta->id) }}">Atr&aacute;s</a>
        </div>
    </div>
</div>
@endsection