@extends('layouts.principal')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-filled">
			<div class="panel-heading">
				<div class="panel-tools">
					<div class="form-group">
					</div>
				</div>
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-calculator"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">C&aacute;lculo del precio de venta</h3>
						<small>
						Gesti&oacute;n de cotizaciones
						</small>
					</div>
				</div>
			</div>
			{!! Form::model($cotizacion_mercaderia,['route'=>['calcular-precio-venta-mer.update',$cotizacion_mercaderia->id],'method'=>'PUT','class'=>'form-group']) !!}
			{!! Form::hidden('ventas_id',$venta->id) !!}
			{!! Form::hidden('mercaderias_id',$mercaderia->id) !!}
			{!! Form::hidden('cotizacion_id',$cotizacion_mercaderia->id) !!}
			{!! Form::hidden('clientes_id',$cliente->id) !!}
			<div class="panel-body">
				<div class="row">
					<div class="col-md-4">
						<div class="panel panel-filled panel-c-accent">
							<div class="panel-heading">
								Detalles de la mercader&iacute;a
							</div>
							<div class="panel-body">
								@if(isset($mercaderia))
								<div class="col-md-4">
									<strong class="c-white">Detalle: </strong>
								</div>
								<div class="col-md-8">
									{{ $mercaderia->nombre_m }}
								</div><br/>
								<div class="col-md-4">
									<strong class="c-white">Nro. &iacute;tem: </strong>
								</div>
								<div class="col-md-8">
                                    @if(filter_var($mercaderia->nro_item, FILTER_VALIDATE_URL) == true) 
                                    <a href="{{ $mercaderia->nro_item }}" target="_blank">{{ str_limit($mercaderia->nro_item,$limit = 25,$end='..') }}</a>
                                @else
                                    {{ str_limit($mercaderia->nro_item,$limit = 25,$end='..')  }}
                                @endif
                                </div><br/>
								<div class="col-md-4">
									<strong class="c-white">Cantidad: </strong>
								</div>
								<div class="col-md-4">
									{{ Form::number('cantidad_mer',$cotizacion_mercaderia->cantidad,['class'=>'form-control',"min"=>1]) }}
								</div><br/>
								@endif
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="panel panel-filled panel-c-primary">
							<div class="panel-heading">
								Proveedores asignados a la mercader&iacute;a
							</div>
							<div class="panel-body">
								<div class="col-md-12">
									<div class="col-md-7">
										<strong class="c-white">Proveedor </strong>
									</div>
									<div class="col-md-3">
										<strong class="c-white">Precio </strong>
									</div>
								</div>
								
								@if(isset($proveedores_cotizacion_mer))
								@for($i = 0; $i < count($proveedores_cotizacion_mer); $i++)
								<div class="col-md-12">
									<div class="col-md-7">
										{{ $proveedores_cotizacion_mer[$i]->nombre_compania_p }}
									</div>
									<div class="col-md-3">
										{{ $proveedores_cotizacion_mer[$i]->precio_cot_mer }}
									</div>
								</div>
								@endfor
								@endif
								
							</div>
						</div>
					</div>
					<div class="col-md-12"><hr></div>
					@if(isset($proveedores_cotizacion_mer))
					<script type="text/javascript">
					function roundNumber(num, scale) {
					var number = Math.round(num * Math.pow(10, scale)) / Math.pow(10, scale);
					if(num - number > 0) {
					return (number + Math.floor(2 * Math.round((num - number) * Math.pow(10, (scale + 1))) / 10) / Math.pow(10, scale));
					} else {
					return number;
					}
					}
					
					var precios_transporte = {!! json_encode($precios_transporte) !!}
					if(precios_transporte.length > 0){
						$(document).on('change','.mer-t-select',function(){
							var div_target = $(this).parents('.transporte-container')
							if(this.value == 'manual'){
								div_target.empty()
								template = '<div class="col-sm-6 center-block text-center">{!! Form::select("precio_transporte", ["lista" => "De la lista", "manual" => "Ingreso manual"], null, ["placeholder" => "Elegir opci&oacute;n..","class"=>"form-control mer-t-select"]); !!}</div><div class="col-sm-6">{!! Form::text("transporte_mer",null,["class"=>"form-control","placeholder"=>"Ingrese el costo..","required"]) !!}</div>'
								div_target.append(template)
							}
							if(this.value == 'lista'){
								div_target.empty()
								var template = '<div class="col-sm-3 center-block text-center">{!! Form::select("precio_transporte", ["lista" => "De la lista", "manual" => "Ingreso manual"], null, ["placeholder" => "Elegir opci&oacute;n..","class"=>"form-control mer-t-select"]); !!}</div><div class="col-sm-9"><select class="form-control"id="select1" name="transporte_mer" size="4" required>'
								var options = ''
								for(i = 0; i < precios_transporte.length; i ++){
									options += '<option value="'+precios_transporte[i].precio+'"><p class="text-accent">'+precios_transporte[i].descripcion+"</p> - "+precios_transporte[i].precio+' $</option>'
								}
							options += '</select></div>'
							template = template + options
							div_target.append(template)
						}
					});
								}
				</script>
				@endif
				@for($i = 0; $i < count($proveedores_cotizacion_mer); $i ++)
				<!-- DIVS OCULTOS -->
				<div class="col-md-12" id="calculosPrecio1">
					<input name="proveedores_id[]" class="providersInputs1" type="hidden" value="{{ $proveedores_cotizacion_mer[$i]->proveedores_id }}">
					<input name="precio_venta_mer_id[]" type="hidden" value="{{ $proveedores_cotizacion_mer[$i]->precio_venta_mer_id }}">
					<input name="precio_cot_mer[]" type="hidden" value="{{ $proveedores_cotizacion_mer[$i]->precio_cot_mer }}">
					<div class="panel panel-filled panel-c-danger">
						<div class="panel-heading">
							<div class="col-sm-10 center-block text-center">
								<strong class="c-white">Transporte</strong>
							</div>
							<div class="col-sm-2 center-block text-center">
								<strong class="c-white">Adicional</strong>
							</div>
						</div>
						<div class="panel-body">
							<br>
							<div class="row">
								<div class="col-md-12">
									<div class="col-sm-10 transporte-container center-block text-center">
										<div class="col-sm-4 center-block text-center">
											{!! Form::select('precio_transporte', ['lista' => 'De la lista', 'manual' => 'Ingreso manual'], null, ['placeholder' => 'Elegir opci&oacute;n..','class'=>'form-control mer-t-select']); !!}
										</div>
										<div class="col-sm-8">{!! Form::text("transporte_mer",$proveedores_cotizacion_mer[$i]->transporte,["class"=>"form-control","placeholder"=>"Ingrese el costo..","required"]) !!}</div>
									</div>
									<div class="col-sm-2 center-block text-center">
										{{ Form::text("adicional_mer",$proveedores_cotizacion_mer[$i]->adicional,["class"=>"form-control","placeholder"=>"Ingrese el costo..","required"]) }}
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- DIVS OCULTOS -->
				@endfor
			</div>
		</div>
	</div>
</div>
</div>
@include('alerts.validation')

<div class="row">
<div class="form-group">
	<div class="col-md-7" >
		{!!Form::submit('Actualizar',['class'=>'btn btn-w-md btn-accent', 'id'=>'submit1'])!!} <a class="btn btn-w-md btn-default" href="{{ url('/administracion/mostrar-cotizacion-principal/show/'.$venta->id ) }}">Cancelar</a>
	</div>
</div>
{!! Form::close() !!}
</div>
<script type="text/javascript">
	toastr.options = {
		"closeButton": true,
		"debug": false,
		"newestOnTop": false,
		"progressBar": false,
		"positionClass": "toast-top-center",
		"preventDuplicates": true,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": "5000",
		"extendedTimeOut": "1000",
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
		};
		toastr.info('<b>NOTA:</b> Puede editar la cantidad en detalles de la mercader&iacute;a. ');
</script>
@endsection