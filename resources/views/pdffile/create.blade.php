@extends('layouts.principal')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-filled">
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-next-2"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Personalizar PDF</h3>
						<small>
						Generar invoice PDF
						</small>
					</div>
				</div>
				<div class="panel-body">
					{!!Form::model($venta,['route'=>['ventas.generarPDFPersonalizado',$venta->id],'method'=>'POST'])!!}
					{!! Form::hidden("ventas_id",$venta->id) !!}
					{{ csrf_field() }}
					@include('pdffile.form.form')
					@include('alerts.validation')
					<div class="form-group">
						
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-7" >
			{!!Form::submit('Generar PDF',['class'=>'btn btn-w-md btn-accent','id'=>'generarBtn'])!!} <a class="btn btn-w-md btn-default" href="{!! url('ventas/'.$venta->id.'/detalle') !!}">Atr&aacute;s</a>
		</div>
		{!! Form::close() !!}
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
		$(".observacionesCB").change(function(){
			if(this.checked){
				template = '{{ Form::textarea("observaciones",null,["class"=>"form-control observacionesInput","placeholder"=>"Ingrese observaciones en el campo","required","min"=>5,"rows"=>5]) }}'
				$(".observacionesDiv").append(template)
			}else {
				$(".observacionesInput").remove()
			}
		});
		$(".metodo_pagoCB").change(function(){
			if(this.checked){
				template = '{{ Form::textarea("metodo_pago",null,["class"=>"form-control metodo_pagoInput","placeholder"=>"Ingrese metodo de pago en el campo","required","min"=>5,"rows"=>5]) }}'
				$(".metodo_pagoDiv").append(template)
			}else {
				$(".metodo_pagoInput").remove()
			}
		});
		$(".plazo_entregaCB").change(function(){
			if(this.checked){
				template = '{{ Form::textarea("plazo_entrega",null,["class"=>"form-control plazo_entregaInput","placeholder"=>"Ingrese el plazo entrega en el campo","required","min"=>5,"rows"=>5]) }}'
				$(".plazo_entregaDiv").append(template)
			}else {
				$(".plazo_entregaInput").remove()
			}
		});
	});
	$("#generarBtn").on('click',function(){
		if($(".observacionesInput").val().length < 10) {
			toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-center",
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      };
                    toastr.error("<b>Error!</b> El campo debe tener un m&iacute;nimo de 10 caracteres para generar el PDF. ");
			return false;
		}
		if($(".metodo_pagoInput").val().length < 10) {
			toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-center",
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      };
                    toastr.error("<b>Error!</b> El campo debe tener un m&iacute;nimo de 10 caracteres para generar el PDF. ");
			return false;
		}
		if($(".plazo_entregaInput").val().length < 10) {
			toastr.options = {
                        "closeButton": true,
                        "debug": false,
                        "newestOnTop": false,
                        "progressBar": false,
                        "positionClass": "toast-top-center",
                        "preventDuplicates": true,
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                      };
                    toastr.error("<b>Error!</b> El campo debe tener un m&iacute;nimo de 10 caracteres para generar el PDF. ");
			return false;
		}
		var referrer =  document.referrer;
	});
</script>
@endsection