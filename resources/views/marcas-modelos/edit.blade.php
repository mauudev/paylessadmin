@extends('layouts.principal')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-filled">
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-car"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Actualizar marca</h3>
						<small>
						Gesti&oacute;n de modelos y marcas de autom&oacute;viles
						</small>
					</div>
				</div>
				<div class="panel-body">
					{!!Form::model($objeto,['route'=>['marcas-modelos.update',$objeto->id],'method'=>'PUT'])!!}
					@include('marcas-modelos.form.form')<br>
					@include('alerts.validation')
					<div class="form-group">
					</div>
					
				</div>
			</div>
		</div>
		<div class="col-md-7" >
			{!!Form::submit('Actualizar',['class'=>'btn btn-w-md btn-accent'])!!} <a class="btn btn-w-md btn-default" href="{!! URL::to('marcas-modelos') !!}">Cancelar</a>
		</div>
		{!! Form::close() !!}
	</div>
</div>
@endsection