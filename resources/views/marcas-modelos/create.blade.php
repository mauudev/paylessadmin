@extends('layouts.principal')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-filled">
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-car"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Registrar nueva marca</h3>
						<small>
						Gesti&oacute;n de modelos y marcas de autom&oacute;viles
						</small>
					</div>
				</div><br>
				<div class="panel-body">
					{!! Form::open(['route'=>'marcas-modelos.store','method'=>'POST','class'=>'form-group',"onsubmit"=>"return checkSubmit();"]) !!}
					@include('marcas-modelos.form.form')<br>
					@include('alerts.validation')
				</div>
			</div>
		</div>
		<div class="col-md-12" >
			{!!Form::submit('Guardar',['class'=>'btn btn-w-md btn-accent'])!!} <a class="btn btn-w-md btn-default" href="{!! URL::to('marcas-modelos') !!}">Cancelar</a>
		</div>
		{!! Form::close() !!}
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(".addModeloBtn").on("click",function(){
			template = '<div class="row modeloAddedDiv"><div class="col-lg-6"><div class="form-group">{!! Form::text('modelo[]',null,['class'=>'form-control','placeholder'=>'Ingrese el modelo...','required','min'=>5]) !!}<br/></div></div><div class="col-lg-6"><div class="form-group"><button type="button" class="btn btn-danger removeModeloBtn"><i class="fa fa-trash"></i> Quitar modelo</button></div></div></div>'
			$(".modelosDiv").after(template);
		});
	});
	$(document).on("click",".removeModeloBtn",function(){
		$(this).parents(".modeloAddedDiv").remove()
	});
</script>
@endsection
