@extends('layouts.principal')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-filled">
      <div class="panel-heading">
        <div class="panel-tools">
          <div class="col-md-12">
            {!! Form::open(['route'=>'marcas-modelos.index','method'=>'GET','class'=>'navbar-form navbar-left pull-right','role'=>'search']) !!}
            {!! Form::text('search',null,['class'=>'form-control','placeholder'=>'Buscar..']) !!}
              <button type="submit" class="btn btn-accent">Buscar</button>
            <a href="{!! URL::to('marcas-modelos/create') !!}" class="btn btn-w-md btn-accent"><i class="fa fa-plus"></i> Nueva marca</a>
            {!! Form::close() !!}
          </div>
        </div>
        <div class="view-header">
          <div class="header-icon">
            <i class="pe page-header-icon pe-7s-car"></i>
          </div>
          <div class="header-title">
            <h3 class="page-header">Marcas registradas</h3>
            <small>
            Gesti&oacute;n de modelos y marcas de autom&oacute;viles
            </small>
          </div>
        </div>
      </div>
      <div class="panel-body">
        @if(count($datos) > 0)
        <div class="table-responsive">
          <table class="table table-hover table-striped">
              <thead>
                            <tr role="row">
                              <th class="sorting_asc left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" >Marca
                              </th>
                              <th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="Browser: activate to sort column ascending" >Modelo
                              </th>
                              <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="CSS grade: activate to sort column ascending" >Acci&oacute;n
                              </th>
                            </tr>
                        </thead>
            <tbody id="resultados1">
              
              @foreach($datos as $dato)
              <tr>
                <td>{{ $dato->marca }}</td>
                <td>{{ $dato->modelo }}</td>
                <td align="center">
                <div style="text-align: center">
                  <a class="btn btn-accent" href="{!! route('marcas-modelos.edit',$parameters = $dato->id) !!}" data-tooltip="Editar usuario"><i class="fa fa-pencil"></i> Editar</a>&nbsp;
                <a class="btn btn-danger" href="{!! route('marcas-modelos.destroy',$parameters = $dato->id) !!}" data-tooltip="Activar usuario"><i class="fa fa-trash"></i> Eliminar</a>
                </td>
                </div>
              </tr>
              @endforeach
            </tbody>
          </table>
          {{ $datos->render() }}
          @include('alerts.success')
        </div>
        @else 
        <div class="col-md-12" id="msg1">
            <div class="alert alert-danger alert-dismissable">
              <strong>No se encontraron resultados</strong> 
            </div>
          </div>
        @endif
      </div>
    </div>
  </div>
</div>
</div>
@endsection