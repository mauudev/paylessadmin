@if(isset($context))
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('marca','Marca: ',['class'=>'control-label']) !!}
			{!! Form::text('marca',null,['class'=>'form-control','placeholder'=>'Ingrese la marca..','required','min'=>5]) !!}<br/>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('modelo','Modelo: ',['class'=>'control-label']) !!}
			{!! Form::text('modelo[]',null,['class'=>'form-control','placeholder'=>'Ingrese el modelo...','required','min'=>5]) !!}<br/>
		</div>
	</div>
</div>
@else
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('marca','Marca: ',['class'=>'control-label']) !!}
			{!! Form::text('marca',null,['class'=>'form-control','placeholder'=>'Ingrese la marca..','required','min'=>5]) !!}<br/>
		</div>
	</div>
</div>
<div class="row modelosDiv">
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('modelo','Modelo: ',['class'=>'control-label']) !!}
			{!! Form::text('modelo[]',null,['class'=>'form-control','placeholder'=>'Ingrese el modelo...','required','min'=>5]) !!}<br/>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="form-group">
			{!! Form::label('accion','Acci&oacute;n: ',['class'=>'control-label']) !!}<br>
			<button type="button" class="btn btn-success addModeloBtn"><i class="fa fa-plus"></i> Agregar modelo</button>
		</div>
	</div>
</div>
@endif
