@extends('layouts.principal')
@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-filled">
      <div class="panel-heading">
        <div class="panel-tools">
          <div class="col-md-12">
            <?php $estados = [1=>'Activos',0=>'Inactivos']?>
            {!! Form::open(['route'=>'usuarios.index','method'=>'GET','class'=>'navbar-form navbar-left pull-right','role'=>'search']) !!}
            <div class="form-group">
              {!!Form::select('estado', $estados,null,['class'=>'form-control','placeholder'=>'Filtrar registros..','id'=>'filtro1'])!!}
              {!! Form::text('search',null,['class'=>'form-control','placeholder'=>'Buscar..']) !!}
            </div>
            <button type="submit" class="btn btn-accent">Buscar</button>
            <a href="{!! URL::to('usuarios/create') !!}" class="btn btn-w-md btn-accent"><i class="fa fa-plus"></i> Nuevo usuario</a>
            {!! Form::close() !!}
          </div>
        </div>
        <div class="view-header">
          <div class="header-icon">
            <i class="pe page-header-icon pe-7s-user"></i>
          </div>
          <div class="header-title">
            <h3 class="page-header">Usuarios registrados</h3>
            <small>
            Gesti&oacute;n de usuarios
            </small>
          </div>
        </div>
      </div>
      <div class="panel-body">
        <div class="table-responsive">
          <table class="table table-hover table-striped">
              <thead>
                            <tr role="row">
                              <th class="sorting_asc left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="Rendering engine: activate to sort column descending" aria-sort="ascending" >Nombre
                              </th>
                              <th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="Browser: activate to sort column ascending" >Apellidos
                              </th>
                              <th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="Platform(s): activate to sort column ascending" >Tel&eacute;fono
                              </th>
                              <th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="Engine version: activate to sort column ascending" >Celular
                              </th>
                              <th class="sorting left-align" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="CSS grade: activate to sort column ascending" >Rol
                              </th>
                              <th class="sorting" tabindex="0" aria-controls="dataTables-example" rowspan="1" colspan="1"
                                  aria-label="CSS grade: activate to sort column ascending" >Acci&oacute;n
                              </th>
                            </tr>
                        </thead>
            <tbody id="resultados1">
              @if(count($usuarios) > 0)
              @foreach($usuarios as $usuario)
              <tr>
                <td data-toggle="modal" data-target="#myModal" data-id="{{ $usuario->id }}">{{ str_limit($usuario->nombre_u,$limit = 35,$end='...')  }}
                {{ Form::hidden('nombre',$usuario->nombre_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('apellidos',$usuario->apellidos_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('telefono',$usuario->telefono_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('celular',$usuario->celular_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('email',$usuario->email_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('username',$usuario->user_name,['class'=>'dataUsuario']) }}
                {{ Form::hidden('tipo',$usuario->tipo,['class'=>'dataUsuario']) }}</td>
                <td data-toggle="modal" data-target="#myModal" data-id="{{ $usuario->id }}">{{ str_limit($usuario->apellidos_u,$limit = 35,$end='...')  }}
                {{ Form::hidden('nombre',$usuario->nombre_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('apellidos',$usuario->apellidos_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('telefono',$usuario->telefono_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('celular',$usuario->celular_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('email',$usuario->email_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('username',$usuario->user_name,['class'=>'dataUsuario']) }}
                {{ Form::hidden('tipo',$usuario->tipo,['class'=>'dataUsuario']) }}</td>
                <td data-toggle="modal" data-target="#myModal" data-id="{{ $usuario->id }}">{{ str_limit($usuario->telefono_u,$limit = 35,$end='...')  }}
                {{ Form::hidden('nombre',$usuario->nombre_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('apellidos',$usuario->apellidos_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('telefono',$usuario->telefono_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('celular',$usuario->celular_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('email',$usuario->email_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('username',$usuario->user_name,['class'=>'dataUsuario']) }}
                {{ Form::hidden('tipo',$usuario->tipo,['class'=>'dataUsuario']) }}</td>
                <td data-toggle="modal" data-target="#myModal" data-id="{{ $usuario->id }}">{{ str_limit($usuario->celular_u,$limit = 35,$end='...')  }}
                {{ Form::hidden('nombre',$usuario->nombre_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('apellidos',$usuario->apellidos_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('telefono',$usuario->telefono_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('celular',$usuario->celular_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('email',$usuario->email_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('username',$usuario->user_name,['class'=>'dataUsuario']) }}
                {{ Form::hidden('tipo',$usuario->tipo,['class'=>'dataUsuario']) }}</td>
                <td data-toggle="modal" data-target="#myModal" data-id="{{ $usuario->id }}">{{ str_limit($usuario->tipo,$limit = 35,$end='...')  }}
                {{ Form::hidden('nombre',$usuario->nombre_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('apellidos',$usuario->apellidos_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('telefono',$usuario->telefono_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('celular',$usuario->celular_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('email',$usuario->email_u,['class'=>'dataUsuario']) }}
                {{ Form::hidden('username',$usuario->user_name,['class'=>'dataUsuario']) }}
                {{ Form::hidden('tipo',$usuario->tipo,['class'=>'dataUsuario']) }}</td>
                <td align="center">
                <div style="text-align: center">
                  <a class="btn btn-accent" href="{!! route('usuarios.edit',$parameters = $usuario->id) !!}" data-tooltip="Editar usuario"><i class="fa fa-pencil"></i> Editar</a>&nbsp;
                @if($usuario->deleted_at == null )
                <a class="btn btn-danger" href="{!! route('usuarios.destroy',$parameters = $usuario->id) !!}" data-tooltip="Desactivar usuario"><i class="fa fa-times"></i> Desactivar</a>
                @else
                <a class="btn btn-success" href="{!! route('usuarios.activate',$parameters = $usuario->id) !!}" data-tooltip="Activar usuario"><i class="fa fa-check"></i> Activar</a>
                @endif
                </td>
                </div>
              </tr>
              @endforeach
            </tbody>
          </table>
          {{ $usuarios->render() }}
          @include('alerts.success')
        </div>
        @else 
        <div class="col-md-12" id="msg1">
            <div class="alert alert-danger alert-dismissable">
              <strong>No se encontraron resultados</strong> 
            </div>
          </div>
        @endif
      </div>
    </div>
  </div>
</div>
</div>
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header center-block">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title text-center">Informaci&oacute;n del usuario</h4>
            </div>
            <div class="modal-body" id="infoUsuarios">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
  $('#filtro1').on('change',function(){
    var filtro = $('#filtro1').val()
    var target = document.getElementById('resultados1')
    var vaciar_msg = document.getElementById('msg1')
    $.get('/administracion/usuarios/filtrar/'+filtro+'',function(response){
      var usuarios = response
      var template = ''
      target.innerHTML = ''
      if(vaciar_msg) vaciar_msg.innerHTML = ''
      var url_edit = '/administracion/usuarios/'
      var url_destroy = '/administracion/usuarios/'
      var url_activate = '/administracion/usuarios/'
      for(i = 0; i < usuarios.length; i++){
        template += '<tr><td data-toggle="modal" data-target="#myModal" data-id="'+usuarios[i].id+'"><input class="dataUsuario" name="nombrex" type="hidden" value="'+usuarios[i].nombre_u+'">'+usuarios[i].nombre_u+'<input class="dataUsuario" name="apellidos" type="hidden" value="'+usuarios[i].apellidos_u+'"><input class="dataUsuario" name="telefono" type="hidden" value="'+usuarios[i].telefono_u+'"><input class="dataUsuario" name="celular" type="hidden" value="'+usuarios[i].celular_u+'"><input class="dataUsuario" name="email" type="hidden" value="'+usuarios[i].email_u+'"><input class="dataUsuario" name="username" type="hidden" value="'+usuarios[i].user_name+'"><input class="dataUsuario" name="tipo" type="hidden" value="'+usuarios[i].tipo+'"></td><td data-toggle="modal" data-target="#myModal" data-id="'+usuarios[i].id+'"><input class="dataUsuario" name="nombrex" type="hidden" value="'+usuarios[i].nombre_u+'"><input class="dataUsuario" name="apellidos" type="hidden" value="'+usuarios[i].apellidos_u+'">'+usuarios[i].apellidos_u+'<input class="dataUsuario" name="telefono" type="hidden" value="'+usuarios[i].telefono_u+'"><input class="dataUsuario" name="celular" type="hidden" value="'+usuarios[i].celular_u+'"><input class="dataUsuario" name="email" type="hidden" value="'+usuarios[i].email_u+'"><input class="dataUsuario" name="username" type="hidden" value="'+usuarios[i].user_name+'"><input class="dataUsuario" name="tipo" type="hidden" value="'+usuarios[i].tipo+'"></td><td data-toggle="modal" data-target="#myModal" data-id="'+usuarios[i].id+'"><input class="dataUsuario" name="nombrex" type="hidden" value="'+usuarios[i].nombre_u+'"><input class="dataUsuario" name="apellidos" type="hidden" value="'+usuarios[i].apellidos_u+'"><input class="dataUsuario" name="telefono" type="hidden" value="'+usuarios[i].telefono_u+'">'+usuarios[i].telefono_u+'<input class="dataUsuario" name="celular" type="hidden" value="'+usuarios[i].celular_u+'"><input class="dataUsuario" name="email" type="hidden" value="'+usuarios[i].email_u+'"><input class="dataUsuario" name="username" type="hidden" value="'+usuarios[i].user_name+'"><input class="dataUsuario" name="tipo" type="hidden" value="'+usuarios[i].tipo+'"></td><td data-toggle="modal" data-target="#myModal" data-id="'+usuarios[i].id+'"><input class="dataUsuario" name="nombrex" type="hidden" value="'+usuarios[i].nombre_u+'"><input class="dataUsuario" name="apellidos" type="hidden" value="'+usuarios[i].apellidos_u+'"><input class="dataUsuario" name="telefono" type="hidden" value="'+usuarios[i].telefono_u+'"><input class="dataUsuario" name="celular" type="hidden" value="'+usuarios[i].celular_u+'">'+usuarios[i].celular_u+'<input class="dataUsuario" name="email" type="hidden" value="'+usuarios[i].email_u+'"><input class="dataUsuario" name="username" type="hidden" value="'+usuarios[i].user_name+'"><input class="dataUsuario" name="tipo" type="hidden" value="'+usuarios[i].tipo+'"></td><td data-toggle="modal" data-target="#myModal" data-id="'+usuarios[i].id+'"><input class="dataUsuario" name="nombrex" type="hidden" value="'+usuarios[i].nombre_u+'"><input class="dataUsuario" name="apellidos" type="hidden" value="'+usuarios[i].apellidos_u+'"><input class="dataUsuario" name="telefono" type="hidden" value="'+usuarios[i].telefono_u+'"><input class="dataUsuario" name="celular" type="hidden" value="'+usuarios[i].celular_u+'"><input class="dataUsuario" name="email" type="hidden" value="'+usuarios[i].email_u+'"><input class="dataUsuario" name="username" type="hidden" value="'+usuarios[i].user_name+'"><input class="dataUsuario" name="tipo" type="hidden" value="'+usuarios[i].tipo+'">'+usuarios[i].tipo+'</td><td align="center"><div style="text-align: center"><a class="btn btn-accent" href="'+url_edit+usuarios[i].id+'/edit" data-tooltip="Editar usuario"><i class="fa fa-pencil"></i> Editar</a>&nbsp;'
        if(filtro == 1){
          template += '&nbsp;<a class="btn btn-danger" href="'+url_destroy+usuarios[i].id+'/destroy" data-tooltip="Desactivar usuario"><i class="fa fa-times"></i> Desactivar</a></div></td></tr>'
        }else{
          template += '&nbsp;<a class="btn btn-success" href="'+url_activate+usuarios[i].id+'/activate" data-tooltip="Activar usuario"><i class="fa fa-check"></i> Activar</a></div></td></tr>'
        }
      }
      target.innerHTML = template;
    });
  });
  $('#myModal').on('show.bs.modal', function (event) {
    //var getIdFromRow = $(event.relatedTarget).attr('data-id');
    // if you wnat to take the text of the first cell  
    //var compania = $(event.relatedTarget).find('td:nth-child(1)').html();
    //var contacto = $(event.relatedTarget).find('td:nth-child(2)').text();
    var data = Array()
    $(event.relatedTarget).find(".dataUsuario").each(function(i) {
      console.log(String(this.value))
      data[i] += String(this.value)
  }); 
    var template2 = '<div class="row"><div class="col-md-2"><strong class="c-white">Nombre: </strong></div><div class="col-md-10">'+data[0].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">Apellidos: </strong></div><div class="col-md-10">'+data[1].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">Tel&eacute;fono: </strong></div><div class="col-md-10">'+data[2].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">Celular: </strong></div><div class="col-md-10">'+data[3].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">E-mail: </strong></div><div class="col-md-10">'+data[4].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">Username: </strong></div><div class="col-md-10">'+data[5].replace('undefined','')+'</div></div><div class="row"><div class="col-md-2"><strong class="c-white">Rol: </strong></div><div class="col-md-10">'+data[6].replace('undefined','')+'</div></div>'

    $(this).find('#infoUsuarios').html($(template2));
});
</script>
@endsection