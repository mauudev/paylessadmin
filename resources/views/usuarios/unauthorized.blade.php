<!DOCTYPE html>
<html>
    <!-- Mirrored from webapplayers.com/luna_admin-v1.1/error.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Aug 2016 21:37:24 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>
        <!-- Page title -->
        <title>Payless | Administraci&oacute;n</title>
        <!-- Vendor styles -->
        {!! Html::style("vendor/fontawesome/css/font-awesome.css") !!}
        {!! Html::style("vendor/animate.css/animate.css") !!}
        {!! Html::style("vendor/bootstrap/css/bootstrap.css") !!}
        <!-- App styles -->
        {!! Html::style("styles/pe-icons/pe-icon-7-stroke.css") !!}
        {!! Html::style("styles/pe-icons/helper.css") !!}
        {!! Html::style("styles/stroke-icons/style.css") !!}
        {!! Html::style("styles/style.css") !!}
    </head>
    <body class="blank">
        <!-- Wrapper-->
        <div class="wrapper">
            <!-- Main content-->
            <section class="content">
                <div class="container-center md animated slideInDown">
                    <div class="view-header">
                        <div class="header-icon">
                            <i class="pe page-header-icon pe-7s-close-circle"></i>
                        </div>
                        <div class="header-title">
                            <h3>Error 401</h3>
                            <small>
                            Acceso restringido !
                            </small>
                        </div>
                    </div>
                    <div class="panel panel-filled">
                        <div class="panel-body">
                            @include('alerts.unauthorized')
                        </div>
                    </div>
                    <div>
                        <a href="{!! URL::to('/cotizaciones/listar/0') !!}" class="btn btn-accent">Regresar</a>
                    </div>
                </div>
            </section>
            <!-- End main content-->
        </div>
        <!-- End wrapper-->
        <!-- Vendor scripts -->
        {!! Html::script("vendor/pacejs/pace.min.js") !!}
        {!! Html::script("vendor/jquery/dist/jquery.min.js") !!}
        {!! Html::script("vendor/bootstrap/js/bootstrap.min.js") !!}
        <!-- App scripts -->
        {!! Html::script("scripts/luna.js") !!}
    </body>
    <!-- Mirrored from webapplayers.com/luna_admin-v1.1/error.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Aug 2016 21:37:24 GMT -->
</html>