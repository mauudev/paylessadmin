@extends('layouts.principal')
@section('content')
<!-- Main content-->
	<div class="container-center md animated slideInDown"><hr>
		<div class="view-header">
			<div class="header-icon">
				<i class="pe page-header-icon pe-7s-paper-plane"></i>
			</div>
			<div class="header-title">
				<h3>Mensaje enviado !</h3>
			</div>
		</div>
		{!! Form::hidden('ventas_id',$venta->id) !!}
		<div class="panel panel-filled">
			<div class="panel-body">
				<h4>El mensaje ha sido enviado correctamente !</h4>
			</div>
		</div>
		<div>
		<a href="{!! route('sendmail.create',$parameters = $venta->id) !!}" class="btn btn-accent"><i class="fa fa-send"></i> Enviar otro mensaje</a>
		<a href="{{ URL::to('/ventas/'.$venta->id.'/detalle') }}" class="btn btn-accent"><i class="fa fa-clipboard"></i> Ir a detalle de venta</a>
		</div><hr>
	</div>
<!-- End main content-->
@endsection
