@extends('layouts.principal')
@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-filled">
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-mail"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Enviar nuevo mensaje</h3>
						<small>
							Env&iacute;os de mensajes
						</small>
					</div>
				</div>

				<div class="panel-body">
					{!! Form::open(['route'=>'sendmail.store','method'=>'POST','class'=>'form-group',"onsubmit"=>"return checkSubmit();"]) !!}
					{!! Form::hidden('ventas_id',$venta->id) !!}
					{!! Form::hidden('nombre_c',$cliente->nombre_c) !!}
					{!! Form::hidden('apellidos_c',$cliente->apellidos_c) !!}
					@include('sendmail.form.email-form')
					@include('alerts.validation')
					<div class="col-lg-12 personalizarFormDiv">
						<b>NOTA:</b> por defecto se env&iacute;a el invoice con el contenido por defecto, haga click en <button type="button" class="btn btn-success btn-sm personalizarBtn"> Personalizar PDF</button> para cambiar los datos.
					</div>
				</div>

			</div>
		</div>
	</div>
@include('alerts.success')
<div class="row">
<div class="form-group">
	<div class="col-md-7" >
		{!!Form::submit('Enviar',['class'=>'btn btn-w-md btn-accent'])!!} <a class="btn btn-w-md btn-default" href="{!! URL::to('ventas/'.$venta->id.'/detalle') !!}">Cancelar</a>
	</div>
</div>
</div>
{!! Form::close() !!}
<script type="text/javascript">
	$(document).ready(function(){
		$(".personalizarBtn").on("click",function(){
			template = '<p>Puede agregar los siguientes elementos al PDF: <code>Observaciones</code>, <code>M&eacute;todo de pago</code>, <code>Plazo de entrega</code></p><br><b>Elementos:</b><br><br><div class="row"><div class="col-lg-4"><div class="form-group observacionesDiv">{!! Form::checkbox("observacionesCB",1,false,["class"=>"observacionesCB"]) !!}{!! Form::label("observaciones"," Observaciones ",["class"=>"control-label"]) !!}<br></div></div><div class="col-lg-4"><div class="form-group metodo_pagoDiv">{!! Form::checkbox("metodo_pagoCB",1,false,["class"=>"metodo_pagoCB"]) !!}{!! Form::label("metodo_pago","M&eacute;todo de pago ",["class"=>"control-label"]) !!}<br></div></div><div class="col-lg-4"><div class="form-group plazo_entregaDiv">{!! Form::checkbox("plazo_entregaCB",1,false,["class"=>"plazo_entregaCB"]) !!}{!! Form::label("plazo_entrega","Plazo de entrega",["class"=>"control-label"]) !!}{!! Form::hidden('personalizado',1) !!}<br></div></div></div><br><br>'
			$(".personalizarFormDiv").html(template)
		});
		$(document).on("click",".holaBtn",function(){
			alert("alo")
		});
		$(document).on("change",".observacionesCB",function(){
			if(this.checked){
				template = '{{ Form::textarea("observaciones",null,["class"=>"form-control observacionesInput","placeholder"=>"Ingrese observaciones en el campo","required","min"=>5,"rows"=>5]) }}'
				$(".observacionesDiv").append(template)
			}else {
				$(".observacionesInput").remove()
			}
		});
		$(document).on("change",".metodo_pagoCB",function(){
			if(this.checked){
				template = '{{ Form::textarea("metodo_pago",null,["class"=>"form-control metodo_pagoInput","placeholder"=>"Ingrese metodo de pago en el campo","required","min"=>5,"rows"=>5]) }}'
				$(".metodo_pagoDiv").append(template)
			}else {
				$(".metodo_pagoInput").remove()
			}
		});
		$(document).on("change",".plazo_entregaCB",function(){
			if(this.checked){
				template = '{{ Form::textarea("plazo_entrega",null,["class"=>"form-control plazo_entregaInput","placeholder"=>"Ingrese el plazo entrega en el campo","required","min"=>5,"rows"=>5]) }}'
				$(".plazo_entregaDiv").append(template)
			}else {
				$(".plazo_entregaInput").remove()
			}
		});	
	});
</script>
@endsection
