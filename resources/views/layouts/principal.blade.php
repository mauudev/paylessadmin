<!DOCTYPE html>
<html lang='en'>
    <!-- Mirrored from webapplayers.com/luna_admin-v1.1/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Aug 2016 21:36:21 GMT -->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--<link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900' rel='stylesheet' type='text/css'>-->
        <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" >
        <!-- Page title -->

        <title>Payless | Administraci&oacute;n</title>
        <!-- Vendor styles -->
        {!! Html::style('css/jquery-ui.min.css') !!}
        {!! Html::style('vendor/fontawesome/css/font-awesome.css') !!}
        {!! Html::style('vendor/animate.css/animate.css') !!}
        {!! Html::style('vendor/bootstrap/css/bootstrap.css') !!}
        {!! Html::style('vendor/toastr/toastr.min.css') !!}
        <!-- App styles -->
        {!! Html::style('styles/pe-icons/pe-icon-7-stroke.css') !!}
        {!! Html::style('styles/pe-icons/helper.css') !!}
        {!! Html::style('styles/stroke-icons/style.css') !!}
        {!! Html::style('styles/style.css') !!}
        {!! Html::style('css/tooltips.css') !!}
        {!! Html::style('css/select2.min.css') !!}
        
        {!! Html::script('js/jquery-3.1.1.js') !!}
        {!! Html::script('js/jquery-ui.min.js') !!}
        {!! Html::script('vendor/pacejs/pace.min.js') !!}
        {!! Html::script('vendor/jquery/dist/jquery.min.js') !!}
        
        {!! Html::script('vendor/bootstrap/js/bootstrap.min.js') !!}
        {!! Html::script('vendor/toastr/toastr.min.js') !!}
        {!! Html::script('vendor/sparkline/index.js') !!}
        <!-- App scripts -->
        {!! Html::script('js/bootbox.min.js') !!}
        {!! Html::script('scripts/luna.js') !!}
        {!! Html::script('js/cliente.js') !!}

        {!! Html::script('js/jquery.maskedinput.js') !!}
        {!! Html::script('js/onSubmits.js') !!}
        {!! Html::script('js/validator.js') !!}
        {!! Html::script('js/select2.min.js') !!}
        {!! Html::script('js/money.min.js') !!}
        
        @yield('scripts')
    </head>
    <body>
        <!-- Wrapper-->
        <div class="wrapper">
            <!-- Header-->
            <nav class="navbar navbar-default navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <div id="mobile-menu">
                            <div class="left-nav-toggle">
                                <a href="#">
                                    <i class="stroke-hamburgermenu"></i>
                                </a>
                            </div>
                        </div>
                        <a class="navbar-brand" >
                            {{-- Payless
                            <span>Sistema de cotizaciones </span> --}}
                        </a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <div class="left-nav-toggle">
                            <a href="#">
                                <i class="stroke-hamburgermenu"></i>
                            </a>
                        </div>
                        <ul class="nav navbar-nav navbar-right">
                        @if (Auth::check())
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">{{Auth::user()->nombre_u}}
                                <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-user">
                                <li><a href="{!! URL::to('logout') !!}"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                            </ul>
                        </li>
                        @else
                        <li class=" profil-link">
                            <a href="{!! URL::to('login') !!}">
                                <span class="profile-address">Login</span>
                                <img src="images/user.png" class="img-circle" alt="">
                            </a>
                        </li>
                        @endif
                    </ul>
                    </div>
                </div>
            </nav>
            <!-- End header-->
            <!-- Navigation-->

            <aside class="navigation">
                <nav>
                    <ul class="nav luna-nav">
                        <li class="active nav-category">
                            <b>Men&uacute;</b>
                        </li>
                        @if(Auth::user()->type == 'admin')
                        <li class="active">
                            <a href="{!! URL::to('usuarios') !!}"><i class="fa fa-user"></i> Usuarios</a>
                        </li>
                        <li class="active">
                            <a href="{!! URL::to('cuentas-bancarias') !!}"><i class="fa fa-money"></i> Cuentas bancarias</a>
                        </li>
                        <li class="active">
                            <a href="{!! URL::to('proveedores') !!}"><i class="fa fa-truck"></i> Proveedores</a>
                        </li>
                        <li class="active">
                            <a href="{!! URL::to('marcas-modelos') !!}"><i class="fa fa-car"></i> Marcas y modelos</a>
                        </li>
                        <li class="active">
                            <a href="{!! URL::to('repuestos') !!}"><i class="fa fa-dashboard"></i> Repuestos</a>
                        </li>
                        <li class="active">
                            <a href="{!! URL::to('mercaderias') !!}"><i class="fa fa-headphones"></i> Productos varios</a>
                        </li>
                        <li class="active">
                            <a href="{!! URL::to('precios-transporte') !!}"><i class="fa fa-money"></i> Precios Transporte</a>
                        </li>
                        <li class="active">
                            <a href="{!! URL::to('ventas') !!}"><i class="fa fa-check-circle"></i> Ventas</a>
                        </li>
                        @endif
                        <li class="active">
                            <a href="#monitoring" data-toggle="collapse" aria-expanded="false">
                                <i class="fa fa-clipboard"></i> Cotizaciones<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                            </a>
                            <ul id="monitoring" class="nav nav-second collapse">
                                <li class="active"><a href="{{ url('/cotizaciones/listar/1') }}"> Finalizadas</a></li>
                                <li class="active"><a href="{{ url('/cotizaciones/listar/0') }}"> Pendientes</a></li>
                            </ul>
                        </li>
                        <li class="active">
                            <a href="{!! URL::to('clientes') !!}"><i class="fa fa-users"></i> Clientes</a>
                        </li>
                        @if (Auth::check())
                        <li id="loginLi" style="display: none;" >
                             <a href="#monitoring1" data-toggle="collapse" aria-expanded="false">
                                <i class="fa fa-user"></i> {{Auth::user()->nombre_u}}<span class="sub-nav-icon"> <i class="stroke-arrow"></i> </span>
                            </a>
                            <ul id="monitoring1" class="nav nav-second collapse">
                                <li class="active"><a href="{!! URL::to('logout') !!}"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                            </ul>
                        </li>
                        @else
                        <li class="profil-link" id="loginLi" style="display: none;">
                            <a href="{!! URL::to('login') !!}">
                                <span class="profile-address">Login</span>
                                <img src="images/user.png" class="img-circle" alt="">
                            </a>
                        </li>
                        @endif 
                    </ul>
                </nav>
            </aside>
            
            <!-- End navigation-->
            <!-- Main content-->
            <section class="content">
                @yield('content')
            </section>
            <!-- End main content-->
            
        </div>
        <!-- End wrapper-->
        <!-- Vendor scripts -->
        <!-- Vendor scripts -->
<script type="text/javascript">
    $(".js-example-theme-multiple").select2({
        placeholder: "Seleccione uno o varios repuestos.."
    });
    if(navigator.userAgent.match(/iPad/i)){
     //code for iPad here 
     $("#loginLi").show()
    }
    if(navigator.userAgent.match(/iPhone/i)){
     //code for iPhone here 
     $("#loginLi").show()
    }
    if(navigator.userAgent.match(/Android/i)){
     //code for Android here 
     $("#loginLi").show()
    }
    if(navigator.userAgent.match(/BlackBerry/i)){
     //code for BlackBerry here 
     $("#loginLi").show()
    }
    if(navigator.userAgent.match(/webOS/i)){
     //code for webOS here 
     $("#loginLi").show()
    }
</script>