@extends('layouts.principal')
@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-filled">
				<div class="view-header">
					<div class="header-icon">
						<i class="pe page-header-icon pe-7s-cash"></i>
					</div>
					<div class="header-title">
						<h3 class="page-header">Actualizar datos de la cuenta bancaria</h3>
						<small>
						Gesti&oacute;n de cuentas bancarias
						</small>
					</div>
				</div>
				<div class="panel-body">	
				{!!Form::model($cuenta,['route'=>['cuentas-bancarias.update',$cuenta->id],'method'=>'PUT',"onsubmit"=>"return validateForm();",'id'=>'myForm'])!!}
					@include('cuentas-bancarias.form.form')
					@include('alerts.validation')
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" >
			<div class="form-group">
				{!!Form::submit('Actualizar',['class'=>'btn btn-w-md btn-accent'])!!} <a class="btn btn-w-md btn-default" href="{!! URL::to('cuentas-bancarias') !!}">Cancelar</a>
				{!! Form::close() !!}
			</div> 
		</div>
	</div>
				
@endsection